package eu.europa.ec.itb.schematron.service;

public interface FileService {
    void write(String msg, String filename);
}
