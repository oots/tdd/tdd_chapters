package eu.europa.ec.itb.schematron.service;

import java.io.IOException;

public interface SchematronValidationService {
    void validationSchema();

    void validationSchematron() throws IOException;
    void validationSchematronSnippet();
}