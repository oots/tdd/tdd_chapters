package eu.europa.ec.itb.schematron.model;

import java.util.List;

/**
 * Model class SchematronMap.java
 * SchematronMap.java model class consist all relevant data structure which needed
 * during the Schematron validator processing.
 *
 * @author  Stevan Ljiljak
 * @version 1.0
 * @since   23-08-2023
 */
public class SchematronMap {
    private List<String> schematronFileList;

    private List<String> xmlFilesValidList;

    private List<String> xmlFilesInvalidList;

    private List<String> xmlSnippetsValidList;

    private List<String> xmlSnippetsInvalidList;

    private boolean crossValidatorType;

    public SchematronMap() {
    }

    public List<String> getSchematronFileList() {
        return schematronFileList;
    }

    public void setSchematronFileList(List<String> schematronFileList) {
        this.schematronFileList = schematronFileList;
    }

    public List<String> getXmlFilesValidList() {
        return xmlFilesValidList;
    }

    public void setXmlFilesValidList(List<String> xmlFilesValidList) {
        this.xmlFilesValidList = xmlFilesValidList;
    }

    public List<String> getXmlFilesInvalidList() {
        return xmlFilesInvalidList;
    }

    public void setXmlFilesInvalidList(List<String> xmlFilesInvalidList) {
        this.xmlFilesInvalidList = xmlFilesInvalidList;
    }

    public List<String> getXmlSnippetsValidList() {
        return xmlSnippetsValidList;
    }

    public void setXmlSnippetsValidList(List<String> xmlSnippetsValidList) {
        this.xmlSnippetsValidList = xmlSnippetsValidList;
    }

    public List<String> getXmlSnippetsInvalidList() {
        return xmlSnippetsInvalidList;
    }

    public void setXmlSnippetsInvalidList(List<String> xmlSnippetsInvalidList) {
        this.xmlSnippetsInvalidList = xmlSnippetsInvalidList;
    }

    public boolean getCrossValidatorType() {
        return crossValidatorType;
    }

    public void setCrossValidatorType(boolean crossValidatorType) {
        this.crossValidatorType = crossValidatorType;
    }
}