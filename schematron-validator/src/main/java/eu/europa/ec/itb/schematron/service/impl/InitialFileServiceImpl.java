package eu.europa.ec.itb.schematron.service.impl;

import eu.europa.ec.itb.schematron.model.SchematronMap;
import eu.europa.ec.itb.schematron.service.InitialFileService;
import eu.europa.ec.itb.schematron.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service class InitialFileServiceImpl.java
 * InitialFileServiceImpl.java service class consist methods for
 * setup and prepare initial states and statuses for
 * validations by Schematron validator processing.
 *
 * @author  Stevan Ljiljak
 * @version 1.0
 * @since   23-08-2023
 */
@Service
public class InitialFileServiceImpl implements InitialFileService {
    private static final Logger LOG = LoggerFactory.getLogger(InitialFileServiceImpl.class);
    @Value("${validator.type}")
    private String validatorType;
    protected static Map<String, SchematronMap> mapOfFiles = new HashMap<>();

    @Override
    public void init() {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(Constants.PATH_FILE_OF_MAPPING)))) {

            String[] keyValuesPair = validatorType.split(",");

            String line;
            List<String> schematronFileList = new ArrayList<>();
            List<String> xmlFilesValidList = new ArrayList<>();
            List<String> xmlFilesInvalidList = new ArrayList<>();
            List<String> xmlSnippetsValidList = new ArrayList<>();
            List<String> xmlSnippetsInvalidList = new ArrayList<>();
            boolean crossValidatorType = false;

            while ((line = reader.readLine()) != null) {
                for (String keyValue : keyValuesPair) {
                    line = line.trim();
                    if (line.contains(String.format("validator.schematronFile.%s", keyValue)) && schematronFileList.isEmpty()) {
                        String[] keyValuePair = line.split("=", 2);
                        if (keyValuePair.length > 1) {
                            String value = keyValuePair[1];
                            String[] keyValuesComaSplitedPair = value.split(",");
                            for (String keyValuesComaSplited : keyValuesComaSplitedPair) {
                                schematronFileList.add(keyValuesComaSplited.trim());
                            }
                        } else {
                            LOG.info(String.format("No Key:Value found in line, ignoring: %s", line));
                        }
                    }
                    if (line.contains(String.format("validator.xmlFile.valid.%s", keyValue)) && xmlFilesValidList.isEmpty()) {
                        String[] keyValuePair = line.split("=", 2);
                        if (keyValuePair.length > 1) {
                            String value = keyValuePair[1].trim();
                            final File folder = new File(value);
                            if (folder.listFiles() != null && folder.listFiles().length > 0) {
                                for (final File fileEntry : folder.listFiles()) {
                                    xmlFilesValidList.add(fileEntry.getPath());
                                }
                            } else {
                                LOG.info(String.format("Folder %s is empty.", value));
                                xmlFilesValidList.add(Constants.EMPTY_FOLDER);
                            }
                        } else {
                            LOG.info(String.format("No Key:Value found in line, ignoring: %s ", line));
                        }
                    }
                    if (line.contains(String.format("validator.xmlFile.invalid.%s", keyValue)) && xmlFilesInvalidList.isEmpty()) {
                        String[] keyValuePair = line.split("=", 2);
                        if (keyValuePair.length > 1) {
                            String value = keyValuePair[1].trim();
                            final File folder = new File(value);
                            if (folder.listFiles() != null && folder.listFiles().length > 0) {
                                for (final File fileEntry : folder.listFiles()) {
                                    xmlFilesInvalidList.add(fileEntry.getPath());
                                }
                            } else {
                                LOG.info(String.format("Folder %s is empty.", value));
                                xmlFilesInvalidList.add(Constants.EMPTY_FOLDER);
                            }
                        } else {
                            LOG.info(String.format("No Key:Value found in line, ignoring: %s ", line));
                        }
                    }
                    if (line.contains(String.format("validator.xmlSnippet.valid.%s", keyValue)) && xmlSnippetsValidList.isEmpty()) {
                        String[] keyValuePair = line.split("=", 2);
                        if (keyValuePair.length > 1) {
                            String value = keyValuePair[1].trim();
                            final File folder = new File(value);
                            if (folder.listFiles() != null && folder.listFiles().length > 0) {
                                for (final File fileEntry : folder.listFiles()) {
                                    xmlSnippetsValidList.add(fileEntry.getPath());
                                }
                            } else {
                                LOG.info(String.format("Folder %s is empty.", value));
                                xmlSnippetsValidList.add(Constants.EMPTY_FOLDER);
                            }
                        } else {
                            LOG.info(String.format("No Key:Value found in line, ignoring: %s ", line));
                        }
                    }
                    if (line.contains(String.format("validator.xmlSnippet.invalid.%s", keyValue)) && xmlSnippetsInvalidList.isEmpty()) {
                        String[] keyValuePair = line.split("=", 2);
                        if (keyValuePair.length > 1) {
                            String value = keyValuePair[1].trim();
                            final File folder = new File(value);
                            if (folder.listFiles() != null && folder.listFiles().length > 0) {
                                for (final File fileEntry : folder.listFiles()) {
                                    xmlSnippetsInvalidList.add(fileEntry.getPath());
                                }
                            } else {
                                LOG.info(String.format("Folder %s is empty.", value));
                                xmlSnippetsInvalidList.add(Constants.EMPTY_FOLDER);
                            }
                        } else {
                            LOG.info(String.format("No Key:Value found in line, ignoring: %s ", line));
                        }
                    }
                    if (line.contains(String.format("validator.type.%s", keyValue))) {
                        String[] keyValuePair = line.split("=", 2);
                        if (keyValuePair.length > 1) {
                            String value = keyValuePair[1].trim();
                            if (value.equals("crossvalidator")){
                                crossValidatorType = true;
                            }
                        } else {
                            LOG.info(String.format("No Key:Value found in line, ignoring: %s ", line));
                        }
                    }

                    if (!schematronFileList.isEmpty() && !xmlFilesValidList.isEmpty() && !xmlFilesInvalidList.isEmpty() && !xmlSnippetsValidList.isEmpty() && !xmlSnippetsInvalidList.isEmpty()) {
                        SchematronMap schematronMap = new SchematronMap();
                        schematronMap.setSchematronFileList(schematronFileList);
                        schematronMap.setXmlFilesValidList(xmlFilesValidList);
                        schematronMap.setXmlFilesInvalidList(xmlFilesInvalidList);
                        schematronMap.setXmlSnippetsValidList(xmlSnippetsValidList);
                        schematronMap.setXmlSnippetsInvalidList(xmlSnippetsInvalidList);
                        schematronMap.setCrossValidatorType(crossValidatorType);
                        mapOfFiles.put(keyValue, schematronMap);
                        schematronFileList = new ArrayList<>();
                        xmlFilesValidList = new ArrayList<>();
                        xmlFilesInvalidList = new ArrayList<>();
                        xmlSnippetsValidList = new ArrayList<>();
                        xmlSnippetsInvalidList = new ArrayList<>();
                        crossValidatorType = false;
                        break;
                    }
                }
            }

            //print in log loaded schematron map
            for (Map.Entry<String, SchematronMap> entry : mapOfFiles.entrySet()) {
                LOG.info(String.format("Types of validation = %s ...Loading schematron map", entry.getKey()));
                SchematronMap schematronMapOutput = entry.getValue();
                schematronFileList = schematronMapOutput.getSchematronFileList();
                if (!schematronFileList.isEmpty()) {
                    for (String schematronFile : schematronFileList) {
                        LOG.info(String.format("Schematron files are: %s", schematronFile));
                    }
                }
                xmlFilesValidList = schematronMapOutput.getXmlFilesValidList();
                if (!xmlFilesValidList.isEmpty()) {
                    for (String xmlFileValid : xmlFilesValidList) {
                        LOG.info(String.format("Valid XML files are: %s", xmlFileValid));
                    }
                }
                xmlFilesInvalidList = schematronMapOutput.getXmlFilesInvalidList();
                if (!xmlFilesInvalidList.isEmpty()) {
                    for (String xmlFileInvalid : xmlFilesInvalidList) {
                        LOG.info(String.format("Invalid XML files are: %s", xmlFileInvalid));
                    }
                }
                xmlSnippetsValidList = schematronMapOutput.getXmlSnippetsValidList();
                if (!xmlSnippetsValidList.isEmpty()) {
                    for (String xmlSnippetsValid : xmlSnippetsValidList) {
                        LOG.info(String.format("Valid XML snippets are: %s", xmlSnippetsValid));
                    }
                }
                xmlSnippetsInvalidList = schematronMapOutput.getXmlSnippetsInvalidList();
                if (!xmlSnippetsInvalidList.isEmpty()) {
                    for (String xmlSnippetsInvalid : xmlSnippetsInvalidList) {
                        LOG.info(String.format("Invalid XML snippets are: %s", xmlSnippetsInvalid));
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not find config.properties file!");
        }
    }

    public static Map<String, SchematronMap> getMapOfFiles() {
        return mapOfFiles;
    }
}