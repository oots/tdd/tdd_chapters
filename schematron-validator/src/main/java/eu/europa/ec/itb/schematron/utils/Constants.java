package eu.europa.ec.itb.schematron.utils;

/**
 * Utility class Constants.java
 * Constants.java utility class consist all relevant static final constants.
 *
 * @author  Stevan Ljiljak
 * @version 1.0
 * @since   23-08-2023
 */
public class Constants {
    private Constants() {
        throw new IllegalStateException("Utility class");
    }

    public static final String PATH_FILE_OF_MAPPING = "/config.properties";
    public static final String PATH_FILE_FOR_COLLECTING_ERROR_LOGS = "./src/main/resources/logger/error.log";
    public static final String PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_LOGS = "./src/main/resources/logger/failed_assert_valid.log";
    public static final String PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_ERROR_LOGS = "./src/main/resources/logger/failed_assert_errors.log";
    public static final String PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_WARNING_LOGS = "./src/main/resources/logger/failed_assert_warning.log";
    public static final String PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_SNIPPET_LOGS = "./src/main/resources/logger/failed_assert_snippet_valid.log";
    public static final String PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_SNIPPET_ERROR_LOGS = "./src/main/resources/logger/failed_assert_snippet_errors.log";
    public static final String ACCESS_ALL_EXTERNAL_SCHEMA = "all";
    public static final String EMPTY_FOLDER = "empty folder";

}