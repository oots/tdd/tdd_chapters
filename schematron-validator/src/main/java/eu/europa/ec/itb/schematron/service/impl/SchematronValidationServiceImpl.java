package eu.europa.ec.itb.schematron.service.impl;

import com.helger.commons.collection.impl.ICommonsList;
import com.helger.schematron.ISchematronResource;
import com.helger.schematron.pure.SchematronResourcePure;
import com.helger.schematron.sch.SchematronProviderXSLTFromSCH;
import com.helger.schematron.sch.SchematronResourceSCH;
import com.helger.schematron.svrl.SVRLFailedAssert;
import com.helger.schematron.svrl.SVRLHelper;
import com.helger.schematron.svrl.SVRLMarshaller;
import com.helger.schematron.svrl.jaxb.SchematronOutputType;
import com.helger.schematron.xslt.SchematronProviderXSLTPrebuild;
import com.helger.xml.transform.CollectingTransformErrorListener;
import eu.europa.ec.itb.schematron.model.SchematronMap;
import eu.europa.ec.itb.schematron.model.ValidationInfo;
import eu.europa.ec.itb.schematron.service.FileService;
import eu.europa.ec.itb.schematron.service.SchematronValidationService;
import eu.europa.ec.itb.schematron.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Service class SchematronValidationServiceImpl.java
 * SchematronValidationServiceImpl.java service class consist parameters and methods for:
 * 1) validation schema
 * 2) validation schematron xml files
 * 3) validation schematron xml snippets
 * 4) matching name of files with relevant type of business logic
 *
 * @author  Stevan Ljiljak
 * @version 1.0
 * @since   23-08-2023
 */
@Service
public class SchematronValidationServiceImpl implements SchematronValidationService {
    private static final Logger LOG = LoggerFactory.getLogger(SchematronValidationServiceImpl.class);

    @Value("${xsd.sdg.oots.driver}")
    private String XSD_SDG_OOTS_DRIVER;
    private final FileService fileService;

    @Autowired
    public SchematronValidationServiceImpl(FileService fileService) {
        this.fileService = fileService;
    }

    @Override
    public void validationSchema() {
        Map<String, SchematronMap> mapOfFiles = InitialFileServiceImpl.getMapOfFiles();
        List<String> xmlFilesValidList;
        List<String> xmlFilesInvalidList;
        for (Map.Entry<String, SchematronMap> entry : mapOfFiles.entrySet()) {
            LOG.info(String.format("Types of validation = %s >>> Schema validation", entry.getKey()));
            SchematronMap schematronMap = entry.getValue();
            xmlFilesValidList = schematronMap.getXmlFilesValidList();
            xmlFilesInvalidList = schematronMap.getXmlFilesInvalidList();
            LOG.info("Schema file = " + XSD_SDG_OOTS_DRIVER);

            if (schematronMap.getCrossValidatorType() == false) {
                for (String xmlFilesValid : xmlFilesValidList) {
                    try {
                        LOG.info(String.format("XML file valid = %s", xmlFilesValid));
                        if (xmlFilesValid.equals(Constants.EMPTY_FOLDER)) break;

                        InputStream xmlInput = new FileInputStream(xmlFilesValid);
                        //Convert to String as Stream would be closed automatically by validate method
                        //as we need to validate it twice and then also unmarshall it
                        //we read it into String and create on demand Streams
                        String xmlInputString = new String(xmlInput.readAllBytes(), StandardCharsets.UTF_8);

                        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                        //Disable access to external entities (just alllows "file protocol" to allow own xsd files usage)
                        schemaFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
                        schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
                        schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, Constants.ACCESS_ALL_EXTERNAL_SCHEMA);
                        //schemaFile
                        File schemaFileDriverUrl = new File(XSD_SDG_OOTS_DRIVER);

                        if (schemaFileDriverUrl == null) {
                            throw new IllegalStateException("Could not find oots driver xsd driver file!");
                        }

                        //First validates against oots_driver_v1.1.0.xsd
                        Schema ootsDriverSchema = schemaFactory.newSchema(schemaFileDriverUrl);
                        ootsDriverSchema.newValidator().validate(new StreamSource(new StringReader(xmlInputString)));
                    } catch (SAXException | IOException ex) {
                        LOG.error(String.format("The Schema validator system in the specified valid file %s ,find the next error message %s", xmlFilesValid, ex.getMessage()));
                        fileService.write(String.format("The Schema validator system in the specified valid file %s ,find the next error message %s", xmlFilesValid, ex.getMessage()), Constants.PATH_FILE_FOR_COLLECTING_ERROR_LOGS);
                    }
                }

                for (String xmlFilesInvalid : xmlFilesInvalidList) {
                    try {
                        LOG.info(String.format("XML file invalid = %s", xmlFilesInvalid));
                        if (xmlFilesInvalid.equals(Constants.EMPTY_FOLDER)) break;

                        InputStream xmlInput = new FileInputStream(xmlFilesInvalid);
                        //Convert String as Stream would be closed automatically by validate method
                        //as we need to validate it twice and then also unmarshall it
                        //we read it into String and create on demand Streams
                        String xmlInputString = new String(xmlInput.readAllBytes(), StandardCharsets.UTF_8);

                        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
                        //Disable access to external entities (just alllows "file protocol" to allow own xsd files usage)
                        schemaFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
                        schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
                        schemaFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, Constants.ACCESS_ALL_EXTERNAL_SCHEMA);
                        //schemaFile
                        File schemaFileDriverUrl = new File(XSD_SDG_OOTS_DRIVER);

                        if (schemaFileDriverUrl == null) {
                            throw new IllegalStateException("Could not find oots driver xsd driver file!");
                        }

                        //First validates against oots_driver_v1.1.0.xsd
                        Schema ootsDriverSchema = schemaFactory.newSchema(schemaFileDriverUrl);
                        ootsDriverSchema.newValidator().validate(new StreamSource(new StringReader(xmlInputString)));
                    } catch (SAXException | IOException ex) {
                        LOG.error(String.format("The Schema validator system in the specified invalid file %s ,find the next error message %s", xmlFilesInvalid, ex.getMessage()));
                        fileService.write(String.format("The Schema validator system in the specified invalid file %s ,find the next error message %s", xmlFilesInvalid, ex.getMessage()), Constants.PATH_FILE_FOR_COLLECTING_ERROR_LOGS);
                    }
                }
            }
        }
    }

    @Override
    public void validationSchematron() {
        Map<String, SchematronMap> mapOfFiles = InitialFileServiceImpl.getMapOfFiles();
        List<String> schematronFileList;
        List<String> xmlFilesValidList;
        List<String> xmlFilesInvalidList;

        for (Map.Entry<String, SchematronMap> entry : mapOfFiles.entrySet()) {
            LOG.info(String.format("Types of validation = %s >>> Schematron validation", entry.getKey()));
            SchematronMap schematronMap = entry.getValue();
            schematronFileList = schematronMap.getSchematronFileList();
            xmlFilesValidList = schematronMap.getXmlFilesValidList();
            xmlFilesInvalidList = schematronMap.getXmlFilesInvalidList();

            for (String schematronFile : schematronFileList) {
                LOG.info(String.format("Schematron file = %s", schematronFile));
                for (String xmlFilesValid : xmlFilesValidList) {
                    LOG.info(String.format("Valid XML file = %s", xmlFilesValid));
                    if (xmlFilesValid.equals(Constants.EMPTY_FOLDER)) break;
                    List<ValidationInfo> validationInfoList = new ArrayList<>();
                    List<ValidationInfo> validationWarningInfoList = new ArrayList<>();
                    try {
                        // Execution of the Schematron validation in the XML file.
                        ISchematronResource aResSCH = SchematronResourceSCH.fromFile(schematronFile);
                        if (!aResSCH.isValidSchematron ())
                            throw new IllegalArgumentException ("Invalid Schematron!");
                        Document svrlDocument = aResSCH.applySchematronValidation(new StreamSource (new File(xmlFilesValid)));
                        SVRLMarshaller marshaller = new SVRLMarshaller(false);
                        SchematronOutputType svrlOutput = marshaller.read(svrlDocument);
                        ICommonsList<SVRLFailedAssert> failedAssertions = SVRLHelper.getAllFailedAssertions(svrlOutput);

                        if (failedAssertions.isNotEmpty()){
                            String message = String.format("The valid xml file %s fails. The file should not fail on the indicated rule(s): \n", xmlFilesValid);
                            String messageWarning = String.format("The valid xml file %s has warnings. List of warning rule(s): \n", xmlFilesValid);
                            int errors = 0;
                            int warnings = 0;
                            for (SVRLFailedAssert failedAssertion : failedAssertions) {
                                if (failedAssertion.getRole().equals("ERROR") || failedAssertion.getRole().equals("FATAL")) {
                                    errors += 1;
                                    message += String.format(" Rule identifier %s, and rule message '%s'. \n", failedAssertion.getID(), failedAssertion.getText());
                                }else{
                                    warnings += 1;
                                    messageWarning += String.format(" Rule identifier %s, and rule message '%s'. \n", failedAssertion.getID(), failedAssertion.getText());
                                }
                            }

                            if(errors > 0){
                                validationInfoList.add(new ValidationInfo(message));
                            }
                            if(warnings > 0){
                                validationWarningInfoList.add(new ValidationInfo(messageWarning));
                            }
                        }

                        if (failedAssertions.isEmpty() || validationInfoList.isEmpty()) {
                            String message = String.format("The valid xml file %s has no failed assertion or the failed assertions(s) is not the expected one, the test is successful.", xmlFilesValid);
                            LOG.info(String.format("failed-assert-valid: %s", message));
                            fileService.write(message, Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_LOGS);
                        }

                        for (ValidationInfo validationInfo : validationWarningInfoList) {
                            LOG.info(String.format("failed-assert-warning: %s", validationInfo.getValidationMessage()));
                            fileService.write(validationInfo.getValidationMessage(), Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_WARNING_LOGS);
                        }

                        for (ValidationInfo validationInfo : validationInfoList) {
                            LOG.info(String.format("failed-assert-error: %s", validationInfo.getValidationMessage()));
                            fileService.write(validationInfo.getValidationMessage(), Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_ERROR_LOGS);
                        }
                    } catch (Exception ex) {
                        LOG.error(String.format("The Schematron validator system in the valid file specified %s ,find the error message %s", xmlFilesValid, ex));
                        fileService.write(String.format("The Schematron validator system in the valid file specified %s ,find the error message %s", xmlFilesValid, ex), Constants.PATH_FILE_FOR_COLLECTING_ERROR_LOGS);
                        fileService.write(String.format("The valid xml file %s has thrown the error %s", xmlFilesValid, ex), Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_ERROR_LOGS);
                    }
                }

                for (String xmlFilesInvalid : xmlFilesInvalidList) {
                    LOG.info(String.format("Invalid XML file = %s", xmlFilesInvalid));
                    if (xmlFilesInvalid.equals(Constants.EMPTY_FOLDER)) break;
                    List<ValidationInfo> invalidationInfoList = new ArrayList<>();
                    try {
                        // Execution of the Schematron validation in the XML file.
                        ISchematronResource schematron = SchematronResourcePure.fromFile(schematronFile);
                        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder builder = factory.newDocumentBuilder();
                        Document document = builder.parse(new File(xmlFilesInvalid));
                        Document svrlDocument = schematron.applySchematronValidation(new DOMSource(document));

                        // Process of the output
                        SVRLMarshaller marshaller = new SVRLMarshaller(false);
                        SchematronOutputType svrlOutput = marshaller.read(svrlDocument);
                        ICommonsList<SVRLFailedAssert> failedAssertions = SVRLHelper.getAllFailedAssertions(svrlOutput);

                        String fileName = new File(xmlFilesInvalid).getName();
                        for (SVRLFailedAssert failedAssertion : failedAssertions) {
                            if (fileName.contains(failedAssertion.getID())) {
                                invalidationInfoList.add(new ValidationInfo(String.format("The invalid xml file %s is with the expected identifier %s, the negative test passed: the test is negative with the expected result and validation message is %s .", xmlFilesInvalid, failedAssertion.getID(), failedAssertion.getText())));
                            }
                        }

                        if (failedAssertions.isEmpty() || invalidationInfoList.isEmpty()) {
                            String message = String.format("The invalid xml file %s has no failed assertion or the failed assertion is not the expected one, the test has failed because system did not find the expected failed-assert.", xmlFilesInvalid);
                            LOG.info(String.format("failed-assert-error: %s", message));
                            fileService.write(message, Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_ERROR_LOGS);
                            continue;
                        }
                        for (ValidationInfo invalidationInfo : invalidationInfoList) {
                            LOG.info(String.format("failed-asser-valid: %s", invalidationInfo.getValidationMessage()));
                            fileService.write(invalidationInfo.getValidationMessage(), Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_LOGS);
                        }
                    } catch (Exception ex) {
                        LOG.error(String.format("The Schematron validator system in the invalid file specified %s ,find the error message %s ", xmlFilesInvalid, ex));
                        fileService.write(String.format("The Schematron validator system in the invalid file specified %s ,find the error message %s ", xmlFilesInvalid, ex), Constants.PATH_FILE_FOR_COLLECTING_ERROR_LOGS);
                        fileService.write(String.format("The invalid xml file %s has thrown the error %s", xmlFilesInvalid, ex), Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_ERROR_LOGS);
                    }
                }
            }
        }
    }

    public void validationSchematronSnippet() {
        Map<String, SchematronMap> mapOfFiles = InitialFileServiceImpl.getMapOfFiles();
        List<String> schematronFileList;
        List<String> xmlSnippetsValidList;
        List<String> xmlSnippetsInvalidList;

        for (Map.Entry<String, SchematronMap> entry : mapOfFiles.entrySet()) {
            LOG.info(String.format("Types of validation = %s >>> Schematron validation", entry.getKey()));
            SchematronMap schematronMap = entry.getValue();
            schematronFileList = schematronMap.getSchematronFileList();
            xmlSnippetsValidList = schematronMap.getXmlSnippetsValidList();
            xmlSnippetsInvalidList = schematronMap.getXmlSnippetsInvalidList();

            for (String schematronFile : schematronFileList) {
                LOG.info(String.format("Schematron file = %s", schematronFile));
                for (String xmlSnippetsValid : xmlSnippetsValidList) {
                    LOG.info(String.format("Valid XML snippet = %s", xmlSnippetsValid));
                    if (xmlSnippetsValid.equals(Constants.EMPTY_FOLDER)) break;

                    List<ValidationInfo> snippetValidationInfoList = new ArrayList<>();
                    //If files schematron and valid xml snippet do not match continue with next comparison
                    if (!matchNameOfFiles(schematronFile, xmlSnippetsValid)) continue;
                    try {
                        // Execution of the Schematron validation in the XML file.
                        ISchematronResource aResSCH = SchematronResourceSCH.fromFile(schematronFile);
                        Document svrlDocument = aResSCH.applySchematronValidation(new StreamSource (new File(xmlSnippetsValid)));
                        SVRLMarshaller marshaller = new SVRLMarshaller(false);
                        SchematronOutputType svrlOutput = marshaller.read(svrlDocument);
                        ICommonsList<SVRLFailedAssert> failedAssertions = SVRLHelper.getAllFailedAssertions(svrlOutput);

                        String fileName = new File(xmlSnippetsValid).getName();
                        for (SVRLFailedAssert failedAssertion : failedAssertions) {
                            if (fileName.contains(failedAssertion.getID())) {
                                snippetValidationInfoList.add(new ValidationInfo(String.format("The valid xml snipped %s, and there was one with the expected identifier %s, the positive test fails: the file should not fail on the indicated rule but it did, the validation message is: %s", xmlSnippetsValid, failedAssertion.getID(), failedAssertion.getText())));
                            }
                        }

                        if (failedAssertions.isEmpty() || snippetValidationInfoList.isEmpty()) {
                            String message = String.format("The valid xml snippet %s has no failed assertion or the failed assertions(s) is not the expected one, the test is successful.", xmlSnippetsValid);
                            LOG.info(String.format("failed-assert-valid: %s", message));
                            fileService.write(message, Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_SNIPPET_LOGS);
                            continue;
                        }

                        for (ValidationInfo snippetValidationInfo : snippetValidationInfoList) {
                            LOG.info(String.format("failed-assert-error: %s", snippetValidationInfo.getValidationMessage()));
                            fileService.write(snippetValidationInfo.getValidationMessage(), Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_SNIPPET_ERROR_LOGS);
                        }
                    } catch (Exception ex) {
                        LOG.error(String.format("The Schematron validator system in the invalid file specified %s ,find the error message %s ", xmlSnippetsValid, ex));
                        fileService.write(String.format("The Schematron validator system in the invalid file specified %s ,find the error message %s ", xmlSnippetsValid, ex), Constants.PATH_FILE_FOR_COLLECTING_ERROR_LOGS);
                        fileService.write(String.format("The valid xml snippet %s has thrown the error %s", xmlSnippetsValid, ex), Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_SNIPPET_ERROR_LOGS);
                    }
                }

                for (String xmlSnippetsInvalid : xmlSnippetsInvalidList) {
                    LOG.info(String.format("Valid XML snippet = %s", xmlSnippetsInvalid));
                    if (xmlSnippetsInvalid.equals(Constants.EMPTY_FOLDER)) break;
                    List<ValidationInfo> snippetValidationInfoList = new ArrayList<>();
                    //If files schematron and invalid xml snippet do not match continue with next comparison
                    if (!matchNameOfFiles(schematronFile, xmlSnippetsInvalid)) continue;
                    try {
                        // Execution of the Schematron validation in the XML file.
                        ISchematronResource aResSCH = SchematronResourceSCH.fromFile(schematronFile);
                        Document svrlDocument = aResSCH.applySchematronValidation(new StreamSource (new File(xmlSnippetsInvalid)));
                        SVRLMarshaller marshaller = new SVRLMarshaller(false);
                        SchematronOutputType svrlOutput = marshaller.read(svrlDocument);
                        ICommonsList<SVRLFailedAssert> failedAssertions = SVRLHelper.getAllFailedAssertions(svrlOutput);

                        String fileName = new File(xmlSnippetsInvalid).getName();
                        for (SVRLFailedAssert failedAssertion : failedAssertions) {
                            if (fileName.contains(failedAssertion.getID())) {
                                snippetValidationInfoList.add(new ValidationInfo(String.format("The invalid xml snippet %s is with the expected identifier %s, the negative test passed: the test is negative with the expected result and validation message is: %s", xmlSnippetsInvalid, failedAssertion.getID(), failedAssertion.getText())));
                            }
                        }

                        if (failedAssertions.isEmpty() || snippetValidationInfoList.isEmpty()) {
                            String message = String.format("The invalid xml snippet %s has no failed assertion or the failed assertion is not the expected one, the test has failed because system did not find the expected failed-assert.", xmlSnippetsInvalid);
                            LOG.info(String.format("failed-assert-error: %s", message));
                            fileService.write(message, Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_SNIPPET_ERROR_LOGS);
                            continue;
                        }
                        for (ValidationInfo snippetValidationInfo : snippetValidationInfoList) {
                            LOG.info(String.format("failed-assert-valid: %s", snippetValidationInfo.getValidationMessage()));
                            fileService.write(snippetValidationInfo.getValidationMessage(), Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_SNIPPET_LOGS);
                        }
                    } catch (Exception ex) {
                        LOG.error(String.format("The Schematron validator system in the invalid file specified %s ,find the error message %s ", xmlSnippetsInvalid, ex));
                        fileService.write(String.format("The Schematron validator system in the invalid file specified %s ,find the error message %s ", xmlSnippetsInvalid, ex), Constants.PATH_FILE_FOR_COLLECTING_ERROR_LOGS);
                        fileService.write(String.format("The invalid xml snippet %s has thrown the error %s", xmlSnippetsInvalid, ex), Constants.PATH_FILE_FOR_COLLECTING_FAILED_ASSERT_SNIPPET_ERROR_LOGS);
                    }
                }
            }
        }
    }

    private static boolean matchNameOfFiles(String schematronFile, String xmlSnippetsValid) {
        String schematronFileName = new File(schematronFile).getName();
        String xmlSnippetsValidName = new File(xmlSnippetsValid).getName();
        int lastDotIndexOfSchematronFileName = schematronFileName.lastIndexOf(".");
        int lastDotIndexOfXmlSnippetsValidName = xmlSnippetsValidName.lastIndexOf(".");
        if (lastDotIndexOfSchematronFileName != -1 && lastDotIndexOfXmlSnippetsValidName != -1) {
            String truncatedNameOfSchematronFileName = schematronFileName.substring(0, lastDotIndexOfSchematronFileName); // Truncate the file name
            String truncatedNameOfXmlSnippetsValidName = xmlSnippetsValidName.substring(0, lastDotIndexOfXmlSnippetsValidName); // Truncate the file name
            if (truncatedNameOfXmlSnippetsValidName.contains(truncatedNameOfSchematronFileName)) return true;
        } else {
            System.out.println("File name has no extension: " + schematronFileName + "," + xmlSnippetsValidName);
        }
        return false;
    }
}