package eu.europa.ec.itb.schematron;

import eu.europa.ec.itb.schematron.service.InitialFileService;
import eu.europa.ec.itb.schematron.service.SchematronValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;
import java.io.File;

/**
 * Main class SchematronApplication.java
 * SchematronApplication.java main class call over thread CommandLineRunner
 * all methods for processing xml (full examples and snippets) data.
 *
 * @author Stevan Ljiljak
 * @version 1.0
 * @since 23-08-2023
 */
@SpringBootApplication
public class SchematronApplication implements CommandLineRunner {
    private static final Logger LOG = LoggerFactory.getLogger(SchematronApplication.class);
    @Resource
    InitialFileService filesStorageService;
    @Resource
    SchematronValidationService schematronValidationService;
    @Value("${logger.path}")
    private String LOGGER_PATH;

    public static void main(String[] args) {
        SpringApplication.run(SchematronApplication.class, args).close();
        LOG.info("Done");
    }
    @Override
    public void run(String... arg) throws Exception {
        /* loading configuration files and initialization data structure */
        filesStorageService.init();
        /* schema validations */
        schematronValidationService.validationSchema();
        /* schematron validations xml files */
        schematronValidationService.validationSchematron();
        /* schematron validations xml snippets */
        schematronValidationService.validationSchematronSnippet();
        /* Trigger for sending email over the Gitlab platform, if Schematron validator generate one or five log files. */
        File folder = new File(LOGGER_PATH);
        File[] files = folder.listFiles((d, name) -> name.contains("failed_assert_errors.log") || name.contains("failed_assert_snippet_errors.log"));
        if (folder.listFiles() == null || (files.length > 0)) {
            System.out.println("Logger folder contain range of error files. Exiting...");
            System.exit(1);
        }
        System.out.println("Logger folder contains one app.log file. Proceeding is done!");
    }
}