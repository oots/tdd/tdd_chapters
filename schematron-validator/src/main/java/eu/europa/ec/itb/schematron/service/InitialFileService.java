package eu.europa.ec.itb.schematron.service;

public interface InitialFileService {
    void init();
}