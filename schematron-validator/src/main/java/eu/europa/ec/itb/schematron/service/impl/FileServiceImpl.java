package eu.europa.ec.itb.schematron.service.impl;

import eu.europa.ec.itb.schematron.service.FileService;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Service class FileServiceImpl.java
 * FileServiceImpl.java service class consist method for writing in file.
 *
 * @author  Stevan Ljiljak
 * @version 1.0
 * @since   23-08-2023
 */
@Service
public class FileServiceImpl implements FileService {
    @Override
    public void write(String msg, String filename) {
        try (FileWriter fileWriter = new FileWriter(filename, true)) {
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.println(msg);
            printWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
