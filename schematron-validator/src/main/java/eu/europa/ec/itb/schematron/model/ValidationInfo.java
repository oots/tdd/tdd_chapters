package eu.europa.ec.itb.schematron.model;

/**
 * Model class ValidationInfo.java
 * ValidationInfo.java model class for collecting validation into.
 *
 * @author  Stevan Ljiljak
 * @version 1.0
 * @since   23-08-2023
 */
public class ValidationInfo {
    private String validationMessage;

    public ValidationInfo(String validationMessage) {
        this.validationMessage = validationMessage;
    }

    public String getValidationMessage() {
        return validationMessage;
    }
}