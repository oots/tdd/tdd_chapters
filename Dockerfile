# Dockerfile used for CI workflow in Gitlab
FROM maven:3.9.3-amazoncorretto-17

# Install Required Packages
RUN yum install -y openssl-devel bzip2-devel libffi-devel wget
RUN yum groupinstall -y "Development Tools"

# Get Python 3.11.4
RUN wget https://www.python.org/ftp/python/3.11.4/Python-3.11.4.tgz

# Extract
RUN tar -xzf Python-3.11.4.tgz

# Enter directory
RUN cd Python-3.11.4 && \
    ./configure --enable-optimizations && \
    make altinstall

# Verify
RUN python3.11 --version