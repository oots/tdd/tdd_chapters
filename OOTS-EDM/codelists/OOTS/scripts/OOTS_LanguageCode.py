from inspect import getsourcefile
import glob, re

#from  saxonpy  import PySaxonProcessor
from  saxonche  import PySaxonProcessor, PyXslt30Processor

with PySaxonProcessor(license=False) as proc:

    processor = PySaxonProcessor(license=False)

    xslt_processor = processor.new_xslt30_processor()

    for source in glob.glob('../External/LanguageCode.gc'):
        output = "OOTS_LanguageCode.gc"

        xslt_processor.transform_to_file(
            source_file=source, stylesheet_file='OOTS_LanguageCode.xsl', output_file=output
        )


