<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
    xmlns:sch="http://purl.oclc.org/dsdl/schematron"
    xmlns:error="https://doi.org/10.5281/zenodo.1495494#error"
    xmlns:gc="http://docs.oasis-open.org/codelist/ns/genericode/1.0/"
    exclude-result-prefixes=""
    version="2.0">
    
    <xsl:output indent="yes"  method="xml"/>


    <xsl:template match="//Row[Value[@ColumnRef='code' and string-length(SimpleValue/text()) !=2]]">
        <xsl:comment>Skipping <xsl:value-of 
            select="Value[@ColumnRef='code']/SimpleValue"/>            
        </xsl:comment> 
    </xsl:template>

    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>