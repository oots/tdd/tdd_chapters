# OOTS Cross-Message Validation Framework 
 
## 1. Context and Scope 

The cross-message validation framework is developed for ensuring the consistency between interrelated XML examples. In the OOTS system, messages are based on each other. For example, an Evidence Request may contain elements that were previously queried in the DSD/EB. An ebMS header must correctly reference the evidence request, etc. Thus, a message flow within the OOTS must share similar data. And it is important that MS that want to connect to the OOTS can check whether the dependencies in the information flow have been implemented correctly.  
 
The cross-message validation framework utilizes the Schematron capabilities for expressing business rules. Typically, two elements of different messages are linked to each other and it is checked whether the value in one message corresponds to the value of another message. 
Cross-Message Validation Framework was implemented for both TDD Team and Member States and has the following features:
* Pair-wise Comparison - the framework/tool compares pairs of XML examples, typically representing related artifacts such as request and response, to identify any inconsistencies. 
* Schematron Validation - the framework/tool employs Schematron to define rules and constraints for XML validation.  
* Customizable Rules - enables users are be able to define custom Schematron rules tailored to their specific requirements. This includes rules for checking element values, attributes, relationships, etc. 
* Flexible Configuration - the framework/tool allows users to configure validation parameters, including the Schematron schema to be used, XPath expressions for selecting elements to validate, and actions to take upon validation failure. 
 
 
## 2. Cross Message Validation Artifacts 

The cross-message validation is realized through a set of artifacts. Business rules thereby define the desired behaviour, whereas Schematron is used for implementing the rules in executable format. Positive and negative XML samples are defined to test if the Schematron rules meets the desired behaviour (regression testing). 

The full set of cross validation artifacts are available here: [Cross-Message Validation](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/crossvalidation?ref_type=heads)                                       
 
### 2.1. Business Rules 
Business rules articulate the specific criteria for ensuring consistency between interrelated pairs of XML examples. These rules could specify requirements such as matching values or IDs, complete incorporation of the data set, valid combination of messages, s of elements or attributes, permissible values, and more. For example, a business rule might state that the DateofBirth in a request XML must correspond to the DateofBirth in the corresponding response XML. Or an ebMS header must declare correctly the action (e.g. ExecuteQueryRequest) according to its associated payload (e.g. Evidence Request).  
Business rules contribute to maintaining the integrity and reliability of XML samples by enforcing consistency constraints. By validating two interrelated XML examples against these rules, the tool that implements the business rules helps to detect and prevent inconsistencies that could compromise data integrity or lead to erroneous processing. 
Business rules are adaptable to evolving business requirements and regulatory constraints.  
 
### 2.2 Schematron Rules  
Business rules drive the definition of custom validation logic within the Schematron rules executed within the tool. By translating business rules into Schematron assertions and patterns, the tool can accurately enforce the desired validation criteria. As business needs change, the tool can be updated to reflect new or modified rules, ensuring continued compliance and alignment with business rules. 
When inconsistencies are detected, the tool can reference the relevant business rules, spot the relevant XML information entity and let the Schematron rules determine the appropriate course of action, such as generating errors or warnings. 
 
### 2.3 Examples for cross-message validation 
The following two examples illustrate XML snippets of different messages that must correspond to each other. The XML snippets are used as positive examples of the regression testing tool.  
 
#### Example 1  
Business rule example: "If present, the value of 'query:QueryRequest/@id' in the Evidence Request MUST be equal to the value of 'query:QueryResponse/@requestId' in the Evidence Response." 
```
<?xml version="1.0" encoding="UTF-8"?>
<Documents> 
    <Document name="edm-req">  	
    … 
        id="urn:uuid:4ffb5281-179d-4578-adf2-39fd13ccc797">  	
    … 
    </Document> 
    <Document name="edm-resp">
    … 
        requestId="urn:uuid:4ffb5281-179d-4578-adf2-39fd13ccc797"> 
    … 
    </Document> 
</Documents> 
```
#### Example  2  
Business rule example: The value of the 'sdg:Publisher/sdg:Identifier/@SchemeID' of the DSD Response MUST be equal to the value of the 'rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID' in the Evidence Request 
``` 
<?xml version="1.0" encoding="UTF-8"?>
<Documents> 
    <Document name="dsd-resp"> 
        <query:QueryResponse> 
        … 
            <sdg:Publisher> 
                <sdg:Identifier schemeID="urn:cef.eu:names:identifier:EAS:9930">DE73524311</sdg:Identifier> 			
                <sdg:Name lang="EN">Civil Registration Office Berlin I</sdg:Name>
                … 
            </sdg:Publisher> 
        …             
        </query:QueryResponse> 
    </Document> 
    <Document name="edm-req"> 
        <query:QueryRequest>     
            <rim:Slot name="EvidenceProvider"> 
                <rim:SlotValue xsi:type="rim:AnyValueType"> 
        …
            <sdg:Agent> 
                <sdg:Identifier schemeID="urn:cef.eu:names:identifier:EAS:9930">DE73524311</sdg:Identifier> 
                <sdg:Name lang="EN">Civil Registration Office Berlin I</sdg:Name>
                …                 
            <sdg:Agent>                
        … 
        <query:QueryRequest>        
    </Document> 
</Documents> 
```

## 3. Integration with the Regression Testing Tool 
The the cross-validation Schematron rules and XML examples were integrated in the [Regression Test Suite](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/schematron-validator?ref_type=heads) following the same pattern used for the other sets of rules and XML samples of the TDDs. 
 
OOTS Specifications outcome includes multiple assets Business Rules, Schematron Rules, XSDs, XML full samples and XML snippets. Considering that frequent updates are made to the assets, for quality assurance and consistency, a Regression Testing Tool was developed.  The primary goal is to ensure efficient change management inside the system and ease the change implementation process for the TDD team. Continuous testing ensures that a change does not break some functionality. The tooling includes a Continuous Integration component that is triggered every time a change is performed on any of the assets available on Git. 
 
The Regression Testing Suite checks that XML samples and snippets (XML minimal examples) are valid when tested against Schematron rules and XSDs and that the rules are working correctly. Also, invalid XML snippets were defined with particular errors expected to be triggered. The OOTS Regression Testing Tool is available on TDD Git Repository: 
https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/schematron-validator 

In the application.properties file was defined a new validation type oots_edm_req_err: 

The different types of validation to support. These values are reflected in other properties.
validator.type=oots_erq,oots_ers,oots_ee,oots_eb_er,oots_dsd_ds_sr,oots_eb_lr,oots_dsd_ds_er,oots_eb_so,oots_dsd_so,oots_lcm_sr,oots_eb_et,oots_lcm_er,oots_eb,oots_dsd_rf,oots_edm_req_resp,oots_edm_req_err,oots_edm_err_req2,oots_edm_req1_req2,oots_eb_req_evi,oots_eb_evi_dsd_resp,oots_eb_evi_dsd_err005,oots_eb_evi_edm_req,oots_dsd_err_resp,oots_dsd_resp_ebms_req,oots_dsd_resp_edm_req,oots_ebms_edm_req,oots_ebms_edm_resp,oots_ebms_edm_err,oots_ebms_req_resp,oots_ebms_err_req2  

In the config.properties file the properties of the validation type oots_edm_req_err are defined:

validator.type.oots_edm_req_err = crossvalidator
validator.schematronFile.oots_edm_req_err = ../OOTS-EDM/crossvalidation/sch/EDM-REQ_EDM-ERR.sch
validator.xmlFile.valid.oots_edm_req_err = ./src/main/resources/validator/xml/EDM-REQ_EDM-ERR/valid
validator.xmlFile.invalid.oots_edm_req_err = ./src/main/resources/validator/xml/EDM-REQ_EDM-ERR/invalid
validator.xmlSnippet.valid.oots_edm_req_err = ./src/main/resources/validator/snippet/EDM-REQ_EDM-ERR/valid
validator.xmlSnippet.invalid.oots_edm_req_err = ./src/main/resources/validator/snippet/EDM-REQ_EDM-ERR/invalid

During execution process the tool generates the following log files: 
* app.log collects all log output 
* error.log collects all error logs 
* failed_assert_errors.log collects all failed assert error for XML files 
* failed_assert_valid.log collects all failed assert error for valid XML files. 
* failed_assert_snippet_errors.log collects all failed assert error for XML snippet. 
* failed_assert_snippet_valid.log collects all failed assert error for valid XML snippet.   
* failed_assert_warning.log collects all failed assert warnings for valid and invalid XML snippet. 

## 5. List of cross-message validation tests and corresponding business rules and Schematrons rules
The detailed list of [Business Rules and Schematron Rules](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/crossvalidation/xsls) is available on GitLab.

|ID                    |Name                                                                                                                                                                                                                                                                                                                                                                                                                          |Description                                                                                                                                                                                                                                                                                                                               |Component|
|----------------------|----------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------|
|EB-REQ_EB-EVI	     |EB get Requirements Query Response and EB get Evidence Types Query Response       |Cross-message validation of the EB get Requirements Query Response and EB get Evidence Types Query Response to prove correspondance of Requirement and Reference Framework                                                                                                                                                                 |EB
|EB-EVI_DSD-RESP	   |EB get Evidence Types Query Response and DSD Response or DSD Error Response       |Cross-message validation of the EB get Evidence Types Query Response and DSD Response or DSD Error to prove correspondance of Evidence Type	                                                                                                                                                                                            |EB/DSD
|EB-EVI_DSD-ERR005	 |EB get Evidence Types Query Response and DSD Response or DSD Error Response       |Cross-message validation of the EB get Evidence Types Query Response and DSD Error Response with error code DSD:ERR:005 to prove correspondance of Evidence Type	                                                                                                                                                                       |EB/DSD
|EB-EVI_EDM-REQ	    |EB get Evidence Types Query Response and Evidence Request                         |Cross-message validation EB get Evidence Types Query Response and Evidence Request to prove correspondance of Requirement and Procedure	                                                                                                                                                                                                |EB/EDM
|DSD-ERR_DSD-RESP	  |DSD Error Response and new DSD Response                                           |Cross-message validation of the DSD Response that was created after an DSD Error Response with code DSD-ERR-0005 to prove corresponance of Data Service Evidence Type, Evidence Provider Classification and Jurisdiction Determination	                                                                                                 |DSD
|DSD-RESP_ebMS_REQ	 |DSD Response and ebMS Request header                                              |Cross-message validation of the DSD Response and ebMS Request header to prove correspondance of the Publisher and Access Service	                                                                                                                                                                                                       |DSD/ebMS
|DSD-RESP_EDM-REQ	  |DSD Response and EDM Request                                                      |Cross-message validation of the DSD Response and EDM Request to prove corresponance of the Data Service Evidence Type, Distribution, the Publisher, Access Service and, if present, the Evidence Provider Classification	                                                                                                               |DSD/EDM
EDM-REQ_EDM-RESP	   |EDM Request and EDM Response                                                      |Cross-message validation of the EDM Request and EDM Response to prove the correspondance of the RequestID, Evidence Requester, Evidence Provider, Evidence Type, Distribution and Evidence Subject. The same cross-validation schematron can be used for the following request-response pairs:EDM-REQ1+EDM-RESP1 and EDM-REQ2+EDM-RESP2	|EDM
EDM-REQ_EDM-ERR	    |EDM Request and EDM Error Response                                                |Cross-message validation of the EDM Request and EDM Error Response to prove the correspondance of the RequestID and  Evidence Requester. The same cross-validation schematron can be used for the following request-response pairs: EDM-REQ1+EDM-ERR1 and EDM-REQ2+EDM-ERR2	                                                            |EDM
EDM-ERR_EDM-REQ2	   |EDM Error Response and 2nd EDM Request                                            |Cross-message validation of the EDM Error Response and second EDM Request to prove correspondance of the Preview Location and the Evidence Requester	                                                                                                                                                                                   |EDM
EDM-REQ1_EDM-REQ2	  |1st Evidence Request and 2nd Evidence Request                                     |Cross-message validation of the 1st Evidence Request and the 2nd evidence Request header to prove equivalence of all elements except the id and the preview location of the Evidence Request	                                                                                                                                           |EDM
ebMS_EDM-REQ	       |ebMS Request header and Evidence Request                                          |Cross-message validation of the ebMS Request header and Evidence Request to prove correspondance of the Evidence Provider and Evidence Requester	                                                                                                                                                                                       |ebMS/EDM
ebMS_EDM-RESP	      |ebMS Response header and Evidence Response                                        |Cross-message validation of the ebMS Response header and Evidence Response to prove correspondance of Evidence Provider, Evidence Requester and Respository Item Ref	                                                                                                                                                                   |ebMS/EDM
ebMS_EDM-ERR	       |ebMS Error Response header and EDM Error Response                                 |Cross-message validation of the ebMS Error Response header and Evidence Error Response to prove correspondance of Evidence Provider and Evidence Requester	                                                                                                                                                                             |ebMS/EDM
ebMS-REQ_ebMS-RESP	 |ebMS Request header and ebMS Response header                                      |Cross-message validation of the ebMS Request header and ebMS Response header to prove corresponance of the From, To, Original Sender, Final Recipient and ConversionID                                                                                                                                                                     |ebMS
ebMS-ERR_ebMS-REQ2	 |ebMS Error Response header and 2nd ebMS Request header                            |Cross-message validation of the ebMS Response header and the 2nd ebMS Request header to prove corresponance of the From, To, Original Sender, Final Recipient and ConversionID in case of re-authentication and preview                                                                                                                    |ebMS
ebMS-REQ1_ebMS-REQ2	|1st ebMS Request header and 2nd ebMS Request header                               |Cross-message validation of the 1st ebMS Request Header and the 2nd ebMS Request header to prove equivalence of eb:PartyInfo, eb:CollaborationInfo and eb:MessageProperties in case of re-authentication and preview                                                                                                                       |ebMS
  
## 6. TDD Assets Continuous Integration Framework 
 
The role of the Continuous Integration framework is to automate and streamline the process of integrating code changes into a shared repository. CI encourages frequent integration of code changes by automatically detecting and addressing integration issues early in the development process.
Upon completion of the regression testing, a notification module is activated. This module sends notifications via email to the Specifications Team to inform them about the status of the latest commit and the results of the regression testing. 
The specs team uses the error log to bugfix the assets, the updates are pushed to Git and the runners trigger again the OOTS Validator in a continuous iterative process. 





