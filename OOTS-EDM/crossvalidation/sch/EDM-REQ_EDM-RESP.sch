<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EDM-REQ_EDM-RESP</sr:identifier>
           <sr:title xml:lang="en">EDM-REQ_EDM-RESP</sr:title>
           <sr:description xml:lang="en">Cross-message validation of the EDM Request and EDM Response to prove the
           correspondance of the RequestID, Evidence Requester, Evidence Provider, Evidence Type,
           Distribution and Evidence Subject.</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CROSS_MESSAGE</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                 <sr:title xml:lang="en">EDM-REQ</sr:title>
                 <sr:description xml:lang="en">Evidence Request</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>PAYLOAD</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-RESP</sr:identifier>
                 <sr:title xml:lang="en">EDM-RESP</sr:title>
                 <sr:description xml:lang="en">Evidence Response</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>PAYLOAD</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.0.4</sr:version>
           <sr:versionNotes xml:lang="en">Updated version number in EDM-REQ_EDM-RESP.sch</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:let name="edm-resp" value="Documents/Document[@name = 'edm-resp']"/>
   <sch:let name="edm-req" value="Documents/Document[@name = 'edm-req']"/>
   <sch:let name="response_has_empty_objectlist" value="count($edm-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject) = 0" />
   
   <sch:pattern>
      <sch:rule context="Documents">
         <sch:assert
            test="$edm-req/query:QueryRequest/@id = $edm-resp/query:QueryResponse/@requestId"
            role="FATAL" id="R-EDM-REQ_EDM-RESP-01">The value of 'query:QueryRequest/@id' in the Evidence Request MUST be equal to the value of 'query:QueryResponse/@requestId' in the
            Evidence Response. (<sch:value-of select="$edm-req/query:QueryRequest/@id"/>, with length <sch:value-of select="string-length($edm-req/query:QueryRequest/@id)"/> vs
            <sch:value-of select="$edm-resp/query:QueryResponse/@requestId"/>, with length <sch:value-of select="string-length($edm-resp/query:QueryResponse/@requestId)"/> )</sch:assert>
      </sch:rule>
      
      <sch:rule
         context="$edm-resp/query:QueryResponse[@status = 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success']/rim:RegistryObjectList/rim:RegistryObject/rim:Slot/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson">
         <sch:let name="edm-req-person" value="$edm-req/query:QueryRequest/query:Query/rim:Slot[@name = 'NaturalPerson']/rim:SlotValue/sdg:Person"/>
         
         <sch:assert
            test="$response_has_empty_objectlist or (count($edm-req-person/sdg:Identifier) + count(sdg:Identifier) = 0) or $edm-req-person/sdg:Identifier = sdg:Identifier"
            role="FATAL" id="R-EDM-REQ_EDM-RESP-02">If present, the value of 'sdg:Person/sdg:Identifier' in the Evidence Request MUST be equal to the value of
            'sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier' in the Evidence Response. (<sch:value-of
               select="$edm-req-person/sdg:Identifier"/>, with length <sch:value-of select="string-length($edm-req-person/sdg:Identifier)"/>  vs <sch:value-of select="sdg:Identifier"
               />, with length <sch:value-of select="string-length(sdg:Identifier)"/> )</sch:assert>
         
         <sch:assert
            test="$response_has_empty_objectlist or (count($edm-req-person/sdg:FamilyName) + count(sdg:FamilyName) = 0) or $edm-req-person/sdg:FamilyName = sdg:FamilyName"
            role="WARNING" id="R-EDM-REQ_EDM-RESP-03">If present, the value of 'sdg:Person/sdg:FamilyName' in the Evidence Request MUST be equal to the value of
            'sdg:IsAbout/sdg:NaturalPerson/sdg:FamilyName' in the Evidence Response. (<sch:value-of
               select="$edm-req-person/sdg:FamilyName"/>, with length <sch:value-of select="string-length($edm-req-person/sdg:FamilyName)"/> vs <sch:value-of select="sdg:FamilyName"
               />, with length <sch:value-of select="string-length(sdg:FamilyName)"/>)</sch:assert>
         
         <sch:assert
            test="$response_has_empty_objectlist or (count($edm-req-person/sdg:GivenName) + count(sdg:GivenName) = 0) or $edm-req-person/sdg:GivenName = sdg:GivenName"
            role="WARNING" id="R-EDM-REQ_EDM-RESP-04">If present, the value of 'sdg:Person/sdg:GivenName' in the Evidence Request MUST be equal to the value of
            'sdg:IsAbout/sdg:NaturalPerson/sdg:GivenName' in the Evidence Response. (<sch:value-of
               select="$edm-req-person/sdg:GivenName"/>, with length <sch:value-of select="string-length($edm-req-person/sdg:GivenName)"/> vs <sch:value-of select="sdg:GivenName"
               />, with length <sch:value-of select="string-length(sdg:GivenName)"/>)</sch:assert>
         
         <sch:assert
            test="$response_has_empty_objectlist or (count($edm-req-person/sdg:DateOfBirth) + count(sdg:DateOfBirth)= 0) or $edm-req-person/sdg:DateOfBirth = sdg:DateOfBirth"
            role="FATAL" id="R-EDM-REQ_EDM-RESP-05">If present, the value of 'sdg:Person/sdg:DateOfBirth' in the Evidence Request MUST be equal to the value of
            'sdg:IsAbout/sdg:NaturalPerson/sdg:DateOfBirth' in the Evidence Response. (<sch:value-of
               select="$edm-req-person/sdg:DateOfBirth"/>, with length <sch:value-of select="string-length($edm-req-person/sdg:DateOfBirth)"/> vs <sch:value-of select="sdg:DateOfBirth"
               />, with length <sch:value-of select="string-length(sdg:DateOfBirth)"/>)</sch:assert>
      </sch:rule>
      
      <sch:rule
         context="$edm-resp/query:QueryResponse[@status = 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success']/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name = 'EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson">
         <sch:let name="edm-req-legalperson" value="$edm-req/query:QueryRequest/query:Query/rim:Slot[@name = 'LegalPerson']/rim:SlotValue/sdg:LegalPerson"/>
         
         <sch:assert
            test="$response_has_empty_objectlist or (count($edm-req-legalperson/sdg:LegalPersonIdentifier) + count(sdg:LegalPersonIdentifier)= 0) or $edm-req-legalperson/sdg:LegalPersonIdentifier = sdg:LegalPersonIdentifier"
            role="FATAL" id="R-EDM-REQ_EDM-RESP-07">If present, the value of 'sdg:LegalPerson/sdg:LegalPersonIdentifier' in the Evidence Request MUST be equal to the
            value of 'sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier' in the Evidence
            Response. (<sch:value-of select="$edm-req-legalperson/sdg:LegalPersonIdentifier"/>, with length <sch:value-of select="string-length($edm-req-legalperson/sdg:LegalPersonIdentifier)"/> vs
            <sch:value-of select="sdg:LegalPersonIdentifier"/>, with length <sch:value-of select="string-length(sdg:LegalPersonIdentifier)"/> )</sch:assert>
         
         <sch:assert
            test="$response_has_empty_objectlist or (count($edm-req-legalperson/sdg:LegalName) + count(sdg:LegalName)= 0) or $edm-req-legalperson/sdg:LegalName = sdg:LegalName"
            role="WARNING" id="R-EDM-REQ_EDM-RESP-08">If present, the value of 'sdg:LegalPerson/sdg:LegalName' in the Evidence Request MUST be equal to the value of
            'sdg:IsAbout/sdg:LegalPerson/sdg:LegalName' in the Evidence Response. (<sch:value-of select="$edm-req-legalperson/sdg:LegalName"/>, 
            with length <sch:value-of select="string-length($edm-req-legalperson/sdg:LegalName)"/> vs <sch:value-of select="sdg:LegalName"/>, with length <sch:value-of select="string-length(sdg:LegalName)"/>)</sch:assert>
      </sch:rule>
      
      <sch:rule
         context="$edm-resp/query:QueryResponse[@status = 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success']/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name = 'EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:EvidenceTypeClassification">
         <sch:let name="edm-req-evidencetype"
            value="$edm-req/query:QueryRequest/query:Query/rim:Slot[@name = 'EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification"/>
         
         <sch:let name="response_has_empty_objectlist" 
            value="count($edm-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject) = 0" />
         
         <sch:assert test="$response_has_empty_objectlist or  $edm-req-evidencetype = ." role="FATAL"
            id="R-EDM-REQ_EDM-RESP-09">The value of
            'sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification' in the Evidence Request MUST be equal to the value of 'sdg:Evidence/sdg:IsConformantTo/sdg:EvidenceTypeClassification' in the Evidence
            Response. (<sch:value-of select="$edm-req-evidencetype"/>, with length <sch:value-of select="string-length($edm-req-evidencetype)"/> vs <sch:value-of
               select="."/>, with length <sch:value-of select="string-length(.)"/>)</sch:assert>
      </sch:rule>
      
      <sch:rule
         context="$edm-resp/query:QueryResponse[@status = 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success']/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name = 'EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution">
         <sch:let name="edm-req-distributedas"
            value="$edm-req/query:QueryRequest/query:Query/rim:Slot[@name = 'EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs"/>
         
         <sch:assert test="$response_has_empty_objectlist or  $edm-req-distributedas/sdg:Format = sdg:Format" role="FATAL"
            id="R-EDM-REQ_EDM-RESP-10">The value of 'sdg:DistributedAs/sdg:Format' in the Evidence Request MUST be equal to the value of 'sdg:Evidence/sdg:Distribution/sdg:Format' in the
            Evidence Response. (<sch:value-of select="$edm-req-distributedas/sdg:Format"/>, with length <sch:value-of select="string-length($edm-req-distributedas/sdg:Format)"/> vs
            <sch:value-of select="sdg:Format"/>, with length <sch:value-of select="string-length(sdg:Format)"/>)</sch:assert>
         
         <sch:assert
            test="$response_has_empty_objectlist or  (count($edm-req-distributedas/sdg:ConformsTo) + count(sdg:ConformsTo) = 0) or $edm-req-distributedas/sdg:ConformsTo = sdg:ConformsTo"
            role="FATAL" id="R-EDM-REQ_EDM-RESP-11">If present, the value of 'sdg:DistributedAs/sdg:ConformsTo' in the Evidence Request MUST be equal to the value of
            'sdg:Evidence/sdg:Distribution/sdg:ConformsTo' in the Evidence Response. (<sch:value-of select="$edm-req-distributedas/sdg:ConformsTo"/>, 
            with length <sch:value-of select="string-length($edm-req-distributedas/sdg:ConformsTo)"/> vs <sch:value-of
               select="sdg:ConformsTo"/>, with length <sch:value-of select="string-length(sdg:ConformsTo)"/>)</sch:assert>
         
         <sch:assert
            test="$response_has_empty_objectlist or  (count($edm-req-distributedas/sdg:Transformation) + count(sdg:Transformation) = 0) or $edm-req-distributedas/sdg:Transformation = sdg:Transformation"
            role="FATAL" id="R-EDM-REQ_EDM-RESP-12">If present, the value of 'sdg:DistributedAs/sdg:Transformation' in the Evidence Request MUST be equal to the
            value of 'sdg:Evidence/sdg:Distribution/sdg:Transformation' in the Evidence Response.
            (<sch:value-of select="$edm-req-distributedas/sdg:Transformation"/>, with length <sch:value-of select="string-length($edm-req-distributedas/sdg:Transformation)"/> vs <sch:value-of
               select="sdg:Transformation"/>, with length <sch:value-of select="string-length(sdg:Transformation)"/>)</sch:assert>
      </sch:rule>
      
      <sch:rule
         context="$edm-req/query:QueryRequest/rim:Slot[@name = 'EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification='ER']/sdg:Identifier">
         <sch:let name="edm-resp-agent" value="$edm-resp/query:QueryResponse/rim:Slot[@name = 'EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier"/>
         
         <sch:assert test=". = $edm-resp-agent" role="FATAL"
            id="R-EDM-REQ_EDM-RESP-13">The value of the Agent 'rim:Element' with
            'sdg:Classification' value 'ER' in 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier'
            in the Evidence Request MUST be equal to the value of 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier' in the
            Evidence Response (<sch:value-of select="."/>, with length <sch:value-of select="string-length(.)"/> vs <sch:value-of
               select="$edm-resp-agent"/>, with length <sch:value-of select="string-length($edm-resp-agent)"/>)</sch:assert>
         
         <sch:assert test="@schemeID = $edm-resp-agent/@schemeID" role="FATAL"
            id="R-EDM-REQ_EDM-RESP-14">The value of of the Agent 'rim:Element' with
            'sdg:Classification' value 'ER' in 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier/@schemeID'
            in the Evidence Request MUST be equal to the value of 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID'
            in the Evidence Response (<sch:value-of select="@schemeID"/>, with length <sch:value-of select="string-length(@schemeID)"/> vs <sch:value-of
               select="$edm-resp-agent/@schemeID"/>, with length <sch:value-of select="string-length($edm-resp-agent/@schemeID)"/>)</sch:assert>
      </sch:rule>
      
      <sch:rule
         context="$edm-req/query:QueryRequest/rim:Slot[@name = 'EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier">
         <sch:let name="edm-resp-agent"
            value="$edm-resp/query:QueryResponse/rim:Slot[@name = 'EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier"/>
         <sch:let name="edm-resp-agent-classification"
            value="$edm-resp/query:QueryResponse/rim:Slot[@name = 'EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Classification"/>
         <sch:assert
            test=". = $edm-resp-agent and $edm-resp-agent-classification = 'IP' or . = $edm-resp-agent and $edm-resp-agent-classification = 'EP'"
            role="FATAL" id="R-EDM-REQ_EDM-RESP-15">The value of 'rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier' in the
            Evidence Request MUST be equal to the value of the Agent 'rim:Element', either with 'sdg:Classification' value 'EP' or 'IP', in
            'rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier'
            of the Evidence Response (<sch:value-of select="."/>, with length <sch:value-of select="string-length(.)"/> vs <sch:value-of
               select="$edm-resp-agent"/>, with length <sch:value-of select="string-length($edm-resp-agent)"/> as <sch:value-of select="$edm-resp-agent"
               />)</sch:assert>
         
         <sch:assert
            test="@schemeID = $edm-resp-agent/@schemeID and $edm-resp-agent-classification = 'IP' or @schemeID=$edm-resp-agent/@schemeID and $edm-resp-agent-classification = 'EP'"
            role="FATAL" id="R-EDM-REQ_EDM-RESP-16">The value of 'rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID' in
            the Evidence Request MUST be equal to the value of one Agent 'rim:Element', either with 'sdg:Classification' value 'EP' or 'IP', in
            'rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier/@schemeID' of the Evidence Response 
            (<sch:value-of select="@schemeID"/>, with length <sch:value-of select="string-length(@schemeID)"/> vs <sch:value-of
               select="$edm-resp-agent/@schemeID"/>, with length <sch:value-of select="string-length($edm-resp-agent/@schemeID)"/> as <sch:value-of
                  select="$edm-resp-agent-classification"/>, with length <sch:value-of select="string-length($edm-resp-agent-classification)"/>)</sch:assert>
      </sch:rule>
      
      <sch:rule context="$edm-resp/query:QueryResponse">
         <sch:assert
            test="@status = 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success' or @status = 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Unavailable'"
            role="FATAL" id="R-EDM-REQ_EDM-RESP-17">The status value of Evidence Response must be
            urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success or urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Unavailable (<sch:value-of select="$edm-resp/query:QueryResponse/@status"/>, 
         </sch:assert>
      </sch:rule>
    
   </sch:pattern>  
</sch:schema>
