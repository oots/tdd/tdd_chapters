<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/" prefix="eb"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <sch:ns uri="http://www.w3.org/XML/1998/namespace" prefix="xml" />
   <sch:ns uri="http://www.w3.org/2003/05/soap-envelope" prefix="S12"/>
   <sch:ns uri="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" prefix="wsu"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/ebMS-ERR_ebMS-REQ2</sr:identifier>
           <sr:title xml:lang="en">ebMS-ERR_ebMS-REQ2</sr:title>
           <sr:description xml:lang="en">Cross-message validation of the ebMS Response header and the 2nd ebMS Request header to prove corresponance of the From, To, Original Sender, Final Recipient and ConversionID in case of re-authentication and preview</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CROSS_MESSAGE</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-ERR</sr:identifier>
                 <sr:title xml:lang="en">EDM-ERR</sr:title>
                 <sr:description xml:lang="en">Evidence Error Response</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>HEADER</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                 <sr:title xml:lang="en">EDM-REQ</sr:title>
                 <sr:description xml:lang="en">Evidence Request</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>HEADER</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.0.2</sr:version>
           <sr:versionNotes xml:lang="en">Added dcat namespace to cross-validation schematrons</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:let name="evidence_error_response_header" value="Documents/Document[@name = 'evidence_error_response_header']"/>
   <sch:let name="evidence_request_header" value="Documents/Document[@name = 'evidence_request_header']"/>
   
   <sch:pattern>
      <sch:rule context="Documents">

         <sch:assert test="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId" role="FATAL" id="R-ebMS-ERR_ebMS-REQ2-01"
            >The 'eb:From/eb:PartyId' in the ebMS header of the Evidence Error Response containing severity="urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired" must be the same as 'eb:To/eb:PartyId' in the ebMS header of the second Evidence Request containing the preview location.
            <sch:value-of select="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId"/>
         </sch:assert>
         <sch:assert test="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId/@type = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId/@type" role="FATAL" id="R-ebMS-ERR_ebMS-REQ2-02"
            >The 'eb:From/eb:PartyId/@type' in the ebMS header of the Evidence Error Response containing severity="urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired" must be the same as 'eb:To/eb:PartyId/@type' in the ebMS header of the second Evidence Request containing the preview location.
            <sch:value-of select="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId/@type"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId/@type"/>
         </sch:assert>
         <sch:assert test="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId" role="FATAL" id="R-ebMS-ERR_ebMS-REQ2-03"
            >The 'eb:To/eb:PartyId' in the ebMS header of the Evidence Error Response containing severity="urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired" must be the same as  'eb:From/eb:PartyId' in the ebMS header of the second Evidence Request containing the preview location.
            <sch:value-of select="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId"/>
         </sch:assert>
         <sch:assert test="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId/@type = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId/@type" role="FATAL" id="R-ebMS-ERR_ebMS-REQ2-04"
            >The 'eb:To/eb:PartyId/@type' in the ebMS header of the Evidence Error Response containing severity="urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired" must be the same as 'eb:From/eb:PartyId/@type' in the ebMS header of the second Evidence Request containing the preview location.
            <sch:value-of select="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId/@type"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId/@type"/>
         </sch:assert>
         <sch:assert test="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient'] = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']" role="FATAL" id="R-ebMS-ERR_ebMS-REQ2-05"
            >The eb:Property[name='originalSender'] in the ebMS header of the Evidence Error Response containing severity="urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired" must be the same as the eb:Property[name='finalRecipient'] in the ebMS header of the second Evidence Request containing the preview location.
            <sch:value-of select="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']"/>
         </sch:assert>
         <sch:assert test="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']/@type" role="FATAL" id="R-ebMS-ERR_ebMS-REQ2-06"
            >The eb:Property[name='finalRecipient'] in the ebMS header of the Evidence Error Response containing severity="urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired" must be the same as the eb:Property[name='originalSender'] in the ebMS header of the second Evidence Request containing the preview location.
            <sch:value-of select="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']/@type"/>
         </sch:assert>
         <sch:assert test="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender'] = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']" role="FATAL" id="R-ebMS-ERR_ebMS-REQ2-07"
            >The eb:Property[name='finalRecipient']/@type in the ebMS header of the Evidence Error Response containing severity="urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired" must be the same as the eb:Property[name='originalSender']/@type in the ebMS header of the second Evidence Request containing the preview location.
            <sch:value-of select="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']"/>
         </sch:assert>
         <sch:assert test="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']/@type = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type" role="FATAL" id="R-ebMS-ERR_ebMS-REQ2-08"
            >The eb:Property[name='finalRecipient']/@type in the ebMS header of the Evidence Error Response containing severity="urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired" must be the same as the eb:Property[name='originalSender']/@type in the ebMS header of the second Evidence Request containing the preview location.
            <sch:value-of select="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']/@type"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type"/>
         </sch:assert>
         <sch:assert test="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId" role="FATAL" id="R-ebMS-ERR_ebMS-REQ2-09"
            >Conversation identifier in the ebMS header of the Evidence Error Response containing severity="urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired" and ebMS header of the second Evidence Request containing the preview location slot must be the same.
            <sch:value-of select="$evidence_error_response_header/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId"/>
         </sch:assert>
      </sch:rule>
   </sch:pattern>

</sch:schema>
