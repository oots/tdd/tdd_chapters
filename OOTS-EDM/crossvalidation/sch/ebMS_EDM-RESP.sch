<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <sch:ns uri="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/" prefix="eb"/>
    <!--Start of SR metadata-->
    <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/ebMS_EDM-RESP</sr:identifier>
            <sr:title xml:lang="en">ebMS_EDM-RESP</sr:title>
            <sr:description xml:lang="en">Cross-message validation of the ebMS and EDM Response to prove the correspondance of the Evidence Requester Identifier, Evidence Requester Scheme ID, Evidence Provider Identifier, Evidence Provider Scheme ID, Repository Item References and eb:Action</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>CROSS_MESSAGE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-RESP</sr:identifier>
                    <sr:title xml:lang="en">EDM-RESP</sr:title>
                    <sr:description xml:lang="en">Evidence Response</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:subject>HEADER</sr:subject>
                    <sr:component>EDM</sr:component>
                </sr:Transaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-RESP</sr:identifier>
                    <sr:title xml:lang="en">EDM-RESP</sr:title>
                    <sr:description xml:lang="en">Evidence Response</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>EDM</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.0.5</sr:version>
            <sr:versionNotes xml:lang="en">Updated version number of the two modified Schematrons</sr:versionNotes>
            </sr:Asset>?>
    <!--End of SR metadata-->
    <sch:let name="evidence_response_header"
        value="Documents/Document[@name = 'evidence_response_header']"/>
    <sch:let name="edm-resp"
        value="Documents/Document[@name = 'edm-resp']"/>
    <sch:pattern>
        <sch:rule context="$evidence_response_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']">
            <sch:let name="ep_agent_identifier" value="$edm-resp/query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification='EP']/sdg:Identifier"/>
            <sch:assert test="$ep_agent_identifier = ." role="FATAL" id="R-ebMS_EDM-RESP-01"
                >The value of the 'eb:Property[name='originalSender']' in the ebMS header of the Evidence Response MUST be equal to the value of the Agent 'rim:Element' with 'sdg:Classification' value 'EP' in 'rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier' in the Evidence Response (<sch:value-of select="$ep_agent_identifier"/>  vs <sch:value-of select="."/>)</sch:assert>
        </sch:rule>
        
        <sch:rule context="$evidence_response_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']/@type">
            <sch:let name="ep_agent_identifier_scheme" value="$edm-resp/query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification='EP']/sdg:Identifier/@schemeID"/>
            <sch:assert test="$ep_agent_identifier_scheme = ." role="FATAL" id="R-ebMS_EDM-RESP-02"
                >The value of the 'eb:Property[name='originalSender']/@type' in the ebMS header of the Evidence Response MUST be equal to the value of the Agent 'rim:Element' with 'sdg:Classification' value 'EP' of 'rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier/@schemeID' in the Evidence Response (<sch:value-of select="$ep_agent_identifier_scheme"/> vs <sch:value-of select="."/>)</sch:assert>
        </sch:rule>
        
        <sch:rule context="$evidence_response_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']">
            <sch:let name="er_agent_identifier" value="$edm-resp/query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier"/>
            <sch:assert test="$er_agent_identifier = ." role="FATAL" id="R-ebMS_EDM-RESP-03"
                >The value of the 'eb:Property[name='finalRecipient']' in the ebMS header of the Evidence Response MUST be equal to the value of the 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier' in the Evidence Response (<sch:value-of select="$er_agent_identifier"/> vs <sch:value-of select="."/>)</sch:assert>
        </sch:rule>
        
        <sch:rule context="$evidence_response_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type">
            <sch:let name="er_agent_identifier_scheme" value="$edm-resp/query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID"/>
            <sch:assert test="$er_agent_identifier_scheme = ." role="FATAL" id="R-ebMS_EDM-RESP-04"
                >The value of the 'eb:Property[name='finalRecipient']/@type' in the ebMS header of the Evidence Response MUST be equal to the value of the 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID' in the Evidence Response (<sch:value-of select="$er_agent_identifier_scheme"/>  vs <sch:value-of select="."/>)</sch:assert>
        </sch:rule>

        <sch:rule context="$edm-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:RepositoryItemRef/@xlink:href"> 
            <sch:let name="ebms_payload_href" value="$evidence_response_header/eb:Messaging/eb:UserMessage/eb:PayloadInfo/eb:PartInfo"/>
            <sch:assert test=". = $ebms_payload_href/@href" role="FATAL" id="R-ebMS_EDM-RESP-05"
                >The values of the 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:RepositoryItemRef/@xlink:href' of the Evidence Response MUST be present in 'eb:PartInfo/@href' of the ebMS header (<sch:value-of select="."/>  vs <sch:value-of select="$ebms_payload_href/@href"/>)</sch:assert>
        </sch:rule>
         
        <sch:rule context="$evidence_response_header/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Action">
            <sch:let name="response_status" value="$edm-resp/query:QueryResponse/@status"/>
            <sch:assert test=". = 'ExecuteQueryResponse' and ($response_status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success' or $response_status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Unavailable')" role="FATAL" id="R-ebMS_EDM-RESP-06"
                >If the value of 'eb:Action' is 'ExecuteQueryResponse' in the ebMS ebMS header of the Evidence Response, the 'status' attribute of a 'QueryResponse' of the Evidence Response MUST be encoded as "urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success" or "urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Unavailable". The response_status is:  <sch:value-of select="$response_status"/></sch:assert>
        </sch:rule> 
    </sch:pattern>   
    
</sch:schema>
