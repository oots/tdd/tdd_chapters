<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <sch:ns prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EB-REQ_EB-EVI</sr:identifier>
           <sr:title xml:lang="en">EB-REQ_EB-EVI</sr:title>
           <sr:description xml:lang="en">Cross-message validation of the EB get Requirements Query Response and EB get Evidence Types Query Response to prove correspondance of Requirement and Reference Framework</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CROSS_MESSAGE</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EB-REQ</sr:identifier>
                 <sr:title xml:lang="en">EB-REQ</sr:title>
                 <sr:description xml:lang="en">Query Response of the EB for the 'Requirement Query'</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:component>EB</sr:component>
              </sr:Transaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EB-EVI</sr:identifier>
                 <sr:title xml:lang="en">EB-EVI</sr:title>
                 <sr:description xml:lang="en">Query Response of the EB for the 'Get Evidence Types for Requirement Query'</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:component>EB</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.0.2</sr:version>
           <sr:versionNotes xml:lang="en">Added dcat namespace to cross-validation schematrons</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:let name="eb-req" value="Documents/Document[@name = 'eb-req']"/>
   <sch:let name="eb-evi" value="Documents/Document[@name = 'eb-evi']"/>
   
   <sch:pattern>
      <sch:rule context="Documents">
         <sch:assert test="$eb-req/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Identifier = $eb-evi/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Identifier" role="FATAL" id="R-EB-REQ_EB-EVI-001"
            >The requirements' identifier in EB get Requirements Query Response (EB-REQ) must be the same as in EB get Evidence Types Query Response (EB-EVI), if the response status is Success. 
            (<sch:value-of select="$eb-req/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Identifier"/> vs <sch:value-of select="$eb-evi/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Identifier"/> </sch:assert>
         <sch:assert test="$eb-req/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Identifier = $eb-evi/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Identifier" role="FATAL" id="R-EB-REQ_EB-EVI-002"
            >The reference framework's identifier in EB get Requirements Query Response (EB-REQ) must be the same as in the EB get Evidence Types Query Response (EB-EVI), if the response status is Success.
            (<sch:value-of select="$eb-req/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Identifier"/> vs <sch:value-of select="$eb-evi/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Identifier"/>)
         </sch:assert>
      </sch:rule>
   </sch:pattern>

</sch:schema>
