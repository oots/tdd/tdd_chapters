<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EDM-REQ1_EDM-REQ2</sr:identifier>
           <sr:title xml:lang="en">EDM-REQ1_EDM-REQ2</sr:title>
           <sr:description xml:lang="en">Cross-message validation of the 1st Evidence Request and the 2nd evidence Request header to prove equivalence of all elements except the id and the preview location of the Evidence Request</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CROSS_MESSAGE</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                 <sr:title xml:lang="en">EDM-REQ</sr:title>
                 <sr:description xml:lang="en">Evidence Request</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>PAYLOAD</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                 <sr:title xml:lang="en">EDM-REQ</sr:title>
                 <sr:description xml:lang="en">Evidence Request</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>PAYLOAD</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.0.4</sr:version>
           <sr:versionNotes xml:lang="en">Updated version number in EDM-REQ1_EDM-REQ2.sch</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:let name="edm-req" value="Documents/Document[@name = 'edm-req']"/>
   <sch:let name="edm-req2" value="Documents/Document[@name = 'edm-req2']"/>
   
   <sch:pattern>
      <sch:rule context="Documents">
         <sch:assert 
            test="$edm-req/query:QueryRequest/@id != $edm-req2/query:QueryRequest/@id" 
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-01"
            >The value of 'query:QueryRequest/@id' in the first Evidence Request MUST be different from the value of 'query:QueryRequest/@id' in the second Evidence Request. (<sch:value-of 
               select="$edm-req/query:QueryRequest/@id" /> vs <sch:value-of 
                  select="$edm-req2/query:QueryRequest/@id"/>)</sch:assert>
         
         <sch:let name='first-issue-datetime' value="$edm-req//rim:Slot[@name='IssueDateTime']//rim:Value/text()"/>
         <sch:let name='second-issue-datetime' value="$edm-req2//rim:Slot[@name='IssueDateTime']//rim:Value/text()"/>
         
         <sch:assert 
            test=" string-length($first-issue-datetime) =0 or $first-issue-datetime != $second-issue-datetime" 
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-02"
            >The value of IssueDateTime of the first Evidence Request MUST be different from the value  of 
            IssueDateTime of second Evidence Request.(<sch:value-of 
               select="$second-issue-datetime" /> vs <sch:value-of 
                  select="$second-issue-datetime"/>)</sch:assert>
      </sch:rule>


      <sch:rule context="$edm-req//rim:Slot[@name != 'IssueDateTime']">
         
         <sch:let name="slot_name" value="@name" />
         <sch:let name="corresponding_slot_count" value="count($edm-req2//rim:Slot[@name =$slot_name])"/>
         <sch:let name="corresponding_slot" value="$edm-req2//rim:Slot[@name =$slot_name]"/>
         
         <sch:assert test="$corresponding_slot_count > 0"            
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-03"
            >For every Slot with name <sch:value-of 
               select="$slot_name"/> in the first request there MUST be an occurrence in the second EDM request.</sch:assert>

         <sch:let name="child_element_count" value="0+count(child::*)"/>
         <sch:let name="other_child_element_count" value="0+count($corresponding_slot/child::*)"/>
         
         <sch:assert 
            test="$corresponding_slot_count > 0 and $child_element_count = $other_child_element_count"
            role="FATAL"
            id="R-EDM-REQ1_EDM-REQ2-04"
            >Two occurences of slot with name <sch:value-of 
               select="$slot_name"/> MUST have the same number of child elements</sch:assert>

      </sch:rule>
         
      <sch:rule context="$edm-req//rim:Slot[@name != 'IssueDateTime']/child::*">
         
         <sch:let name="name" value="name()"></sch:let>
         <sch:let name="count_preceding" value="0+count(preceding-sibling::*)"></sch:let>
         <sch:let name="position" value="1+$count_preceding" />
         
         <sch:let name="slot_name" value="parent::*/@name" />
         <sch:let name="corresponding_slot" value="$edm-req2//rim:Slot[@name =$slot_name]"/>
         
         <sch:let name="other_element" 
            value="$edm-req2//rim:Slot[@name =$slot_name]/child::*[0+count(preceding-sibling::*)=$count_preceding]" />
         <sch:let name="other_name" value="$other_element/name()"></sch:let>

         <sch:assert test="$name = $other_name"
            role="FATAL"
            id="R-EDM-REQ1_EDM-REQ2-05"
            >Two child elements of a Slot with the same position <sch:value-of select="$position"/>
            MUST have the same element name
            (<sch:value-of select="$name"/> vs <sch:value-of select="$other_name"/>) 
            count_preceding <sch:value-of select="$count_preceding"/>
         </sch:assert>

         <sch:let name="value_av" value="string(./@value)"/>
         <sch:let name="other_value_av" value="string($other_element/@value)"/>
         <sch:assert test="$value_av = $other_value_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-19"
            >The attribute "value" MUST have the same value on the corresponding node (<sch:value-of 
               select="$value_av"/> vs <sch:value-of select="$other_value_av"/>)</sch:assert>          
         <sch:let name="lang_av" value="string(./@lang)"/>
         <sch:let name="other_lang_av" value="string($other_element/@lang)"/>
         <sch:assert test="$lang_av = $other_lang_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-24"
            >The attribute "lang" MUST have the same value on the corresponding node (<sch:value-of 
               select="$lang_av"/> vs <sch:value-of select="$other_lang_av"/>)</sch:assert>              
         <sch:let name="xmllang_av" value="string(./@xml:lang)"/>
         <sch:let name="other_xmllang_av" value="string($other_element/@xml:lang)"/>
         <sch:assert test="$xmllang_av = $other_xmllang_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-29"
            >The attribute "xml:lang" MUST have same value on the corresponding node (<sch:value-of 
               select="$xmllang_av"/> vs <sch:value-of select="$other_xmllang_av"/>)</sch:assert>            
         <sch:let name="schemeid_av" value="string(./@schemeID)"/>
         <sch:let name="other_schemeid_av" value="string($other_element/@schemeID)"/>
         <sch:assert test="$schemeid_av = $other_schemeid_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-34"
            >The attribute "schemeid" MUST have the same value on the corresponding node (<sch:value-of 
               select="$schemeid_av"/> vs <sch:value-of select="$other_schemeid_av"/>)</sch:assert>
         
         <sch:let name="child_element_count" value="0+count(child::*)"/>
         <sch:let name="other_child_element_count" value="0+count($other_element/child::*)"/>
         
         <sch:assert 
            test="$child_element_count = $other_child_element_count"
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-06"
            >Two child elements of a Slot<sch:value-of 
               select="$name"/> with the same position MUST have the same number of child elements: <sch:value-of 
                  select="$child_element_count"/> vs <sch:value-of 
                     select="$other_child_element_count"/>             
         </sch:assert>
      </sch:rule>
      
      <sch:rule context="$edm-req//rim:Slot[@name != 'IssueDateTime']/*/*">
         <sch:let name="name" value="name()"></sch:let>
         <sch:let name="count_preceding" value="0+count(preceding-sibling::*)"></sch:let>
         <sch:let name="position" value="1+$count_preceding" />
         <sch:let name="parent-name" value="parent::*/name()"></sch:let>
         <sch:let name="parent-position" value="1+count(parent::*/preceding-sibling::*)" />

         <sch:let name="slot_name" value="../../@name" />
         <sch:let name="corresponding_slot" 
            value="$edm-req2//rim:Slot[@name =$slot_name]"/>
         
         <sch:let name="other_element" 
            value="$edm-req2//rim:Slot[@name =$slot_name]/child::*[
                   position()=$parent-position]/child::*[
                   position()=$position]" />
         <sch:let name="other_name" value="$other_element/name()"></sch:let>

         <sch:assert test="$name = $other_name"
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-07"
            >Two second level descendant elements of a Slot <sch:value-of 
               select="$name"/> with the same position MUST have the same name (<sch:value-of 
                  select="$name"/> vs <sch:value-of select="$other_name"/>   
                  parent position <sch:value-of select="$parent-position"/> name <sch:value-of select="$parent-name"/>    
            )
         </sch:assert>

         <sch:let name="value_av" value="string(./@value)"/>
         <sch:let name="other_value_av" value="string($other_element/@value)"/>
         <sch:assert test="$value_av = $other_value_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-20"
            >The attribute "value" MUST have the same value on the corresponding node (<sch:value-of 
               select="$value_av"/> vs <sch:value-of select="$other_value_av"/>)</sch:assert>       
         <sch:let name="lang_av" value="string(./@lang)"/>
         <sch:let name="other_lang_av" value="string($other_element/@lang)"/>
         <sch:assert test="$lang_av = $other_lang_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-25"
            >The attribute "lang" MUST have the same value on the corresponding node (<sch:value-of 
               select="$lang_av"/> vs <sch:value-of select="$other_lang_av"/>)</sch:assert> 
         <sch:let name="xmllang_av" value="string(./@xml:lang)"/>
         <sch:let name="other_xmllang_av" value="string($other_element/@xml:lang)"/>
         <sch:assert test="$xmllang_av = $other_xmllang_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-30"
            >The attribute "xml:lang" MUST have the same value on the corresponding node (<sch:value-of 
               select="$xmllang_av"/> vs <sch:value-of select="$other_xmllang_av"/>)</sch:assert>         
         <sch:let name="schemeid_av" value="string(./@schemeID)"/>
         <sch:let name="other_schemeid_av" value="string($other_element/@schemeID)"/>
         <sch:assert test="$schemeid_av = $other_schemeid_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-35"
            >The attribute "schemeid" MUST have the same value on the corresponding node (<sch:value-of 
               select="$schemeid_av"/> vs <sch:value-of select="$other_schemeid_av"/>)</sch:assert>
         
         <sch:let name="child_element_count" value="0+count(child::*)"/>
         <sch:let name="other_child_element_count" value="0+count($other_element/child::*)"/>
         <sch:assert 
            test="$child_element_count = $other_child_element_count"
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-08"
            >Two second level descendant elements of a Slot <sch:value-of 
               select="$name"/> with the same position MUST have the same number of child elements: <sch:value-of 
                  select="$child_element_count"/> vs <sch:value-of 
                     select="$other_child_element_count"/>             
         </sch:assert>
         <sch:let name="text_content" value="string-join(text())" />
         <sch:let name="other_text_content" value="string-join($other_element/text())" />
         <sch:assert 
            test="not($child_element_count = 0) or $text_content = $other_text_content"
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-09"            
            >Two second level descendant elements of a Slot <sch:value-of 
               select="$name"/> with the same position MUST have the same text content: <sch:value-of 
               select="$text_content"/> vs <sch:value-of 
                  select="$other_text_content"/></sch:assert>        
      </sch:rule>

      <sch:rule context="$edm-req//rim:Slot[@name != 'IssueDateTime']/*/*/*">
         <sch:let name="name" value="name()"></sch:let>
         <sch:let name="count_preceding" value="0+count(preceding-sibling::*)"></sch:let>
         <sch:let name="position" value="1+$count_preceding" />
         
         <sch:let name="parent-name" value="parent::*/name()"></sch:let>
         <sch:let name="parent-position" value="1+count(parent::*/preceding-sibling::*)" />

         <sch:let name="grand-parent-name" value="parent::*/parent::*/name()"></sch:let>
         <sch:let name="grand-parent-position" value="1+count(parent::*/parent::*/preceding-sibling::*)" />
         
         <sch:let name="slot_name" value="../../../@name" />
         <sch:let name="corresponding_slot" 
            value="$edm-req2//rim:Slot[@name =$slot_name]"/>
         
         <sch:let name="other_element" 
            value="$edm-req2//rim:Slot[@name =$slot_name]/child::*[
                    position()=$grand-parent-position]/child::*[
                    position()=$parent-position]/child::*[
                    position()=$position]" />
         <sch:let name="other_name" value="$other_element/name()"></sch:let>

         <sch:assert test="$name = $other_name"
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-10" 
            >Two third level descendant elements of a Slot <sch:value-of 
               select="$name"/> with the same position MUST have the same name (<sch:value-of 
                  select="$name"/> vs <sch:value-of select="$other_name"/> 
            grand parent position <sch:value-of select="$grand-parent-position"/> 
            name <sch:value-of select="$grand-parent-name"/>
            parent position <sch:value-of select="$parent-position"/> 
            name <sch:value-of select="$parent-name"/>    
            )
         </sch:assert>
         
         <sch:let name="value_av" value="string(./@value)"/>
         <sch:let name="other_value_av" value="string($other_element/@value)"/>
         <sch:assert test="$value_av = $other_value_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-21"
            >The attribute "value" MUST have the same value on the corresponding node (<sch:value-of 
               select="$value_av"/> vs <sch:value-of select="$other_value_av"/>)</sch:assert>       
         <sch:let name="lang_av" value="string(./@lang)"/>
         <sch:let name="other_lang_av" value="string($other_element/@lang)"/>
         <sch:assert test="$lang_av = $other_lang_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-26"
            >The attribute "lang" MUST have the same value on the corresponding node  (<sch:value-of 
               select="$lang_av"/> vs <sch:value-of select="$other_lang_av"/>)</sch:assert>   
         <sch:let name="xmllang_av" value="string(./@xml:lang)"/>
         <sch:let name="other_xmllang_av" value="string($other_element/@xml:lang)"/>
         <sch:assert test="$xmllang_av = $other_xmllang_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-31"
            >31 The attribute "xml:lang" MUST have the same value on the corresponding node  (<sch:value-of 
               select="$xmllang_av"/> vs <sch:value-of select="$other_xmllang_av"/>)</sch:assert>         
         <sch:let name="schemeid_av" value="string(./@schemeID)"/>
         <sch:let name="other_schemeid_av" value="string($other_element/@schemeID)"/>
         <sch:assert test="$schemeid_av = $other_schemeid_av"  role="FATAL" id="R-EDM-REQ1_EDM-REQ2-36"
            >The attribute "schemeid" MUST have the same value on the corresponding node  (<sch:value-of 
               select="$schemeid_av"/> vs <sch:value-of select="$other_schemeid_av"/>)</sch:assert>
         
         <sch:let name="child_element_count" value="0+count(child::*)"/>
         <sch:let name="other_child_element_count" value="0+count($other_element/child::*)"/>
         <sch:assert test="$child_element_count = $other_child_element_count"
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-11"            
            >Two third level descendant elements of a Slot <sch:value-of 
               select="$name"/> with the same position MUST have the same number of child elements(<sch:value-of 
                  select="$child_element_count"/> vs <sch:value-of 
                     select="$other_child_element_count"/>)             
         </sch:assert>
         
         <sch:let name="text_content" value="string-join(text())" />
         <sch:let name="other_text_content" value="string-join($other_element/text())" />
         <sch:assert 
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-12"            
            test="not($child_element_count = 0) or $text_content = $other_text_content"
            >Two third level descendant elements of a Slot <sch:value-of 
               select="$name"/> with the same position MUST have the same text content: "<sch:value-of 
               select="$text_content"/>" vs "<sch:value-of 
                  select="$other_text_content"/>" <sch:value-of select="$child_element_count"/> 
         </sch:assert>
      </sch:rule>

      <sch:rule context="$edm-req//rim:Slot[@name != 'IssueDateTime']/*/*/*/*">
         <sch:let name="name" value="name()"></sch:let>
         <sch:let name="count_preceding" value="0+count(preceding-sibling::*)"></sch:let>
         <sch:let name="position" value="1+$count_preceding" />
         
         <sch:let name="parent-name" value="parent::*/name()"></sch:let>
         <sch:let name="parent-position" value="1+count(parent::*/preceding-sibling::*)" />
         
         <sch:let name="grand-parent-name" 
            value="parent::*/parent::*/name()"></sch:let>
         <sch:let name="grand-parent-position" 
            value="1+count(parent::*/parent::*/preceding-sibling::*)" />

         <sch:let name="great-grand-parent-name" 
            value="parent::*/parent::*/parent::*/name()"></sch:let>
         <sch:let name="great-grand-parent-position" 
            value="1+count(parent::*/parent::*/parent::*/preceding-sibling::*)" />
         

         <sch:let name="slot_name" value="../../../../@name" />
         <sch:let name="corresponding_slot" 
            value="$edm-req2//rim:Slot[@name =$slot_name]"/>
         
         <sch:let name="other_element" 
            value="$edm-req2//rim:Slot[@name =$slot_name]/child::*[
                   position()=$great-grand-parent-position]/child::*[
                   position()=$grand-parent-position]/child::*[
                   position()=$parent-position]/child::*[
                   position()=$position]" />
         <sch:let name="other_name" value="$other_element/name()"></sch:let>

         <sch:assert test="$name = $other_name"
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-13"            
            >Two fourth level descendant elements of a Slot <sch:value-of 
               select="$name"/> with the same position <sch:value-of select="$position"/> MUST have the same name (<sch:value-of 
                  select="$name"/> vs <sch:value-of select="$other_name"/>
            great grand parent position <sch:value-of select="$great-grand-parent-position"/> 
            grand parent position <sch:value-of select="$grand-parent-position"/> 
            name <sch:value-of select="$grand-parent-name"/>
            parent position <sch:value-of select="$parent-position"/> 
            name <sch:value-of select="$parent-name"/>    
            )</sch:assert>

         <sch:let name="value_av" value="string(./@value)"/>
         <sch:let name="other_value_av" value="string($other_element/@value)"/>
         <sch:assert test="$value_av = $other_value_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-22"
            >The attribute "value" MUST have the same value on the corresponding node (<sch:value-of 
               select="$value_av"/> vs <sch:value-of select="$other_value_av"/>)</sch:assert>              
         <sch:let name="lang_av" value="string(./@lang)"/>
         <sch:let name="other_lang_av" value="string($other_element/@lang)"/>
         <sch:assert test="$lang_av = $other_lang_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-27"
            >The attribute "lang" MUST have the same value on the corresponding node (<sch:value-of 
               select="$lang_av"/> vs <sch:value-of select="$other_lang_av"/>)</sch:assert>
         <sch:let name="xmllang_av" value="string(./@xml:lang)"/>
         <sch:let name="other_xmllang_av" value="string($other_element/@xml:lang)"/>
         <sch:assert test="$xmllang_av = $other_xmllang_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-32"
            >The attribute "xml:lang" MUST have the same value on the corresponding node (<sch:value-of 
               select="$xmllang_av"/> vs <sch:value-of select="$other_xmllang_av"/>)</sch:assert>         
         <sch:let name="schemeid_av" value="string(./@schemeID)"/>
         <sch:let name="other_schemeid_av" value="string($other_element/@schemeID)"/>
         <sch:assert test="$schemeid_av = $other_schemeid_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-37"
            >The attribute "schemeid" MUST have the same value on the corresponding node (<sch:value-of 
               select="$schemeid_av"/> vs <sch:value-of select="$other_schemeid_av"/>)</sch:assert>
         
         <sch:let name="child_element_count" value="0+count(child::*)"/>
         <sch:let name="other_child_element_count" value="0+count($other_element/child::*)"/>
         <sch:assert test="$child_element_count = $other_child_element_count"
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-14"
            >Two fourth level descendant elements of a Slot <sch:value-of 
               select="$name"/> with the same position <sch:value-of 
                  select="$position"/> MUST have the number of child elements: <sch:value-of 
                  select="$child_element_count"/> vs <sch:value-of 
                     select="$other_child_element_count"/></sch:assert>

         <sch:let name="text_content" value="string-join(text())" />
         <sch:let name="other_text_content" value="string-join($other_element/text())" />
         <sch:assert 
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-15"            
            test="not($child_element_count = 0) or $text_content = $other_text_content"
            >Two fourth level descendant elements of a Slot <sch:value-of 
               select="$name"/> with the same position <sch:value-of 
                  select="$position"/> MUST have the same text content: <sch:value-of 
               select="$text_content"/> vs <sch:value-of 
                  select="$other_text_content"/></sch:assert>                  
      </sch:rule>

      <sch:rule context="$edm-req//rim:Slot[@name != 'IssueDateTime']/*/*/*/*/*">
         <sch:let name="slot_name" value="../../../../../@name" />

         <sch:let name="name" value="name()"></sch:let>
         <sch:let name="count_preceding" value="0+count(preceding-sibling::*)"></sch:let>
         <sch:let name="position" value="1+$count_preceding" />


         <sch:let name="parent-name" value="parent::*/name()"></sch:let>
         <sch:let name="parent-position" value="1+count(parent::*/preceding-sibling::*)" />
         <sch:let name="grand-parent-name" 
            value="parent::*/parent::*/name()"></sch:let>
         <sch:let name="grand-parent-position" 
            value="1+count(parent::*/parent::*/preceding-sibling::*)" />
         <sch:let name="great-grand-parent-name" 
            value="parent::*/parent::*/parent::*/name()"></sch:let>
         <sch:let name="great-grand-parent-position" 
            value="1+count(parent::*/parent::*/parent::*/preceding-sibling::*)" />
         <sch:let name="great-great-grand-parent-name" 
            value="parent::*/parent::*/parent::*/parent::*/name()"></sch:let>
         <sch:let name="great-great-grand-parent-position" 
            value="1+count(parent::*/parent::*/parent::*/parent::*/preceding-sibling::*)" />

         <sch:let name="corresponding_slot" 
            value="$edm-req2//rim:Slot[@name =$slot_name]"/>
         
         <sch:let name="other_element" 
            value="$edm-req2//rim:Slot[@name =$slot_name]/child::*[
                    position()=$great-great-grand-parent-position]/child::*[
                    position()=$great-grand-parent-position]/child::*[
                    position()=$grand-parent-position]/child::*[
                    position()=$parent-position]/child::*[
                    position()=$position]" />
         <sch:let name="other_name" value="$other_element/name()"></sch:let>

         <sch:assert test="$name = $other_name"
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-16"            
            >Two fifth level descendant elements of a Slot <sch:value-of 
               select="$name"/> with the same position <sch:value-of select="$position"/> MUST have the same name (<sch:value-of 
                  select="$name"/> vs <sch:value-of select="$other_name"/>
            great great grand parent position <sch:value-of select="$great-great-grand-parent-position"/> 
            great grand parent position <sch:value-of select="$great-grand-parent-position"/> 
            grand parent position <sch:value-of select="$grand-parent-position"/> 
            name <sch:value-of select="$grand-parent-name"/>
            parent position <sch:value-of select="$parent-position"/> 
            name <sch:value-of select="$parent-name"/>    
            )
         </sch:assert>

         <sch:let name="value_av" value="string(./@value)"/>
         <sch:let name="other_value_av" value="string($other_element/@value)"/>
         <sch:assert test="$value_av = $other_value_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-23"
            >The attribute "value" MUST have the same value on the corresponding node  (<sch:value-of 
               select="$value_av"/> vs <sch:value-of select="$other_value_av"/>)</sch:assert>      
         <sch:let name="lang_av" value="string(./@lang)"/>
         <sch:let name="other_lang_av" value="string($other_element/@lang)"/>
         <sch:assert test="$lang_av = $other_lang_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-28"
            >The attribute "lang" MUST have the same value on the corresponding node  (<sch:value-of 
               select="$lang_av"/> vs <sch:value-of select="$other_lang_av"/>)</sch:assert>  
         <sch:let name="xmllang_av" value="string(./@xml:lang)"/>
         <sch:let name="other_xmllang_av" value="string($other_element/@xml:lang)"/>
         <sch:assert test="$xmllang_av = $other_xmllang_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-33"
            >The attribute "xml:lang" MUST have the same value on the corresponding node  (<sch:value-of 
               select="$xmllang_av"/> vs <sch:value-of select="$other_xmllang_av"/>)</sch:assert> 
         <sch:let name="schemeid_av" value="string(./@schemeID)"/>
         <sch:let name="other_schemeid_av" value="string($other_element/@schemeID)"/>
         <sch:assert test="$schemeid_av = $other_schemeid_av" role="FATAL" id="R-EDM-REQ1_EDM-REQ2-38"
            >The attribute "schemeid" MUST have the same value on the corresponding node  (<sch:value-of 
               select="$schemeid_av"/> vs <sch:value-of select="$other_schemeid_av"/>)</sch:assert>
         
         <sch:let name="child_element_count" value="0+count(child::*)"/>
         <sch:let name="other_child_element_count" value="0+count($other_element/child::*)"/>
         <sch:assert test="$child_element_count = $other_child_element_count"
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-17"            
            >Two fifth level descendant elements of a Slot <sch:value-of 
               select="$name"/> with the same position <sch:value-of 
                  select="$position"/> MUST have the same number of child elements for <sch:value-of 
               select="$name"/>: <sch:value-of 
                  select="$child_element_count"/> vs <sch:value-of 
                     select="$other_child_element_count"/>             
         </sch:assert>
         
         <sch:let name="text_content" value="string-join(text())" />
         <sch:let name="other_text_content" value="string-join($other_element/text())" />
         <sch:assert 
            test="not($child_element_count = 0) or $text_content = $other_text_content"
            role="FATAL" 
            id="R-EDM-REQ1_EDM-REQ2-18"            
            >Two fifth level descendant elements of a Slot <sch:value-of 
               select="$name"/> with the same position <sch:value-of 
                  select="$position"/> MUST have the same content: <sch:value-of 
               select="$text_content"/> vs <sch:value-of 
                  select="$other_text_content"/></sch:assert>
        
      </sch:rule>
      
      
   </sch:pattern>
   
</sch:schema>
