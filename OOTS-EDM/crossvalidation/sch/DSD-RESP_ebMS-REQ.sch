<?xml version="1.0" encoding="UTF-8"?>
<sch:schema 
   xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/" prefix="eb"/>   
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/DSD-RESP_ebMS-REQ</sr:identifier>
           <sr:title xml:lang="en">DSD-RESP_ebMS-REQ</sr:title>
           <sr:description xml:lang="en">Cross-message validation of the DSD Response and ebMS Request header to prove correspondance of the Publisher and Access Service</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CROSS_MESSAGE</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-RESP</sr:identifier>
                 <sr:title xml:lang="en">DSD-RESP</sr:title>
                 <sr:description xml:lang="en">Query Response of the DSD</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:component>DSD</sr:component>
              </sr:Transaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                 <sr:title xml:lang="en">EDM-REQ</sr:title>
                 <sr:description xml:lang="en">Evidence Request</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>HEADER</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.0.2</sr:version>
           <sr:versionNotes xml:lang="en">Updated the version information in Schematron and added Version information to cross validaton Schematron and</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:let name="dsd-resp" value="Documents/Document[@name = 'dsd-resp']"/>
   <sch:let name="evidence_request_header" value="Documents/Document[@name = 'evidence_request_header']"/>
   
   <sch:pattern>
      <sch:rule context="Documents">
         
         <sch:assert test="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']" role="FATAL" id="R-DSD-RESP_ebMS-01"
            >The value of the 'sdg:Publisher/sdg:Identifier' of the DSD Response MUST be equal to the value of the 'eb:Property[name='finalRecipient']' in the ebMS header of the Evidence Request
            <sch:value-of select="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']"/>
         </sch:assert>
         <sch:assert test="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier/@schemeID = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type" role="FATAL" id="R-DSD-RESP_ebMS-02"
            >The value of the 'sdg:Publisher/sdg:Identifier/@SchemeID' of the DSD Response MUST be equal to the value of the 'eb:Property[name='finalRecipient']/@type' in the ebMS header of the Evidence Request
            <sch:value-of select="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier/@schemeID"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type"/>
         </sch:assert>
         <sch:assert test="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Identifier = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId" role="FATAL" id="R-DSD-RESP_ebMS-03"
            >The 'sdg:AccessService/sdg:Identifier' in the DSD Response MUST be the same as  'eb:To/eb:PartyId' in the ebMS header of the Evidence Request.
            <sch:value-of select="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Identifier"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId"/>
         </sch:assert>
         <sch:assert test="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Identifier/@schemeID = $evidence_request_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId/@type" role="FATAL" id="R-DSD-RESP_ebMS-04"
            >The 'sdg:AccessService/sdg:Identifier/@schemeID' in the DSD Response MUST be the same as 'eb:To/eb:PartyId/@type' in the ebMS header of the Evidence Request.
            <sch:value-of select="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Identifier/@schemeID"/> vs <sch:value-of select="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId/@type"/>
         </sch:assert>
         
      </sch:rule>
   </sch:pattern>
   
</sch:schema>
