<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/" prefix="eb"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <sch:ns uri="http://www.w3.org/XML/1998/namespace" prefix="xml" />
   <sch:ns uri="http://www.w3.org/2003/05/soap-envelope" prefix="S12"/>
   <sch:ns uri="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" prefix="wsu"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/ebMS-REQ1_ebMS-REQ2</sr:identifier>
           <sr:title xml:lang="en">ebMS-REQ1_ebMS-REQ2</sr:title>
           <sr:description xml:lang="en">Cross-message validation of the 1st ebMS Request Header and the 2nd ebMS Request header to prove equivalence of eb:PartyInfo, eb:CollaborationInfo and eb:MessageProperties in case of re-authentication and preview</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CROSS_MESSAGE</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                 <sr:title xml:lang="en">EDM-REQ</sr:title>
                 <sr:description xml:lang="en">Evidence Request</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>HEADER</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                 <sr:title xml:lang="en">EDM-REQ</sr:title>
                 <sr:description xml:lang="en">Evidence Request</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>HEADER</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.0.2</sr:version>
           <sr:versionNotes xml:lang="en">Added dcat namespace to cross-validation schematrons</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:let name="evidence_request_header1" value="Documents/Document[@name = 'evidence_request_header1']"/>
   <sch:let name="evidence_request_header2" value="Documents/Document[@name = 'evidence_request_header2']"/>
   
   <sch:pattern>
      <sch:rule context="Documents">

         <sch:assert test="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId = $evidence_request_header2/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId" role="FATAL" id="R-ebMS_ebMS-01"
            >The 'eb:From/eb:PartyId' in the first and second ebMS Evidence Request header must be the same.
            <sch:value-of select="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId"/> vs <sch:value-of select="$evidence_request_header2/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId"/>
         </sch:assert>
         <sch:assert test="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId/@type = $evidence_request_header2/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId/@type" role="FATAL" id="R-ebMS_ebMS-02"
            >The 'eb:From/eb:PartyId/@type' in the first and second ebMS Evidence Request header must be the same.
            <sch:value-of select="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId/@type"/> vs <sch:value-of select="$evidence_request_header2/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:PartyId/@type"/>
         </sch:assert>
         <sch:assert test="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId = $evidence_request_header2/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId" role="FATAL" id="R-ebMS_ebMS-03"
            >The 'eb:To/eb:PartyId' in the first and second ebMS Evidence Request header must be the same.
            <sch:value-of select="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId"/> vs <sch:value-of select="$evidence_request_header2/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId"/>
         </sch:assert>
         <sch:assert test="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId/@type = $evidence_request_header2/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId/@type" role="FATAL" id="R-ebMS_ebMS-04"
            >The 'eb:To/eb:PartyId/@type' in the first and second ebMS Evidence Request header must be the same.
            <sch:value-of select="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId/@type"/> vs <sch:value-of select="$evidence_request_header2/eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId/@type"/>
         </sch:assert>
         <sch:assert test="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient'] = $evidence_request_header2/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']" role="FATAL" id="R-ebMS_ebMS-05"
            >The eb:Property[name='originalSender'] in the first and second ebMS Evidence Request header must be the same.
            <sch:value-of select="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']"/> vs <sch:value-of select="$evidence_request_header2/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']"/>
         </sch:assert>
         <sch:assert test="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type = $evidence_request_header2/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type" role="FATAL" id="R-ebMS_ebMS-06"
            >The eb:Property[name='originalSender']/@type in the first and second ebMS Evidence Request header must be the same.
            <sch:value-of select="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type"/> vs <sch:value-of select="$evidence_request_header2/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type"/>
         </sch:assert>
         <sch:assert test="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender'] = $evidence_request_header2/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']" role="FATAL" id="R-ebMS_ebMS-07"
            >The eb:Property[name='finalRecipient'] in the first and second ebMS Evidence Request header must be the same.
            <sch:value-of select="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']"/> vs <sch:value-of select="$evidence_request_header2/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']"/>
         </sch:assert>
         <sch:assert test="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']/@type = $evidence_request_header2/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']/@type" role="FATAL" id="R-ebMS_ebMS-08"
            >The eb:Property[name='finalRecipient']/@type in the first and second ebMS Evidence Request header must be the same.
            <sch:value-of select="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']/@type"/> vs <sch:value-of select="$evidence_request_header2/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']/@type"/>
         </sch:assert>
         <sch:assert test="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId = $evidence_request_header2/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId" role="FATAL" id="R-ebMS_ebMS-09"
            >Conversation identifier in the in the first and second ebMS Evidence Request header must be the same.
            <sch:value-of select="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId"/> vs <sch:value-of select="$evidence_request_header2/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId"/>
         </sch:assert>
         <sch:assert test="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Service = $evidence_request_header2/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Service" role="FATAL" id="R-ebMS_ebMS-10"
            >eb:Service in the in the first and second ebMS Evidence Request header must be the same.
            <sch:value-of select="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Service"/> vs <sch:value-of select="$evidence_request_header2/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Service"/>
         </sch:assert>
         <sch:assert test="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Action = $evidence_request_header2/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Action" role="FATAL" id="R-ebMS_ebMS-11"
            >eb:Action in the in the first and second ebMS Evidence Request header must be the same.
            <sch:value-of select="$evidence_request_header1/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Action"/> vs <sch:value-of select="$evidence_request_header2/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Action"/>
         </sch:assert>
      </sch:rule>
   </sch:pattern>

</sch:schema>
