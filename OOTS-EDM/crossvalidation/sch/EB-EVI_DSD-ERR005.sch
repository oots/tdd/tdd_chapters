<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <sch:ns prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EB-EVI_DSD-ERR005</sr:identifier>
           <sr:title xml:lang="en">EB-EVI_DSD-ERR005</sr:title>
           <sr:description xml:lang="en">Cross-message validation of the EB get Evidence Types Query Response and DSD Error Response with error code DSD:ERR:005 to prove correspondance of Evidence Type</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CROSS_MESSAGE</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EB-EVI</sr:identifier>
                 <sr:title xml:lang="en">EB-EVI</sr:title>
                 <sr:description xml:lang="en">Query Response of the EB for the 'Get Evidence Types for Requirement Query'</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:component>EB</sr:component>
              </sr:Transaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-ERR</sr:identifier>
                 <sr:title xml:lang="en">DSD-ERR</sr:title>
                 <sr:description xml:lang="en">Query Error Response of the DSD</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:component>DSD</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.0.2</sr:version>
           <sr:versionNotes xml:lang="en">Added dcat namespace to cross-validation schematrons</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:let name="eb-evi" value="Documents/Document[@name = 'eb-evi']"/>
   <sch:let name="dsd-err-005" value="Documents/Document[@name = 'dsd-err-005']"/>
   
   <sch:pattern>
      <sch:rule context="$dsd-err-005/query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure']/rs:Exception[@code='DSD:ERR:0005']/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification">
         <sch:assert test=". = $eb-evi/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:EvidenceTypeClassification" role="FATAL" id="R-EB-EVI_DSD-ERR005-001"
            >The value of one EvidenceTypeClassification of the EB get Evidence Types Query Response (EB-EVI) must be the same as in DSD Error Response (DSD-ERR), if the DSD response status is Failure and the code "DSD:ERR:0005" is used in the rs:Exception.
            <sch:value-of select="$eb-evi/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:EvidenceTypeClassification"/> vs <sch:value-of select="."/>
         </sch:assert>
      </sch:rule>
   </sch:pattern>

</sch:schema>
