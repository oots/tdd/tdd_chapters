<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <sch:ns uri="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/" prefix="eb"/>
    <!--Start of SR metadata-->
    <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/ebMS_EDM-REQ</sr:identifier>
            <sr:title xml:lang="en">ebMS_EDM-REQ</sr:title>
            <sr:description xml:lang="en">Cross-message validation of the ebMS and EDM Request to prove the correspondance of the Evidence Requester Identifier, Evidence Requester Scheme ID, Evidence Provider Identifier, Evidence Provider Scheme ID and eb:Action</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>CROSS_MESSAGE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                    <sr:title xml:lang="en">EDM-REQ</sr:title>
                    <sr:description xml:lang="en">Evidence Request</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:subject>HEADER</sr:subject>
                    <sr:component>EDM</sr:component>
                </sr:Transaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                    <sr:title xml:lang="en">EDM-REQ</sr:title>
                    <sr:description xml:lang="en">Evidence Request</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>EDM</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.0.2</sr:version>
            <sr:versionNotes xml:lang="en">Added dcat namespace to cross-validation schematrons</sr:versionNotes>
            </sr:Asset>?>
    <!--End of SR metadata-->
    <sch:let name="evidence_request_header"
        value="Documents/Document[@name = 'evidence_request_header']"/>
    <sch:let name="edm-req"
        value="Documents/Document[@name = 'edm-req']"/>
    <sch:pattern>
        <sch:rule context="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']">
            <sch:let name="er_agent_identifier" value="$edm-req/query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification='ER']/sdg:Identifier"/>
            <sch:assert test="$er_agent_identifier = ." role="FATAL" id="R-ebMS_EDM-REQ-01"
                >The value of the 'eb:Property[name='originalSender']' in the ebMS header of the Evidence Request MUST be equal to the value of the Agent 'rim:Element' with 'sdg:Classification' value 'ER' in 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier' in the Evidence Request (<sch:value-of select="$er_agent_identifier"/> vs <sch:value-of select="."/>)</sch:assert>
        </sch:rule>
        
        <sch:rule context="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']/@type">
            <sch:let name="er_identifier_scheme" value="$edm-req/query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification='ER']/sdg:Identifier/@schemeID"/>
            <sch:assert test="$er_identifier_scheme = . " role="FATAL" id="R-ebMS_EDM-REQ-02"
                >The value of the 'eb:Property[name='originalSender']/@type' in the ebMS header of the Evidence Request MUST be equal to the value of the Agent 'rim:Element' with 'sdg:Classification' value 'ER' in 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier' in the Evidence Request (<sch:value-of select="$er_identifier_scheme"/>  vs <sch:value-of select="."/>)</sch:assert>
        </sch:rule>
        
        <sch:rule context="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']">
            <sch:let name="ep_agent_identifier" value="$edm-req/query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier"/>
            <sch:assert test="$ep_agent_identifier = ." role="FATAL" id="R-ebMS_EDM-REQ-03"
                >The value of the 'eb:Property[name='finalRecipient']' in the ebMS header of the Evidence Request MUST be equal to the value of the 'rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier' in the Evidence Request (<sch:value-of select="$ep_agent_identifier"/>  vs <sch:value-of select="."/>)</sch:assert>
        </sch:rule>
        
        <sch:rule context="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type">
            <sch:let name="ep_agent_identifier_scheme" value="$edm-req/query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID"/>
            <sch:assert test="$ep_agent_identifier_scheme = ." role="FATAL" id="R-ebMS_EDM-REQ-04"
                >The value of the 'eb:Property[name='finalRecipient']/@type' in the ebMS header of the Evidence Request MUST be equal to the value of the 'rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID' in the Evidence Request (<sch:value-of select="$ep_agent_identifier_scheme"/>  vs <sch:value-of select="."/>)</sch:assert>
        </sch:rule>
        
        <sch:rule context="$evidence_request_header/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Action">
            <sch:let name="query_type" value="$edm-req/query:QueryRequest"/>
            <sch:assert test=". = 'ExecuteQueryRequest' and $edm-req/query:QueryRequest" role="FATAL" id="R-ebMS_EDM-REQ-05"
                >If the value of 'eb:Action' of the ebMS Evidence Request Header is 'ExecuteQueryRequest', the Evidence Request must be a 'query:QueryRequest' (<sch:value-of select="$query_type"/>  vs <sch:value-of select="."/>)</sch:assert>
        </sch:rule> 
    </sch:pattern>   
    
</sch:schema>
