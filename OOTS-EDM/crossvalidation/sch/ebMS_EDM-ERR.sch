<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <sch:ns uri="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/" prefix="eb"/>
    <!--Start of SR metadata-->
    <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/ebMS_EDM-ERR</sr:identifier>
            <sr:title xml:lang="en">ebMS_EDM-ERR</sr:title>
            <sr:description xml:lang="en">Cross-message validation of the ebMS Error Response header and Evidence Error Response to prove correspondance of Evidence Provider and Evidence Requester</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>CROSS_MESSAGE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-ERR</sr:identifier>
                    <sr:title xml:lang="en">EDM-ERR</sr:title>
                    <sr:description xml:lang="en">Evidence Error Response</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:subject>HEADER</sr:subject>
                    <sr:component>EDM</sr:component>
                </sr:Transaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-ERR</sr:identifier>
                    <sr:title xml:lang="en">EDM-ERR</sr:title>
                    <sr:description xml:lang="en">Evidence Error Response</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>EDM</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.0.2</sr:version>
            <sr:versionNotes xml:lang="en">Added dcat namespace to cross-validation schematrons</sr:versionNotes>
            </sr:Asset>?>
    <!--End of SR metadata-->
    <sch:let name="evidence_error_header"
        value="Documents/Document[@name = 'evidence_error_header']"/>
    <sch:let name="edm-err"
        value="Documents/Document[@name = 'edm-err']"/>
    <sch:pattern>
        <sch:rule context="$evidence_error_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']">
            <sch:let name="err_agent_identifier" value="$edm-err/query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier"/>
            <sch:assert test="$err_agent_identifier = ." role="FATAL" id="R-ebMS_EDM-ERR-01"
                >The value of the 'eb:Property[name='originalSender']' in the ebMS header of the Evidence Error Response MUST be equal to the value of the 'rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier' in the Evidence Error Response
                <sch:value-of select="$err_agent_identifier"/> vs <sch:value-of select="."/>
            </sch:assert>
        </sch:rule>
        
        <sch:rule context="$evidence_error_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='originalSender']/@type">
            <sch:let name="err_identifier_scheme" value="$edm-err/query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID"/>
            <sch:assert test="$err_identifier_scheme = ." role="FATAL" id="R-ebMS_EDM-ERR-02"
                >The value of the 'eb:Property[name='originalSender']/@type' in the ebMS header of the Evidence Error Response MUST be equal to the value of the 'rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID' in the Evidence Error Response
                <sch:value-of select="$err_identifier_scheme"/> vs <sch:value-of select="."/>
            </sch:assert>
        </sch:rule>
          
        <sch:rule context="$evidence_error_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']">
            <sch:let name="err_agent_identifier" value="$edm-err/query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier"/>
            <sch:let name="present" value="count($edm-err/query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier)"/>
            <sch:assert test="not($present) or $err_agent_identifier = ." role="FATAL" id="R-ebMS_EDM-ERR-03"
                >If present, the value of the 'eb:Property[name='finalRecipient']' in the ebMS header of the Evidence Error Response MUST be equal to the value of the 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier' in the Evidence Error Response
                <sch:value-of select="$err_agent_identifier"/> vs <sch:value-of select="."/>
            </sch:assert>
        </sch:rule>
        
        <sch:rule context="$evidence_error_header/eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property[@name='finalRecipient']/@type">
            <sch:let name="err_agent_identifier_scheme" value="$edm-err/query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID"/>
            <sch:assert test="$err_agent_identifier_scheme = ." role="FATAL" id="R-ebMS_EDM-ERR-04"
                >If present, the value of the 'eb:Property[name='finalRecipient']/@type' in the ebMS header of the Evidence Error Response MUST be equal to the value of the 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID' in the Evidence Error Response
                <sch:value-of select="$err_agent_identifier_scheme"/> vs <sch:value-of select="."/>
            </sch:assert>
        </sch:rule>
        
        <sch:rule context="$evidence_error_header/eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Action">
            <sch:let name="response_status" value="$edm-err/query:QueryResponse"/>
            <sch:assert test=". = 'ExceptionResponse' and $response_status[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure']" role="FATAL" id="R-ebMS_EDM-ERR-05"
                >If the value of 'eb:Action' is 'ExceptionResponse' in the ebMS ebMS header of the Evidence Error Response, the 'status' attribute of a 'QueryResponse' MUST be encoded as "urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure"
                eb:Action = <sch:value-of select="."/> and @status = <sch:value-of select="$response_status/@status"/>
            </sch:assert>
        </sch:rule> 
        
    </sch:pattern>   
    
</sch:schema>
