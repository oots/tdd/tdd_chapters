<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <!--Start of SR metadata-->
    <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EDM-REQ_EDM-ERR</sr:identifier>
            <sr:title xml:lang="en">EDM-REQ_EDM-ERR</sr:title>
            <sr:description xml:lang="en">Cross-message validation of the EDM Request and EDM Error Response to prove the correspondance of the RequestID and  Evidence Requester. The same cross-validation schematron can be used for the following request-response pairs.
            -EDM-REQ1+EDM-ERR1
            -EDM-REQ2+EDM-ERR2</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>CROSS_MESSAGE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                    <sr:title xml:lang="en">EDM-REQ</sr:title>
                    <sr:description xml:lang="en">Evidence Request</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>EDM</sr:component>
                </sr:Transaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-ERR</sr:identifier>
                    <sr:title xml:lang="en">EDM-ERR</sr:title>
                    <sr:description xml:lang="en">Evidence Error Response</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>EDM</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.0.2</sr:version>
            <sr:versionNotes xml:lang="en">Added dcat namespace to cross-validation schematrons</sr:versionNotes>
            </sr:Asset>?>
    <!--End of SR metadata-->
    <sch:let name="edm-req" value="Documents/Document[@name = 'edm-req']"/>
    <sch:let name="edm-err" value="Documents/Document[@name = 'edm-err']"/>
    <sch:let name="edm-req-agent" value="$edm-req/query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent"/> 
    <sch:let name="edm-err-agent" value="$edm-err/query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent"/> 
    <sch:pattern>
        <sch:rule context="Documents">
            <sch:assert test= "(count($edm-req/query:QueryRequest/@id) + count($edm-err/query:QueryResponse/@requestId)=0) or $edm-req/query:QueryRequest/@id = $edm-err/query:QueryResponse/@requestId" role="FATAL" id="R-EDM-REQ_EDM-ERR-01"
                >If present, the value of 'query:QueryRequest/@id' in the Evidence Request MUST be equal to the value of 'query:QueryResponse/@requestId' in the Evidence Error Response. (<sch:value-of select="$edm-req/query:QueryRequest/@id"/> vs <sch:value-of select="$edm-err/query:QueryResponse/@requestId"/>)</sch:assert>  
        </sch:rule>   
        <sch:rule context="$edm-req-agent[sdg:Classification='ER']">
            <sch:assert test="(count(sdg:Identifier)+count($edm-err-agent/sdg:Identifier)=0) or sdg:Identifier=$edm-err-agent/sdg:Identifier" role="FATAL" id="R-EDM-REQ_EDM-ERR-02"
                >If present, the value of the Agent 'rim:Element' with 'sdg:Classification' value 'ER' in 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier' in the Evidence Request MUST be equal to the value of 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier' in the Evidence Error Response (<sch:value-of select="sdg:Identifier"/> vs <sch:value-of select="$edm-err-agent/sdg:Identifier"/>)</sch:assert>
            <sch:assert test="(count(sdg:Identifier/@schemeID)+count($edm-err-agent/sdg:Identifier/@schemeID)=0) or sdg:Identifier/@schemeID=$edm-err-agent/sdg:Identifier/@schemeID" role="FATAL" id="R-EDM-REQ_EDM-ERR-03"
                >If present, the value of the Agent 'rim:Element' with 'sdg:Classification' value 'ER' in 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier/@schemeID' in the Evidence Request MUST be equal to the value of 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID' in the Evidence Error Response (<sch:value-of select="sdg:Identifier/@schemeID"/> vs <sch:value-of select="$edm-err-agent/sdg:Identifier/@schemeID"/>)</sch:assert>
        </sch:rule>
        <sch:rule context="$edm-err">
            <sch:assert test="query:QueryResponse/@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure'" role="FATAL" id="R-EDM-REQ_EDM-ERR-04"
                >The status value of Evidence Error must be urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure (<sch:value-of select="$edm-err/query:QueryResponse/@status"/> vs urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure)</sch:assert>
        </sch:rule>
    </sch:pattern>
    
</sch:schema>
