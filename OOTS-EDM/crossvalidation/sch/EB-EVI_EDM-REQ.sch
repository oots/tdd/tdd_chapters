<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <sch:ns prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EB-EVI_EDM-REQ</sr:identifier>
           <sr:title xml:lang="en">EB-EVI_EDM-REQ</sr:title>
           <sr:description xml:lang="en">Cross-message validation EB get Evidence Types Query Response and Evidence Request to prove correspondance of Requirement and Procedure</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CROSS_MESSAGE</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EB-EVI</sr:identifier>
                 <sr:title xml:lang="en">EB-EVI</sr:title>
                 <sr:description xml:lang="en">Query Response of the EB for the 'Get Evidence Types for Requirement Query'</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:component>EB</sr:component>
              </sr:Transaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                 <sr:title xml:lang="en">EDM-REQ</sr:title>
                 <sr:description xml:lang="en">Evidence Request</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>PAYLOAD</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.0.3</sr:version>
           <sr:versionNotes xml:lang="en">Small fix EB-EVI_EDM-REQ cross validation on Requirements</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:let name="eb-evi" value="Documents/Document[@name = 'eb-evi']"/>
   <sch:let name="edm-req" value="Documents/Document[@name = 'edm-req']"/>
   
   <sch:pattern>
      <sch:rule context="Documents">
         <sch:assert test="$eb-evi/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Identifier = $edm-req/query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Identifier" role="FATAL" id="R-EB-EVI_EDM-REQ-01"
            >The value of the 'sdg:Requirement/sdg:Identifier' of the EB get Evidence Types Query Response (EB-EVI)  MUST be present in on 'rim:Element' of  'rim:SlotValue/rim:Element/sdg:Requirements/sdg:Identifier' in the Evidence Request.
            <sch:value-of select="$eb-evi/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Identifier"/>vs <sch:value-of select="$edm-req/query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Identifier"/>
         </sch:assert>
         <sch:assert test="$eb-evi/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:RelatedTo/sdg:Identifier = $edm-req/query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@value" role="FATAL" id="R-EB-EVI_EDM-REQ-02"
            >One of the values of the 'sdg:RelatedTo/sdg:Identifier' of the EB get Evidence Types Query Response (EB-EVI) MUST be equal to 'rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@value' in the Evidence Request.
            <sch:value-of select="$eb-evi/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:RelatedTo/sdg:Identifier"/> vs <sch:value-of select="$edm-req/query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@value"/>
         </sch:assert>
      </sch:rule>
   </sch:pattern>

</sch:schema>
