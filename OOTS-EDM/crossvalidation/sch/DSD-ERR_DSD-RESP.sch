<?xml version="1.0" encoding="UTF-8"?>
<sch:schema 
   xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/DSD-ERR_DSD-RESP</sr:identifier>
           <sr:title xml:lang="en">DSD-ERR_DSD-RESP</sr:title>
           <sr:description xml:lang="en">Cross-message validation of the DSD Response that was created after an DSD Error Response with code DSD-ERR-0005 to prove corresponance of Data Service Evidence Type, Evidence Provider Classification and Jurisdiction Determination</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CROSS_MESSAGE</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-ERR</sr:identifier>
                 <sr:title xml:lang="en">DSD-ERR</sr:title>
                 <sr:description xml:lang="en">Query Error Response of the DSD</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:component>DSD</sr:component>
              </sr:Transaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-RESP</sr:identifier>
                 <sr:title xml:lang="en">DSD-RESP</sr:title>
                 <sr:description xml:lang="en">Query Response of the DSD</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:component>DSD</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.0.2</sr:version>
           <sr:versionNotes xml:lang="en">Fix DSD-ERR_DSD-RESP-02 schematron rule</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:let name="dsd-err" value="Documents/Document[@name = 'dsd-err']"/>
   <sch:let name="dsd-resp" value="Documents/Document[@name = 'dsd-resp']"/>
   
   <sch:pattern>
      <sch:rule context="Documents">
         
         <sch:assert test="$dsd-err/query:QueryResponse/rs:Exception/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier = $dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier" role="FATAL" id="R-DSD-ERR_DSD-RESP-01"
         >If the DSD response status is Failure and the code "DSD:ERR:0005" is used in the rs:Exception, the DSD Query Response (DSD-RESP) MUST contain the same value for the sdg:Identifier of sdg:DataServiceEvidenceType , that was listed in the DSD Error Response (DSD-ERR)
            <sch:value-of select="$dsd-err/query:QueryResponse/rs:Exception/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier"/> vs <sch:value-of select="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier"/>
         </sch:assert>
         <sch:assert test="(count($dsd-err/query:QueryResponse/rs:Exception/rim:Slot[@name='JurisdictionDetermination']/rim:SlotValue/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId) + count($dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId)=0) or $dsd-err/query:QueryResponse/rs:Exception/rim:Slot[@name='JurisdictionDetermination']/rim:SlotValue/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId = $dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId" role="FATAL" id="R-DSD-ERR_DSD-RESP-02"
         >If the DSD response status is Failure and the code "DSD:ERR:0005" is used in the rs:Exception, the DSD Query Response (DSD-RESP) MUST contain the same value for sdg:JurisdictionContextId of sdg:EvidenceProviderJurisdictionDetermination, that was listed in the DSD Error Response (DSD-ERR)
            <sch:value-of select="$dsd-err/query:QueryResponse/rs:Exception/rim:Slot[@name='JurisdictionDetermination']/rim:SlotValue/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId"/> vs <sch:value-of select="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId"/>
         </sch:assert>
         <sch:assert test="$dsd-err/query:QueryResponse/rs:Exception/rim:Slot[@name='UserRequestedClassificationConcepts']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Identifier = $dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier" role="FATAL" id="R-DSD-ERR_DSD-RESP-03"
         >If the DSD response status is Failure and the code "DSD:ERR:0005" is used in the rs:Exception, the DSD Query Response (DSD-RESP) MUST contain the same value for sdg:Identifier of sdg:EvidenceProviderClassification, that was listed in the DSD Error Response (DSD-ERR)
            <sch:value-of select="$dsd-err/query:QueryResponse/rs:Exception/rim:Slot[@name='UserRequestedClassificationConcepts']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Identifier"/> vs <sch:value-of select="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier"/>
         </sch:assert>
         </sch:rule>

   </sch:pattern>

</sch:schema>
