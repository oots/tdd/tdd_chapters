<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EDM-ERR_EDM-REQ2</sr:identifier>
           <sr:title xml:lang="en">EDM-ERR_EDM-REQ2</sr:title>
           <sr:description xml:lang="en">Cross-message validation of the EDM Error Response and second EDM Request to prove correspondance of the Preview Location and the Evidence Requester</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CROSS_MESSAGE</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-ERR</sr:identifier>
                 <sr:title xml:lang="en">EDM-ERR</sr:title>
                 <sr:description xml:lang="en">Evidence Error Response</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>PAYLOAD</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                 <sr:title xml:lang="en">EDM-REQ</sr:title>
                 <sr:description xml:lang="en">Evidence Request</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>PAYLOAD</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.0.2</sr:version>
           <sr:versionNotes xml:lang="en">Added dcat namespace to cross-validation schematrons</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:let name="edm-req" value="Documents/Document[@name='edm-req']"/>
   <sch:let name="edm-err" value="Documents/Document[@name='edm-err']"/>
   
   <sch:pattern>
      <sch:rule context="$edm-err/query:QueryResponse[@status = 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure']/rs:Exception[@severity='urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired']/rim:Slot[@name = 'PreviewLocation']/rim:SlotValue/rim:Value">
         <sch:let name="edm-req-value" value="$edm-req/query:QueryRequest/rim:Slot[@name = 'PreviewLocation']/rim:SlotValue/rim:Value"/>
         <sch:assert test=". = $edm-req-value" role="FATAL" id="R-EDM-ERR_EDM-REQ2-01"
            >In an Evidence Error Response with status 'Failure' which has the 'rs:Exception/@severity' with value be 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired', the 'rim:Slot[@name = 'PreviewLocation']rim:SlotValue/rim:Value' must equal with the 'rim:Slot[@name = 'PreviewLocation']rim:SlotValue/rim:Value' of the second Evidence Request. (<sch:value-of select="."/> vs <sch:value-of select="$edm-req-value"/>)</sch:assert>
      </sch:rule>
      
      <sch:rule context="$edm-err/query:QueryResponse[@status = 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure' and rs:Exception/@severity='urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired']/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier">
         <sch:let name="edm-req-id" value="$edm-req/query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier"/>
         <sch:assert test=". = $edm-req-id" role="FATAL" id="R-EDM-ERR_EDM-REQ2-02"
            >In an Evidence Error Response with status 'Failure' which has the 'rs:Exception/@severity' with value  be 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired', the 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier' must equal with the 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier' of the second Evidence Request. (<sch:value-of select="."/> vs <sch:value-of select="$edm-req-id"/>)</sch:assert>
         
         <sch:assert test="@schemeID = $edm-req-id/@schemeID" role="FATAL" id="R-EDM-ERR_EDM-REQ2-03"
            >In an Evidence Error Response with status 'Failure' which has the 'rs:Exception/@severity' with value  be 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired', the 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID' must equal with the 'rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID' of the second Evidence Request. (<sch:value-of select="@schemeID"/> vs <sch:value-of select="$edm-req-id/@schemeID"/>)</sch:assert>
      </sch:rule>
   </sch:pattern>
   
</sch:schema>
