<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
      <sch:ns uri="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/" prefix="eb"/>   
      <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
      <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
      <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
      <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
      <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
      <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
      <!--Start of SR metadata-->
      <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/DSD-RESP_EDM-REQ</sr:identifier>
           <sr:title xml:lang="en">DSD-RESP_EDM-REQ</sr:title>
           <sr:description xml:lang="en">Cross-message validation of the DSD Response and EDM Request to prove corresponance of the Data Service Evidence Type, Distribution, the Publisher, Access Service and, if present, the Evidence Provider Classification</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CROSS_MESSAGE</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-RESP</sr:identifier>
                 <sr:title xml:lang="en">DSD-RESP</sr:title>
                 <sr:description xml:lang="en">Query Response of the DSD</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:component>DSD</sr:component>
              </sr:Transaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                 <sr:title xml:lang="en">EDM-REQ</sr:title>
                 <sr:description xml:lang="en">Evidence Request</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>PAYLOAD</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.0.3</sr:version>
           <sr:versionNotes xml:lang="en">Update Version information in DSD-RESP_EDM-REQ.sch</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:let name="dsd-resp" value="Documents/Document[@name = 'dsd-resp']"/>
   <sch:let name="edm-req" value="Documents/Document[@name = 'edm-req']"/>
   
   <sch:pattern>
      <sch:rule context="Documents">
                                                      
         <sch:assert test="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier = $edm-req/query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier" role="FATAL" id="R-DSD-RESP_EDM-REQ-01"
            >The value of the 'sdg:Publisher/sdg:Identifier' of the DSD Response MUST be equal to the value of the 'rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier' in the Evidence Request
            <sch:value-of select="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier"/> vs <sch:value-of select="$edm-req/query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier"/>
         </sch:assert>
         <sch:assert test="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier/@schemeID = $edm-req/query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID" role="FATAL" id="R-DSD-RESP_EDM-REQ-02"
            >The value of the 'sdg:Publisher/sdg:Identifier/@SchemeID' of the DSD Response MUST be equal to the value of the 'rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID' in the Evidence Request
            <sch:value-of select="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier/@schemeID"/> vs <sch:value-of select="$edm-req/query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier/@schemeID"/>
         </sch:assert>  
         <sch:assert test="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier = $edm-req/query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier" role="FATAL" id="R-DSD-RESP_EDM-REQ-04"
            >The value of the 'sdg:DataServiceEvidenceType/sdg:Identifier' of the DSD Response MUST be equal to the value of the 'sdg:DataServiceEvidenceType/sdg:Identifier' in the Evidence Request
            <sch:value-of select="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier"/> vs <sch:value-of select="$edm-req/query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier"/>
         </sch:assert>
         <sch:assert test="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification = $edm-req/query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification" role="FATAL" id="R-DSD-RESP_EDM-REQ-05"
            >The value of the 'sdg:EvidenceTypeClassification' of the DSD Response MUST be equal to the value of the 'sdg:EvidenceTypeClassification' in the Evidence Request
            <sch:value-of select="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification"/> vs <sch:value-of select="$edm-req/query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification"/>
         </sch:assert>     
         <sch:assert test="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:ConformsTo=$edm-req/query:QueryRequest/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue/rim:Value"  role="FATAL" id="R-DSD-RESP_EDM-REQ-09"
            >The value of the 'rim:Slot[@name='SpecificationIdentifier' of the Evidence Request MUST be equal to on of the values found in 'sdg:AccessService/sdg:ConformsTo' in the DSD Response
            <sch:value-of select="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:ConformsTo"/> vs <sch:value-of select="$edm-req/query:QueryRequest/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue/rim:Value"/>     
         </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="Documents">
         <sch:let name="response_has_empty_objectlist" value="count($dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject) = 0" />
         <sch:let name="dsd-resp-ep-classification" value="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification"/>
         <sch:let name="edm-req-ep-classification" value="$edm-req/query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification"/>     
         <sch:assert test="$response_has_empty_objectlist or (count($dsd-resp-ep-classification/sdg:Identifier) +count($edm-req-ep-classification/sdg:Identifier)=0) or $dsd-resp-ep-classification/sdg:Identifier = $edm-req-ep-classification/sdg:Identifier" role="FATAL" id="R-DSD-RESP_EDM-REQ-03"
            >If present, the value of the 'sdg:EvidenceProviderClassification/sdg:Identifier' of the DSD Response MUST be equal to the value of the 'sdg:EvidenceProviderClassification/sdg:Identifier' in the Evidence Request
            <sch:value-of select="$dsd-resp-ep-classification/sdg:Identifier"/> vs <sch:value-of select="$edm-req-ep-classification/sdg:Identifier"/>
         </sch:assert>
         <sch:let name="dsd-resp-distributedas" value="$dsd-resp/query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs"/>
         <sch:let name="edm-req-distributedas" value="$edm-req/query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs" />
         <sch:assert test="$response_has_empty_objectlist or (count($edm-req-distributedas/sdg:Format) +count($dsd-resp-distributedas/sdg:Format)=0) or $dsd-resp-distributedas/sdg:Format = $edm-req-distributedas/sdg:Format" role="FATAL" id="R-DSD-RESP_EDM-REQ-06"
            >If present, the value of the 'sdg:DistributedAs/sdg:Format' of the DSD Response MUST be equal to the value of the 'sdg:DistributedAs/sdg:Format' in the Evidence Request
            <sch:value-of select="$dsd-resp-distributedas/sdg:Format"/> vs <sch:value-of select="$edm-req-distributedas/sdg:Format"/>
         </sch:assert>
         <sch:assert test="$response_has_empty_objectlist or (count($edm-req-distributedas/sdg:ConformsTo) +count($dsd-resp-distributedas/sdg:ConformsTo)=0) or $dsd-resp-distributedas/sdg:ConformsTo = $edm-req-distributedas/sdg:ConformsTo" role="FATAL" id="R-DSD-RESP_EDM-REQ-07"
            >If present, the value of the 'sdg:DistributedAs/sdg:ConformsTo' of the DSD Response MUST be equal to the value of the 'sdg:DistributedAs/sdg:ConformsTo' in the Evidence Request
            <sch:value-of select="$dsd-resp-distributedas/sdg:ConformsTo"/> vs <sch:value-of select="$edm-req-distributedas/sdg:ConformsTo"/>
         </sch:assert>
         <sch:assert test="$response_has_empty_objectlist or (count($edm-req-distributedas/sdg:Transformation) +count($dsd-resp-distributedas/sdg:Transformation)=0) or $dsd-resp-distributedas/sdg:Transformation = $edm-req-distributedas/sdg:Transformation" role="FATAL" id="R-DSD-RESP_EDM-REQ-08"
            >If present, the value of the 'sdg:DistributedAs/sdg:Transformation' of the DSD Response MUST be equal to the value of the 'sdg:DistributedAs/sdg:Transformation' in the Evidence Request
            <sch:value-of select="$dsd-resp-distributedas/sdg:Transformation"/> vs <sch:value-of select="$edm-req-distributedas/sdg:Transformation"/>         
         </sch:assert>
      </sch:rule>
   </sch:pattern>

</sch:schema>
