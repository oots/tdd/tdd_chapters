<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform xmlns:error="https://doi.org/10.5281/zenodo.1495494#error"
               xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"
               xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
               xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
               xmlns:sch="http://purl.oclc.org/dsdl/schematron"
               xmlns:schxslt="https://doi.org/10.5281/zenodo.1495494"
               xmlns:schxslt-api="https://doi.org/10.5281/zenodo.1495494#api"
               xmlns:sdg="http://data.europa.eu/p4s"
               xmlns:x="https://www.w3.org/TR/REC-html40"
               xmlns:xlink="http://www.w3.org/1999/xlink"
               xmlns:xs="http://www.w3.org/2001/XMLSchema"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               version="2.0">
   <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:dct="http://purl.org/dc/terms/"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
      <dct:creator>
         <dct:Agent>
            <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
            <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
         </dct:Agent>
      </dct:creator>
      <dct:created>2024-09-08T11:21:09.317682+02:00</dct:created>
   </rdf:Description>
   <xsl:output indent="yes"/>
   <xsl:param name="EEA_COUNTRY-CODELIST_code"
              select="tokenize('AT BE BG HR CY CZ DK EE FI FR DE EL HU IS IE IT LV LI LT LU MT NL NO PL PT RO SK SI ES SE', '\s')"/>
   <xsl:param name="AGENTCLASSIFICATION-CODELIST_code"
              select="tokenize('ER IP EP ERRP', '\s')"/>
   <xsl:param name="OOTSMEDIATYPES-CODELIST_code"
              select="tokenize('image/jpeg application/json image/png application/pdf application/xml image/svg+xml', '\s')"/>
   <xsl:param name="LANGUAGECODE-CODELIST_code"
              select="tokenize('AA AB AE AF AK AM AN AR AS AV AY AZ BA BE BG BH BI BM BN BO BO BR BS CA CE CH CO CR CS CS CU CV CY CY DA DE DE DV DZ EE EL EL EN EO ES ET EU EU FA FA FF FI FJ FO FR FR FY GA GD GL GN GU GV HA HE HI HO HR HT HU HY HY HZ IA ID IE IG II IK IO IS IS IT IU JA JV KA KA KG KI KJ KK KL KM KN KO KR KS KU KV KW KY LA LB LG LI LN LO LT LU LV MG MH MI MI MK MK ML MN MR MS MS MT MY MY NA NB ND NE NG NL NL NN NO NR NV NY OC OJ OM OR OS PA PI PL PS PT QU RM RN RO RO RU RW SA SC SD SE SG SI SK SK SL SM SN SO SQ SQ SR SS ST SU SV SW TA TE TG TH TI TK TL TN TO TR TS TT TW TY UG UK UR UZ VE VI VO WA WO XH YI YO ZA ZH ZH ZU', '\s')"/>
   <xsl:param name="EAS_Code"
              select="tokenize('0002 0007 0009 0037 0060 0088 0096 0097 0106 0130 0135 0142 0147 0151 0170 0183 0184 0188 0190 0191 0192 0193 0194 0195 0196 0198 0199 0200 0201 0202 0203 0204 0205 0208 0209 0210 0211 0212 0213 0215 0216 0217 0218 0221 0225 0230 9901 9910 9913 9914 9915 9918 9919 9920 9922 9923 9924 9925 9926 9927 9928 9929 9930 9931 9932 9933 9934 9935 9936 9937 9938 9939 9940 9941 9942 9943 9944 9945 9946 9947 9948 9949 9950 9951 9952 9953 9957 9959 AN AQ AS AU EM', '\s')"/>
   <xsl:param name="COUNTRYIDENTIFICATIONCODE-CODELIST_code"
              select="tokenize('AF AL DZ AS AD AO AI AQ AG AR AM AW AU AT AZ BS BH BD BB BY BE BZ BJ BM BT BO BQ BA BW BV BR IO BN BG BF BI CV KH CM CA KY CF TD CL CN CX CC CO KM CD CG CK CR HR CU CW CY CZ CI DK DJ DM DO EC EG SV GQ ER EE SZ ET FK FO FJ FI FR GF PF TF GA GM GE DE GH GI EL GL GD GP GU GT GG GN GW GY HT HM VA HN HK HU IS IN ID IR IQ IE IM IL IT JM JP JE JO KZ KE KI KP KR KW KG LA LV LB LS LR LY LI LT LU MO MG MW MY MV ML MT MH MQ MR MU YT MX FM MD MC MN ME MS MA MZ MM NA NR NP NL NC NZ NI NE NG NU NF MK MP NO OM PK PW PS PA PG PY PE PH PN PL PT PR QA RO RU RW RE BL SH KN LC MF PM VC WS SM ST SA SN RS SC SL SG SX SK SI SB SO ZA GS SS ES LK SD SR SJ SE CH SY TW TJ TZ TH TL TG TK TO TT TN TR TM TC TV UG UA AE GB UM US UY UZ VU VE VN VG VI WF EH YE ZM ZW AX', '\s')"/>
   <xsl:template match="root()">
      <xsl:variable name="metadata" as="element()?">
         <svrl:metadata xmlns:dct="http://purl.org/dc/terms/"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                        xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
            <dct:creator>
               <dct:Agent>
                  <skos:prefLabel>
                     <xsl:value-of separator="/"
                                   select="(system-property('xsl:product-name'), system-property('xsl:product-version'))"/>
                  </skos:prefLabel>
               </dct:Agent>
            </dct:creator>
            <dct:created>
               <xsl:value-of select="current-dateTime()"/>
            </dct:created>
            <dct:source>
               <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/">
                  <dct:creator>
                     <dct:Agent>
                        <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
                        <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
                     </dct:Agent>
                  </dct:creator>
                  <dct:created>2024-09-08T11:21:09.317682+02:00</dct:created>
               </rdf:Description>
            </dct:source>
         </svrl:metadata>
      </xsl:variable>
      <xsl:variable name="report" as="element(schxslt:report)">
         <schxslt:report>
            <xsl:call-template name="d19e36"/>
         </schxslt:report>
      </xsl:variable>
      <xsl:variable name="schxslt:report" as="node()*">
         <xsl:sequence select="$metadata"/>
         <xsl:for-each select="$report/schxslt:document">
            <xsl:for-each select="schxslt:pattern">
               <xsl:sequence select="node()"/>
               <xsl:sequence select="../schxslt:rule[@pattern = current()/@id]/node()"/>
            </xsl:for-each>
         </xsl:for-each>
      </xsl:variable>
      <svrl:schematron-output xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <svrl:ns-prefix-in-attribute-values prefix="sdg" uri="http://data.europa.eu/p4s"/>
         <svrl:ns-prefix-in-attribute-values prefix="rs" uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="rim" uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="query" uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
         <svrl:ns-prefix-in-attribute-values prefix="xlink" uri="http://www.w3.org/1999/xlink"/>
         <svrl:ns-prefix-in-attribute-values prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
         <xsl:sequence select="$schxslt:report"/>
      </svrl:schematron-output>
   </xsl:template>
   <xsl:template match="text() | @*" mode="#all" priority="-10"/>
   <xsl:template match="/" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template match="*" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="@*"/>
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template name="d19e36">
      <schxslt:document>
         <schxslt:pattern id="d19e36">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e46">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e55">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e64">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e73">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e82">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e91">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e101">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e110">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e119">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e130">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e139">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e148">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e158">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e167">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e178">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e187">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e196">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e205">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e215">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e226">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e235">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e244">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e253">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e262">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e272">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e281">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e290">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e308">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e317">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e326">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d19e336">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <xsl:apply-templates mode="d19e36" select="root()"/>
      </schxslt:document>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue"
                 priority="39"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e36']">
            <schxslt:rule pattern="d19e36">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e36">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(rim:Value='oots-edm:v1.1')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C002">
                     <xsl:attribute name="test">rim:Value='oots-edm:v1.1'</xsl:attribute>
                     <svrl:text>The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-edm:v1.1".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e36')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceResponseIdentifier']/rim:SlotValue/rim:Value"
                 priority="38"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e46']">
            <schxslt:rule pattern="d19e46">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceResponseIdentifier']/rim:SlotValue/rim:Value" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceResponseIdentifier']/rim:SlotValue/rim:Value</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e46">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceResponseIdentifier']/rim:SlotValue/rim:Value</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C003">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The 'rim:Value' of the 'EvidenceResponseIdentifier' MUST be unique UUID (RFC 4122) for each response.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e46')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='IssueDateTime']/rim:SlotValue/rim:Value"
                 priority="37"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e55']">
            <schxslt:rule pattern="d19e55">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='IssueDateTime']/rim:SlotValue/rim:Value" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='IssueDateTime']/rim:SlotValue/rim:Value</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e55">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='IssueDateTime']/rim:SlotValue/rim:Value</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C004">
                     <xsl:attribute name="test">matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}','i')</xsl:attribute>
                     <svrl:text>
            The 'rim:Value' of 'IssueDateTime' MUST be according to xsd:dateTime.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e55')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='ResponseAvailableDateTime']/rim:SlotValue/rim:Value"
                 priority="36"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e64']">
            <schxslt:rule pattern="d19e64">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='ResponseAvailableDateTime']/rim:SlotValue/rim:Value" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ResponseAvailableDateTime']/rim:SlotValue/rim:Value</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e64">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ResponseAvailableDateTime']/rim:SlotValue/rim:Value</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C005">
                     <xsl:attribute name="test">matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}','i')</xsl:attribute>
                     <svrl:text>
            The 'rim:Value' of 'ResponseAvailableDateTime' MUST be according to xsd:dateTime. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e64')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier"
                 priority="35"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e73']">
            <schxslt:rule pattern="d19e73">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e73">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C006">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e73')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent"
                 priority="34"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e82']">
            <schxslt:rule pattern="d19e82">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e82">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:Classification)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C010">
                     <xsl:attribute name="test">not(normalize-space(sdg:Classification)='')</xsl:attribute>
                     <svrl:text>The value for 'Agent/Classification' MUST be provided.  </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e82')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier"
                 priority="33"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e91']">
            <schxslt:rule pattern="d19e91">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e91">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C012">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e91')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Identifier"
                 priority="32"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e101']">
            <schxslt:rule pattern="d19e101">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e101">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C015">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The value of 'Identifier' of an 'Evidence' MUST be unique UUID (RFC 4122).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e101')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingDate"
                 priority="31"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e110']">
            <schxslt:rule pattern="d19e110">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingDate" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingDate</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e110">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingDate</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C016">
                     <xsl:attribute name="test">matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}','i')</xsl:attribute>
                     <svrl:text>
            The value of 'IssuingDate' of an 'Evidence' MUST be according to xsd:date.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e110')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:EvidenceTypeClassification"
                 priority="30"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e119']">
            <schxslt:rule pattern="d19e119">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:EvidenceTypeClassification" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:EvidenceTypeClassification</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e119">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:EvidenceTypeClassification</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(., '^https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(oots|(' || string-join($EEA_COUNTRY-CODELIST_code, '|') || '))/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C017">
                     <xsl:attribute name="test">matches(., '^https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(oots|(' || string-join($EEA_COUNTRY-CODELIST_code, '|') || '))/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$')</xsl:attribute>
                     <svrl:text>The value of 'EvidenceTypeClassification' of 'IsConformantTo' MUST be a UUID desribed in the Evidence Broker and include a code of the code list 
            'EEA_Country-CodeList' (ISO 3166-1' alpha-2 codes EEA subset of country codes) using the prefix and scheme 
            'https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/[EEA_CountryCode]/[UUID]' pointing to the Semantic Repository. For testing purposes and agreed OOTS data models the code "oots" can be used.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e119')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Title"
                 priority="29"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e130']">
            <schxslt:rule pattern="d19e130">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Title" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Title</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e130">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Title</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@lang)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C019">
                     <xsl:attribute name="test">@lang</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e130')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Description"
                 priority="28"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e139']">
            <schxslt:rule pattern="d19e139">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Description" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Description</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e139">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Description</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@lang)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C021">
                     <xsl:attribute name="test">@lang</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e139')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:ConformsTo"
                 priority="27"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e148']">
            <schxslt:rule pattern="d19e148">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:ConformsTo" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:ConformsTo</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e148">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:ConformsTo</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C022">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')</xsl:attribute>
                     <svrl:text>The value of 'ConformsTo' of the requested distribution MUST be a persistent URL with a link to a "DataModelScheme" of the Evidence Type described in the Semantic Repository which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]". </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e148')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson"
                 priority="26"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e158']">
            <schxslt:rule pattern="d19e158">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e158">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:Identifier)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="WARNING"
                                      id="R-EDM-RESP-C027">
                     <xsl:attribute name="test">not(normalize-space(sdg:Identifier)='')</xsl:attribute>
                     <svrl:text>The value of a Person 'Identifier' SHOULD be provided. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e158')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier"
                 priority="25"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e167']">
            <schxslt:rule pattern="d19e167">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e167">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C028">
                     <xsl:attribute name="test">matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i')</xsl:attribute>
                     <svrl:text>The value of a Person 'Identifier' MUST have the format XX/YY/ZZZZZZZZZ where XX is the Nationality Code of the identifier and YY is the Nationality Code of 
            the destination country (EEA_Country-CodeList subset of country codes) and ZZZZZZZZZ is an undefined combination of characters which uniquely identifies the identity asserted in the country of origin.
            Example: ES/AT/02635542Y</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e167')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier"
                 priority="24"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e178']">
            <schxslt:rule pattern="d19e178">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e178">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C030">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e178')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier"
                 priority="23"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e187']">
            <schxslt:rule pattern="d19e187">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e187">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID='eidas')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C031">
                     <xsl:attribute name="test">@schemeID='eidas'</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of the 'Identifier' MUST have the fixed value 'eidas'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e187')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:DateOfBirth"
                 priority="22"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e196']">
            <schxslt:rule pattern="d19e196">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:DateOfBirth" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:DateOfBirth</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e196">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:DateOfBirth</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^[0-9]{4}-[0-9]{2}-[0-9]{2}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C032">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^[0-9]{4}-[0-9]{2}-[0-9]{2}$','i')</xsl:attribute>
                     <svrl:text>
            The value of 'DateOfBirth' MUST use the following format YYYY + “-“ + MM + “-“ + DD (as defined for xsd:date) </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e196')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson"
                 priority="21"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e205']">
            <schxslt:rule pattern="d19e205">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e205">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:LegalPersonIdentifier)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C033">
                     <xsl:attribute name="test">not(normalize-space(sdg:LegalPersonIdentifier)='')</xsl:attribute>
                     <svrl:text>The value of a Legal Person 'LegalPersonIdentifier' MUST be provided.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e205')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier"
                 priority="20"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e215']">
            <schxslt:rule pattern="d19e215">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e215">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C035">
                     <xsl:attribute name="test">matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i')</xsl:attribute>
                     <svrl:text>The value of a 'LegalPersonIdentifier' MUST have the format XX/YY/ZZZZZZZ where the values of XX and YY MUST be part of the code list 'EEA_CountryCodeList' (ISO 3166-1 alpha-2 codes).  Example: ES/AT/02635542Y </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e215')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier"
                 priority="19"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e226']">
            <schxslt:rule pattern="d19e226">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e226">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C036">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'LegalPersonIdentifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e226')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier"
                 priority="18"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e235']">
            <schxslt:rule pattern="d19e235">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e235">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID='eidas')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C037">
                     <xsl:attribute name="test">@schemeID='eidas'</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of the 'LegalPersonIdentifier' MUST have the fixed value 'eidas'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e235')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingAuthority/sdg:Identifier"
                 priority="17"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e244']">
            <schxslt:rule pattern="d19e244">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingAuthority/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingAuthority/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e244">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingAuthority/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C038">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e244')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:ValidityPeriod/sdg:StartDate"
                 priority="16"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e253']">
            <schxslt:rule pattern="d19e253">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:ValidityPeriod/sdg:StartDate" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:ValidityPeriod/sdg:StartDate</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e253">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:ValidityPeriod/sdg:StartDate</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C040">
                     <xsl:attribute name="test">matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}','i')</xsl:attribute>
                     <svrl:text>
            The value of 'StartDate' of an 'ValidityPeriod' MUST be according to xsd:date.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e253')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:ValidityPeriod/sdg:EndDate"
                 priority="15"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e262']">
            <schxslt:rule pattern="d19e262">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:ValidityPeriod/sdg:EndDate" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:ValidityPeriod/sdg:EndDate</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e262">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:ValidityPeriod/sdg:EndDate</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C041">
                     <xsl:attribute name="test">matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}','i')</xsl:attribute>
                     <svrl:text>
            The value of 'EndDate' of an 'ValidityPeriod' MUST be according to xsd:date.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e262')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:RepositoryItemRef"
                 priority="14"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e272']">
            <schxslt:rule pattern="d19e272">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:RepositoryItemRef" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:RepositoryItemRef</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e272">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:RepositoryItemRef</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(@xlink:href, '^cid:[A-Za-z0-9-.]*@[A-Za-z0-9.]*$'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C042">
                     <xsl:attribute name="test">matches(@xlink:href, '^cid:[A-Za-z0-9-.]*@[A-Za-z0-9.]*$')</xsl:attribute>
                     <svrl:text> The value of the attribute "xlink:href" of "rim:RepositoryItemRef" MUST follow the URI scheme cid and start with the prefix 'cid:…@…'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e272')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Transformation"
                 priority="13"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e281']">
            <schxslt:rule pattern="d19e281">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Transformation" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Transformation</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e281">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Transformation</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C043">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')</xsl:attribute>
                     <svrl:text>The value of 'Transformation' of the requested distribution MUST be a persistent URL with link to a "DataModelScheme" and "Subset" of the EvidenceType described in the Semantic Repository which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]/[Subset]".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e281')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution"
                 priority="12"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="ct1" select="count(sdg:ConformsTo)"/>
      <xsl:variable name="isxml" select="matches(sdg:Format,'application/xml')"/>
      <xsl:variable name="isjson" select="matches(sdg:Format, 'application/json')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e290']">
            <schxslt:rule pattern="d19e290">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e290">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not($isxml and $ct1=0))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="WARNING"
                                      id="R-EDM-RESP-C044">
                     <xsl:attribute name="test">not($isxml and $ct1=0)</xsl:attribute>
                     <svrl:text>The value of 'sdg:ConformsTo' of the distribution SHOULD be present if the 'sdg:DistributedAs/sdg:Format' uses the codes 'application/xml' of the codelist 'OOTSMediaTypes'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(not($isjson and $ct1=0))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="WARNING"
                                      id="R-EDM-RESP-C045">
                     <xsl:attribute name="test">not($isjson and $ct1=0)</xsl:attribute>
                     <svrl:text>The value of 'sdg:ConformsTo' of the distribution SHOULD be present if the 'sdg:DistributedAs/sdg:Format' uses the codes 'application/json' of the codelist 'OOTSMediaTypes'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e290')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue"
                 priority="11"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e308']">
            <schxslt:rule pattern="d19e308">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e308">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(rim:Element/sdg:Agent[sdg:Classification='EP'])=1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C046">
                     <xsl:attribute name="test">count(rim:Element/sdg:Agent[sdg:Classification='EP'])=1</xsl:attribute>
                     <svrl:text>The EvidenceProvider slot MUST include one Agent with the classification value EP.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e308')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification/text() = 'EP']/sdg:Address"
                 priority="10"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e317']">
            <schxslt:rule pattern="d19e317">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification/text() = 'EP']/sdg:Address" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification/text() = 'EP']/sdg:Address</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e317">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification/text() = 'EP']/sdg:Address</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(sdg:AdminUnitLevel1)=1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C047">
                     <xsl:attribute name="test">count(sdg:AdminUnitLevel1)=1</xsl:attribute>
                     <svrl:text>A value for 'AdminUnitLevel1' MUST be provided if the sdg:Agent/sdg:Classification value is "EP".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e317')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Transformation"
                 priority="9"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e326']">
            <schxslt:rule pattern="d19e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Transformation" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Transformation</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Transformation</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(../sdg:ConformsTo)=1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C048">
                     <xsl:attribute name="test">count(../sdg:ConformsTo)=1</xsl:attribute>
                     <svrl:text>The value of  'sdg:ConformsTo'  of the distribution MUST be present if the Element 'sdg:Transformation' of the distribution is present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier"
                 priority="8"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:variable name="suffix1"
                    select="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e336']">
            <schxslt:rule pattern="d19e336">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e336">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C007">
                     <xsl:attribute name="test">((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256</xsl:attribute>
                     <svrl:text>The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_CountryCodeList’. For testing purposes the code "oots" can be used. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e336')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier"
                 priority="7"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:variable name="suffix1"
                    select="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e336']">
            <schxslt:rule pattern="d19e336">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e336">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C013">
                     <xsl:attribute name="test">((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256</xsl:attribute>
                     <svrl:text>The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_CountryCodeList’. For testing purposes the code "oots" can be used.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e336')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingAuthority/sdg:Identifier"
                 priority="6"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:variable name="suffix1"
                    select="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e336']">
            <schxslt:rule pattern="d19e336">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingAuthority/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingAuthority/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e336">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingAuthority/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C039">
                     <xsl:attribute name="test">((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256</xsl:attribute>
                     <svrl:text>The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_CountryCodeList’. For testing purposes the code "oots" can be used.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e336')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1"
                 priority="5"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e336']">
            <schxslt:rule pattern="d19e336">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e336">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C008">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e336')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent"
                 priority="4"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e336']">
            <schxslt:rule pattern="d19e336">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e336">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not((some $code in $AGENTCLASSIFICATION-CODELIST_code satisfies sdg:Classification=$code) and (matches(sdg:Classification,'EP') or matches(sdg:Classification,'IP')))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C011">
                     <xsl:attribute name="test">(some $code in $AGENTCLASSIFICATION-CODELIST_code satisfies sdg:Classification=$code) and (matches(sdg:Classification,'EP') or matches(sdg:Classification,'IP'))</xsl:attribute>
                     <svrl:text>The value (<xsl:value-of select="sdg:Classification"/>) MUST be part of the code list 'AgentClassification' and must be one of the codes EP (Evidence Provider) or IP (Intermediary Platform). The codes 'ER' and 'ERRP' must not be used by this transaction. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e336')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Title/@lang"
                 priority="3"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e336']">
            <schxslt:rule pattern="d19e336">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Title/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Title/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e336">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Title/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C018">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Title/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e336')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Description/@lang"
                 priority="2"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e336']">
            <schxslt:rule pattern="d19e336">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Description/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Description/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e336">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Description/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C020">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context ' query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Description/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e336')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Language"
                 priority="1"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e336']">
            <schxslt:rule pattern="d19e336">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Language" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Language</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e336">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Language</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C026">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Language'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e336')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Format"
                 priority="0"
                 mode="d19e36">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd19e336']">
            <schxslt:rule pattern="d19e336">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Format" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Format</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d19e336">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Format</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-RESP-C023">
                     <xsl:attribute name="test">some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'OOTSMediaTypes-CodeList' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Format'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd19e336')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:function name="schxslt:location" as="xs:string">
      <xsl:param name="node" as="node()"/>
      <xsl:variable name="segments" as="xs:string*">
         <xsl:for-each select="($node/ancestor-or-self::node())">
            <xsl:variable name="position">
               <xsl:number level="single"/>
            </xsl:variable>
            <xsl:choose>
               <xsl:when test=". instance of element()">
                  <xsl:value-of select="concat('Q{', namespace-uri(.), '}', local-name(.), '[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of attribute()">
                  <xsl:value-of select="concat('@Q{', namespace-uri(.), '}', local-name(.))"/>
               </xsl:when>
               <xsl:when test=". instance of processing-instruction()">
                  <xsl:value-of select="concat('processing-instruction(&#34;', name(.), '&#34;)[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of comment()">
                  <xsl:value-of select="concat('comment()[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of text()">
                  <xsl:value-of select="concat('text()[', $position, ']')"/>
               </xsl:when>
               <xsl:otherwise/>
            </xsl:choose>
         </xsl:for-each>
      </xsl:variable>
      <xsl:value-of select="concat('/', string-join($segments, '/'))"/>
   </xsl:function>
</xsl:transform>
