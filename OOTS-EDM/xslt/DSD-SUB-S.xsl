<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform xmlns:error="https://doi.org/10.5281/zenodo.1495494#error"
               xmlns:lcm="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0"
               xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
               xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
               xmlns:sch="http://purl.oclc.org/dsdl/schematron"
               xmlns:schxslt="https://doi.org/10.5281/zenodo.1495494"
               xmlns:schxslt-api="https://doi.org/10.5281/zenodo.1495494#api"
               xmlns:sdg="http://data.europa.eu/p4s"
               xmlns:xlink="http://www.w3.org/1999/xlink"
               xmlns:xs="http://www.w3.org/2001/XMLSchema"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               version="2.0">
   <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:dct="http://purl.org/dc/terms/"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
      <dct:creator>
         <dct:Agent>
            <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
            <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
         </dct:Agent>
      </dct:creator>
      <dct:created>2024-07-01T11:21:00.066668+02:00</dct:created>
   </rdf:Description>
   <xsl:output indent="yes"/>
   <xsl:template match="root()">
      <xsl:variable name="metadata" as="element()?">
         <svrl:metadata xmlns:dct="http://purl.org/dc/terms/"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                        xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
            <dct:creator>
               <dct:Agent>
                  <skos:prefLabel>
                     <xsl:value-of separator="/"
                                   select="(system-property('xsl:product-name'), system-property('xsl:product-version'))"/>
                  </skos:prefLabel>
               </dct:Agent>
            </dct:creator>
            <dct:created>
               <xsl:value-of select="current-dateTime()"/>
            </dct:created>
            <dct:source>
               <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/">
                  <dct:creator>
                     <dct:Agent>
                        <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
                        <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
                     </dct:Agent>
                  </dct:creator>
                  <dct:created>2024-07-01T11:21:00.066668+02:00</dct:created>
               </rdf:Description>
            </dct:source>
         </svrl:metadata>
      </xsl:variable>
      <xsl:variable name="report" as="element(schxslt:report)">
         <schxslt:report>
            <xsl:call-template name="d13e21"/>
         </schxslt:report>
      </xsl:variable>
      <xsl:variable name="schxslt:report" as="node()*">
         <xsl:sequence select="$metadata"/>
         <xsl:for-each select="$report/schxslt:document">
            <xsl:for-each select="schxslt:pattern">
               <xsl:sequence select="node()"/>
               <xsl:sequence select="../schxslt:rule[@pattern = current()/@id]/node()"/>
            </xsl:for-each>
         </xsl:for-each>
      </xsl:variable>
      <svrl:schematron-output xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <svrl:ns-prefix-in-attribute-values prefix="sdg" uri="http://data.europa.eu/p4s"/>
         <svrl:ns-prefix-in-attribute-values prefix="rs" uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="rim" uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="lcm" uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
         <svrl:ns-prefix-in-attribute-values prefix="xlink" uri="http://www.w3.org/1999/xlink"/>
         <xsl:sequence select="$schxslt:report"/>
      </svrl:schematron-output>
   </xsl:template>
   <xsl:template match="text() | @*" mode="#all" priority="-10"/>
   <xsl:template match="/" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template match="*" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="@*"/>
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template name="d13e21">
      <schxslt:document>
         <schxslt:pattern id="d13e21">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e32">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e44">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e53">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e62">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e77">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e93">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e124">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e136">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e147">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e158">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e169">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e178">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e187">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e197">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e208">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e225">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e234">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e255">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e264">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e288">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e308">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e317">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e328">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e337">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e346">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e356">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e365">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e374">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e383">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e392">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e401">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e411">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e428">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e448">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e470">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e479">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <xsl:apply-templates mode="d13e21" select="root()"/>
      </schxslt:document>
   </xsl:template>
   <xsl:template match="/node()" priority="37" mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="ln" select="local-name(/node())"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e21']">
            <schxslt:rule pattern="d13e21">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "/node()" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">/node()</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e21">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">/node()</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not($ln ='SubmitObjectsRequest')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S001">
                     <xsl:attribute name="test">$ln ='SubmitObjectsRequest'</xsl:attribute>
                     <svrl:text>The root element of a query response document MUST be 'lcm:SubmitObjectsRequest'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e21')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="/node()" priority="36" mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="ns" select="namespace-uri(/node())"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e32']">
            <schxslt:rule pattern="d13e32">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "/node()" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">/node()</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e32">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">/node()</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not($ns ='urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S002">
                     <xsl:attribute name="test">$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0'</xsl:attribute>
                     <svrl:text>The namespace of root element of a 'lcm:SubmitObjectsRequest' must be 'urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e32')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest" priority="35" mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e44']">
            <schxslt:rule pattern="d13e44">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e44">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@id)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S003">
                     <xsl:attribute name="test">@id</xsl:attribute>
                     <svrl:text>The 'id' attribute of a 'SubmitObjectsRequest' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e44')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest" priority="34" mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e53']">
            <schxslt:rule pattern="d13e53">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e53">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(@id),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S004">
                     <xsl:attribute name="test">matches(normalize-space(@id),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The 'id' attribute of a 'SubmitObjectsRequest' MUST be unique UUID (RFC 4122).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e53')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest" priority="33" mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e62']">
            <schxslt:rule pattern="d13e62">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e62">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(rim:RegistryObjectList)&gt;0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S005">
                     <xsl:attribute name="test">count(rim:RegistryObjectList)&gt;0</xsl:attribute>
                     <svrl:text>A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObjectList'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e62')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList"
                 priority="32"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e62']">
            <schxslt:rule pattern="d13e62">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e62">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(rim:RegistryObject)&gt;0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S036">
                     <xsl:attribute name="test">count(rim:RegistryObject)&gt;0</xsl:attribute>
                     <svrl:text>A 'SubmitObjectsRequest' MUST include minimum a 'rim:RegistryObject'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e62')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="rim:RegistryObject" priority="31" mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="idattr" select="@id"/>
      <xsl:variable name="st" select="substring-after(@xsi:type, ':')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e77']">
            <schxslt:rule pattern="d13e77">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "rim:RegistryObject" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e77">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(string-length($idattr)&gt;0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S006">
                     <xsl:attribute name="test">string-length($idattr)&gt;0</xsl:attribute>
                     <svrl:text>Each 'rim:RegistryObject' MUST include an 'id' attribute</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(($st != 'AssociationType' and count(rim:Classification)&gt;0) or ($st = 'AssociationType' and count(rim:Classification)=0))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S007">
                     <xsl:attribute name="test">($st != 'AssociationType' and count(rim:Classification)&gt;0) or ($st = 'AssociationType' and count(rim:Classification)=0)</xsl:attribute>
                     <svrl:text>Each 'rim:RegistryObject' MUST include a 'rim:Classification' if the 'rim:RegistryObject' is not an 'xsi:type="rim:AssociationType"'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e77')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="rim:RegistryObject/rim:Classification"
                 priority="30"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="idattr" select="@id"/>
      <xsl:variable name="schemeattr" select="@classificationScheme"/>
      <xsl:variable name="nodeattr" select="@classificationNode"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e93']">
            <schxslt:rule pattern="d13e93">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "rim:RegistryObject/rim:Classification" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject/rim:Classification</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e93">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject/rim:Classification</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(string-length($idattr)&gt;0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S008">
                     <xsl:attribute name="test">string-length($idattr)&gt;0</xsl:attribute>
                     <svrl:text>Each 'rim:Classification' MUST include an 'id' attribute</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(matches(normalize-space(@id),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S009">
                     <xsl:attribute name="test">matches(normalize-space(@id),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>Each id of 'rim:Classification' MUST be unique UUID (RFC 4122).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(string-length($schemeattr)&gt;0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S010">
                     <xsl:attribute name="test">string-length($schemeattr)&gt;0</xsl:attribute>
                     <svrl:text>Each 'rim:Classification' MUST include an 'classificationScheme' attribute</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(string-length($nodeattr)&gt;0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S011">
                     <xsl:attribute name="test">string-length($nodeattr)&gt;0</xsl:attribute>
                     <svrl:text>Each 'rim:Classification' MUST include an 'classificationNode' attribute</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not($schemeattr = 'urn:fdc:oots:classification:dsd')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S012">
                     <xsl:attribute name="test">$schemeattr = 'urn:fdc:oots:classification:dsd'</xsl:attribute>
                     <svrl:text> The 'classificationScheme' attribute MUST be 'urn:fdc:oots:classification:dsd'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not($nodeattr = 'EvidenceProvider' or $nodeattr = 'DataServiceEvidenceType')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S013">
                     <xsl:attribute name="test">$nodeattr = 'EvidenceProvider' or $nodeattr = 'DataServiceEvidenceType'</xsl:attribute>
                     <svrl:text> The 'classificationNode' attribute MUST be 'EvidenceProvider' or 'DataServiceEvidenceType'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e93')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']"
                 priority="29"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="nodeattr" select="../rim:Classification/@classificationNode"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e124']">
            <schxslt:rule pattern="d13e124">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e124">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not($nodeattr = 'EvidenceProvider')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S014">
                     <xsl:attribute name="test">$nodeattr = 'EvidenceProvider'</xsl:attribute>
                     <svrl:text>A 'rim:RegistryObject' with the classificationNode 'EvidenceProvider' MUST include a rim:Slot name="EvidenceProvider" and no other</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e124')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']"
                 priority="28"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="nodeattr" select="../rim:Classification/@classificationNode"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e136']">
            <schxslt:rule pattern="d13e136">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e136">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not($nodeattr = 'DataServiceEvidenceType')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S015">
                     <xsl:attribute name="test">$nodeattr = 'DataServiceEvidenceType'</xsl:attribute>
                     <svrl:text>A 'rim:RegistryObject' with the classificationNode 'DataServiceEvidenceType' MUST include a  rim:Slot name="DataServiceEvidenceType" and no other</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e136')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue"
                 priority="27"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="st" select="substring-after(@xsi:type, ':')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e147']">
            <schxslt:rule pattern="d13e147">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e147">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not($st ='AnyValueType')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S016">
                     <xsl:attribute name="test">$st ='AnyValueType'</xsl:attribute>
                     <svrl:text>The rim:SlotValue of rim:Slot name="EvidenceProvider" MUST be of "rim:AnyValueType"</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e147')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue"
                 priority="26"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="st" select="substring-after(@xsi:type, ':')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e158']">
            <schxslt:rule pattern="d13e158">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e158">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not($st ='AnyValueType')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S017">
                     <xsl:attribute name="test">$st ='AnyValueType'</xsl:attribute>
                     <svrl:text>The rim:SlotValue of rim:Slot name="DataServiceEvidenceType" MUST be of "rim:AnyValueType"</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e158')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']"
                 priority="25"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e169']">
            <schxslt:rule pattern="d13e169">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e169">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(rim:SlotValue/sdg:AccessService)=1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S018">
                     <xsl:attribute name="test">count(rim:SlotValue/sdg:AccessService)=1</xsl:attribute>
                     <svrl:text>A 'rim:Slot[@name='EvidenceProvider']/rim:SlotValue' MUST contain one sdg:AccessService of the targetNamespace="http://data.europa.eu/p4s"</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e169')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']"
                 priority="24"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e178']">
            <schxslt:rule pattern="d13e178">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e178">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(rim:SlotValue/sdg:DataServiceEvidenceType)=1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S019">
                     <xsl:attribute name="test">count(rim:SlotValue/sdg:DataServiceEvidenceType)=1</xsl:attribute>
                     <svrl:text>A 'rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue' MUST contain one sdg:DataServiceEvidenceType of the targetNamespace="http://data.europa.eu/p4s"</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e178')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue"
                 priority="23"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e187']">
            <schxslt:rule pattern="d13e187">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e187">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(sdg:AccessService)=0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S020">
                     <xsl:attribute name="test">count(sdg:AccessService)=0</xsl:attribute>
                     <svrl:text>A DataServiceEvidenceType 'rim:Slot[@name='DataServiceEvidenceType]/rim:SlotValue' MUST not contain an 'sdg:AccessService'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e187')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject"
                 priority="22"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="idtype" select="@xsi:type"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e197']">
            <schxslt:rule pattern="d13e197">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e197">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not((count(rim:Classification) = 0 and matches(normalize-space($idtype),'AssociationType$','i')) or                  (count(rim:Classification) = 1 and not(matches(normalize-space($idtype),'AssociationType$','i'))))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S022">
                     <xsl:attribute name="test">(count(rim:Classification) = 0 and matches(normalize-space($idtype),'AssociationType$','i')) or                  (count(rim:Classification) = 1 and not(matches(normalize-space($idtype),'AssociationType$','i')))</xsl:attribute>
                     <svrl:text>If a 'rim:RegistryObject' does not have a 'rim:Classification" it MUST have the attribute 'xsi:type=rim:AssociationType'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e197')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="rim:RegistryObject[@sourceObject and @targetObject]"
                 priority="21"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="sourceObject" select="@sourceObject"/>
      <xsl:variable name="targetObject" select="@targetObject"/>
      <xsl:variable name="etl_count"
                    select="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceProvider']])"/>
      <xsl:variable name="et_count"
                    select="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='DataServiceEvidenceType']])"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e208']">
            <schxslt:rule pattern="d13e208">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "rim:RegistryObject[@sourceObject and @targetObject]" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject[@sourceObject and @targetObject]</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e208">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject[@sourceObject and @targetObject]</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not($etl_count + $et_count = 2)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S023">
                     <xsl:attribute name="test">$etl_count + $et_count = 2</xsl:attribute>
                     <svrl:text>Each 'rim:RegistryObject' with a classificationNode 'EvidenceProvider' or 'DataServiceEvidenceType' MUST have an association described in a
                the attribute 'xsi:type="rim:AssociationType"'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e208')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="rim:RegistryObject[@xsi:type='rim:AssociationType']"
                 priority="20"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e225']">
            <schxslt:rule pattern="d13e225">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "rim:RegistryObject[@xsi:type='rim:AssociationType']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject[@xsi:type='rim:AssociationType']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e225">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject[@xsi:type='rim:AssociationType']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@sourceObject)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S024">
                     <xsl:attribute name="test">@sourceObject</xsl:attribute>
                     <svrl:text>A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' MUST have an attribute 'sourceObject'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e225')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="rim:RegistryObject[@xsi:type='rim:AssociationType']"
                 priority="19"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="sourceObject" select="@sourceObject"/>
      <xsl:variable name="targetObject" select="@targetObject"/>
      <xsl:variable name="registrySourceObject"
                    select="//rim:RegistryObject[@id = $sourceObject]"/>
      <xsl:variable name="registryTargetObject"
                    select="//rim:RegistryObject[@id = $targetObject]"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e234']">
            <schxslt:rule pattern="d13e234">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "rim:RegistryObject[@xsi:type='rim:AssociationType']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject[@xsi:type='rim:AssociationType']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e234">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject[@xsi:type='rim:AssociationType']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not($registrySourceObject/rim:Classification[@classificationNode='EvidenceProvider'])">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S025">
                     <xsl:attribute name="test">$registrySourceObject/rim:Classification[@classificationNode='EvidenceProvider']</xsl:attribute>
                     <svrl:text>A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' MUST have an attribute 'sourceObject' that lists the id of the 
                'rim:RegistryObject' (starting with prefix "urn:uuid:") from classificationNode 'EvidenceProvider' </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not($registryTargetObject/rim:Classification[@classificationNode='DataServiceEvidenceType'])">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S027">
                     <xsl:attribute name="test">$registryTargetObject/rim:Classification[@classificationNode='DataServiceEvidenceType']</xsl:attribute>
                     <svrl:text>A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' MUST have an attribute 'targetObject' that lists the id of the 
                'rim:RegistryObject' (starting with prefix "urn:uuid:") with classificationNode 'DataServiceEvidenceType'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e234')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="rim:RegistryObject[@xsi:type='rim:AssociationType']"
                 priority="18"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e255']">
            <schxslt:rule pattern="d13e255">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "rim:RegistryObject[@xsi:type='rim:AssociationType']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject[@xsi:type='rim:AssociationType']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e255">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject[@xsi:type='rim:AssociationType']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@targetObject)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S026">
                     <xsl:attribute name="test">@targetObject</xsl:attribute>
                     <svrl:text>A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' MUST have an attribute 'targetObject'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e255')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ServesEvidenceType']"
                 priority="17"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="sourceObject" select="@sourceObject"/>
      <xsl:variable name="registrySourceObject"
                    select="//rim:RegistryObject[@id = $sourceObject]"/>
      <xsl:variable name="targetObject" select="@targetObject"/>
      <xsl:variable name="registryTargetObject"
                    select="//rim:RegistryObject[@id = $targetObject]"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e264']">
            <schxslt:rule pattern="d13e264">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ServesEvidenceType']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ServesEvidenceType']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e264">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ServesEvidenceType']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not($registrySourceObject/rim:Classification[@classificationNode='EvidenceProvider'])">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S028">
                     <xsl:attribute name="test">$registrySourceObject/rim:Classification[@classificationNode='EvidenceProvider']</xsl:attribute>
                     <svrl:text>Source object of ServesEvidenceType association must be classified as EvidenceProvider <xsl:value-of select="$sourceObject"/>
                     </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not($registryTargetObject/rim:Classification[@classificationNode='DataServiceEvidenceType'])">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S037">
                     <xsl:attribute name="test">$registryTargetObject/rim:Classification[@classificationNode='DataServiceEvidenceType']</xsl:attribute>
                     <svrl:text>Target object of ServesEvidenceType association must be classified as DataServiceEvidenceType <xsl:value-of select="$targetObject"/>
                     </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e264')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="rim:RegistryObject[@sourceObject and @targetObject]"
                 priority="16"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="sourceObject" select="@sourceObject"/>
      <xsl:variable name="targetObject" select="@targetObject"/>
      <xsl:variable name="type" select="@type"/>
      <xsl:variable name="etl_count"
                    select="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceProvider']])"/>
      <xsl:variable name="et_count"
                    select="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='DataServiceEvidenceType']])"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e288']">
            <schxslt:rule pattern="d13e288">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "rim:RegistryObject[@sourceObject and @targetObject]" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject[@sourceObject and @targetObject]</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e288">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:RegistryObject[@sourceObject and @targetObject]</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(number($etl_count) + number($et_count) != 2 or $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:ServesEvidenceType')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S029">
                     <xsl:attribute name="test">number($etl_count) + number($et_count) != 2 or $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:ServesEvidenceType'</xsl:attribute>
                     <svrl:text>A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' linking 'rim:RegistryObject' with the classificationNode 
                'EvidenceProvider' (@sourceObject) to 'rim:RegistryObject' with the classificationNode 'DataServiceEvidenceType' (@targetObject) MUST 
                use the type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ServesEvidenceType"</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e288')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest" priority="15" mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e308']">
            <schxslt:rule pattern="d13e308">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e308">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(rim:Slot)=0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S030">
                     <xsl:attribute name="test">count(rim:Slot)=0</xsl:attribute>
                     <svrl:text>A 'lcm:SubmitObjectsRequest' MUST not contain any other rim:Slots.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e308')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification"
                 priority="14"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="hasSupportedValue"
                    select="//rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:SupportedValue"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e317']">
            <schxslt:rule pattern="d13e317">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e317">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(empty($hasSupportedValue)))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S031">
                     <xsl:attribute name="test">not(empty($hasSupportedValue))</xsl:attribute>
                     <svrl:text>A value for 'sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:SupportedValue' MUST be provided if the  
                'sdg:DataServiceEvidenceType' which is associated to the 'sdg:AccessService' has the element 'sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification'.
            </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e317')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/@id"
                 priority="13"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e328']">
            <schxslt:rule pattern="d13e328">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/@id" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/@id</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e328">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/@id</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S032">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>Each id of 'rim:RegistryObject' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:". 
            </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e328')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList"
                 priority="12"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e337']">
            <schxslt:rule pattern="d13e337">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e337">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(rim:RegistryObject/rim:Slot[@name='EvidenceProvider'])">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S033">
                     <xsl:attribute name="test">rim:RegistryObject/rim:Slot[@name='EvidenceProvider']</xsl:attribute>
                     <svrl:text>A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="EvidenceProvider"  
            </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e337')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList"
                 priority="11"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e346']">
            <schxslt:rule pattern="d13e346">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e346">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType'])">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S034">
                     <xsl:attribute name="test">rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']</xsl:attribute>
                     <svrl:text>
                A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="DataServiceEvidenceType"  
            </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e346')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList"
                 priority="10"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e356']">
            <schxslt:rule pattern="d13e356">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e356">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(rim:RegistryObject[@xsi:type='rim:AssociationType'])">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S035">
                     <xsl:attribute name="test">rim:RegistryObject[@xsi:type='rim:AssociationType']</xsl:attribute>
                     <svrl:text>
                A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"
            </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e356')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService"
                 priority="9"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e365']">
            <schxslt:rule pattern="d13e365">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e365">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(sdg:Publisher)=1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S038">
                     <xsl:attribute name="test">count(sdg:Publisher)=1</xsl:attribute>
                     <svrl:text>The xs:element name="sdg:Publisher" type="sdg:EvidenceProviderType" MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e365')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest" priority="8" mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e374']">
            <schxslt:rule pattern="d13e374">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e374">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@checkReferences)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S039">
                     <xsl:attribute name="test">@checkReferences</xsl:attribute>
                     <svrl:text>The 'checkReferences' attribute of a 'SubmitObjectsRequest' MUST be present. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e374')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest" priority="7" mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e383']">
            <schxslt:rule pattern="d13e383">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e383">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@checkReferences='true')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S040">
                     <xsl:attribute name="test">@checkReferences='true'</xsl:attribute>
                     <svrl:text>The 'checkReferences' attribute of a 'SubmitObjectsRequest' MUST be set on "true".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e383')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject"
                 priority="6"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e392']">
            <schxslt:rule pattern="d13e392">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e392">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(rim:Slot[not(@name=('EvidenceProvider', 'DataServiceEvidenceType', 'AccessService'))]))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S041">
                     <xsl:attribute name="test">not(rim:Slot[not(@name=('EvidenceProvider', 'DataServiceEvidenceType', 'AccessService'))])</xsl:attribute>
                     <svrl:text>A 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/' MUST not contain any other rim:Slots than 'EvidenceProvider', 'DataServiceEvidenceType' or 'AccessService'. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e392')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject[@xsi:type='rim:AssociationType']"
                 priority="5"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e401']">
            <schxslt:rule pattern="d13e401">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject[@xsi:type='rim:AssociationType']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject[@xsi:type='rim:AssociationType']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e401">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject[@xsi:type='rim:AssociationType']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(rim:Slot))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S042">
                     <xsl:attribute name="test">not(rim:Slot)</xsl:attribute>
                     <svrl:text>A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType" MUST not include any slots</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e401')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept"
                 priority="4"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="accessSeviceID" select="sdg:Identifier"/>
      <xsl:variable name="dataServiceID"
                    select="//rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier"/>
      <xsl:variable name="sourceObjectID" select="ancestor-or-self::rim:RegistryObject/@id"/>
      <xsl:variable name="targetObjectID"
                    select="//rim:RegistryObject[@sourceObject=$sourceObjectID]/@targetObject"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e411']">
            <schxslt:rule pattern="d13e411">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e411">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not($accessSeviceID=$dataServiceID and not($targetObjectID=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S043">
                     <xsl:attribute name="test">$accessSeviceID=$dataServiceID and not($targetObjectID='')</xsl:attribute>
                     <svrl:text>The value of 'sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Identifier' MUST match with one existing  'sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier'  
                provided by the  'sdg:DataServiceEvidenceType' which is associated to the 'EvidenceProvider' (sdg:AccessService).      
            </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e411')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction"
                 priority="3"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="adminLevel2" select="boolean(sdg:AdminUnitLevel2)"/>
      <xsl:variable name="sourceObjectID" select="ancestor-or-self::rim:RegistryObject/@id"/>
      <xsl:variable name="targetObjectID"
                    select="//rim:RegistryObject[@sourceObject=$sourceObjectID]/@targetObject"/>
      <xsl:variable name="checkJurisdiction"
                    select="//rim:RegistryObject[@id = $targetObjectID]/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel"/>
      <xsl:variable name="checkNuts" select="contains($checkJurisdiction, 'NUTS')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e428']">
            <schxslt:rule pattern="d13e428">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e428">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(($adminLevel2 and $checkNuts) or not($checkNuts))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="WARNING"
                                      id="R-DSD-SUB-S044">
                     <xsl:attribute name="test">($adminLevel2 and $checkNuts) or not($checkNuts)</xsl:attribute>
                     <svrl:text>The element 'sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel2'  SHOULD be provided if the  'sdg:DataServiceEvidenceType' which is associated to 
                the 'sdg:AccessService' has on of the values 'NUTS1', 'NUTS2' or 'NUTS3' in the element 'sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel'.
            </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e428')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction"
                 priority="2"
                 mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="adminLevel3" select="boolean(sdg:AdminUnitLevel3)"/>
      <xsl:variable name="sourceObjectID" select="ancestor-or-self::rim:RegistryObject/@id"/>
      <xsl:variable name="targetObjectID"
                    select="//rim:RegistryObject[@sourceObject=$sourceObjectID]/@targetObject"/>
      <xsl:variable name="checkJurisdiction"
                    select="//rim:RegistryObject[@id = $targetObjectID]/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel"/>
      <xsl:variable name="checkLau" select="contains($checkJurisdiction, 'LAU')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e448']">
            <schxslt:rule pattern="d13e448">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e448">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(($adminLevel3 and $checkLau) or not($checkLau))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="WARNING"
                                      id="R-DSD-SUB-S045">
                     <xsl:attribute name="test">($adminLevel3 and $checkLau) or not($checkLau)</xsl:attribute>
                     <svrl:text>The element 'sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel3'  SHOULD be provided if the 'sdg:DataServiceEvidenceType' which is associated to 
                the 'sdg:AccessService' has the value 'LAU' in the element 'sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel'.
            </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e448')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="rim:Slot" priority="1" mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e470']">
            <schxslt:rule pattern="d13e470">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "rim:Slot" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:Slot</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e470">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:Slot</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(child::rim:SlotValue) = 1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S046">
                     <xsl:attribute name="test">count(child::rim:SlotValue) = 1</xsl:attribute>
                     <svrl:text>A rim:Slot MUST have a rim:SlotValue child element</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e470')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="rim:SlotValue" priority="0" mode="d13e21">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e479']">
            <schxslt:rule pattern="d13e479">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e479">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(child::*) &gt; 0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-S047">
                     <xsl:attribute name="test">count(child::*) &gt; 0</xsl:attribute>
                     <svrl:text>A rim:SlotValue MUST have child element content</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e479')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:function name="schxslt:location" as="xs:string">
      <xsl:param name="node" as="node()"/>
      <xsl:variable name="segments" as="xs:string*">
         <xsl:for-each select="($node/ancestor-or-self::node())">
            <xsl:variable name="position">
               <xsl:number level="single"/>
            </xsl:variable>
            <xsl:choose>
               <xsl:when test=". instance of element()">
                  <xsl:value-of select="concat('Q{', namespace-uri(.), '}', local-name(.), '[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of attribute()">
                  <xsl:value-of select="concat('@Q{', namespace-uri(.), '}', local-name(.))"/>
               </xsl:when>
               <xsl:when test=". instance of processing-instruction()">
                  <xsl:value-of select="concat('processing-instruction(&#34;', name(.), '&#34;)[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of comment()">
                  <xsl:value-of select="concat('comment()[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of text()">
                  <xsl:value-of select="concat('text()[', $position, ']')"/>
               </xsl:when>
               <xsl:otherwise/>
            </xsl:choose>
         </xsl:for-each>
      </xsl:variable>
      <xsl:value-of select="concat('/', string-join($segments, '/'))"/>
   </xsl:function>
</xsl:transform>
