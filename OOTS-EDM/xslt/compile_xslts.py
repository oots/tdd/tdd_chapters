
# test
from inspect import getsourcefile
import glob, re

from oots.utils.schematron import SchematronValidator

def to_xslt(source):
    print(source)
    m = re.search(r'([\w_-]*)\.sch$', source)
    output = "{}.xsl".format(m[1])
    print(output)

    v = SchematronValidator(xml_file_name=source)
    with open(output, 'wb') as fd:
        fd.write(v.as_xslt())


for source in glob.glob('../sch/*.sch'):
    to_xslt(source)

for source in glob.glob('../sch_fatal_error/*.sch'):
    to_xslt(source)



