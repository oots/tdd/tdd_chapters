<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform xmlns:error="https://doi.org/10.5281/zenodo.1495494#error"
               xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"
               xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
               xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
               xmlns:sch="http://purl.oclc.org/dsdl/schematron"
               xmlns:schxslt="https://doi.org/10.5281/zenodo.1495494"
               xmlns:schxslt-api="https://doi.org/10.5281/zenodo.1495494#api"
               xmlns:sdg="http://data.europa.eu/p4s"
               xmlns:xlink="http://www.w3.org/1999/xlink"
               xmlns:xs="http://www.w3.org/2001/XMLSchema"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               version="2.0">
   <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:dct="http://purl.org/dc/terms/"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
      <dct:creator>
         <dct:Agent>
            <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
            <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
         </dct:Agent>
      </dct:creator>
      <dct:created>2024-09-08T11:21:11.701936+02:00</dct:created>
   </rdf:Description>
   <xsl:output indent="yes"/>
   <xsl:param name="EEA_COUNTRY-CODELIST_code"
              select="tokenize('AT BE BG HR CY CZ DK EE FI FR DE EL HU IS IE IT LV LI LT LU MT NL NO PL PT RO SK SI ES SE', '\s')"/>
   <xsl:param name="AGENTCLASSIFICATION-CODELIST_code"
              select="tokenize('ER IP EP ERRP', '\s')"/>
   <xsl:param name="OOTSMEDIATYPES-CODELIST_code"
              select="tokenize('image/jpeg application/json image/png application/pdf application/xml image/svg+xml', '\s')"/>
   <xsl:param name="LANGUAGECODE-CODELIST_code"
              select="tokenize('AA AB AE AF AK AM AN AR AS AV AY AZ BA BE BG BH BI BM BN BO BO BR BS CA CE CH CO CR CS CS CU CV CY CY DA DE DE DV DZ EE EL EL EN EO ES ET EU EU FA FA FF FI FJ FO FR FR FY GA GD GL GN GU GV HA HE HI HO HR HT HU HY HY HZ IA ID IE IG II IK IO IS IS IT IU JA JV KA KA KG KI KJ KK KL KM KN KO KR KS KU KV KW KY LA LB LG LI LN LO LT LU LV MG MH MI MI MK MK ML MN MR MS MS MT MY MY NA NB ND NE NG NL NL NN NO NR NV NY OC OJ OM OR OS PA PI PL PS PT QU RM RN RO RO RU RW SA SC SD SE SG SI SK SK SL SM SN SO SQ SQ SR SS ST SU SV SW TA TE TG TH TI TK TL TN TO TR TS TT TW TY UG UK UR UZ VE VI VO WA WO XH YI YO ZA ZH ZH ZU', '\s')"/>
   <xsl:param name="EAS_Code"
              select="tokenize('0002 0007 0009 0037 0060 0088 0096 0097 0106 0130 0135 0142 0147 0151 0170 0183 0184 0188 0190 0191 0192 0193 0194 0195 0196 0198 0199 0200 0201 0202 0203 0204 0205 0208 0209 0210 0211 0212 0213 0215 0216 0217 0218 0221 0225 0230 9901 9910 9913 9914 9915 9918 9919 9920 9922 9923 9924 9925 9926 9927 9928 9929 9930 9931 9932 9933 9934 9935 9936 9937 9938 9939 9940 9941 9942 9943 9944 9945 9946 9947 9948 9949 9950 9951 9952 9953 9957 9959 AN AQ AS AU EM', '\s')"/>
   <xsl:param name="COUNTRYIDENTIFICATIONCODE-CODELIST_code"
              select="tokenize('AF AL DZ AS AD AO AI AQ AG AR AM AW AU AT AZ BS BH BD BB BY BE BZ BJ BM BT BO BQ BA BW BV BR IO BN BG BF BI CV KH CM CA KY CF TD CL CN CX CC CO KM CD CG CK CR HR CU CW CY CZ CI DK DJ DM DO EC EG SV GQ ER EE SZ ET FK FO FJ FI FR GF PF TF GA GM GE DE GH GI EL GL GD GP GU GT GG GN GW GY HT HM VA HN HK HU IS IN ID IR IQ IE IM IL IT JM JP JE JO KZ KE KI KP KR KW KG LA LV LB LS LR LY LI LT LU MO MG MW MY MV ML MT MH MQ MR MU YT MX FM MD MC MN ME MS MA MZ MM NA NR NP NL NC NZ NI NE NG NU NF MK MP NO OM PK PW PS PA PG PY PE PH PN PL PT PR QA RO RU RW RE BL SH KN LC MF PM VC WS SM ST SA SN RS SC SL SG SX SK SI SB SO ZA GS SS ES LK SD SR SJ SE CH SY TW TJ TZ TH TL TG TK TO TT TN TR TM TC TV UG UA AE GB UM US UY UZ VU VE VN VG VI WF EH YE ZM ZW AX', '\s')"/>
   <xsl:param name="LEVELSOFASSURANCE-CODELIST_code"
              select="tokenize('Low Substantial High', '\s')"/>
   <xsl:param name="JURISDICTIONLEVEL-CODELIST_code"
              select="tokenize('Country NUTS1 NUTS2 NUTS3 LAU', '\s')"/>
   <xsl:template match="root()">
      <xsl:variable name="metadata" as="element()?">
         <svrl:metadata xmlns:dct="http://purl.org/dc/terms/"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                        xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
            <dct:creator>
               <dct:Agent>
                  <skos:prefLabel>
                     <xsl:value-of separator="/"
                                   select="(system-property('xsl:product-name'), system-property('xsl:product-version'))"/>
                  </skos:prefLabel>
               </dct:Agent>
            </dct:creator>
            <dct:created>
               <xsl:value-of select="current-dateTime()"/>
            </dct:created>
            <dct:source>
               <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/">
                  <dct:creator>
                     <dct:Agent>
                        <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
                        <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
                     </dct:Agent>
                  </dct:creator>
                  <dct:created>2024-09-08T11:21:11.701936+02:00</dct:created>
               </rdf:Description>
            </dct:source>
         </svrl:metadata>
      </xsl:variable>
      <xsl:variable name="report" as="element(schxslt:report)">
         <schxslt:report>
            <xsl:call-template name="d21e56"/>
         </schxslt:report>
      </xsl:variable>
      <xsl:variable name="schxslt:report" as="node()*">
         <xsl:sequence select="$metadata"/>
         <xsl:for-each select="$report/schxslt:document">
            <xsl:for-each select="schxslt:pattern">
               <xsl:sequence select="node()"/>
               <xsl:sequence select="../schxslt:rule[@pattern = current()/@id]/node()"/>
            </xsl:for-each>
         </xsl:for-each>
      </xsl:variable>
      <svrl:schematron-output xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <svrl:ns-prefix-in-attribute-values prefix="sdg" uri="http://data.europa.eu/p4s"/>
         <svrl:ns-prefix-in-attribute-values prefix="rs" uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="rim" uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="query" uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
         <svrl:ns-prefix-in-attribute-values prefix="xlink" uri="http://www.w3.org/1999/xlink"/>
         <xsl:sequence select="$schxslt:report"/>
      </svrl:schematron-output>
   </xsl:template>
   <xsl:template match="text() | @*" mode="#all" priority="-10"/>
   <xsl:template match="/" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template match="*" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="@*"/>
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template name="d21e56">
      <schxslt:document>
         <schxslt:pattern id="d21e56">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e65">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e76">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e85">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e94">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e104">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e113">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e122">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e131">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e140">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e155">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e165">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e174">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e186">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e195">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e204">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e213">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e223">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e234">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e245">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e258">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e267">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e276">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e286">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e295">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e304">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e315">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d21e326">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <xsl:apply-templates mode="d21e56" select="root()"/>
      </schxslt:document>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier"
                 priority="42"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e56']">
            <schxslt:rule pattern="d21e56">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e56">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C002">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The value of 'Identifier' of an 'DataServiceEvidenceType' MUST be unique UUID (RFC 4122).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e56')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification"
                 priority="41"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e65']">
            <schxslt:rule pattern="d21e65">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e65">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(., '^https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(oots|(' || string-join($EEA_COUNTRY-CODELIST_code, '|') || '))/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C003">
                     <xsl:attribute name="test">matches(., '^https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(oots|(' || string-join($EEA_COUNTRY-CODELIST_code, '|') || '))/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$')</xsl:attribute>
                     <svrl:text>The value of 'EvidenceTypeClassification' of a 'DataServiceEvidenceType' MUST be a UUID of the Evidence Broker and include a code of the code list 'EEA_Country-CodeList'
            (ISO 3166-1' alpha-2 codes EEA subset of the country codes) using the prefix and scheme 'https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/[EEA_CountryCode]/[UUID]'' pointing to the Semantic Repository. For testing purposes and agreed OOTS data models the code "oots" can be used.  </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e65')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title"
                 priority="40"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e76']">
            <schxslt:rule pattern="d21e76">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e76">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C005">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e76')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description"
                 priority="39"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e85']">
            <schxslt:rule pattern="d21e85">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e85">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C007">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e85')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo"
                 priority="38"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e94']">
            <schxslt:rule pattern="d21e94">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e94">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C010">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')</xsl:attribute>
                     <svrl:text>The value of 'ConformsTo' of the distribution MUST be a persistent URL with a link to a "DataModelScheme" of the Evidence Type described in the 
            Semantic Repository which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e94')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Identifier"
                 priority="37"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e104']">
            <schxslt:rule pattern="d21e104">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e104">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@schemeID)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C012">
                     <xsl:attribute name="test">not(normalize-space(@schemeID)='')</xsl:attribute>
                     <svrl:text>The 'schemeID' MUST be provided.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e104')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService"
                 priority="36"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e113']">
            <schxslt:rule pattern="d21e113">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e113">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(sdg:ConformsTo='oots-edm:v1.0' or sdg:ConformsTo='oots-edm:v1.1')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C015">
                     <xsl:attribute name="test">sdg:ConformsTo='oots-edm:v1.0' or sdg:ConformsTo='oots-edm:v1.1'</xsl:attribute>
                     <svrl:text>The value of 'ConformsTo' of the Access Service MUST point to the underlying eDelivery and Evidence Exchange Data Model Profile e.g. 'oots-edm:v1.0', 'oots-edm:v1.1', ... .</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e113')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier"
                 priority="35"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e122']">
            <schxslt:rule pattern="d21e122">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e122">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C016">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e122')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Identifier"
                 priority="34"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e131']">
            <schxslt:rule pattern="d21e131">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e131">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C023">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The value of 'Identifier' of MUST be unique UUID (RFC 4122).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e131')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept"
                 priority="33"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e140']">
            <schxslt:rule pattern="d21e140">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e140">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:Type)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C024">
                     <xsl:attribute name="test">not(normalize-space(sdg:Type)='')</xsl:attribute>
                     <svrl:text>A 'Type' MUST be provided.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(count(sdg:Description)&gt;0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C025">
                     <xsl:attribute name="test">count(sdg:Description)&gt;0</xsl:attribute>
                     <svrl:text>A 'Description' MUST be provided.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(count(sdg:SupportedValue)&gt;0 and not(normalize-space(sdg:SupportedValue)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C028">
                     <xsl:attribute name="test">count(sdg:SupportedValue)&gt;0 and not(normalize-space(sdg:SupportedValue)='')</xsl:attribute>
                     <svrl:text>A value for 'SupportedValue' MUST be provided if the 'sdg:ClassificationConcept' is present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e140')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description"
                 priority="32"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e155']">
            <schxslt:rule pattern="d21e155">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e155">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C027">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e155')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier"
                 priority="31"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e165']">
            <schxslt:rule pattern="d21e165">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e165">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C029">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The value of 'Identifier' of MUST be unique UUID (RFC 4122).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e165')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification"
                 priority="30"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e174']">
            <schxslt:rule pattern="d21e174">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e174">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:Type)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C030">
                     <xsl:attribute name="test">not(normalize-space(sdg:Type)='')</xsl:attribute>
                     <svrl:text>A 'Type' MUST be provided.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(count(sdg:Description)&gt;0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C031">
                     <xsl:attribute name="test">count(sdg:Description)&gt;0</xsl:attribute>
                     <svrl:text>A 'Description' MUST be provided.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e174')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description"
                 priority="29"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e186']">
            <schxslt:rule pattern="d21e186">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e186">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C033">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e186')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId"
                 priority="28"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e195']">
            <schxslt:rule pattern="d21e195">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e195">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C034">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The value of 'JurisdictionContextId' of MUST be unique UUID (RFC 4122).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e195')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext"
                 priority="27"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e204']">
            <schxslt:rule pattern="d21e204">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e204">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C036">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e204')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation"
                 priority="26"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e213']">
            <schxslt:rule pattern="d21e213">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e213">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C038">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')</xsl:attribute>
                     <svrl:text>The value of 'Transformation' of the distribution MUST be a persistent URL with link to a "DataModelScheme" and 
            "Subset" of the EvidenceType described in the Semantic Repository which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]/[Subset]".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e213')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs[sdg:Format = 'application/xml']"
                 priority="25"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="otherCodes"
                    select="count(../sdg:DistributedAs/sdg:Format[some $code in $OOTSMEDIATYPES-CODELIST_code[. != 'application/json' and . != 'application/xml'] satisfies .=$code])"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e223']">
            <schxslt:rule pattern="d21e223">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs[sdg:Format = 'application/xml']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs[sdg:Format = 'application/xml']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e223">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs[sdg:Format = 'application/xml']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not((count(sdg:ConformsTo) &gt; 0 and $otherCodes = 0) or $otherCodes &gt; 0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C039">
                     <xsl:attribute name="test">(count(sdg:ConformsTo) &gt; 0 and $otherCodes = 0) or $otherCodes &gt; 0</xsl:attribute>
                     <svrl:text>The value of 'sdg:ConformsTo' of the distribution MUST be present if the 'sdg:DistributedAs/sdg:Format' uses the code 'application/xml' of the codelist 'OOTSMediaTypes' unless another 'sdg:DistributedAs/sdg:Format' exists that uses the code 'image/jpeg' or 'image/jpg' or 'image/png' or 'application/pdf' or 'image/svg+xml' for the same DataServiceEvidenceType.
         </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e223')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs[sdg:Format = 'application/json']"
                 priority="24"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="otherCodes"
                    select="count(../sdg:DistributedAs/sdg:Format[some $code in $OOTSMEDIATYPES-CODELIST_code[. != 'application/json' and . != 'application/xml'] satisfies .=$code])"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e234']">
            <schxslt:rule pattern="d21e234">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs[sdg:Format = 'application/json']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs[sdg:Format = 'application/json']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e234">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs[sdg:Format = 'application/json']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not((count(sdg:ConformsTo) &gt; 0 and $otherCodes = 0) or $otherCodes &gt; 0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C041">
                     <xsl:attribute name="test">(count(sdg:ConformsTo) &gt; 0 and $otherCodes = 0) or $otherCodes &gt; 0</xsl:attribute>
                     <svrl:text>The value of 'sdg:ConformsTo' of the distribution MUST be present if the 'sdg:DistributedAs/sdg:Format' uses the code 'application/json' of the codelist 'OOTSMediaTypes' unless another 'sdg:DistributedAs/sdg:Format' exists that uses the code 'image/jpeg' or 'image/jpg' or 'image/png' or 'application/pdf' or 'image/svg+xml' for the same DataServiceEvidenceType.
         </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e234')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs"
                 priority="23"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="tr" select="count(sdg:Transformation)"/>
      <xsl:variable name="ct" select="count(sdg:ConformsTo)"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e245']">
            <schxslt:rule pattern="d21e245">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e245">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not($tr=1 and $ct=0))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C040">
                     <xsl:attribute name="test">not($tr=1 and $ct=0)</xsl:attribute>
                     <svrl:text>The value of 'sdg:ConformsTo' of the distribution MUST be present if the Element 'sdg:Transformation' of the distribution is present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e245')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note"
                 priority="22"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e258']">
            <schxslt:rule pattern="d21e258">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e258">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C043">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e258')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Name"
                 priority="21"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e267']">
            <schxslt:rule pattern="d21e267">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Name" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Name</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e267">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Name</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C045">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e267')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Name"
                 priority="20"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e276']">
            <schxslt:rule pattern="d21e276">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Name" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Name</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e276">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Name</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C047">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e276')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Description"
                 priority="19"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e286']">
            <schxslt:rule pattern="d21e286">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Description" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Description</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e286">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Description</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C049">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e286')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue"
                 priority="18"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e295']">
            <schxslt:rule pattern="d21e295">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e295">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(rim:Value='oots-cs:v1.1')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C050">
                     <xsl:attribute name="test">rim:Value='oots-cs:v1.1'</xsl:attribute>
                     <svrl:text>The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-cs:v1.1".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e295')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction"
                 priority="17"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e304']">
            <schxslt:rule pattern="d21e304">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e304">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(sdg:AdminUnitLevel3) or (sdg:AdminUnitLevel1 and sdg:AdminUnitLevel2))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C051">
                     <xsl:attribute name="test">not(sdg:AdminUnitLevel3) or (sdg:AdminUnitLevel1 and sdg:AdminUnitLevel2)</xsl:attribute>
                     <svrl:text>
            If AdminUnitLevel 3 of the Jurisdiction is present, both AdminUnitLevel 1 and AdminUnitLevel 2 of the Jurisdiction must be present.
         </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e304')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction"
                 priority="16"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e315']">
            <schxslt:rule pattern="d21e315">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e315">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(sdg:AdminUnitLevel2) or sdg:AdminUnitLevel1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C052">
                     <xsl:attribute name="test">not(sdg:AdminUnitLevel2) or sdg:AdminUnitLevel1</xsl:attribute>
                     <svrl:text>
            If AdminUnitLevel 2 of the Jurisdiction is present, AdminUnitLevel 1 of the Jurisdiction must be present.
         </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e315')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang"
                 priority="15"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C004">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang"
                 priority="14"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C006">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description/@lang"
                 priority="13"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C026">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description/@lang"
                 priority="12"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C032">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang"
                 priority="11"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C035">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Description/@lang"
                 priority="10"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Description/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Description/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Description/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C048">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance"
                 priority="9"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C008">
                     <xsl:attribute name="test">some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LevelsOfAssurance-CodeList' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format"
                 priority="8"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C009">
                     <xsl:attribute name="test">some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'OOTSMediaTypes-CodeList' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Identifier"
                 priority="7"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:variable name="suffix1"
                    select="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C014">
                     <xsl:attribute name="test">((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256</xsl:attribute>
                     <svrl:text>The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country-CodeList’. For testing purposes the code "oots" can be used. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier"
                 priority="6"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:variable name="suffix1"
                    select="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C017">
                     <xsl:attribute name="test">((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256</xsl:attribute>
                     <svrl:text>The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country-CodeList’. For testing purposes the code "oots" can be used. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Address/sdg:AdminUnitLevel1"
                 priority="5"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Address/sdg:AdminUnitLevel1" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Address/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Address/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C018">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Address/sdg:AdminUnitLevel1'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel1"
                 priority="4"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel1" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $EEA_COUNTRY-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C020">
                     <xsl:attribute name="test">some $code in $EEA_COUNTRY-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'EEA_Country-CodeList' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel1'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel"
                 priority="3"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $JURISDICTIONLEVEL-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C037">
                     <xsl:attribute name="test">some $code in $JURISDICTIONLEVEL-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'JurisdictionLevel-CodeList' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang"
                 priority="2"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C042">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Name/@lang"
                 priority="1"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Name/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Name/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Name/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C044">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Name/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Name/@lang"
                 priority="0"
                 mode="d21e56">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd21e326']">
            <schxslt:rule pattern="d21e326">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Name/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Name/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d21e326">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Name/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-RESP-C046">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService/sdg:Publisher/sdg:Name/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd21e326')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:function name="schxslt:location" as="xs:string">
      <xsl:param name="node" as="node()"/>
      <xsl:variable name="segments" as="xs:string*">
         <xsl:for-each select="($node/ancestor-or-self::node())">
            <xsl:variable name="position">
               <xsl:number level="single"/>
            </xsl:variable>
            <xsl:choose>
               <xsl:when test=". instance of element()">
                  <xsl:value-of select="concat('Q{', namespace-uri(.), '}', local-name(.), '[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of attribute()">
                  <xsl:value-of select="concat('@Q{', namespace-uri(.), '}', local-name(.))"/>
               </xsl:when>
               <xsl:when test=". instance of processing-instruction()">
                  <xsl:value-of select="concat('processing-instruction(&#34;', name(.), '&#34;)[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of comment()">
                  <xsl:value-of select="concat('comment()[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of text()">
                  <xsl:value-of select="concat('text()[', $position, ']')"/>
               </xsl:when>
               <xsl:otherwise/>
            </xsl:choose>
         </xsl:for-each>
      </xsl:variable>
      <xsl:value-of select="concat('/', string-join($segments, '/'))"/>
   </xsl:function>
</xsl:transform>
