<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform xmlns:error="https://doi.org/10.5281/zenodo.1495494#error"
               xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"
               xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
               xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
               xmlns:sch="http://purl.oclc.org/dsdl/schematron"
               xmlns:schxslt="https://doi.org/10.5281/zenodo.1495494"
               xmlns:schxslt-api="https://doi.org/10.5281/zenodo.1495494#api"
               xmlns:sdg="http://data.europa.eu/p4s"
               xmlns:x="https://www.w3.org/TR/REC-html40"
               xmlns:xlink="http://www.w3.org/1999/xlink"
               xmlns:xs="http://www.w3.org/2001/XMLSchema"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               version="2.0">
   <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:dct="http://purl.org/dc/terms/"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
      <dct:creator>
         <dct:Agent>
            <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
            <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
         </dct:Agent>
      </dct:creator>
      <dct:created>2024-09-08T11:21:08.67303+02:00</dct:created>
   </rdf:Description>
   <xsl:output indent="yes"/>
   <xsl:param name="EEA_COUNTRY-CODELIST_code"
              select="tokenize('AT BE BG HR CY CZ DK EE FI FR DE EL HU IS IE IT LV LI LT LU MT NL NO PL PT RO SK SI ES SE', '\s')"/>
   <xsl:param name="AGENTCLASSIFICATION-CODELIST_code"
              select="tokenize('ER IP EP ERRP', '\s')"/>
   <xsl:param name="OOTSMEDIATYPES-CODELIST_code"
              select="tokenize('image/jpeg application/json image/png application/pdf application/xml image/svg+xml', '\s')"/>
   <xsl:param name="LANGUAGECODE-CODELIST_code"
              select="tokenize('AA AB AE AF AK AM AN AR AS AV AY AZ BA BE BG BH BI BM BN BO BO BR BS CA CE CH CO CR CS CS CU CV CY CY DA DE DE DV DZ EE EL EL EN EO ES ET EU EU FA FA FF FI FJ FO FR FR FY GA GD GL GN GU GV HA HE HI HO HR HT HU HY HY HZ IA ID IE IG II IK IO IS IS IT IU JA JV KA KA KG KI KJ KK KL KM KN KO KR KS KU KV KW KY LA LB LG LI LN LO LT LU LV MG MH MI MI MK MK ML MN MR MS MS MT MY MY NA NB ND NE NG NL NL NN NO NR NV NY OC OJ OM OR OS PA PI PL PS PT QU RM RN RO RO RU RW SA SC SD SE SG SI SK SK SL SM SN SO SQ SQ SR SS ST SU SV SW TA TE TG TH TI TK TL TN TO TR TS TT TW TY UG UK UR UZ VE VI VO WA WO XH YI YO ZA ZH ZH ZU', '\s')"/>
   <xsl:param name="EAS_Code"
              select="tokenize('0002 0007 0009 0037 0060 0088 0096 0097 0106 0130 0135 0142 0147 0151 0170 0183 0184 0188 0190 0191 0192 0193 0194 0195 0196 0198 0199 0200 0201 0202 0203 0204 0205 0208 0209 0210 0211 0212 0213 0215 0216 0217 0218 0221 0225 0230 9901 9910 9913 9914 9915 9918 9919 9920 9922 9923 9924 9925 9926 9927 9928 9929 9930 9931 9932 9933 9934 9935 9936 9937 9938 9939 9940 9941 9942 9943 9944 9945 9946 9947 9948 9949 9950 9951 9952 9953 9957 9959 AN AQ AS AU EM', '\s')"/>
   <xsl:param name="COUNTRYIDENTIFICATIONCODE-CODELIST_code"
              select="tokenize('AF AL DZ AS AD AO AI AQ AG AR AM AW AU AT AZ BS BH BD BB BY BE BZ BJ BM BT BO BQ BA BW BV BR IO BN BG BF BI CV KH CM CA KY CF TD CL CN CX CC CO KM CD CG CK CR HR CU CW CY CZ CI DK DJ DM DO EC EG SV GQ ER EE SZ ET FK FO FJ FI FR GF PF TF GA GM GE DE GH GI EL GL GD GP GU GT GG GN GW GY HT HM VA HN HK HU IS IN ID IR IQ IE IM IL IT JM JP JE JO KZ KE KI KP KR KW KG LA LV LB LS LR LY LI LT LU MO MG MW MY MV ML MT MH MQ MR MU YT MX FM MD MC MN ME MS MA MZ MM NA NR NP NL NC NZ NI NE NG NU NF MK MP NO OM PK PW PS PA PG PY PE PH PN PL PT PR QA RO RU RW RE BL SH KN LC MF PM VC WS SM ST SA SN RS SC SL SG SX SK SI SB SO ZA GS SS ES LK SD SR SJ SE CH SY TW TJ TZ TH TL TG TK TO TT TN TR TM TC TV UG UA AE GB UM US UY UZ VU VE VN VG VI WF EH YE ZM ZW AX', '\s')"/>
   <xsl:param name="EDMERRORCODES-CODELIST_code"
              select="tokenize('EDM:ERR:0001 EDM:ERR:0002 EDM:ERR:0003 EDM:ERR:0004 EDM:ERR:0005 EDM:ERR:0006 EDM:ERR:0007 EDM:ERR:0008', '\s')"/>
   <xsl:param name="EDMERRORCODES-CODELIST_name-Type"
              select="tokenize('rs:AuthenticationExceptionType rs:AuthorizationExceptionType rs:InvalidRequestExceptionType rs:ObjectNotFoundExceptionType rs:TimeoutExceptionType rs:UnresolvedReferenceExceptionType rs:UnsupportedCapabilityExceptionType rs:QueryExceptionType', '\s')"/>
   <xsl:param name="ERRORSEVERITY-CODELIST_code"
              select="tokenize('urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:DSDErrorResponse:AdditionalInput urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired', '\s')"/>
   <xsl:template match="root()">
      <xsl:variable name="metadata" as="element()?">
         <svrl:metadata xmlns:dct="http://purl.org/dc/terms/"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                        xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
            <dct:creator>
               <dct:Agent>
                  <skos:prefLabel>
                     <xsl:value-of separator="/"
                                   select="(system-property('xsl:product-name'), system-property('xsl:product-version'))"/>
                  </skos:prefLabel>
               </dct:Agent>
            </dct:creator>
            <dct:created>
               <xsl:value-of select="current-dateTime()"/>
            </dct:created>
            <dct:source>
               <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/">
                  <dct:creator>
                     <dct:Agent>
                        <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
                        <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
                     </dct:Agent>
                  </dct:creator>
                  <dct:created>2024-09-08T11:21:08.67303+02:00</dct:created>
               </rdf:Description>
            </dct:source>
         </svrl:metadata>
      </xsl:variable>
      <xsl:variable name="report" as="element(schxslt:report)">
         <schxslt:report>
            <xsl:call-template name="d22e43"/>
         </schxslt:report>
      </xsl:variable>
      <xsl:variable name="schxslt:report" as="node()*">
         <xsl:sequence select="$metadata"/>
         <xsl:for-each select="$report/schxslt:document">
            <xsl:for-each select="schxslt:pattern">
               <xsl:sequence select="node()"/>
               <xsl:sequence select="../schxslt:rule[@pattern = current()/@id]/node()"/>
            </xsl:for-each>
         </xsl:for-each>
      </xsl:variable>
      <svrl:schematron-output xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <svrl:ns-prefix-in-attribute-values prefix="sdg" uri="http://data.europa.eu/p4s"/>
         <svrl:ns-prefix-in-attribute-values prefix="rs" uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="rim" uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="query" uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
         <svrl:ns-prefix-in-attribute-values prefix="xlink" uri="http://www.w3.org/1999/xlink"/>
         <svrl:ns-prefix-in-attribute-values prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
         <xsl:sequence select="$schxslt:report"/>
      </svrl:schematron-output>
   </xsl:template>
   <xsl:template match="text() | @*" mode="#all" priority="-10"/>
   <xsl:template match="/" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template match="*" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="@*"/>
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template name="d22e43">
      <schxslt:document>
         <schxslt:pattern id="d22e43">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e52">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e61">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e70">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e80">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e89">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e98">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e107">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e116">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e125">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e135">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e144">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e153">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e162">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e171">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e180">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e190">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e199">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <xsl:apply-templates mode="d22e43" select="root()"/>
      </schxslt:document>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue"
                 priority="24"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e43']">
            <schxslt:rule pattern="d22e43">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e43">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(rim:Value='oots-edm:v1.1')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C001">
                     <xsl:attribute name="test">rim:Value='oots-edm:v1.1'</xsl:attribute>
                     <svrl:text>The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-edm:v1.1".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e43')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceResponseIdentifier']/rim:SlotValue/rim:Value"
                 priority="23"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e52']">
            <schxslt:rule pattern="d22e52">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceResponseIdentifier']/rim:SlotValue/rim:Value" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceResponseIdentifier']/rim:SlotValue/rim:Value</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e52">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceResponseIdentifier']/rim:SlotValue/rim:Value</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C002">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The 'rim:Value' of the 'EvidenceResponseIdentifier' MUST be unique UUID (RFC 4122) for each response.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e52')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier"
                 priority="22"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e61']">
            <schxslt:rule pattern="d22e61">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e61">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C003">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e61')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent"
                 priority="21"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e70']">
            <schxslt:rule pattern="d22e70">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e70">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:Classification)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C007">
                     <xsl:attribute name="test">not(normalize-space(sdg:Classification)='')</xsl:attribute>
                     <svrl:text>The value for 'Agent/Classification' MUST be provided.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e70')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier"
                 priority="20"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e80']">
            <schxslt:rule pattern="d22e80">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e80">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C009">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e80')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure']"
                 priority="19"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e89']">
            <schxslt:rule pattern="d22e89">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e89">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(rs:Exception)=1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C011">
                     <xsl:attribute name="test">count(rs:Exception)=1</xsl:attribute>
                     <svrl:text>The 'rs:Exception' class of a 'QueryResponse' MUST be present if  'status' attribute of a 'QueryResponse' is 
            "urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e89')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rs:Exception"
                 priority="18"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e98']">
            <schxslt:rule pattern="d22e98">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rs:Exception" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e98">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@xsi:type)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C012">
                     <xsl:attribute name="test">@xsi:type</xsl:attribute>
                     <svrl:text>The 'xsi:type' attribute of a 'rs:Exception' MUST be present. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e98')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rs:Exception[@code != 'EDM:ERR:0002']"
                 priority="17"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e107']">
            <schxslt:rule pattern="d22e107">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rs:Exception[@code != 'EDM:ERR:0002']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception[@code != 'EDM:ERR:0002']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e107">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception[@code != 'EDM:ERR:0002']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(@severity,'urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C015">
                     <xsl:attribute name="test">matches(@severity,'urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error')</xsl:attribute>
                     <svrl:text>The value of 'severity' attribute of a 'rs:Exception' MUST  be 'urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error' if the 
            rs:Exception  (@code 'EDM:ERR:0002') is NOT used.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e107')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rs:Exception"
                 priority="16"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e116']">
            <schxslt:rule pattern="d22e116">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rs:Exception" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e116">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@message)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C016">
                     <xsl:attribute name="test">@message</xsl:attribute>
                     <svrl:text>The 'message' attribute of a 'rs:Exception' MUST be present. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e116')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rs:Exception"
                 priority="15"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e125']">
            <schxslt:rule pattern="d22e125">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rs:Exception" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e125">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C026">
                     <xsl:attribute name="test">@code</xsl:attribute>
                     <svrl:text>The 'code' attribute of a 'rs:Exception' MUST be present. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e125')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rs:Exception/rim:Slot[@name='Timestamp']/rim:SlotValue/rim:Value"
                 priority="14"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e135']">
            <schxslt:rule pattern="d22e135">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rs:Exception/rim:Slot[@name='Timestamp']/rim:SlotValue/rim:Value" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/rim:Slot[@name='Timestamp']/rim:SlotValue/rim:Value</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e135">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/rim:Slot[@name='Timestamp']/rim:SlotValue/rim:Value</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C018">
                     <xsl:attribute name="test">matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}','i')</xsl:attribute>
                     <svrl:text>The 'rim:Value' of 'Timestamp' MUST be according to xsd:dateTime.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e135')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewLocation']/rim:SlotValue"
                 priority="13"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e144']">
            <schxslt:rule pattern="d22e144">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewLocation']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewLocation']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e144">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewLocation']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(starts-with(lower-case(rim:Value/text()), 'https://'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C019">
                     <xsl:attribute name="test">starts-with(lower-case(rim:Value/text()), 'https://')</xsl:attribute>
                     <svrl:text>The 'rim:Value' of a 'PreviewLocation' MUST be a URI starting with 'https://' according to RFC 3986.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e144')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewMethod']/rim:SlotValue"
                 priority="12"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e153']">
            <schxslt:rule pattern="d22e153">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewMethod']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewMethod']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e153">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewMethod']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(rim:Value, '^GET$') or matches(rim:Value, '^POST$') or matches(rim:Value, '^PUT$'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C021">
                     <xsl:attribute name="test">matches(rim:Value, '^GET$') or matches(rim:Value, '^POST$') or matches(rim:Value, '^PUT$')</xsl:attribute>
                     <svrl:text>The 'rim:Value' of a 'PreviewMethod' MUST be either the HTTP verb 'GET', 'POST' or 'PUT'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e153')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewLocation']"
                 priority="11"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e162']">
            <schxslt:rule pattern="d22e162">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewLocation']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewLocation']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e162">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewLocation']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(//@severity='urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C022">
                     <xsl:attribute name="test">//@severity='urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired'</xsl:attribute>
                     <svrl:text>If a 'rim:Slot[@name='PreviewLocation']' is provided, the 'rs:Exception' MUST  be 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired' and use the rs:Exception xsi:type='rs:AuthorizationExceptionType' (@code 'EDM:ERR:0002').</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e162')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue"
                 priority="10"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e171']">
            <schxslt:rule pattern="d22e171">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e171">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(sdg:Agent)=1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C023">
                     <xsl:attribute name="test">count(sdg:Agent)=1</xsl:attribute>
                     <svrl:text>In the ErrorProvider slot there MUST be a single agent.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e171')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Address"
                 priority="9"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e180']">
            <schxslt:rule pattern="d22e180">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Address" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Address</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e180">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Address</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(sdg:AdminUnitLevel1)=1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C024">
                     <xsl:attribute name="test">count(sdg:AdminUnitLevel1)=1</xsl:attribute>
                     <svrl:text>A value for 'AdminUnitLevel1' MUST be provided.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e180')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse" priority="8" mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e190']">
            <schxslt:rule pattern="d22e190">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e190">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@requestId or rs:Exception/@xsi:type='rs:InvalidRequestExceptionType')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C025">
                     <xsl:attribute name="test">@requestId or rs:Exception/@xsi:type='rs:InvalidRequestExceptionType'</xsl:attribute>
                     <svrl:text>If  the query:QueryResponse does not contain a requestId, the 'rs:Exception' MUST use the rs:Exception xsi:type='rs:InvalidRequestExceptionType' (@code 'EDM:ERR:0003') and no other value.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e190')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier"
                 priority="7"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:variable name="suffix1"
                    select="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e199']">
            <schxslt:rule pattern="d22e199">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e199">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C010">
                     <xsl:attribute name="test">((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256</xsl:attribute>
                     <svrl:text>The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_CountryCodeList’. For testing purposes the code "oots" can be used.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e199')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier"
                 priority="6"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:variable name="suffix1"
                    select="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e199']">
            <schxslt:rule pattern="d22e199">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e199">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C004">
                     <xsl:attribute name="test">((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256</xsl:attribute>
                     <svrl:text>The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_CountryCodeList’. For testing purposes the code "oots" can be used. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e199')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1"
                 priority="5"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e199']">
            <schxslt:rule pattern="d22e199">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e199">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C005">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e199')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent"
                 priority="4"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e199']">
            <schxslt:rule pattern="d22e199">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e199">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not((some $code in $AGENTCLASSIFICATION-CODELIST_code satisfies sdg:Classification=$code) and (matches(sdg:Classification,'EP') or matches(sdg:Classification,'IP') or matches(sdg:Classification,'ERRP')))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C008">
                     <xsl:attribute name="test">(some $code in $AGENTCLASSIFICATION-CODELIST_code satisfies sdg:Classification=$code) and (matches(sdg:Classification,'EP') or matches(sdg:Classification,'IP') or matches(sdg:Classification,'ERRP'))</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="sdg:Classification"/>' is unacceptable for constraints identified by 'AgentClassification-CodeList' in the context 'query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Classification'.Default value: ERRP (Error Provider). The code 'ER' shall not be used by this transaction.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e199')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rs:Exception/@xsi:type"
                 priority="3"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e199']">
            <schxslt:rule pattern="d22e199">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rs:Exception/@xsi:type" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/@xsi:type</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e199">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/@xsi:type</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $EDMERRORCODES-CODELIST_name-Type satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="ERROR"
                                      id="R-EDM-ERR-C013">
                     <xsl:attribute name="test">some $code in $EDMERRORCODES-CODELIST_name-Type satisfies .=$code</xsl:attribute>
                     <svrl:text>
            Value supplied '<xsl:value-of select="@xsi:type"/>' is unacceptable for constraints identified by 'EDMErrorCodes-CodeList' in the context 'query:QueryResponse/rs:Exception/@xsi:type' check <xsl:value-of select="."/>
                     </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e199')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rs:Exception/@code"
                 priority="2"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e199']">
            <schxslt:rule pattern="d22e199">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rs:Exception/@code" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/@code</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e199">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/@code</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $EDMERRORCODES-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C017">
                     <xsl:attribute name="test">some $code in $EDMERRORCODES-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'EDMErrorCodes-CodeList' in the context 'query:QueryResponse/rs:Exception/@code'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e199')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rs:Exception/@severity"
                 priority="1"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e199']">
            <schxslt:rule pattern="d22e199">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rs:Exception/@severity" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/@severity</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e199">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/@severity</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not((some $code in $ERRORSEVERITY-CODELIST_code satisfies .=$code) and not(.='urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:DSDErrorResponse:AdditionalInput') )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C014">
                     <xsl:attribute name="test">(some $code in $ERRORSEVERITY-CODELIST_code satisfies .=$code) and not(.='urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:DSDErrorResponse:AdditionalInput') </xsl:attribute>
                     <svrl:text>The 'severity' attribute of a 'rs:Exception' MUST be present. It must be part of the code list 'ErrorSeverity' (Error Severity). The code 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:DSDErrorResponse:AdditionalInput' shall not be used by this transaction.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e199')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewDescription']/rim:SlotValue/rim:Value/rim:LocalizedString/@xml:lang"
                 priority="0"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e199']">
            <schxslt:rule pattern="d22e199">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewDescription']/rim:SlotValue/rim:Value/rim:LocalizedString/@xml:lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewDescription']/rim:SlotValue/rim:Value/rim:LocalizedString/@xml:lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e199">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewDescription']/rim:SlotValue/rim:Value/rim:LocalizedString/@xml:lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ERR-C020">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewDescription']/rim:SlotValue/rim:Value/rim:LocalizedString/@*[namespace-uri()='http://www.w3.org/XML/1998/namespace' and local-name()='lang']'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e199')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:function name="schxslt:location" as="xs:string">
      <xsl:param name="node" as="node()"/>
      <xsl:variable name="segments" as="xs:string*">
         <xsl:for-each select="($node/ancestor-or-self::node())">
            <xsl:variable name="position">
               <xsl:number level="single"/>
            </xsl:variable>
            <xsl:choose>
               <xsl:when test=". instance of element()">
                  <xsl:value-of select="concat('Q{', namespace-uri(.), '}', local-name(.), '[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of attribute()">
                  <xsl:value-of select="concat('@Q{', namespace-uri(.), '}', local-name(.))"/>
               </xsl:when>
               <xsl:when test=". instance of processing-instruction()">
                  <xsl:value-of select="concat('processing-instruction(&#34;', name(.), '&#34;)[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of comment()">
                  <xsl:value-of select="concat('comment()[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of text()">
                  <xsl:value-of select="concat('text()[', $position, ']')"/>
               </xsl:when>
               <xsl:otherwise/>
            </xsl:choose>
         </xsl:for-each>
      </xsl:variable>
      <xsl:value-of select="concat('/', string-join($segments, '/'))"/>
   </xsl:function>
</xsl:transform>
