<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform xmlns:S12="http://www.w3.org/2003/05/soap-envelope"
               xmlns:eb="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/"
               xmlns:error="https://doi.org/10.5281/zenodo.1495494#error"
               xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               xmlns:sch="http://purl.oclc.org/dsdl/schematron"
               xmlns:schxslt="https://doi.org/10.5281/zenodo.1495494"
               xmlns:schxslt-api="https://doi.org/10.5281/zenodo.1495494#api"
               xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"
               xmlns:xlink="http://www.w3.org/1999/xlink"
               xmlns:xs="http://www.w3.org/2001/XMLSchema"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               version="2.0">
   <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:dct="http://purl.org/dc/terms/"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
      <dct:creator>
         <dct:Agent>
            <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
            <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
         </dct:Agent>
      </dct:creator>
      <dct:created>2024-09-08T11:21:11.857705+02:00</dct:created>
   </rdf:Description>
   <xsl:output indent="yes"/>
   <xsl:param name="EAS_Code"
              select="tokenize('0002 0007 0009 0037 0060 0088 0096 0097 0106 0130 0135 0142 0147 0151 0170 0183 0184 0188 0190 0191 0192 0193 0194 0195 0196 0198 0199 0200 0201 0202 0203 0204 0205 0208 0209 0210 0211 0212 0213 0215 0216 0217 0218 0221 0225 0230 9901 9910 9913 9914 9915 9918 9919 9920 9922 9923 9924 9925 9926 9927 9928 9929 9930 9931 9932 9933 9934 9935 9936 9937 9938 9939 9940 9941 9942 9943 9944 9945 9946 9947 9948 9949 9950 9951 9952 9953 9957 9959 AN AQ AS AU EM', '\s')"/>
   <xsl:param name="EEA_COUNTRY-CODELIST_code"
              select="tokenize('AT BE BG HR CY CZ DK EE FI FR DE EL HU IS IE IT LV LI LT LU MT NL NO PL PT RO SK SI ES SE', '\s')"/>
   <xsl:param name="OOTSMEDIATYPES-CODELIST_code"
              select="tokenize('image/jpeg application/json image/png application/pdf application/xml image/svg+xml', '\s')"/>
   <xsl:template match="root()">
      <xsl:variable name="metadata" as="element()?">
         <svrl:metadata xmlns:dct="http://purl.org/dc/terms/"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                        xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
            <dct:creator>
               <dct:Agent>
                  <skos:prefLabel>
                     <xsl:value-of separator="/"
                                   select="(system-property('xsl:product-name'), system-property('xsl:product-version'))"/>
                  </skos:prefLabel>
               </dct:Agent>
            </dct:creator>
            <dct:created>
               <xsl:value-of select="current-dateTime()"/>
            </dct:created>
            <dct:source>
               <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/">
                  <dct:creator>
                     <dct:Agent>
                        <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
                        <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
                     </dct:Agent>
                  </dct:creator>
                  <dct:created>2024-09-08T11:21:11.857705+02:00</dct:created>
               </rdf:Description>
            </dct:source>
         </svrl:metadata>
      </xsl:variable>
      <xsl:variable name="report" as="element(schxslt:report)">
         <schxslt:report>
            <xsl:call-template name="d16e34"/>
         </schxslt:report>
      </xsl:variable>
      <xsl:variable name="schxslt:report" as="node()*">
         <xsl:sequence select="$metadata"/>
         <xsl:for-each select="$report/schxslt:document">
            <xsl:for-each select="schxslt:pattern">
               <xsl:sequence select="node()"/>
               <xsl:sequence select="../schxslt:rule[@pattern = current()/@id]/node()"/>
            </xsl:for-each>
         </xsl:for-each>
      </xsl:variable>
      <svrl:schematron-output xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <svrl:ns-prefix-in-attribute-values prefix="eb"
                                             uri="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/"/>
         <svrl:ns-prefix-in-attribute-values prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
         <svrl:ns-prefix-in-attribute-values prefix="xlink" uri="http://www.w3.org/1999/xlink"/>
         <svrl:ns-prefix-in-attribute-values prefix="xml" uri="http://www.w3.org/XML/1998/namespace"/>
         <svrl:ns-prefix-in-attribute-values prefix="S12" uri="http://www.w3.org/2003/05/soap-envelope"/>
         <svrl:ns-prefix-in-attribute-values prefix="wsu"
                                             uri="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"/>
         <xsl:sequence select="$schxslt:report"/>
      </svrl:schematron-output>
   </xsl:template>
   <xsl:template match="text() | @*" mode="#all" priority="-10"/>
   <xsl:template match="/" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template match="*" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="@*"/>
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template name="d16e34">
      <schxslt:document>
         <schxslt:pattern id="d16e34">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e46">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e55">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e68">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e77">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e94">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e109">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e124">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e140">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e149">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e158">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e167">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e189">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e201">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e215">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e233">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e248">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e257">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d16e266">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <xsl:apply-templates mode="d16e34" select="root()"/>
      </schxslt:document>
   </xsl:template>
   <xsl:template match="//eb:Messaging/@*" priority="20" mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="namespace" select="namespace-uri()"/>
      <xsl:variable name="attribute" select="local-name()"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e34']">
            <schxslt:rule pattern="d16e34">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/@*" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/@*</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e34">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/@*</xsl:attribute>
               </svrl:fired-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e34')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging" priority="19" mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e46']">
            <schxslt:rule pattern="d16e46">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e46">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(eb:SignalMessage) + count(eb:UserMessage/eb:MessageInfo/eb:RefToMessageId) +              count(eb:UserMessage/eb:CollaborationInfo/eb:AgreementRef) + count(eb:UserMessage/eb:PayloadInfo/eb:PartInfo/eb:Schema) +             count(eb:UserMessage/eb:PayloadInfo/eb:PartInfo/eb:Description) = 0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-001">
                     <xsl:attribute name="test">count(eb:SignalMessage) + count(eb:UserMessage/eb:MessageInfo/eb:RefToMessageId) +              count(eb:UserMessage/eb:CollaborationInfo/eb:AgreementRef) + count(eb:UserMessage/eb:PayloadInfo/eb:PartInfo/eb:Schema) +             count(eb:UserMessage/eb:PayloadInfo/eb:PartInfo/eb:Description) = 0</xsl:attribute>
                     <svrl:text>The eb:Messaging envelope should not contain the elements 'SignalMessage', 'MessageInfo/RefToMessageId', 'CollaborationInfo/AgreementRef', 'PartInfo/Schema' and 'PartInfo/Description'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e46')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage" priority="18" mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e55']">
            <schxslt:rule pattern="d16e55">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e55">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(eb:MessageProperties)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-002">
                     <xsl:attribute name="test">eb:MessageProperties</xsl:attribute>
                     <svrl:text>There MUST be exactly one element eb:MessageProperties.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(eb:PayloadInfo)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-003">
                     <xsl:attribute name="test">eb:PayloadInfo</xsl:attribute>
                     <svrl:text>There MUST be one element eb:PayloadInfo.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e55')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:MessageInfo/eb:MessageId"
                 priority="17"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e68']">
            <schxslt:rule pattern="d16e68">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:MessageInfo/eb:MessageId" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:MessageInfo/eb:MessageId</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e68">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:MessageInfo/eb:MessageId</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-z0-9!#$%*+/=?^_`|~-]+(?:\.[a-z0-9!#$%*+/=?^_`|~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-004">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-z0-9!#$%*+/=?^_`|~-]+(?:\.[a-z0-9!#$%*+/=?^_`|~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$','i')</xsl:attribute>
                     <svrl:text>eb:MessageId MUST be expressed according to RFC2822.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e68')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From"
                 priority="16"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e77']">
            <schxslt:rule pattern="d16e77">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e77">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(eb:PartyId) = 1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-005">
                     <xsl:attribute name="test">count(eb:PartyId) = 1</xsl:attribute>
                     <svrl:text>The eb:PartyId MUST be used only once.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e77')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To"
                 priority="15"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e77']">
            <schxslt:rule pattern="d16e77">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e77">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(eb:PartyId) = 1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-009">
                     <xsl:attribute name="test">count(eb:PartyId) = 1</xsl:attribute>
                     <svrl:text>The eb:PartyId MUST be used only once.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e77')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId"
                 priority="14"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@type, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:variable name="suffix1"
                    select="substring-after(@type, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e94']">
            <schxslt:rule pattern="d16e94">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e94">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-010">
                     <xsl:attribute name="test">((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256</xsl:attribute>
                     <svrl:text>
            The value of the 'eb:PartyId/@type' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country-CodeList’. For testing purposes the code "oots" can be used.
         </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e94')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:Role"
                 priority="13"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e109']">
            <schxslt:rule pattern="d16e109">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:Role" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:Role</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e109">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:Role</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^http://sdg.europa.eu/edelivery/gateway$'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-008">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^http://sdg.europa.eu/edelivery/gateway$')</xsl:attribute>
                     <svrl:text>The eb:Role MUST be set to fixed value http://sdg.europa.eu/edelivery/gateway.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e109')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:Role"
                 priority="12"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e109']">
            <schxslt:rule pattern="d16e109">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:Role" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:Role</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e109">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:Role</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^http://sdg.europa.eu/edelivery/gateway$'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-012">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^http://sdg.europa.eu/edelivery/gateway$')</xsl:attribute>
                     <svrl:text>The eb:Role MUST be set to fixed value http://sdg.europa.eu/edelivery/gateway.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e109')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Service"
                 priority="11"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e124']">
            <schxslt:rule pattern="d16e124">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Service" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Service</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e124">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Service</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^QueryManager$'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-013">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^QueryManager$')</xsl:attribute>
                     <svrl:text>The eb:Service MUST be set to fixed value 'QueryManager'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(@type)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-014">
                     <xsl:attribute name="test">@type</xsl:attribute>
                     <svrl:text>The eb:Service/@type MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(matches(normalize-space(@type),'^urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0$'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-015">
                     <xsl:attribute name="test">matches(normalize-space(@type),'^urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0$')</xsl:attribute>
                     <svrl:text>The eb:Service/@type MUST be set to fixed value 'urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e124')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Action"
                 priority="10"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e140']">
            <schxslt:rule pattern="d16e140">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Action" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Action</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e140">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Action</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^ExecuteQueryRequest$') or matches(normalize-space(text()),'^ExecuteQueryResponse$') or matches(normalize-space(text()),'^ExceptionResponse$'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-016">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^ExecuteQueryRequest$') or matches(normalize-space(text()),'^ExecuteQueryResponse$') or matches(normalize-space(text()),'^ExceptionResponse$')</xsl:attribute>
                     <svrl:text>eb:Action MUST use the values 'ExecuteQueryRequest' 'ExecuteQueryResponse' or 'ExceptionResponse'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e140')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId"
                 priority="9"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e149']">
            <schxslt:rule pattern="d16e149">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e149">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-017">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The eb:ConversationId MUST be expressed as UUID.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e149')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:MessageProperties"
                 priority="8"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e158']">
            <schxslt:rule pattern="d16e158">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:MessageProperties" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:MessageProperties</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e158">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:MessageProperties</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(eb:Property) = 2)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-018">
                     <xsl:attribute name="test">count(eb:Property) = 2</xsl:attribute>
                     <svrl:text>There MUST be exately two elements eb:Property of eb:MessageProperties.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e158')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property"
                 priority="7"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@type, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:variable name="suffix1"
                    select="substring-after(@type, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e167']">
            <schxslt:rule pattern="d16e167">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e167">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(@name),'^originalSender$') or matches(normalize-space(@name),'^finalRecipient$'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-019">
                     <xsl:attribute name="test">matches(normalize-space(@name),'^originalSender$') or matches(normalize-space(@name),'^finalRecipient$')</xsl:attribute>
                     <svrl:text>One of the values from eb:Property/@name uses 'originalSender' and the other one 'finalRecipient'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(@type)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-020">
                     <xsl:attribute name="test">@type</xsl:attribute>
                     <svrl:text>There MUST be one element eb:Property/@type for eb:Property.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-021">
                     <xsl:attribute name="test">((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256</xsl:attribute>
                     <svrl:text>
            The value of the 'eb:PartyId/@type' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country-CodeList’. For testing purposes the code "oots" can be used.
         </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e167')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage" priority="6" mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e189']">
            <schxslt:rule pattern="d16e189">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e189">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not((count(eb:PayloadInfo/eb:PartInfo) = 1 and (matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExecuteQueryRequest$') or matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExceptionResponse$'))) or not(matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExecuteQueryRequest$') or matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExceptionResponse$')))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-023">
                     <xsl:attribute name="test">(count(eb:PayloadInfo/eb:PartInfo) = 1 and (matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExecuteQueryRequest$') or matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExceptionResponse$'))) or not(matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExecuteQueryRequest$') or matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExceptionResponse$'))</xsl:attribute>
                     <svrl:text>There MUST be exactly one element eb:PartInfo in the eb:PayloadInfo if the eb:Action value is 'ExecuteQueryRequest' or 'ExceptionResponse'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not((count(eb:PayloadInfo/eb:PartInfo) &gt; 0 and matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExecuteQueryResponse$')) or not(matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExecuteQueryResponse$')))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-024">
                     <xsl:attribute name="test">(count(eb:PayloadInfo/eb:PartInfo) &gt; 0 and matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExecuteQueryResponse$')) or not(matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExecuteQueryResponse$'))</xsl:attribute>
                     <svrl:text>There MUST be at least one element eb:PartInfo in the eb:PayloadInfo if the eb:Action value is 'ExecuteQueryResponse'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e189')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:PayloadInfo"
                 priority="5"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="mt"
                    select="eb:PartInfo/eb:PartProperties/eb:Property[@name='MimeType']"/>
      <xsl:variable name="total" select="$mt[text() = 'application/x-ebrs+xml']"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e201']">
            <schxslt:rule pattern="d16e201">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:PayloadInfo" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PayloadInfo</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e201">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PayloadInfo</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count($mt) &gt;= 1 and count($total) = 1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-030">
                     <xsl:attribute name="test">count($mt) &gt;= 1 and count($total) = 1</xsl:attribute>
                     <svrl:text>In the message there MUST be exactly one eb:PartInfo/eb:PartProperties/eb:Property with name="MimeType" that has the fixed value 'application/x-ebrs+xml'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e201')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:PayloadInfo/eb:PartInfo"
                 priority="4"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e215']">
            <schxslt:rule pattern="d16e215">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:PayloadInfo/eb:PartInfo" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PayloadInfo/eb:PartInfo</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e215">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PayloadInfo/eb:PartInfo</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@href)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-025">
                     <xsl:attribute name="test">@href</xsl:attribute>
                     <svrl:text>The attribute href of eb:PartInfo MUST be provided.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(matches(normalize-space(@href),'^cid:[a-z0-9!#$%*+/=?^_`|~-]+(?:\.[a-z0-9!#$%*+/=?^_`|~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-026">
                     <xsl:attribute name="test">matches(normalize-space(@href),'^cid:[a-z0-9!#$%*+/=?^_`|~-]+(?:\.[a-z0-9!#$%*+/=?^_`|~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$','i')</xsl:attribute>
                     <svrl:text>eb:PartInfo/@href MUST use the prefix 'cid:' and be expressed according to RFC2822.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(matches(normalize-space(@href),'^cid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}@[a-z]*','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-027">
                     <xsl:attribute name="test">matches(normalize-space(@href),'^cid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}@[a-z]*','i')</xsl:attribute>
                     <svrl:text>In eb:PartInfo/@href, the value on the right side of the prefix 'cid:' and left from the symbol '@' MUST be a valid UUID.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(eb:PartProperties)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-028">
                     <xsl:attribute name="test">eb:PartProperties</xsl:attribute>
                     <svrl:text>There MUST be exactly one element eb:PartProperties.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e215')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage/eb:PayloadInfo/eb:PartInfo/eb:PartProperties"
                 priority="3"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e233']">
            <schxslt:rule pattern="d16e233">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage/eb:PayloadInfo/eb:PartInfo/eb:PartProperties" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PayloadInfo/eb:PartInfo/eb:PartProperties</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e233">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage/eb:PayloadInfo/eb:PartInfo/eb:PartProperties</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(eb:Property) = 1 or count(eb:Property) = 2)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-029">
                     <xsl:attribute name="test">count(eb:Property) = 1 or count(eb:Property) = 2</xsl:attribute>
                     <svrl:text>There MUST be one or two eb:Property of eb:PartProperties.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(count(eb:Property[@name='MimeType']) = 1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-033">
                     <xsl:attribute name="test">count(eb:Property[@name='MimeType']) = 1</xsl:attribute>
                     <svrl:text>There MUST be one eb:Property/@name with the fixed value "MimeType".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(count(eb:Property) = 1 or count(eb:Property) = count(eb:Property[@name='MimeType']) + count(eb:Property[@name='CompressionType']))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-035">
                     <xsl:attribute name="test">count(eb:Property) = 1 or count(eb:Property) = count(eb:Property[@name='MimeType']) + count(eb:Property[@name='CompressionType'])</xsl:attribute>
                     <svrl:text>If there is a second eb:Property the eb:Property MUST have a name attribute with the fixed value "CompressionType".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e233')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="eb:PartProperties/eb:Property[@name='CompressionType']"
                 priority="2"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e248']">
            <schxslt:rule pattern="d16e248">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "eb:PartProperties/eb:Property[@name='CompressionType']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">eb:PartProperties/eb:Property[@name='CompressionType']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e248">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">eb:PartProperties/eb:Property[@name='CompressionType']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(text()='application/gzip')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-034">
                     <xsl:attribute name="test">text()='application/gzip'</xsl:attribute>
                     <svrl:text>A eb:Property with name="CompressionType" MUST have the fixed value 'application/gzip'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e248')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage[eb:CollaborationInfo/eb:Action/text() = 'ExecuteQueryResponse']/eb:PayloadInfo/eb:PartInfo/eb:PartProperties/eb:Property[@name='MimeType']"
                 priority="1"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e257']">
            <schxslt:rule pattern="d16e257">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage[eb:CollaborationInfo/eb:Action/text() = 'ExecuteQueryResponse']/eb:PayloadInfo/eb:PartInfo/eb:PartProperties/eb:Property[@name='MimeType']" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage[eb:CollaborationInfo/eb:Action/text() = 'ExecuteQueryResponse']/eb:PayloadInfo/eb:PartInfo/eb:PartProperties/eb:Property[@name='MimeType']</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e257">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage[eb:CollaborationInfo/eb:Action/text() = 'ExecuteQueryResponse']/eb:PayloadInfo/eb:PartInfo/eb:PartProperties/eb:Property[@name='MimeType']</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^application/x-ebrs\+xml$','i') or (some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-ebMS-031">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^application/x-ebrs\+xml$','i') or (some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code) </xsl:attribute>
                     <svrl:text>If the eb:Action value is 'ExecuteQueryResponse' the eb:Property with  name="MimeType" MUST be either 'application/x-ebrs+xml' or a a valid mime type of the code list 'OOTSMediaTypes'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e257')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="//eb:Messaging/eb:UserMessage[eb:CollaborationInfo/eb:Action/text() = 'ExecuteQueryResponse']/eb:PayloadInfo/eb:PartInfo[1]/eb:PartProperties"
                 priority="0"
                 mode="d16e34">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="mt" select="eb:Property[@name='MimeType']"/>
      <xsl:variable name="total" select="$mt[text() = 'application/x-ebrs+xml']"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd16e266']">
            <schxslt:rule pattern="d16e266">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "//eb:Messaging/eb:UserMessage[eb:CollaborationInfo/eb:Action/text() = 'ExecuteQueryResponse']/eb:PayloadInfo/eb:PartInfo[1]/eb:PartProperties" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage[eb:CollaborationInfo/eb:Action/text() = 'ExecuteQueryResponse']/eb:PayloadInfo/eb:PartInfo[1]/eb:PartProperties</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d16e266">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">//eb:Messaging/eb:UserMessage[eb:CollaborationInfo/eb:Action/text() = 'ExecuteQueryResponse']/eb:PayloadInfo/eb:PartInfo[1]/eb:PartProperties</xsl:attribute>
               </svrl:fired-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd16e266')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:function name="schxslt:location" as="xs:string">
      <xsl:param name="node" as="node()"/>
      <xsl:variable name="segments" as="xs:string*">
         <xsl:for-each select="($node/ancestor-or-self::node())">
            <xsl:variable name="position">
               <xsl:number level="single"/>
            </xsl:variable>
            <xsl:choose>
               <xsl:when test=". instance of element()">
                  <xsl:value-of select="concat('Q{', namespace-uri(.), '}', local-name(.), '[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of attribute()">
                  <xsl:value-of select="concat('@Q{', namespace-uri(.), '}', local-name(.))"/>
               </xsl:when>
               <xsl:when test=". instance of processing-instruction()">
                  <xsl:value-of select="concat('processing-instruction(&#34;', name(.), '&#34;)[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of comment()">
                  <xsl:value-of select="concat('comment()[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of text()">
                  <xsl:value-of select="concat('text()[', $position, ']')"/>
               </xsl:when>
               <xsl:otherwise/>
            </xsl:choose>
         </xsl:for-each>
      </xsl:variable>
      <xsl:value-of select="concat('/', string-join($segments, '/'))"/>
   </xsl:function>
</xsl:transform>
