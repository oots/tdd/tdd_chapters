<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform xmlns:error="https://doi.org/10.5281/zenodo.1495494#error"
               xmlns:lcm="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0"
               xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
               xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
               xmlns:sch="http://purl.oclc.org/dsdl/schematron"
               xmlns:schxslt="https://doi.org/10.5281/zenodo.1495494"
               xmlns:schxslt-api="https://doi.org/10.5281/zenodo.1495494#api"
               xmlns:sdg="http://data.europa.eu/p4s"
               xmlns:x="https://www.w3.org/TR/REC-html40"
               xmlns:xlink="http://www.w3.org/1999/xlink"
               xmlns:xs="http://www.w3.org/2001/XMLSchema"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               version="2.0">
   <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:dct="http://purl.org/dc/terms/"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
      <dct:creator>
         <dct:Agent>
            <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
            <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
         </dct:Agent>
      </dct:creator>
      <dct:created>2024-09-08T11:21:11.361107+02:00</dct:created>
   </rdf:Description>
   <xsl:output indent="yes"/>
   <xsl:template match="root()">
      <xsl:variable name="metadata" as="element()?">
         <svrl:metadata xmlns:dct="http://purl.org/dc/terms/"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                        xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
            <dct:creator>
               <dct:Agent>
                  <skos:prefLabel>
                     <xsl:value-of separator="/"
                                   select="(system-property('xsl:product-name'), system-property('xsl:product-version'))"/>
                  </skos:prefLabel>
               </dct:Agent>
            </dct:creator>
            <dct:created>
               <xsl:value-of select="current-dateTime()"/>
            </dct:created>
            <dct:source>
               <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/">
                  <dct:creator>
                     <dct:Agent>
                        <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
                        <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
                     </dct:Agent>
                  </dct:creator>
                  <dct:created>2024-09-08T11:21:11.361107+02:00</dct:created>
               </rdf:Description>
            </dct:source>
         </svrl:metadata>
      </xsl:variable>
      <xsl:variable name="report" as="element(schxslt:report)">
         <schxslt:report>
            <xsl:call-template name="d13e23"/>
         </schxslt:report>
      </xsl:variable>
      <xsl:variable name="schxslt:report" as="node()*">
         <xsl:sequence select="$metadata"/>
         <xsl:for-each select="$report/schxslt:document">
            <xsl:for-each select="schxslt:pattern">
               <xsl:sequence select="node()"/>
               <xsl:sequence select="../schxslt:rule[@pattern = current()/@id]/node()"/>
            </xsl:for-each>
         </xsl:for-each>
      </xsl:variable>
      <svrl:schematron-output xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <svrl:ns-prefix-in-attribute-values prefix="sdg" uri="http://data.europa.eu/p4s"/>
         <svrl:ns-prefix-in-attribute-values prefix="rs" uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="rim" uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="lcm" uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
         <svrl:ns-prefix-in-attribute-values prefix="xlink" uri="http://www.w3.org/1999/xlink"/>
         <svrl:ns-prefix-in-attribute-values prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
         <xsl:sequence select="$schxslt:report"/>
      </svrl:schematron-output>
   </xsl:template>
   <xsl:template match="text() | @*" mode="#all" priority="-10"/>
   <xsl:template match="/" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template match="*" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="@*"/>
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template name="d13e23">
      <schxslt:document>
         <schxslt:pattern id="d13e23">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e33">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e42">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e51">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e60">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e69">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e78">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e88">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e100">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e109">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e118">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e127">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e136">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e146">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e155">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e164">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e179">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e188">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e215">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e229">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e238">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e247">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e256">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d13e265">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <xsl:apply-templates mode="d13e23" select="root()"/>
      </schxslt:document>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier"
                 priority="38"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e23']">
            <schxslt:rule pattern="d13e23">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e23">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C001">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The value of 'Identifier' of an 'DataServiceEvidenceType' MUST be unique UUID (RFC 4122).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e23')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification"
                 priority="37"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e33']">
            <schxslt:rule pattern="d13e33">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e33">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(AT|BE|BG|HR|CY|CZ|DK|EE|FI|FR|DE|EL|HU|IS|IE|IT|LV|LI|LT|LU|MT|NL|NO|PL|PT|RO|SK|SI|ES|SE|oots)/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C002">
                     <xsl:attribute name="test">matches(normalize-space((.)),'https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(AT|BE|BG|HR|CY|CZ|DK|EE|FI|FR|DE|EL|HU|IS|IE|IT|LV|LI|LT|LU|MT|NL|NO|PL|PT|RO|SK|SI|ES|SE|oots)/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The value of 'EvidenceTypeClassification' of a 'DataServiceEvidenceType' MUST be a UUID of the Evidence Broker and inlude a code of the code list 'EEA_Country-CodeList' (ISO 3166-1' alpha-2 codes subset of EEA countries) 
            using the prefix and scheme 'https://sr.oots.tech.eceuropa.eu/evidencetypeclassifications/[EEA_CountryCode]/[UUID]' pointing to the Semantic Repository. For testing purposes and agreed OOTS data models the code "oots" can be used. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e33')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title"
                 priority="36"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e42']">
            <schxslt:rule pattern="d13e42">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e42">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C004">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e42')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description"
                 priority="35"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e51']">
            <schxslt:rule pattern="d13e51">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e51">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C006">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e51')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo"
                 priority="34"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e60']">
            <schxslt:rule pattern="d13e60">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e60">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C008">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')</xsl:attribute>
                     <svrl:text>The value of 'ConformsTo' of the distribution MUST be a persistent URL with a link to a "DataModelScheme" of the Evidence Type described in the Semantic Repository
            which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e60')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation"
                 priority="33"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e69']">
            <schxslt:rule pattern="d13e69">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e69">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C009">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')</xsl:attribute>
                     <svrl:text>The value of 'Transformation' of the distribution MUST be a persistent URL with link to a "DataModelScheme" and "Subset" of the EvidenceType desribed in the 
            Semantic Repository which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]/[Subset]".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e69')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier"
                 priority="32"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e78']">
            <schxslt:rule pattern="d13e78">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e78">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C010">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The value of 'Identifier' of MUST be unique UUID (RFC 4122).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e78')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification"
                 priority="31"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e88']">
            <schxslt:rule pattern="d13e88">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e88">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(sdg:Type) &gt; 0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C011">
                     <xsl:attribute name="test">count(sdg:Type) &gt; 0</xsl:attribute>
                     <svrl:text>A 'Type' MUST be provided. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(count(sdg:Description) &gt; 0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C012">
                     <xsl:attribute name="test">count(sdg:Description) &gt; 0</xsl:attribute>
                     <svrl:text>A 'Description' MUST be provided. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e88')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description"
                 priority="30"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e100']">
            <schxslt:rule pattern="d13e100">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e100">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C014">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e100')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId"
                 priority="29"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e109']">
            <schxslt:rule pattern="d13e109">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e109">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C015">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The value of 'JurisdictionContextId' of MUST be unique UUID (RFC 4122).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e109')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext"
                 priority="28"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e118']">
            <schxslt:rule pattern="d13e118">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e118">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C017">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e118')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Identifier"
                 priority="27"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e127']">
            <schxslt:rule pattern="d13e127">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e127">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@schemeID)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C020">
                     <xsl:attribute name="test">not(normalize-space(@schemeID)='')</xsl:attribute>
                     <svrl:text>The 'schemeID' MUST be provided. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e127')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService"
                 priority="26"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e136']">
            <schxslt:rule pattern="d13e136">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e136">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(sdg:ConformsTo='oots-edm:v1.0')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C023">
                     <xsl:attribute name="test">sdg:ConformsTo='oots-edm:v1.0'</xsl:attribute>
                     <svrl:text>The value of 'ConformsTo' of the Access Service MUST point to the underlying eDelivery Profile 'oots-edm:v1.0'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e136')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Identifier"
                 priority="25"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e146']">
            <schxslt:rule pattern="d13e146">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e146">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@schemeID)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C024">
                     <xsl:attribute name="test">not(normalize-space(@schemeID)='')</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e146')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Identifier"
                 priority="24"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e155']">
            <schxslt:rule pattern="d13e155">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e155">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C031">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The value of 'Identifier' of MUST be unique UUID (RFC 4122).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e155')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept"
                 priority="23"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e164']">
            <schxslt:rule pattern="d13e164">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e164">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(sdg:Type) &gt; 0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C032">
                     <xsl:attribute name="test">count(sdg:Type) &gt; 0</xsl:attribute>
                     <svrl:text>A 'Type' MUST be provided. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(count(sdg:Description) &gt; 0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C033">
                     <xsl:attribute name="test">count(sdg:Description) &gt; 0</xsl:attribute>
                     <svrl:text>A 'Description' MUST be provided. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(count(sdg:SupportedValue) &gt; 0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C036">
                     <xsl:attribute name="test">count(sdg:SupportedValue) &gt; 0</xsl:attribute>
                     <svrl:text>A value for 'SupportedValue' MUST be provided if the 'sdg:ClassificationConcept' is present</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e164')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description"
                 priority="22"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e179']">
            <schxslt:rule pattern="d13e179">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e179">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C035">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e179')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs"
                 priority="21"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="ct1" select="count(sdg:ConformsTo)"/>
      <xsl:variable name="isxml" select="matches(sdg:Format,'xml')"/>
      <xsl:variable name="isjson" select="matches(sdg:Format, 'json')"/>
      <xsl:variable name="otherformat" select="../sdg:DistributedAs/sdg:Format"/>
      <xsl:variable name="other"
                    select="some $format in $otherformat satisfies matches(string($format), 'application/pdf') or matches(string($format), 'image/jpeg') or matches(string($format), 'image/jpg') or matches(string($format), 'image/png') or matches(string($format), 'application/pdf') or matches(string($format), 'image/svg')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e188']">
            <schxslt:rule pattern="d13e188">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e188">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not($isxml and $ct1=0) or $other)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C038">
                     <xsl:attribute name="test">not($isxml and $ct1=0) or $other</xsl:attribute>
                     <svrl:text>The value of 'sdg:ConformsTo' of the distribution MUST be present if the 'sdg:DistributedAs/sdg:Format' uses the code 'application/xml' of the codelist 'OOTSMediaTypes' 
            unless another 'sdg:DistributedAs/sdg:Format' exists that uses the code 'image/jpeg' or 'image/jpg' or'image/png' or 'application/pdf' or 'image/svg+xml' for the same 
            DataServiceEvidenceType.check <xsl:value-of select="$otherformat"/> check <xsl:value-of select="$other"/>
                     </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(not($isjson and $ct1=0) or $other)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C040">
                     <xsl:attribute name="test">not($isjson and $ct1=0) or $other</xsl:attribute>
                     <svrl:text>The value of 'sdg:ConformsTo' of the distribution MUST be present if the 'sdg:DistributedAs/sdg:Format' uses the code 'application/json' of the codelist 'OOTSMediaTypes' 
            unless another 'sdg:DistributedAs/sdg:Format' exists that uses the code 'image/jpeg' or 'image/jpg' or 'image/png' or 'application/pdf' or 'image/svg+xml' for the same 
            DataServiceEvidenceType.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e188')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs"
                 priority="20"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="tr" select="count(sdg:Transformation)"/>
      <xsl:variable name="ct" select="count(sdg:ConformsTo)"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e215']">
            <schxslt:rule pattern="d13e215">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e215">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(($tr=1 and $ct=1) or $tr=0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C039">
                     <xsl:attribute name="test">($tr=1 and $ct=1) or $tr=0</xsl:attribute>
                     <svrl:text>The value of  'sdg:ConformsTo'  of the distribution MUST be present if the Element 'sdg:Transformation' of the distribution is present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e215')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note"
                 priority="19"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e229']">
            <schxslt:rule pattern="d13e229">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e229">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C042">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e229')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Name"
                 priority="18"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e238']">
            <schxslt:rule pattern="d13e238">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Name" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Name</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e238">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Name</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C044">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e238')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Name"
                 priority="17"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e247']">
            <schxslt:rule pattern="d13e247">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Name" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Name</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e247">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Name</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C046">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e247')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Description"
                 priority="16"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e256']">
            <schxslt:rule pattern="d13e256">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Description" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Description</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e256">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Description</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C048">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e256')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang"
                 priority="15"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C003">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) </xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang"
                 priority="14"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C005">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) </xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Description/@lang"
                 priority="13"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Description/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Description/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Description/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C047">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) </xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Address/sdg:AdminUnitLevel1"
                 priority="12"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Address/sdg:AdminUnitLevel1" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Address/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Address/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;AF&#x7f;AL&#x7f;DZ&#x7f;AS&#x7f;AD&#x7f;AO&#x7f;AI&#x7f;AQ&#x7f;AG&#x7f;AR&#x7f;AM&#x7f;AW&#x7f;AU&#x7f;AT&#x7f;AZ&#x7f;BS&#x7f;BH&#x7f;BD&#x7f;BB&#x7f;BY&#x7f;BE&#x7f;BZ&#x7f;BJ&#x7f;BM&#x7f;BT&#x7f;BO&#x7f;BQ&#x7f;BA&#x7f;BW&#x7f;BV&#x7f;BR&#x7f;IO&#x7f;BN&#x7f;BG&#x7f;BF&#x7f;BI&#x7f;CV&#x7f;KH&#x7f;CM&#x7f;CA&#x7f;KY&#x7f;CF&#x7f;TD&#x7f;CL&#x7f;CN&#x7f;CX&#x7f;CC&#x7f;CO&#x7f;KM&#x7f;CD&#x7f;CG&#x7f;CK&#x7f;CR&#x7f;HR&#x7f;CU&#x7f;CW&#x7f;CY&#x7f;CZ&#x7f;CI&#x7f;DK&#x7f;DJ&#x7f;DM&#x7f;DO&#x7f;EC&#x7f;EG&#x7f;SV&#x7f;GQ&#x7f;ER&#x7f;EE&#x7f;SZ&#x7f;ET&#x7f;FK&#x7f;FO&#x7f;FJ&#x7f;FI&#x7f;FR&#x7f;GF&#x7f;PF&#x7f;TF&#x7f;GA&#x7f;GM&#x7f;GE&#x7f;DE&#x7f;GH&#x7f;GI&#x7f;EL&#x7f;GL&#x7f;GD&#x7f;GP&#x7f;GU&#x7f;GT&#x7f;GG&#x7f;GN&#x7f;GW&#x7f;GY&#x7f;HT&#x7f;HM&#x7f;VA&#x7f;HN&#x7f;HK&#x7f;HU&#x7f;IS&#x7f;IN&#x7f;ID&#x7f;IR&#x7f;IQ&#x7f;IE&#x7f;IM&#x7f;IL&#x7f;IT&#x7f;JM&#x7f;JP&#x7f;JE&#x7f;JO&#x7f;KZ&#x7f;KE&#x7f;KI&#x7f;KP&#x7f;KR&#x7f;KW&#x7f;KG&#x7f;LA&#x7f;LV&#x7f;LB&#x7f;LS&#x7f;LR&#x7f;LY&#x7f;LI&#x7f;LT&#x7f;LU&#x7f;MO&#x7f;MG&#x7f;MW&#x7f;MY&#x7f;MV&#x7f;ML&#x7f;MT&#x7f;MH&#x7f;MQ&#x7f;MR&#x7f;MU&#x7f;YT&#x7f;MX&#x7f;FM&#x7f;MD&#x7f;MC&#x7f;MN&#x7f;ME&#x7f;MS&#x7f;MA&#x7f;MZ&#x7f;MM&#x7f;NA&#x7f;NR&#x7f;NP&#x7f;NL&#x7f;NC&#x7f;NZ&#x7f;NI&#x7f;NE&#x7f;NG&#x7f;NU&#x7f;NF&#x7f;MK&#x7f;MP&#x7f;NO&#x7f;OM&#x7f;PK&#x7f;PW&#x7f;PS&#x7f;PA&#x7f;PG&#x7f;PY&#x7f;PE&#x7f;PH&#x7f;PN&#x7f;PL&#x7f;PT&#x7f;PR&#x7f;QA&#x7f;RO&#x7f;RU&#x7f;RW&#x7f;RE&#x7f;BL&#x7f;SH&#x7f;KN&#x7f;LC&#x7f;MF&#x7f;PM&#x7f;VC&#x7f;WS&#x7f;SM&#x7f;ST&#x7f;SA&#x7f;SN&#x7f;RS&#x7f;SC&#x7f;SL&#x7f;SG&#x7f;SX&#x7f;SK&#x7f;SI&#x7f;SB&#x7f;SO&#x7f;ZA&#x7f;GS&#x7f;SS&#x7f;ES&#x7f;LK&#x7f;SD&#x7f;SR&#x7f;SJ&#x7f;SE&#x7f;CH&#x7f;SY&#x7f;TW&#x7f;TJ&#x7f;TZ&#x7f;TH&#x7f;TL&#x7f;TG&#x7f;TK&#x7f;TO&#x7f;TT&#x7f;TN&#x7f;TR&#x7f;TM&#x7f;TC&#x7f;TV&#x7f;UG&#x7f;UA&#x7f;AE&#x7f;GB&#x7f;UM&#x7f;US&#x7f;UY&#x7f;UZ&#x7f;VU&#x7f;VE&#x7f;VN&#x7f;VG&#x7f;VI&#x7f;WF&#x7f;EH&#x7f;YE&#x7f;ZM&#x7f;ZW&#x7f;AX&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C026">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;AF&#x7f;AL&#x7f;DZ&#x7f;AS&#x7f;AD&#x7f;AO&#x7f;AI&#x7f;AQ&#x7f;AG&#x7f;AR&#x7f;AM&#x7f;AW&#x7f;AU&#x7f;AT&#x7f;AZ&#x7f;BS&#x7f;BH&#x7f;BD&#x7f;BB&#x7f;BY&#x7f;BE&#x7f;BZ&#x7f;BJ&#x7f;BM&#x7f;BT&#x7f;BO&#x7f;BQ&#x7f;BA&#x7f;BW&#x7f;BV&#x7f;BR&#x7f;IO&#x7f;BN&#x7f;BG&#x7f;BF&#x7f;BI&#x7f;CV&#x7f;KH&#x7f;CM&#x7f;CA&#x7f;KY&#x7f;CF&#x7f;TD&#x7f;CL&#x7f;CN&#x7f;CX&#x7f;CC&#x7f;CO&#x7f;KM&#x7f;CD&#x7f;CG&#x7f;CK&#x7f;CR&#x7f;HR&#x7f;CU&#x7f;CW&#x7f;CY&#x7f;CZ&#x7f;CI&#x7f;DK&#x7f;DJ&#x7f;DM&#x7f;DO&#x7f;EC&#x7f;EG&#x7f;SV&#x7f;GQ&#x7f;ER&#x7f;EE&#x7f;SZ&#x7f;ET&#x7f;FK&#x7f;FO&#x7f;FJ&#x7f;FI&#x7f;FR&#x7f;GF&#x7f;PF&#x7f;TF&#x7f;GA&#x7f;GM&#x7f;GE&#x7f;DE&#x7f;GH&#x7f;GI&#x7f;EL&#x7f;GL&#x7f;GD&#x7f;GP&#x7f;GU&#x7f;GT&#x7f;GG&#x7f;GN&#x7f;GW&#x7f;GY&#x7f;HT&#x7f;HM&#x7f;VA&#x7f;HN&#x7f;HK&#x7f;HU&#x7f;IS&#x7f;IN&#x7f;ID&#x7f;IR&#x7f;IQ&#x7f;IE&#x7f;IM&#x7f;IL&#x7f;IT&#x7f;JM&#x7f;JP&#x7f;JE&#x7f;JO&#x7f;KZ&#x7f;KE&#x7f;KI&#x7f;KP&#x7f;KR&#x7f;KW&#x7f;KG&#x7f;LA&#x7f;LV&#x7f;LB&#x7f;LS&#x7f;LR&#x7f;LY&#x7f;LI&#x7f;LT&#x7f;LU&#x7f;MO&#x7f;MG&#x7f;MW&#x7f;MY&#x7f;MV&#x7f;ML&#x7f;MT&#x7f;MH&#x7f;MQ&#x7f;MR&#x7f;MU&#x7f;YT&#x7f;MX&#x7f;FM&#x7f;MD&#x7f;MC&#x7f;MN&#x7f;ME&#x7f;MS&#x7f;MA&#x7f;MZ&#x7f;MM&#x7f;NA&#x7f;NR&#x7f;NP&#x7f;NL&#x7f;NC&#x7f;NZ&#x7f;NI&#x7f;NE&#x7f;NG&#x7f;NU&#x7f;NF&#x7f;MK&#x7f;MP&#x7f;NO&#x7f;OM&#x7f;PK&#x7f;PW&#x7f;PS&#x7f;PA&#x7f;PG&#x7f;PY&#x7f;PE&#x7f;PH&#x7f;PN&#x7f;PL&#x7f;PT&#x7f;PR&#x7f;QA&#x7f;RO&#x7f;RU&#x7f;RW&#x7f;RE&#x7f;BL&#x7f;SH&#x7f;KN&#x7f;LC&#x7f;MF&#x7f;PM&#x7f;VC&#x7f;WS&#x7f;SM&#x7f;ST&#x7f;SA&#x7f;SN&#x7f;RS&#x7f;SC&#x7f;SL&#x7f;SG&#x7f;SX&#x7f;SK&#x7f;SI&#x7f;SB&#x7f;SO&#x7f;ZA&#x7f;GS&#x7f;SS&#x7f;ES&#x7f;LK&#x7f;SD&#x7f;SR&#x7f;SJ&#x7f;SE&#x7f;CH&#x7f;SY&#x7f;TW&#x7f;TJ&#x7f;TZ&#x7f;TH&#x7f;TL&#x7f;TG&#x7f;TK&#x7f;TO&#x7f;TT&#x7f;TN&#x7f;TR&#x7f;TM&#x7f;TC&#x7f;TV&#x7f;UG&#x7f;UA&#x7f;AE&#x7f;GB&#x7f;UM&#x7f;US&#x7f;UY&#x7f;UZ&#x7f;VU&#x7f;VE&#x7f;VN&#x7f;VG&#x7f;VI&#x7f;WF&#x7f;EH&#x7f;YE&#x7f;ZM&#x7f;ZW&#x7f;AX&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) )</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Address/sdg:AdminUnitLevel1'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel1"
                 priority="11"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel1" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;AT&#x7f;BE&#x7f;BG&#x7f;HR&#x7f;CY&#x7f;CZ&#x7f;DK&#x7f;EE&#x7f;FI&#x7f;FR&#x7f;DE&#x7f;EL&#x7f;HU&#x7f;IS&#x7f;IE&#x7f;IT&#x7f;LV&#x7f;LI&#x7f;LT&#x7f;LU&#x7f;MT&#x7f;NL&#x7f;NO&#x7f;PL&#x7f;PT&#x7f;RO&#x7f;SK&#x7f;SI&#x7f;ES&#x7f;SE&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C028">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;AT&#x7f;BE&#x7f;BG&#x7f;HR&#x7f;CY&#x7f;CZ&#x7f;DK&#x7f;EE&#x7f;FI&#x7f;FR&#x7f;DE&#x7f;EL&#x7f;HU&#x7f;IS&#x7f;IE&#x7f;IT&#x7f;LV&#x7f;LI&#x7f;LT&#x7f;LU&#x7f;MT&#x7f;NL&#x7f;NO&#x7f;PL&#x7f;PT&#x7f;RO&#x7f;SK&#x7f;SI&#x7f;ES&#x7f;SE&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) )</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'EEA_Country-CodeList' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Juristiction/sdg:AdminUnitLevel1'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format"
                 priority="10"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;image/jpeg&#x7f;image/jpg&#x7f;application/json&#x7f;image/png&#x7f;application/pdf&#x7f;application/xml&#x7f;image/svg+xml&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C007">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;image/jpeg&#x7f;image/jpg&#x7f;application/json&#x7f;image/png&#x7f;application/pdf&#x7f;application/xml&#x7f;image/svg+xml&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) </xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'OOTSMediaTypes-CodeList' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description/@lang"
                 priority="9"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C013">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) </xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang"
                 priority="8"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C016">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) </xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description/@lang"
                 priority="7"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C034">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) </xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel"
                 priority="6"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;Country&#x7f;NUTS1&#x7f;NUTS2&#x7f;NUTS3&#x7f;LAU&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C018">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;Country&#x7f;NUTS1&#x7f;NUTS2&#x7f;NUTS3&#x7f;LAU&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) )</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'JurisdictionLevel-CodeList' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Identifier"
                 priority="5"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;0002&#x7f;0007&#x7f;0009&#x7f;0037&#x7f;0060&#x7f;0088&#x7f;0096&#x7f;0097&#x7f;0106&#x7f;0130&#x7f;0135&#x7f;0142&#x7f;0147&#x7f;0151&#x7f;0170&#x7f;0183&#x7f;0184&#x7f;0188&#x7f;0190&#x7f;0191&#x7f;0192&#x7f;0193&#x7f;0194&#x7f;0195&#x7f;0196&#x7f;0198&#x7f;0199&#x7f;0200&#x7f;0201&#x7f;0202&#x7f;0203&#x7f;0204&#x7f;0205&#x7f;0208&#x7f;0209&#x7f;0210&#x7f;0211&#x7f;0212&#x7f;0213&#x7f;0215&#x7f;0216&#x7f;0217&#x7f;0218&#x7f;0221&#x7f;0225&#x7f;0230&#x7f;9901&#x7f;9910&#x7f;9913&#x7f;9914&#x7f;9915&#x7f;9918&#x7f;9919&#x7f;9920&#x7f;9922&#x7f;9923&#x7f;9924&#x7f;9925&#x7f;9926&#x7f;9927&#x7f;9928&#x7f;9929&#x7f;9930&#x7f;9931&#x7f;9932&#x7f;9933&#x7f;9934&#x7f;9935&#x7f;9936&#x7f;9937&#x7f;9938&#x7f;9939&#x7f;9940&#x7f;9941&#x7f;9942&#x7f;9943&#x7f;9944&#x7f;9945&#x7f;9946&#x7f;9947&#x7f;9948&#x7f;9949&#x7f;9950&#x7f;9951&#x7f;9952&#x7f;9953&#x7f;9957&#x7f;9959&#x7f;AN&#x7f;AQ&#x7f;AS&#x7f;AU&#x7f;EM&#x7f;',concat('&#x7f;',$suffix,'&#x7f;')) ) or              matches(normalize-space(@schemeID), 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:(AT|BE|BG|HR|CY|CZ|DK|EE|FI|FR|DE|EL|HU|IS|IE|IT|LV|LI|LT|LU|MT|NL|NO|PL|PT|RO|SK|SI|ES|SE|oots)(:[\S]{1,256})*$', 'i') ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C022">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;0002&#x7f;0007&#x7f;0009&#x7f;0037&#x7f;0060&#x7f;0088&#x7f;0096&#x7f;0097&#x7f;0106&#x7f;0130&#x7f;0135&#x7f;0142&#x7f;0147&#x7f;0151&#x7f;0170&#x7f;0183&#x7f;0184&#x7f;0188&#x7f;0190&#x7f;0191&#x7f;0192&#x7f;0193&#x7f;0194&#x7f;0195&#x7f;0196&#x7f;0198&#x7f;0199&#x7f;0200&#x7f;0201&#x7f;0202&#x7f;0203&#x7f;0204&#x7f;0205&#x7f;0208&#x7f;0209&#x7f;0210&#x7f;0211&#x7f;0212&#x7f;0213&#x7f;0215&#x7f;0216&#x7f;0217&#x7f;0218&#x7f;0221&#x7f;0225&#x7f;0230&#x7f;9901&#x7f;9910&#x7f;9913&#x7f;9914&#x7f;9915&#x7f;9918&#x7f;9919&#x7f;9920&#x7f;9922&#x7f;9923&#x7f;9924&#x7f;9925&#x7f;9926&#x7f;9927&#x7f;9928&#x7f;9929&#x7f;9930&#x7f;9931&#x7f;9932&#x7f;9933&#x7f;9934&#x7f;9935&#x7f;9936&#x7f;9937&#x7f;9938&#x7f;9939&#x7f;9940&#x7f;9941&#x7f;9942&#x7f;9943&#x7f;9944&#x7f;9945&#x7f;9946&#x7f;9947&#x7f;9948&#x7f;9949&#x7f;9950&#x7f;9951&#x7f;9952&#x7f;9953&#x7f;9957&#x7f;9959&#x7f;AN&#x7f;AQ&#x7f;AS&#x7f;AU&#x7f;EM&#x7f;',concat('&#x7f;',$suffix,'&#x7f;')) ) or              matches(normalize-space(@schemeID), 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:(AT|BE|BG|HR|CY|CZ|DK|EE|FI|FR|DE|EL|HU|IS|IE|IT|LV|LI|LT|LU|MT|NL|NO|PL|PT|RO|SK|SI|ES|SE|oots)(:[\S]{1,256})*$', 'i') ) </xsl:attribute>
                     <svrl:text>The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country-CodeList’. For testing purposes the code "oots" can be used. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Identifier"
                 priority="4"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('0002&#x7f;0007&#x7f;0009&#x7f;0037&#x7f;0060&#x7f;0088&#x7f;0096&#x7f;0097&#x7f;0106&#x7f;0130&#x7f;0135&#x7f;0142&#x7f;0147&#x7f;0151&#x7f;0170&#x7f;0183&#x7f;0184&#x7f;0188&#x7f;0190&#x7f;0191&#x7f;0192&#x7f;0193&#x7f;0194&#x7f;0195&#x7f;0196&#x7f;0198&#x7f;0199&#x7f;0200&#x7f;0201&#x7f;0202&#x7f;0203&#x7f;0204&#x7f;0205&#x7f;0208&#x7f;0209&#x7f;0210&#x7f;0211&#x7f;0212&#x7f;0213&#x7f;0215&#x7f;0216&#x7f;0217&#x7f;0218&#x7f;0221&#x7f;0225&#x7f;0230&#x7f;9901&#x7f;9910&#x7f;9913&#x7f;9914&#x7f;9915&#x7f;9918&#x7f;9919&#x7f;9920&#x7f;9922&#x7f;9923&#x7f;9924&#x7f;9925&#x7f;9926&#x7f;9927&#x7f;9928&#x7f;9929&#x7f;9930&#x7f;9931&#x7f;9932&#x7f;9933&#x7f;9934&#x7f;9935&#x7f;9936&#x7f;9937&#x7f;9938&#x7f;9939&#x7f;9940&#x7f;9941&#x7f;9942&#x7f;9943&#x7f;9944&#x7f;9945&#x7f;9946&#x7f;9947&#x7f;9948&#x7f;9949&#x7f;9950&#x7f;9951&#x7f;9952&#x7f;9953&#x7f;9957&#x7f;9959&#x7f;AN&#x7f;AQ&#x7f;AS&#x7f;AU&#x7f;EM&#x7f;',concat('&#x7f;',$suffix,'&#x7f;')) ) or              matches(normalize-space(@schemeID), 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:(AT|BE|BG|HR|CY|CZ|DK|EE|FI|FR|DE|EL|HU|IS|IE|IT|LV|LI|LT|LU|MT|NL|NO|PL|PT|RO|SK|SI|ES|SE|oots)(:[\S]{1,256})*$', 'i') ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C025">
                     <xsl:attribute name="test">( false() or ( contains('0002&#x7f;0007&#x7f;0009&#x7f;0037&#x7f;0060&#x7f;0088&#x7f;0096&#x7f;0097&#x7f;0106&#x7f;0130&#x7f;0135&#x7f;0142&#x7f;0147&#x7f;0151&#x7f;0170&#x7f;0183&#x7f;0184&#x7f;0188&#x7f;0190&#x7f;0191&#x7f;0192&#x7f;0193&#x7f;0194&#x7f;0195&#x7f;0196&#x7f;0198&#x7f;0199&#x7f;0200&#x7f;0201&#x7f;0202&#x7f;0203&#x7f;0204&#x7f;0205&#x7f;0208&#x7f;0209&#x7f;0210&#x7f;0211&#x7f;0212&#x7f;0213&#x7f;0215&#x7f;0216&#x7f;0217&#x7f;0218&#x7f;0221&#x7f;0225&#x7f;0230&#x7f;9901&#x7f;9910&#x7f;9913&#x7f;9914&#x7f;9915&#x7f;9918&#x7f;9919&#x7f;9920&#x7f;9922&#x7f;9923&#x7f;9924&#x7f;9925&#x7f;9926&#x7f;9927&#x7f;9928&#x7f;9929&#x7f;9930&#x7f;9931&#x7f;9932&#x7f;9933&#x7f;9934&#x7f;9935&#x7f;9936&#x7f;9937&#x7f;9938&#x7f;9939&#x7f;9940&#x7f;9941&#x7f;9942&#x7f;9943&#x7f;9944&#x7f;9945&#x7f;9946&#x7f;9947&#x7f;9948&#x7f;9949&#x7f;9950&#x7f;9951&#x7f;9952&#x7f;9953&#x7f;9957&#x7f;9959&#x7f;AN&#x7f;AQ&#x7f;AS&#x7f;AU&#x7f;EM&#x7f;',concat('&#x7f;',$suffix,'&#x7f;')) ) or              matches(normalize-space(@schemeID), 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:(AT|BE|BG|HR|CY|CZ|DK|EE|FI|FR|DE|EL|HU|IS|IE|IT|LV|LI|LT|LU|MT|NL|NO|PL|PT|RO|SK|SI|ES|SE|oots)(:[\S]{1,256})*$', 'i') ) </xsl:attribute>
                     <svrl:text>The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country-CodeList’. For testing purposes the code "oots" can be used. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance"
                 priority="3"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;Low&#x7f;Substantial&#x7f;High&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C037">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;Low&#x7f;Substantial&#x7f;High&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) </xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LevelsOfAssurance-CodeList' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang"
                 priority="2"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C041">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) </xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Name/@lang"
                 priority="1"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Name/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Name/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Name/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C043">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) </xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Name/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Name/@lang"
                 priority="0"
                 mode="d13e23">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd13e265']">
            <schxslt:rule pattern="d13e265">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Name/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Name/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d13e265">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Name/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) )">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-DSD-SUB-C045">
                     <xsl:attribute name="test">( false() or ( contains('&#x7f;AA&#x7f;AAR&#x7f;AB&#x7f;ABK&#x7f;ACE&#x7f;ACH&#x7f;ADA&#x7f;ADY&#x7f;AE&#x7f;AF&#x7f;AFA&#x7f;AFH&#x7f;AFR&#x7f;AIN&#x7f;AK&#x7f;AKA&#x7f;AKK&#x7f;ALB (B) SQI (T)&#x7f;ALB (B) SQI (T)&#x7f;ALE&#x7f;ALG&#x7f;ALT&#x7f;AM&#x7f;AMH&#x7f;AN&#x7f;ANG&#x7f;ANP&#x7f;APA&#x7f;AR&#x7f;ARA&#x7f;ARC&#x7f;ARG&#x7f;ARM (B) HYE (T)&#x7f;ARM (B) HYE (T)&#x7f;ARN&#x7f;ARP&#x7f;ART&#x7f;ARW&#x7f;AS&#x7f;ASM&#x7f;AST&#x7f;ATH&#x7f;AUS&#x7f;AV&#x7f;AVA&#x7f;AVE&#x7f;AWA&#x7f;AY&#x7f;AYM&#x7f;AZ&#x7f;AZE&#x7f;BA&#x7f;BAD&#x7f;BAI&#x7f;BAK&#x7f;BAL&#x7f;BAM&#x7f;BAN&#x7f;BAQ (B) EUS (T)&#x7f;BAQ (B) EUS (T)&#x7f;BAS&#x7f;BAT&#x7f;BE&#x7f;BEJ&#x7f;BEL&#x7f;BEM&#x7f;BEN&#x7f;BER&#x7f;BG&#x7f;BH&#x7f;BHO&#x7f;BI&#x7f;BIH&#x7f;BIK&#x7f;BIN&#x7f;BIS&#x7f;BLA&#x7f;BM&#x7f;BN&#x7f;BNT&#x7f;BO&#x7f;BO&#x7f;BOS&#x7f;BR&#x7f;BRA&#x7f;BRE&#x7f;BS&#x7f;BTK&#x7f;BUA&#x7f;BUG&#x7f;BUL&#x7f;BUR (B) MYA (T)&#x7f;BUR (B) MYA (T)&#x7f;BYN&#x7f;CA&#x7f;CAD&#x7f;CAI&#x7f;CAR&#x7f;CAT&#x7f;CAU&#x7f;CE&#x7f;CEB&#x7f;CEL&#x7f;CH&#x7f;CHA&#x7f;CHB&#x7f;CHE&#x7f;CHG&#x7f;CHI (B) ZHO (T)&#x7f;CHI (B) ZHO (T)&#x7f;CHK&#x7f;CHM&#x7f;CHN&#x7f;CHO&#x7f;CHP&#x7f;CHR&#x7f;CHU&#x7f;CHV&#x7f;CHY&#x7f;CMC&#x7f;CNR&#x7f;CO&#x7f;COP&#x7f;COR&#x7f;COS&#x7f;CPE&#x7f;CPF&#x7f;CPP&#x7f;CR&#x7f;CRE&#x7f;CRH&#x7f;CRP&#x7f;CS&#x7f;CS&#x7f;CSB&#x7f;CU&#x7f;CUS&#x7f;CV&#x7f;CY&#x7f;CY&#x7f;CZE (B) CES (T)&#x7f;CZE (B) CES (T)&#x7f;DA&#x7f;DAK&#x7f;DAN&#x7f;DAR&#x7f;DAY&#x7f;DE&#x7f;DE&#x7f;DEL&#x7f;DEN&#x7f;DGR&#x7f;DIN&#x7f;DIV&#x7f;DOI&#x7f;DRA&#x7f;DSB&#x7f;DUA&#x7f;DUM&#x7f;DUT (B) NLD (T)&#x7f;DUT (B) NLD (T)&#x7f;DV&#x7f;DYU&#x7f;DZ&#x7f;DZO&#x7f;EE&#x7f;EFI&#x7f;EGY&#x7f;EKA&#x7f;EL&#x7f;EL&#x7f;ELX&#x7f;EN&#x7f;ENG&#x7f;ENM&#x7f;EO&#x7f;EPO&#x7f;ES&#x7f;EST&#x7f;ET&#x7f;EU&#x7f;EU&#x7f;EWE&#x7f;EWO&#x7f;FA&#x7f;FA&#x7f;FAN&#x7f;FAO&#x7f;FAT&#x7f;FF&#x7f;FI&#x7f;FIJ&#x7f;FIL&#x7f;FIN&#x7f;FIU&#x7f;FJ&#x7f;FO&#x7f;FON&#x7f;FR&#x7f;FR&#x7f;FRE (B) FRA (T)&#x7f;FRE (B) FRA (T)&#x7f;FRM&#x7f;FRO&#x7f;FRR&#x7f;FRS&#x7f;FRY&#x7f;FUL&#x7f;FUR&#x7f;FY&#x7f;GA&#x7f;GAA&#x7f;GAY&#x7f;GBA&#x7f;GD&#x7f;GEM&#x7f;GEO (B) KAT (T)&#x7f;GEO (B) KAT (T)&#x7f;GER (B) DEU (T)&#x7f;GER (B) DEU (T)&#x7f;GEZ&#x7f;GIL&#x7f;GL&#x7f;GLA&#x7f;GLE&#x7f;GLG&#x7f;GLV&#x7f;GMH&#x7f;GN&#x7f;GOH&#x7f;GON&#x7f;GOR&#x7f;GOT&#x7f;GRB&#x7f;GRC&#x7f;GRE (B) ELL (T)&#x7f;GRE (B) ELL (T)&#x7f;GRN&#x7f;GSW&#x7f;GU&#x7f;GUJ&#x7f;GV&#x7f;GWI&#x7f;HA&#x7f;HAI&#x7f;HAT&#x7f;HAU&#x7f;HAW&#x7f;HE&#x7f;HEB&#x7f;HER&#x7f;HI&#x7f;HIL&#x7f;HIM&#x7f;HIN&#x7f;HIT&#x7f;HMN&#x7f;HMO&#x7f;HO&#x7f;HR&#x7f;HRV&#x7f;HSB&#x7f;HT&#x7f;HU&#x7f;HUN&#x7f;HUP&#x7f;HY&#x7f;HY&#x7f;HZ&#x7f;IA&#x7f;IBA&#x7f;IBO&#x7f;ICE (B) ISL (T)&#x7f;ICE (B) ISL (T)&#x7f;ID&#x7f;IDO&#x7f;IE&#x7f;IG&#x7f;II&#x7f;III&#x7f;IJO&#x7f;IK&#x7f;IKU&#x7f;ILE&#x7f;ILO&#x7f;INA&#x7f;INC&#x7f;IND&#x7f;INE&#x7f;INH&#x7f;IO&#x7f;IPK&#x7f;IRA&#x7f;IRO&#x7f;IS&#x7f;IS&#x7f;IT&#x7f;ITA&#x7f;IU&#x7f;JA&#x7f;JAV&#x7f;JBO&#x7f;JPN&#x7f;JPR&#x7f;JRB&#x7f;JV&#x7f;KA&#x7f;KA&#x7f;KAA&#x7f;KAB&#x7f;KAC&#x7f;KAL&#x7f;KAM&#x7f;KAN&#x7f;KAR&#x7f;KAS&#x7f;KAU&#x7f;KAW&#x7f;KAZ&#x7f;KBD&#x7f;KG&#x7f;KHA&#x7f;KHI&#x7f;KHM&#x7f;KHO&#x7f;KI&#x7f;KIK&#x7f;KIN&#x7f;KIR&#x7f;KJ&#x7f;KK&#x7f;KL&#x7f;KM&#x7f;KMB&#x7f;KN&#x7f;KO&#x7f;KOK&#x7f;KOM&#x7f;KON&#x7f;KOR&#x7f;KOS&#x7f;KPE&#x7f;KR&#x7f;KRC&#x7f;KRL&#x7f;KRO&#x7f;KRU&#x7f;KS&#x7f;KU&#x7f;KUA&#x7f;KUM&#x7f;KUR&#x7f;KUT&#x7f;KV&#x7f;KW&#x7f;KY&#x7f;LA&#x7f;LAD&#x7f;LAH&#x7f;LAM&#x7f;LAO&#x7f;LAT&#x7f;LAV&#x7f;LB&#x7f;LEZ&#x7f;LG&#x7f;LI&#x7f;LIM&#x7f;LIN&#x7f;LIT&#x7f;LN&#x7f;LO&#x7f;LOL&#x7f;LOZ&#x7f;LT&#x7f;LTZ&#x7f;LU&#x7f;LUA&#x7f;LUB&#x7f;LUG&#x7f;LUI&#x7f;LUN&#x7f;LUO&#x7f;LUS&#x7f;LV&#x7f;MAC (B) MKD (T)&#x7f;MAC (B) MKD (T)&#x7f;MAD&#x7f;MAG&#x7f;MAH&#x7f;MAI&#x7f;MAK&#x7f;MAL&#x7f;MAN&#x7f;MAO (B) MRI (T)&#x7f;MAO (B) MRI (T)&#x7f;MAP&#x7f;MAR&#x7f;MAS&#x7f;MAY (B) MSA (T)&#x7f;MAY (B) MSA (T)&#x7f;MDF&#x7f;MDR&#x7f;MEN&#x7f;MG&#x7f;MGA&#x7f;MH&#x7f;MI&#x7f;MI&#x7f;MIC&#x7f;MIN&#x7f;MIS&#x7f;MK&#x7f;MK&#x7f;MKH&#x7f;ML&#x7f;MLG&#x7f;MLT&#x7f;MN&#x7f;MNC&#x7f;MNI&#x7f;MNO&#x7f;MOH&#x7f;MON&#x7f;MOS&#x7f;MR&#x7f;MS&#x7f;MS&#x7f;MT&#x7f;MUL&#x7f;MUN&#x7f;MUS&#x7f;MWL&#x7f;MWR&#x7f;MY&#x7f;MY&#x7f;MYN&#x7f;MYV&#x7f;NA&#x7f;NAH&#x7f;NAI&#x7f;NAP&#x7f;NAU&#x7f;NAV&#x7f;NB&#x7f;NBL&#x7f;ND&#x7f;NDE&#x7f;NDO&#x7f;NDS&#x7f;NE&#x7f;NEP&#x7f;NEW&#x7f;NG&#x7f;NIA&#x7f;NIC&#x7f;NIU&#x7f;NL&#x7f;NL&#x7f;NN&#x7f;NNO&#x7f;NO&#x7f;NOB&#x7f;NOG&#x7f;NON&#x7f;NOR&#x7f;NQO&#x7f;NR&#x7f;NSO&#x7f;NUB&#x7f;NV&#x7f;NWC&#x7f;NY&#x7f;NYA&#x7f;NYM&#x7f;NYN&#x7f;NYO&#x7f;NZI&#x7f;OC&#x7f;OCI&#x7f;OJ&#x7f;OJI&#x7f;OM&#x7f;OR&#x7f;ORI&#x7f;ORM&#x7f;OS&#x7f;OSA&#x7f;OSS&#x7f;OTA&#x7f;OTO&#x7f;PA&#x7f;PAA&#x7f;PAG&#x7f;PAL&#x7f;PAM&#x7f;PAN&#x7f;PAP&#x7f;PAU&#x7f;PEO&#x7f;PER (B) FAS (T)&#x7f;PER (B) FAS (T)&#x7f;PHI&#x7f;PHN&#x7f;PI&#x7f;PL&#x7f;PLI&#x7f;POL&#x7f;PON&#x7f;POR&#x7f;PRA&#x7f;PRO&#x7f;PS&#x7f;PT&#x7f;PUS&#x7f;QAA-QTZ&#x7f;QU&#x7f;QUE&#x7f;RAJ&#x7f;RAP&#x7f;RAR&#x7f;RM&#x7f;RN&#x7f;RO&#x7f;RO&#x7f;ROA&#x7f;ROH&#x7f;ROM&#x7f;RU&#x7f;RUM (B) RON (T)&#x7f;RUM (B) RON (T)&#x7f;RUN&#x7f;RUP&#x7f;RUS&#x7f;RW&#x7f;SA&#x7f;SAD&#x7f;SAG&#x7f;SAH&#x7f;SAI&#x7f;SAL&#x7f;SAM&#x7f;SAN&#x7f;SAS&#x7f;SAT&#x7f;SC&#x7f;SCN&#x7f;SCO&#x7f;SD&#x7f;SE&#x7f;SEL&#x7f;SEM&#x7f;SG&#x7f;SGA&#x7f;SGN&#x7f;SHN&#x7f;SI&#x7f;SID&#x7f;SIN&#x7f;SIO&#x7f;SIT&#x7f;SK&#x7f;SK&#x7f;SL&#x7f;SLA&#x7f;SLO (B) SLK (T)&#x7f;SLO (B) SLK (T)&#x7f;SLV&#x7f;SM&#x7f;SMA&#x7f;SME&#x7f;SMI&#x7f;SMJ&#x7f;SMN&#x7f;SMO&#x7f;SMS&#x7f;SN&#x7f;SNA&#x7f;SND&#x7f;SNK&#x7f;SO&#x7f;SOG&#x7f;SOM&#x7f;SON&#x7f;SOT&#x7f;SPA&#x7f;SQ&#x7f;SQ&#x7f;SR&#x7f;SRD&#x7f;SRN&#x7f;SRP&#x7f;SRR&#x7f;SS&#x7f;SSA&#x7f;SSW&#x7f;ST&#x7f;SU&#x7f;SUK&#x7f;SUN&#x7f;SUS&#x7f;SUX&#x7f;SV&#x7f;SW&#x7f;SWA&#x7f;SWE&#x7f;SYC&#x7f;SYR&#x7f;TA&#x7f;TAH&#x7f;TAI&#x7f;TAM&#x7f;TAT&#x7f;TE&#x7f;TEL&#x7f;TEM&#x7f;TER&#x7f;TET&#x7f;TG&#x7f;TGK&#x7f;TGL&#x7f;TH&#x7f;THA&#x7f;TI&#x7f;TIB (B) BOD (T)&#x7f;TIB (B) BOD (T)&#x7f;TIG&#x7f;TIR&#x7f;TIV&#x7f;TK&#x7f;TKL&#x7f;TL&#x7f;TLH&#x7f;TLI&#x7f;TMH&#x7f;TN&#x7f;TO&#x7f;TOG&#x7f;TON&#x7f;TPI&#x7f;TR&#x7f;TS&#x7f;TSI&#x7f;TSN&#x7f;TSO&#x7f;TT&#x7f;TUK&#x7f;TUM&#x7f;TUP&#x7f;TUR&#x7f;TUT&#x7f;TVL&#x7f;TW&#x7f;TWI&#x7f;TY&#x7f;TYV&#x7f;UDM&#x7f;UG&#x7f;UGA&#x7f;UIG&#x7f;UK&#x7f;UKR&#x7f;UMB&#x7f;UND&#x7f;UR&#x7f;URD&#x7f;UZ&#x7f;UZB&#x7f;VAI&#x7f;VE&#x7f;VEN&#x7f;VI&#x7f;VIE&#x7f;VO&#x7f;VOL&#x7f;VOT&#x7f;WA&#x7f;WAK&#x7f;WAL&#x7f;WAR&#x7f;WAS&#x7f;WEL (B) CYM (T)&#x7f;WEL (B) CYM (T)&#x7f;WEN&#x7f;WLN&#x7f;WO&#x7f;WOL&#x7f;XAL&#x7f;XH&#x7f;XHO&#x7f;YAO&#x7f;YAP&#x7f;YI&#x7f;YID&#x7f;YO&#x7f;YOR&#x7f;YPK&#x7f;ZA&#x7f;ZAP&#x7f;ZBL&#x7f;ZEN&#x7f;ZGH&#x7f;ZH&#x7f;ZH&#x7f;ZHA&#x7f;ZND&#x7f;ZU&#x7f;ZUL&#x7f;ZUN&#x7f;ZXX&#x7f;ZZA&#x7f;',concat('&#x7f;',.,'&#x7f;')) ) ) </xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Name/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd13e265')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:function name="schxslt:location" as="xs:string">
      <xsl:param name="node" as="node()"/>
      <xsl:variable name="segments" as="xs:string*">
         <xsl:for-each select="($node/ancestor-or-self::node())">
            <xsl:variable name="position">
               <xsl:number level="single"/>
            </xsl:variable>
            <xsl:choose>
               <xsl:when test=". instance of element()">
                  <xsl:value-of select="concat('Q{', namespace-uri(.), '}', local-name(.), '[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of attribute()">
                  <xsl:value-of select="concat('@Q{', namespace-uri(.), '}', local-name(.))"/>
               </xsl:when>
               <xsl:when test=". instance of processing-instruction()">
                  <xsl:value-of select="concat('processing-instruction(&#34;', name(.), '&#34;)[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of comment()">
                  <xsl:value-of select="concat('comment()[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of text()">
                  <xsl:value-of select="concat('text()[', $position, ']')"/>
               </xsl:when>
               <xsl:otherwise/>
            </xsl:choose>
         </xsl:for-each>
      </xsl:variable>
      <xsl:value-of select="concat('/', string-join($segments, '/'))"/>
   </xsl:function>
</xsl:transform>
