# Compiled XSLT

## Overview

In some contexts, it is useful to have compiled XSLTs instead
of Schematron sets. This folder contains an experimental  script
that does this for all the rule sets in the [sch](../sch) or 
[sch_fatal_error](../sch_fatal_error) folders.

It uses the [oots.utils.schematron](https://code.europa.eu/oots/tdd/oots_ex/-/blob/main/oots/utils/schematron.py) library 
module from a related repository.

## WARNING

These files are generated automatically.  The functionality is defined and maintained in Schematrons in the 
[OOTS EDM Schematron](../sch) folder.

Also note that these files have NOT BEEN TESTED.




