<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform xmlns:error="https://doi.org/10.5281/zenodo.1495494#error"
               xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"
               xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"
               xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"
               xmlns:sch="http://purl.oclc.org/dsdl/schematron"
               xmlns:schxslt="https://doi.org/10.5281/zenodo.1495494"
               xmlns:schxslt-api="https://doi.org/10.5281/zenodo.1495494#api"
               xmlns:sdg="http://data.europa.eu/p4s"
               xmlns:x="https://www.w3.org/TR/REC-html40"
               xmlns:xlink="http://www.w3.org/1999/xlink"
               xmlns:xs="http://www.w3.org/2001/XMLSchema"
               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               version="2.0">
   <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/"
                    xmlns:dct="http://purl.org/dc/terms/"
                    xmlns:skos="http://www.w3.org/2004/02/skos/core#">
      <dct:creator>
         <dct:Agent>
            <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
            <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
         </dct:Agent>
      </dct:creator>
      <dct:created>2024-09-08T11:21:09.516377+02:00</dct:created>
   </rdf:Description>
   <xsl:output indent="yes"/>
   <xsl:param name="EEA_COUNTRY-CODELIST_code"
              select="tokenize('AT BE BG HR CY CZ DK EE FI FR DE EL HU IS IE IT LV LI LT LU MT NL NO PL PT RO SK SI ES SE', '\s')"/>
   <xsl:param name="LEVELSOFASSURANCE-CODELIST_code"
              select="tokenize('Low Substantial High', '\s')"/>
   <xsl:param name="AGENTCLASSIFICATION-CODELIST_code"
              select="tokenize('ER IP EP ERRP', '\s')"/>
   <xsl:param name="OOTSMEDIATYPES-CODELIST_code"
              select="tokenize('image/jpeg application/json image/png application/pdf application/xml image/svg+xml', '\s')"/>
   <xsl:param name="LANGUAGECODE-CODELIST_code"
              select="tokenize('AA AB AE AF AK AM AN AR AS AV AY AZ BA BE BG BH BI BM BN BO BO BR BS CA CE CH CO CR CS CS CU CV CY CY DA DE DE DV DZ EE EL EL EN EO ES ET EU EU FA FA FF FI FJ FO FR FR FY GA GD GL GN GU GV HA HE HI HO HR HT HU HY HY HZ IA ID IE IG II IK IO IS IS IT IU JA JV KA KA KG KI KJ KK KL KM KN KO KR KS KU KV KW KY LA LB LG LI LN LO LT LU LV MG MH MI MI MK MK ML MN MR MS MS MT MY MY NA NB ND NE NG NL NL NN NO NR NV NY OC OJ OM OR OS PA PI PL PS PT QU RM RN RO RO RU RW SA SC SD SE SG SI SK SK SL SM SN SO SQ SQ SR SS ST SU SV SW TA TE TG TH TI TK TL TN TO TR TS TT TW TY UG UK UR UZ VE VI VO WA WO XH YI YO ZA ZH ZH ZU', '\s')"/>
   <xsl:param name="EAS_Code"
              select="tokenize('0002 0007 0009 0037 0060 0088 0096 0097 0106 0130 0135 0142 0147 0151 0170 0183 0184 0188 0190 0191 0192 0193 0194 0195 0196 0198 0199 0200 0201 0202 0203 0204 0205 0208 0209 0210 0211 0212 0213 0215 0216 0217 0218 0221 0225 0230 9901 9910 9913 9914 9915 9918 9919 9920 9922 9923 9924 9925 9926 9927 9928 9929 9930 9931 9932 9933 9934 9935 9936 9937 9938 9939 9940 9941 9942 9943 9944 9945 9946 9947 9948 9949 9950 9951 9952 9953 9957 9959 AN AQ AS AU EM', '\s')"/>
   <xsl:param name="PROCEDURES-CODELIST_code"
              select="tokenize('R1 S1 T1 T2 T3 U1 U2 U3 U4 V1 V2 V3 V4 W1 W2 X1 X2 X3 X4 X5 X6 X10 X11 AK1 AL1', '\s')"/>
   <xsl:param name="COUNTRYIDENTIFICATIONCODE-CODELIST_code"
              select="tokenize('AF AL DZ AS AD AO AI AQ AG AR AM AW AU AT AZ BS BH BD BB BY BE BZ BJ BM BT BO BQ BA BW BV BR IO BN BG BF BI CV KH CM CA KY CF TD CL CN CX CC CO KM CD CG CK CR HR CU CW CY CZ CI DK DJ DM DO EC EG SV GQ ER EE SZ ET FK FO FJ FI FR GF PF TF GA GM GE DE GH GI EL GL GD GP GU GT GG GN GW GY HT HM VA HN HK HU IS IN ID IR IQ IE IM IL IT JM JP JE JO KZ KE KI KP KR KW KG LA LV LB LS LR LY LI LT LU MO MG MW MY MV ML MT MH MQ MR MU YT MX FM MD MC MN ME MS MA MZ MM NA NR NP NL NC NZ NI NE NG NU NF MK MP NO OM PK PW PS PA PG PY PE PH PN PL PT PR QA RO RU RW RE BL SH KN LC MF PM VC WS SM ST SA SN RS SC SL SG SX SK SI SB SO ZA GS SS ES LK SD SR SJ SE CH SY TW TJ TZ TH TL TG TK TO TT TN TR TM TC TV UG UA AE GB UM US UY UZ VU VE VN VG VI WF EH YE ZM ZW AX', '\s')"/>
   <xsl:param name="IDENTIFIERSCHEMES-CODELIST_code"
              select="tokenize('VAT TAX BusinessCode LEI EORI SEED SIC', '\s')"/>
   <xsl:variable name="attrval"
                 select="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:SectorSpecificAttribute/sdg:AttributeValue"/>
   <xsl:template match="root()">
      <xsl:variable name="metadata" as="element()?">
         <svrl:metadata xmlns:dct="http://purl.org/dc/terms/"
                        xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                        xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
            <dct:creator>
               <dct:Agent>
                  <skos:prefLabel>
                     <xsl:value-of separator="/"
                                   select="(system-property('xsl:product-name'), system-property('xsl:product-version'))"/>
                  </skos:prefLabel>
               </dct:Agent>
            </dct:creator>
            <dct:created>
               <xsl:value-of select="current-dateTime()"/>
            </dct:created>
            <dct:source>
               <rdf:Description xmlns:dc="http://purl.org/dc/elements/1.1/">
                  <dct:creator>
                     <dct:Agent>
                        <skos:prefLabel>SchXslt/1.9.5 SAXON/HE 12.3</skos:prefLabel>
                        <schxslt.compile.typed-variables xmlns="https://doi.org/10.5281/zenodo.1495494#">true</schxslt.compile.typed-variables>
                     </dct:Agent>
                  </dct:creator>
                  <dct:created>2024-09-08T11:21:09.516377+02:00</dct:created>
               </rdf:Description>
            </dct:source>
         </svrl:metadata>
      </xsl:variable>
      <xsl:variable name="report" as="element(schxslt:report)">
         <schxslt:report>
            <xsl:call-template name="d22e43"/>
         </schxslt:report>
      </xsl:variable>
      <xsl:variable name="schxslt:report" as="node()*">
         <xsl:sequence select="$metadata"/>
         <xsl:for-each select="$report/schxslt:document">
            <xsl:for-each select="schxslt:pattern">
               <xsl:sequence select="node()"/>
               <xsl:sequence select="../schxslt:rule[@pattern = current()/@id]/node()"/>
            </xsl:for-each>
         </xsl:for-each>
      </xsl:variable>
      <svrl:schematron-output xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
         <svrl:ns-prefix-in-attribute-values prefix="sdg" uri="http://data.europa.eu/p4s"/>
         <svrl:ns-prefix-in-attribute-values prefix="rs" uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="rim" uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="query" uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"/>
         <svrl:ns-prefix-in-attribute-values prefix="xsi" uri="http://www.w3.org/2001/XMLSchema-instance"/>
         <svrl:ns-prefix-in-attribute-values prefix="xlink" uri="http://www.w3.org/1999/xlink"/>
         <svrl:ns-prefix-in-attribute-values prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
         <xsl:sequence select="$schxslt:report"/>
      </svrl:schematron-output>
   </xsl:template>
   <xsl:template match="text() | @*" mode="#all" priority="-10"/>
   <xsl:template match="/" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template match="*" mode="#all" priority="-10">
      <xsl:apply-templates mode="#current" select="@*"/>
      <xsl:apply-templates mode="#current" select="node()"/>
   </xsl:template>
   <xsl:template name="d22e43">
      <schxslt:document>
         <schxslt:pattern id="d22e43">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e52">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e61">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e70">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e80">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e89">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e98">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e107">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e116">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e125">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e135">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e144">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e153">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e162">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e171">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e180">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e192">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e201">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e210">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e219">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e228">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e237">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e247">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e256">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e267">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e276">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e285">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e294">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e304">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e313">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e322">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e333">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e344">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e353">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e363">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e372">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e381">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e390">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e399">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e408">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e418">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e429">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e438">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e447">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e456">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e474">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e484">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e493">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e502">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <schxslt:pattern id="d22e511">
            <xsl:if test="exists(base-uri(root()))">
               <xsl:attribute name="documents" select="base-uri(root())"/>
            </xsl:if>
            <xsl:for-each select="root()">
               <svrl:active-pattern xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="documents" select="base-uri(.)"/>
               </svrl:active-pattern>
            </xsl:for-each>
         </schxslt:pattern>
         <xsl:apply-templates mode="d22e43" select="root()"/>
      </schxslt:document>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue"
                 priority="78"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e43']">
            <schxslt:rule pattern="d22e43">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e43">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(rim:Value='oots-edm:v1.1')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C001">
                     <xsl:attribute name="test">rim:Value='oots-edm:v1.1'</xsl:attribute>
                     <svrl:text>The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-edm:v1.1".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e43')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='IssueDateTime']/rim:SlotValue/rim:Value"
                 priority="77"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e52']">
            <schxslt:rule pattern="d22e52">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='IssueDateTime']/rim:SlotValue/rim:Value" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='IssueDateTime']/rim:SlotValue/rim:Value</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e52">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='IssueDateTime']/rim:SlotValue/rim:Value</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C002">
                     <xsl:attribute name="test">matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}','i')</xsl:attribute>
                     <svrl:text>The 'rim:Value' of 'IssueDateTime' MUST be according to xsd:dateTime.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e52')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='PreviewLocation']/rim:SlotValue"
                 priority="76"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e61']">
            <schxslt:rule pattern="d22e61">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='PreviewLocation']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='PreviewLocation']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e61">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='PreviewLocation']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(starts-with(lower-case(rim:Value/text()), 'https://'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C005">
                     <xsl:attribute name="test">starts-with(lower-case(rim:Value/text()), 'https://')</xsl:attribute>
                     <svrl:text>The 'rim:Value' of a 'PreviewLocation' MUST be a URI starting with 'https://' according to RFC 3986.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e61')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Identifier"
                 priority="75"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e70']">
            <schxslt:rule pattern="d22e70">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e70">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/requirements/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C008">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/requirements/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$')</xsl:attribute>
                     <svrl:text>The value of 'Identifier' of a 'Requirement' MUST be a unique UUID (RFC 4122) listed in the EvidenceBroker and use the prefix ''https://sr.oots.tech.ec.europa.eu/requirements/[UUID]'' pointing to the Semantic Repository.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e70')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Name"
                 priority="74"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e80']">
            <schxslt:rule pattern="d22e80">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Name" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Name</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e80">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Name</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C010">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e80')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier"
                 priority="73"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e89']">
            <schxslt:rule pattern="d22e89">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e89">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C011">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e89')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent"
                 priority="72"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e98']">
            <schxslt:rule pattern="d22e98">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e98">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:Classification)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C013">
                     <xsl:attribute name="test">not(normalize-space(sdg:Classification)='')</xsl:attribute>
                     <svrl:text>The value for 'Agent/Classification' MUST be provided. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e98')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier"
                 priority="71"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e107']">
            <schxslt:rule pattern="d22e107">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e107">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C017">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e107')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Identifier"
                 priority="70"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e116']">
            <schxslt:rule pattern="d22e116">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e116">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C019">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text>The value of 'Identifier' of MUST be unique UUID (RFC 4122).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e116')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification"
                 priority="69"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e125']">
            <schxslt:rule pattern="d22e125">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e125">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:Type)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C020">
                     <xsl:attribute name="test">not(normalize-space(sdg:Type)='')</xsl:attribute>
                     <svrl:text>The value for 'Type' MUST be provided.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e125')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description"
                 priority="68"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e135']">
            <schxslt:rule pattern="d22e135">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e135">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C022">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e135')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification"
                 priority="67"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e144']">
            <schxslt:rule pattern="d22e144">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e144">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:SupportedValue)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C023">
                     <xsl:attribute name="test">not(normalize-space(sdg:SupportedValue)='')</xsl:attribute>
                     <svrl:text>A value for 'SupportedValue' MUST be provided. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e144')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:ResponseOption"
                 priority="66"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e153']">
            <schxslt:rule pattern="d22e153">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:ResponseOption" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:ResponseOption</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e153">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:ResponseOption</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@returnType='LeafClassWithRepositoryItem')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C024">
                     <xsl:attribute name="test">@returnType='LeafClassWithRepositoryItem'</xsl:attribute>
                     <svrl:text>The 'returnType' attribute of 'ResponseOption' MUST be the fixed value "LeafClassWithRepositoryItem".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e153')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query" priority="65" mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e162']">
            <schxslt:rule pattern="d22e162">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e162">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@queryDefinition='DocumentQuery')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C025">
                     <xsl:attribute name="test">@queryDefinition='DocumentQuery'</xsl:attribute>
                     <svrl:text>The 'queryDefinition' attribute of 'Query' MUST be the fixed value "DocumentQuery".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e162')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier"
                 priority="64"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e171']">
            <schxslt:rule pattern="d22e171">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e171">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C026">
                     <xsl:attribute name="test">matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')</xsl:attribute>
                     <svrl:text> The value of 'Identifier' of an 'DataServiceEvidenceType' MUST be unique UUID (RFC 4122) retrieved from the Data Service Directory.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e171')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification"
                 priority="63"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e180']">
            <schxslt:rule pattern="d22e180">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e180">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(., '^https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(oots|(' || string-join($EEA_COUNTRY-CODELIST_code, '|') || '))/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C027">
                     <xsl:attribute name="test">matches(., '^https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(oots|(' || string-join($EEA_COUNTRY-CODELIST_code, '|') || '))/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$')</xsl:attribute>
                     <svrl:text>The value of 'EvidenceTypeClassification' of a 'DataServiceEvidenceType' MUST be a UUID retrieved from the Evidence Broker and include a code of the code list 'EEA_Country-CodeList' (ISO 3166-1' alpha-2 codes subset of EEA countries) using the prefix and scheme ''https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/[EEA_Country-Codelist]/[UUID]'' pointing to the Semantic Repository. For testing purposes and agreed OOTS data models the code "oots" can be used. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e180')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title"
                 priority="62"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e192']">
            <schxslt:rule pattern="d22e192">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e192">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C029">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e192')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description"
                 priority="61"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e201']">
            <schxslt:rule pattern="d22e201">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e201">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(@lang)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C031">
                     <xsl:attribute name="test">not(normalize-space(@lang)='')</xsl:attribute>
                     <svrl:text>The value of 'lang' attribute MUST be provided. Default choice "EN".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e201')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType"
                 priority="60"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e210']">
            <schxslt:rule pattern="d22e210">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e210">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(sdg:DistributedAs)=1 or count(sdg:DistributedAs)=0)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C032">
                     <xsl:attribute name="test">count(sdg:DistributedAs)=1 or count(sdg:DistributedAs)=0</xsl:attribute>
                     <svrl:text>The Element 'DistributedAs' must occur not more than once (maxOccurs="1)' in the EvidenceRequest. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e210')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo"
                 priority="59"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e219']">
            <schxslt:rule pattern="d22e219">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e219">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C034">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')</xsl:attribute>
                     <svrl:text>The value of 'ConformsTo' of the requested distribution MUST be a persistent URL with a link to a "DataModelScheme" of the Evidence Type retrieved by the DSD and described in the Semantic Repository which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]". </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e219')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation"
                 priority="58"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e228']">
            <schxslt:rule pattern="d22e228">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e228">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C035">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')</xsl:attribute>
                     <svrl:text>The value of 'Transformation' of the requested distribution MUST be a persistent URL with link to a "DataModelScheme" and "Subset" of the EvidenceType retrieved from the Data Service Directory, described in the Semantic Repository which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]/[Subset]".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e228')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person"
                 priority="57"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e237']">
            <schxslt:rule pattern="d22e237">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e237">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:LevelOfAssurance)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C036">
                     <xsl:attribute name="test">not(normalize-space(sdg:LevelOfAssurance)='')</xsl:attribute>
                     <svrl:text>The Element 'LevelOfAssurance' must be provided  (minOccurs="1)' in the EvidenceRequest when rim:Slot[@name='NaturalPerson'] is used. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e237')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person"
                 priority="56"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e247']">
            <schxslt:rule pattern="d22e247">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e247">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:Identifier)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="WARNING"
                                      id="R-EDM-REQ-C038">
                     <xsl:attribute name="test">not(normalize-space(sdg:Identifier)='')</xsl:attribute>
                     <svrl:text>The value of a Person 'Identifier' SHOULD be provided.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e247')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier"
                 priority="55"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e256']">
            <schxslt:rule pattern="d22e256">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e256">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C040">
                     <xsl:attribute name="test">matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i')</xsl:attribute>
                     <svrl:text>The value of Person 'Identifier' MUST have the format XX/YY/Z...Z where the values of XX and YY MUST be part of the code list 'EEA_CountryCodeList' 
            (ISO 3166-1' alpha-2 codes subset of EEA countries ) and Z...Z is an undefined combination of up to 256 characters which uniquely identifies the 
            identity asserted in the country of origin. Example: ES/AT/02635542Y  </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e256')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier"
                 priority="54"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e267']">
            <schxslt:rule pattern="d22e267">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e267">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C041">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e267')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier"
                 priority="53"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e276']">
            <schxslt:rule pattern="d22e276">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e276">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID='eidas')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C042">
                     <xsl:attribute name="test">@schemeID='eidas'</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of the 'Identifier' MUST have the fixed value 'eidas'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e276')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:DateOfBirth"
                 priority="52"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e285']">
            <schxslt:rule pattern="d22e285">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:DateOfBirth" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:DateOfBirth</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e285">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:DateOfBirth</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^[0-9]{4}-[0-9]{2}-[0-9]{2}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C043">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^[0-9]{4}-[0-9]{2}-[0-9]{2}$','i')</xsl:attribute>
                     <svrl:text>The value of 'DateOfBirth' MUST use the following format YYYY + “-“ + MM + “-“ + DD (as defined for xsd:date) </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e285')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson"
                 priority="51"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e294']">
            <schxslt:rule pattern="d22e294">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e294">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:LevelOfAssurance)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C047">
                     <xsl:attribute name="test">not(normalize-space(sdg:LevelOfAssurance)='')</xsl:attribute>
                     <svrl:text>The Element 'LevelOfAssurance' must be provided  (minOccurs="1)' in the EvidenceRequest when rim:Slot[@name='LegalPerson'] is used. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e294')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson"
                 priority="50"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e304']">
            <schxslt:rule pattern="d22e304">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e304">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:LegalPersonIdentifier)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C049">
                     <xsl:attribute name="test">not(normalize-space(sdg:LegalPersonIdentifier)='')</xsl:attribute>
                     <svrl:text>The value of a Legal Person 'LegalPersonIdentifier' MUST be provided. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e304')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson"
                 priority="49"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e313']">
            <schxslt:rule pattern="d22e313">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e313">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:LegalPersonIdentifier)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C084">
                     <xsl:attribute name="test">not(normalize-space(sdg:LegalPersonIdentifier)='')</xsl:attribute>
                     <svrl:text>The value of a Legal Person 'LegalPersonIdentifier' MUST be provided. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e313')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier"
                 priority="48"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e322']">
            <schxslt:rule pattern="d22e322">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e322">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C051">
                     <xsl:attribute name="test">matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i')</xsl:attribute>
                     <svrl:text>The value of a 'LegalPersonIdentifier' MUST have the format XX/YY/Z…Z where the values of XX and YY MUST be part of the code list 'EEA_Country-CodeList' 
            (ISO 3166-1' alpha-2 codes subset of EEA countries) and Z...Z is an undefined combination of up to 256 characters which uniquely identifies the identity asserted in the 
            country of origin. Example: ES/AT/02635542Y   </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e322')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier"
                 priority="47"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e333']">
            <schxslt:rule pattern="d22e333">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e333">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C085">
                     <xsl:attribute name="test">matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i')</xsl:attribute>
                     <svrl:text>The value of a 'LegalPersonIdentifier' MUST have the format XX/YY/Z…Z where the values of XX and YY MUST be part of the code list 'EEA_Country-CodeList'
            (ISO 3166-1' alpha-2 codes subset of EEA countries) and Z...Z is an undefined combination of up to 256 characters which uniquely identifies the identity asserted in the
            country of origin. Example: ES/AT/02635542Y   </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e333')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier"
                 priority="46"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e344']">
            <schxslt:rule pattern="d22e344">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e344">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C052">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'LegalPersonIdentifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e344')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier"
                 priority="45"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e353']">
            <schxslt:rule pattern="d22e353">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e353">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C086">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'LegalPersonIdentifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e353')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier"
                 priority="44"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e363']">
            <schxslt:rule pattern="d22e363">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e363">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID='eidas')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C053">
                     <xsl:attribute name="test">@schemeID='eidas'</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of the 'LegalPersonIdentifier' MUST have the fixed value 'eidas'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e363')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier"
                 priority="43"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e372']">
            <schxslt:rule pattern="d22e372">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e372">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID='eidas')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C087">
                     <xsl:attribute name="test">@schemeID='eidas'</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of the 'LegalPersonIdentifier' MUST have the fixed value 'eidas'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e372')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier"
                 priority="42"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e381']">
            <schxslt:rule pattern="d22e381">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e381">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C054">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e381')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier"
                 priority="41"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e390']">
            <schxslt:rule pattern="d22e390">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e390">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C088">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e390')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person"
                 priority="40"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e399']">
            <schxslt:rule pattern="d22e399">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e399">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:LevelOfAssurance)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C058">
                     <xsl:attribute name="test">not(normalize-space(sdg:LevelOfAssurance)='')</xsl:attribute>
                     <svrl:text>The Element 'LevelOfAssurance' must be provided (minOccurs="1)' in the EvidenceRequest when rim:Slot[@name='AuthorizedRepresentative'] is used.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e399')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person"
                 priority="39"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e408']">
            <schxslt:rule pattern="d22e408">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e408">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(sdg:Identifier)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="WARNING"
                                      id="R-EDM-REQ-C060">
                     <xsl:attribute name="test">sdg:Identifier</xsl:attribute>
                     <svrl:text>The value of a Person 'Identifier' SHOULD be provided. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e408')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier"
                 priority="38"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e418']">
            <schxslt:rule pattern="d22e418">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e418">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C061">
                     <xsl:attribute name="test">matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i')</xsl:attribute>
                     <svrl:text>The value of a Person 'Identifier' MUST have the format XX/YY/Z...Z where XX and YY MUST be part of the code list 'EEA_Country-CodeList' (ISO 3166-1' alpha-2 codes subset 
            of EEA countries) and Z...Z is an undefined combination of up to 256 characters which uniquely identifies the identity asserted in the country of origin. Example: ES/AT/02635542Y </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e418')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier"
                 priority="37"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e429']">
            <schxslt:rule pattern="d22e429">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e429">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C062">
                     <xsl:attribute name="test">@schemeID</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of 'Identifier' MUST be present.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e429')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier"
                 priority="36"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e438']">
            <schxslt:rule pattern="d22e438">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e438">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(@schemeID='eidas')">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C063">
                     <xsl:attribute name="test">@schemeID='eidas'</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of the 'Identifier' MUST have the fixed value 'eidas'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e438')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:DateOfBirth"
                 priority="35"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e447']">
            <schxslt:rule pattern="d22e447">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:DateOfBirth" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:DateOfBirth</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e447">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:DateOfBirth</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(matches(normalize-space(text()),'^[0-9]{4}-[0-9]{2}-[0-9]{2}$','i'))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C064">
                     <xsl:attribute name="test">matches(normalize-space(text()),'^[0-9]{4}-[0-9]{2}-[0-9]{2}$','i')</xsl:attribute>
                     <svrl:text>The value of 'DateOfBirth' MUST use the following format YYYY + “-“ + MM + “-“ + DD (as defined for xsd:date)  </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e447')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs"
                 priority="34"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="ct1" select="count(sdg:ConformsTo)"/>
      <xsl:variable name="isxml" select="matches(sdg:Format,'xml')"/>
      <xsl:variable name="isjson" select="matches(sdg:Format, 'json')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e456']">
            <schxslt:rule pattern="d22e456">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e456">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not($isxml and $ct1=0))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="WARNING"
                                      id="R-EDM-REQ-C070">
                     <xsl:attribute name="test">not($isxml and $ct1=0)</xsl:attribute>
                     <svrl:text>The value of 'sdg:ConformsTo' of the distribution SHOULD be present if the 'sdg:DistributedAs/sdg:Format' uses the codes 'application/xml' of the codelist 'OOTSMediaTypes'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
               <xsl:if test="not(not($isjson and $ct1=0))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="WARNING"
                                      id="R-EDM-REQ-C071">
                     <xsl:attribute name="test">not($isjson and $ct1=0)</xsl:attribute>
                     <svrl:text>The value of 'sdg:ConformsTo' of the distribution SHOULD be present if the 'sdg:DistributedAs/sdg:Format' uses the codes 'application/json' of the codelist 'OOTSMediaTypes'.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e456')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation"
                 priority="33"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e474']">
            <schxslt:rule pattern="d22e474">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e474">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(../sdg:ConformsTo)=1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C072">
                     <xsl:attribute name="test">count(../sdg:ConformsTo)=1</xsl:attribute>
                     <svrl:text>The value of  'sdg:ConformsTo'  of the distribution MUST be present if the Element 'sdg:Transformation' of the distribution is present. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e474')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification/text() = 'ER']/sdg:Address"
                 priority="32"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e484']">
            <schxslt:rule pattern="d22e484">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification/text() = 'ER']/sdg:Address" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification/text() = 'ER']/sdg:Address</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e484">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification/text() = 'ER']/sdg:Address</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(sdg:AdminUnitLevel1)=1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C073">
                     <xsl:attribute name="test">count(sdg:AdminUnitLevel1)=1</xsl:attribute>
                     <svrl:text>A value for 'AdminUnitLevel1' MUST be provided if the sdg:Agent/sdg:Classification value is "ER".</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e484')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue"
                 priority="31"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e493']">
            <schxslt:rule pattern="d22e493">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e493">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(count(rim:Element/sdg:Agent[sdg:Classification='ER'])=1)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C074">
                     <xsl:attribute name="test">count(rim:Element/sdg:Agent[sdg:Classification='ER'])=1</xsl:attribute>
                     <svrl:text>The EvidenceRequester slot MUST include one Agent with the classification value ER.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e493')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson"
                 priority="30"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e502']">
            <schxslt:rule pattern="d22e502">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e502">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(not(normalize-space(sdg:LevelOfAssurance)=''))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C082">
                     <xsl:attribute name="test">not(normalize-space(sdg:LevelOfAssurance)='')</xsl:attribute>
                     <svrl:text>The Element 'LevelOfAssurance' must be provided  (minOccurs="1)' in the EvidenceRequest when rim:Slot[@name='AuthorizedRepresentativeLegalPerson'] is used.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e502')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Classification"
                 priority="29"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Classification" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Classification</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Classification</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $AGENTCLASSIFICATION-CODELIST_code[. != 'EP' and . != 'ERRP'] satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C014">
                     <xsl:attribute name="test">some $code in $AGENTCLASSIFICATION-CODELIST_code[. != 'EP' and . != 'ERRP'] satisfies .=$code</xsl:attribute>
                     <svrl:text>The value MUST be part of the code list 'AgentClassification' and shall be one of the codes ER (Evidence Requester) or IP(Intermediary Platform). The codes 'EP' and 'ERRP' shall not be used by this transaction. </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance"
                 priority="28"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C037">
                     <xsl:attribute name="test">some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LevelsOfAssurance-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance' <xsl:value-of select="."/>
                     </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance"
                 priority="27"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C048">
                     <xsl:attribute name="test">some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LevelsOfAssurance-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance"
                 priority="26"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C083">
                     <xsl:attribute name="test">some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LevelsOfAssurance-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance"
                 priority="25"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C059">
                     <xsl:attribute name="test">some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LevelsOfAssurance-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@value"
                 priority="24"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@value" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@value</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@value</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $PROCEDURES-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C003">
                     <xsl:attribute name="test">some $code in $PROCEDURES-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'Procedures-CodeList' in the context 'query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@value'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format"
                 priority="23"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C033">
                     <xsl:attribute name="test">some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'OOTSMediaTypes-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier"
                 priority="22"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:variable name="suffix1"
                    select="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C018">
                     <xsl:attribute name="test">((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256</xsl:attribute>
                     <svrl:text>The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country_CodeList’. For testing purposes the code "oots" can be used.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier"
                 priority="21"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:variable name="suffix"
                    select="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
      <xsl:variable name="suffix1"
                    select="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C012">
                     <xsl:attribute name="test">((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256</xsl:attribute>
                     <svrl:text>The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country-CodeList’. For testing purposes the code "oots" can be used.</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Name/@lang"
                 priority="20"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Name/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Name/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Name/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C009">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryRequest/rim:Slot[@name='Requirement']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Name/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description/@lang"
                 priority="19"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C021">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang"
                 priority="18"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C028">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@xml:lang"
                 priority="17"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@xml:lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@xml:lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@xml:lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C004">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@xml:lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang"
                 priority="16"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C030">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/@xml:lang" priority="15" mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/@xml:lang" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/@xml:lang</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/@xml:lang</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C069">
                     <xsl:attribute name="test">some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryRequest/@xml:lang'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1"
                 priority="14"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C015">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1"
                 priority="13"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C045">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier/@schemeID"
                 priority="12"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier/@schemeID" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier/@schemeID</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier/@schemeID</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $IDENTIFIERSCHEMES-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C055">
                     <xsl:attribute name="test">some $code in $IDENTIFIERSCHEMES-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:RegisteredAddress/sdg:AdminUnitLevel1"
                 priority="11"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:RegisteredAddress/sdg:AdminUnitLevel1" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:RegisteredAddress/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:RegisteredAddress/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C056">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:RegisteredAddress/sdg:AdminUnitLevel1'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1"
                 priority="10"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C067">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:CurrentAddress/sdg:AdminUnitLevel1"
                 priority="9"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:CurrentAddress/sdg:AdminUnitLevel1" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:CurrentAddress/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:CurrentAddress/sdg:AdminUnitLevel1</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C090">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>The value of the 'AdminUnitLevel1' MUST be part of the code list the code list 'CountryIdentificationCode' (ISO 3166-1' alpha-2 codes).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Nationality"
                 priority="8"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Nationality" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Nationality</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Nationality</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C075">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Nationality'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth"
                 priority="7"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C076">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence"
                 priority="6"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C077">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Nationality"
                 priority="5"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Nationality" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Nationality</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Nationality</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C078">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Nationality'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth"
                 priority="4"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C079">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence"
                 priority="3"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C080">
                     <xsl:attribute name="test">some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:SectorSpecificAttribute/sdg:AttributeValue"
                 priority="2"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:SectorSpecificAttribute/sdg:AttributeValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:SectorSpecificAttribute/sdg:AttributeValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:SectorSpecificAttribute/sdg:AttributeValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $PROCEDURES-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C081">
                     <xsl:attribute name="test">some $code in $PROCEDURES-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>Value supplied '<xsl:value-of select="."/>' is unacceptable for constraints identified by 'Procedures-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:SectorSpecificAttribute/sdg:AttributeValue'</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier/@schemeID"
                 priority="1"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier/@schemeID" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier/@schemeID</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier/@schemeID</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(some $code in $IDENTIFIERSCHEMES-CODELIST_code satisfies .=$code)">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C089">
                     <xsl:attribute name="test">some $code in $IDENTIFIERSCHEMES-CODELIST_code satisfies .=$code</xsl:attribute>
                     <svrl:text>The 'schemeID' attribute of the 'Identifier' MUST have be part of the code list 'IdentifierSchemes' (eIDAS Legal Person Identifier Schemes).</svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:SectorSpecificAttribute/sdg:AttributeValue"
                 priority="0"
                 mode="d22e43">
      <xsl:param name="schxslt:patterns-matched" as="xs:string*"/>
      <xsl:choose>
         <xsl:when test="$schxslt:patterns-matched[. = 'd22e511']">
            <schxslt:rule pattern="d22e511">
               <xsl:comment xmlns:svrl="http://purl.oclc.org/dsdl/svrl">WARNING: Rule for context "query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:SectorSpecificAttribute/sdg:AttributeValue" shadowed by preceding rule</xsl:comment>
               <svrl:suppressed-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:SectorSpecificAttribute/sdg:AttributeValue</xsl:attribute>
               </svrl:suppressed-rule>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="$schxslt:patterns-matched"/>
            </xsl:next-match>
         </xsl:when>
         <xsl:otherwise>
            <schxslt:rule pattern="d22e511">
               <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl">
                  <xsl:attribute name="context">query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:SectorSpecificAttribute/sdg:AttributeValue</xsl:attribute>
               </svrl:fired-rule>
               <xsl:if test="not(every $val in tokenize($attrval, ',\s*') satisfies (some $code in $PROCEDURES-CODELIST_code satisfies ( $val=$code)))">
                  <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                      location="{schxslt:location(.)}"
                                      role="FATAL"
                                      id="R-EDM-REQ-C091">
                     <xsl:attribute name="test">every $val in tokenize($attrval, ',\s*') satisfies (some $code in $PROCEDURES-CODELIST_code satisfies ( $val=$code))</xsl:attribute>
                     <svrl:text>The 'AttributeValue' of a 'SectorSpecificAttribute' with the URI 'http://data.europa.eu/p4s/attributes/PowerOfRepresentationScope' MUST be one or more comma-separated values a value of the code list 'Procedures' <xsl:value-of select="tokenize(replace($attrval, ',', ' '), ' ')"/>
                     </svrl:text>
                  </svrl:failed-assert>
               </xsl:if>
            </schxslt:rule>
            <xsl:next-match>
               <xsl:with-param name="schxslt:patterns-matched"
                               as="xs:string*"
                               select="($schxslt:patterns-matched, 'd22e511')"/>
            </xsl:next-match>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:function name="schxslt:location" as="xs:string">
      <xsl:param name="node" as="node()"/>
      <xsl:variable name="segments" as="xs:string*">
         <xsl:for-each select="($node/ancestor-or-self::node())">
            <xsl:variable name="position">
               <xsl:number level="single"/>
            </xsl:variable>
            <xsl:choose>
               <xsl:when test=". instance of element()">
                  <xsl:value-of select="concat('Q{', namespace-uri(.), '}', local-name(.), '[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of attribute()">
                  <xsl:value-of select="concat('@Q{', namespace-uri(.), '}', local-name(.))"/>
               </xsl:when>
               <xsl:when test=". instance of processing-instruction()">
                  <xsl:value-of select="concat('processing-instruction(&#34;', name(.), '&#34;)[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of comment()">
                  <xsl:value-of select="concat('comment()[', $position, ']')"/>
               </xsl:when>
               <xsl:when test=". instance of text()">
                  <xsl:value-of select="concat('text()[', $position, ']')"/>
               </xsl:when>
               <xsl:otherwise/>
            </xsl:choose>
         </xsl:for-each>
      </xsl:variable>
      <xsl:value-of select="concat('/', string-join($segments, '/'))"/>
   </xsl:function>
</xsl:transform>
