import sys, re, lxml.etree, os

nss = {
    'o' : "urn:schemas-microsoft-com:office:office",
    'x': "urn:schemas-microsoft-com:office:excel",
    'ss': "urn:schemas-microsoft-com:office:spreadsheet",
    'h': "http://www.w3.org/1999/xhtml"
}

strike_through_styles = []

def namespaced(ns, name):
    return '{{{}}}{}'.format(nss[ns], name)

def br_xslsx_to_html(filein, fileoutbase, sheets=[], params={}):
    d = lxml.etree.parse(filein)
    add_strike_through(d)
    root = lxml.etree.Element("html")
    root.append(lxml.etree.fromstring("""
    <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><title>Business Rules</title><link rel="stylesheet" type="text/css" href="style.css" media="screen"/></head>
    """))
    b = lxml.etree.SubElement(root, "body")
    htdoc = lxml.etree.ElementTree(root)
    md = load_sheet_metadata(d, b)
    cd = load_codelist_metadata(d, b)
    create_section_list(d, b, fileoutbase, md, cd, sheets, params)
    create_section_divs(d, b, fileoutbase, md, cd, sheets, params)
    return lxml.etree.tostring(htdoc, pretty_print=True)

def add_strike_through(d):
    for style in d.xpath(
        "child::ss:Styles/ss:Style[ss:Font[@ss:StrikeThrough='1']]",
        namespaces=nss
    ):
        style_id = style.get(namespaced('ss','ID'))
        strike_through_styles.append(style_id)

def load_sheet_metadata(d, b):
    md = {}
    for sheet in d.xpath('/ss:Workbook/ss:Worksheet', namespaces=nss):
        name = sheet.get(namespaced('ss','Name'))
        if name == 'Sheets and Rule IDs':
            for row in sheet.xpath('ss:Table/ss:Row', namespaces=nss)[1:]:
                cells = row.xpath('ss:Cell', namespaces=nss)
                if len(cells) > 2:
                    c1 = row.xpath('ss:Cell[1]/ss:Data/text()', namespaces=nss)[0]
                    c2 = row.xpath('ss:Cell[2]/ss:Data/text()', namespaces=nss)[0]
                    c3 = row.xpath('ss:Cell[3]/ss:Data/text()', namespaces=nss)[0]
                    c5 = row.xpath('ss:Cell[5]/ss:Data/text()', namespaces=nss)[0]
                    md[c1] = "{} ({}) version: v{}".format(c2, c3, c5)
    return md

def load_codelist_metadata(d, b):
    cd = {}
    for sheet in d.xpath('/ss:Workbook/ss:Worksheet', namespaces=nss):
        name = sheet.get(namespaced('ss','Name'))
        if name == 'CodesList Overview':
            for row in sheet.xpath('ss:Table/ss:Row', namespaces=nss)[1:]:
                cells = row.xpath('ss:Cell', namespaces=nss)
                if len(cells) > 2:
                    c1 = row.xpath('ss:Cell[1]/ss:Data/text()', namespaces=nss)[0]
                    c2 = row.xpath('ss:Cell[2]/ss:Data/text()', namespaces=nss)[0]
                    cd[c1] = c2
    return cd

def get_title(name, md):
    for (i,j) in md.items():
        if re.match(i, name):
            t = j
            if re.match('-C$', name):
                t = t + ' (Content)'
            if re.match('-S', name):
                t = t + ' (Structure)'
            return t

def create_section_list(d, b, fileoutbase, md, cd, sheets=[],params={}):
    div = lxml.etree.SubElement(b, 'div', id="index")

    level = params.get('headerlevel', 1)
    h = lxml.etree.SubElement(div, 'h{}'.format(level))
    h.text = params.get('headertext','Business Rules')
    if params.get('tocheader', True):
        htoc = lxml.etree.SubElement(div, 'h{}'.format(level+1))
        htoc.text = 'Table of Contents'
    ol = lxml.etree.SubElement(div, 'ol')
    #print(d.getroot())
    for sheet in d.xpath('/ss:Workbook/ss:Worksheet', namespaces=nss):
        name = sheet.get(namespaced('ss','Name'))
        if sheets == [] or name in sheets:
            if name not in ['Sheets and Rule IDs', 'CodesList Overview']:
                li = lxml.etree.SubElement(ol, 'li')
                a = lxml.etree.SubElement(li, 'a', href="#{}".format(name))
                #a.text = name
                a.text = "{}: {}".format(name, get_title(name, md))

def create_section_divs(d, b, fileoutbase, md, cd, sheets=[], params={}):
    level = params.get('headerlevel', 2)
    for sheet in d.xpath('/ss:Workbook/ss:Worksheet', namespaces=nss):
        name = sheet.get(namespaced('ss','Name'))
        if sheets == [] or name in sheets:
            if name not in ['Sheets and Rule IDs', 'CodesList Overview']:
                print('Processing tab {}'.format(name))
                a = lxml.etree.SubElement(b, 'a', name=name, id=name)
                a.text = ' '
                div = lxml.etree.SubElement(b, 'div')
                h = lxml.etree.SubElement(div, 'h{}'.format(level+1))
                h.text = '{}: {}'.format(name, get_title(name, md))
                table = lxml.etree.SubElement(div,'table')
                for (counter, row)  in enumerate(
                        sheet.xpath('ss:Table/ss:Row', namespaces=nss)[1:],
                        start=1
                ):
                    print('___ Processing row {}'.format(counter))
                    cells = row.xpath('ss:Cell', namespaces=nss)
                    if len(cells) > 2:
                        data2 = row.xpath('ss:Cell[2]/ss:Data', namespaces=nss)
                        if len(data2) > 0:
                            if not row_has_strike_through(row):
                                tr = lxml.etree.SubElement(table, 'tr')
                                td = lxml.etree.SubElement(tr,'td')
                                #print(row)
                                #print(lxml.etree.tostring(row))

                                rule_id = row.xpath('ss:Cell[2]/ss:Data/text()', namespaces=nss)[0]
                                #print(rule_id)

                                data = find_value_from_column(row, 2)
                                if data is not None:
                                    a = lxml.etree.SubElement(td, 'a', href="#{}".format(data))
                                    td.text = data
                                td2 = lxml.etree.SubElement(tr, 'td')
                                table2 = lxml.etree.SubElement(td2, 'table')
                                table2.set('style', 'width:100%')
                                add_property_from_column(row, 1, table2, 'RuleType', css='head')
                                add_property_from_column(row, 2, table2, 'Rule ID')
                                add_property_from_column(row, 3, table2, 'Element')
                                add_property_from_column(row, 4, table2, 'Location')
                                add_property_from_column(row, 5, table2, 'Rule')
                                add_property_from_column(row, 6, table2, 'Role')

                                add_property_from_column(row, 8, table2, 'ShortName',
                                                         separator_row='Codelist Information')
                                add_property_from_column(row, 9, table2, 'LongName @xml:lang="en"')
                                add_property_from_column(row, 10, table2, 'LongName @Identifier="listID"', cd=cd)
                                add_property_from_column(row, 11, table2, 'Version')
                                add_property_from_column(row, 12, table2, 'LocationURI')
                                add_property_from_column(row, 13, table2, 'Agency')
                                add_property_from_column(row, 14, table2, 'AlternateFormatLocationUri')

                                add_property_from_column(row, 15, table2, 'ShortName',
                                                         separator_row='Additional codelist Information')
                                add_property_from_column(row, 16, table2, 'LongName @xml:lang="en"')
                                add_property_from_column(row, 17, table2, 'LongName @Identifier="listID"', cd=cd)
                                add_property_from_column(row, 18, table2, 'Version')
                                add_property_from_column(row, 19, table2, 'LocationURI')
                                add_property_from_column(row, 20, table2, 'Agency')
                                add_property_from_column(row, 21, table2, 'AlternateFormatLocationUri')



def row_has_strike_through(row):
    data = find_value_from_column(row, 2)
    cells = row.xpath('child::ss:Cell', namespaces=nss)
    for c in cells:
        st = c.get(namespaced('ss', 'StyleID'))
        if st is not None:
            if st in strike_through_styles:
                print('Strike through for {}'.format(data))
                return True
    return False

def add_property_from_column(inrow, requested_position, table2, property, css=None, cd=None, separator_row=None):
    data = find_value_from_column(inrow, requested_position)
    if data is not None and separator_row is not None:
        outrow = lxml.etree.SubElement(table2, 'tr')
        td1 = lxml.etree.SubElement(outrow, 'td')
        highlighted = lxml.etree.SubElement(td1, 'b')
        highlighted.text = separator_row
    if data is not None:
        outrow = lxml.etree.SubElement(table2, 'tr')
        td1 = lxml.etree.SubElement(outrow, 'td')
        td1.text = property
        td2 = lxml.etree.SubElement(outrow, 'td')
        if data == "0":
            print("Column {} has 0 content".format(property))
            if property  == 'LongName @Identifier="listID"':
                short_name = find_value_from_column(inrow, 8)
                data = cd[short_name]
                print('Assuming long value {} for {}'.format(
                    data, short_name)
                )

        if data[0:7] == 'http://' or data[0:8] == 'https://':
            a = lxml.etree.SubElement(td2, 'a', href=data)
            a.text = data
        else:
            td2.text = data

def find_value_from_column(inrow, requested_position):
    #print('Process column searching for {} at {}'.format(property, requested_position))
    position = 0
    for cell in inrow.xpath('ss:Cell', namespaces=nss):
        #print('Position is {} requested is {}'.format(position, requested_position))
        indexv = cell.get(namespaced('ss','Index'))
        if indexv is not None:
            position = int(indexv)
            #print('Setting position from index to {}'.format(position))
        else:
            position += 1
            #print('Setting position sequence to {}'.format(position))
        if position == requested_position:
            #print('Found value {} {}'.format(position, requested_position))
            data = ''.join(
                cell.xpath(
                    'descendant-or-self::text()', namespaces=nss)).replace(
                "\n", "").lstrip(' ').rstrip(' ')
            if bool(re.match('\S',data)):
                return(data)
            else:
                return None
    return None


if __name__ == "__main__":
    if len(sys.argv) == 2:
        infile = sys.argv[1]
        base = os.path.splitext(infile)[0]
        print(infile)
        for (sheets, filenamebase, params) in [
            ([], base, {}),
            (['EDM-ebMS'],  '4.7.2', {}),
            (['EDM-REQ-C', 'EDM-REQ-S', 'EDM-RESP-C',
              'EDM-RESP-S', 'EDM-ERR-C', 'EDM-ERR-S'], '4.6', {
                'headertext': '4.6.2. Business Rules'
            }),
            (['DSD-RESP-C', 'DSD-RESP-S',
              'DSD-ERR-C', 'DSD-ERR-S',
              'DSD-SUB-C', 'DSD-SUB-S',
              'DSD-SUB_RF-C', 'DSD-SUB_RF-S'], '3.1.7', {}),
            (['EB-EVI-C', 'EB-EVI-S',
              'EB-REQ-C', 'EB-REQ-S',
              'EB-ERR',
              'EB-SUB-C', 'EB-SUB-S'],'3.2.6', {}),
            (['LCM-ERR', 'LCM-SUC', 'LCM-SUB-S'], '3.6.3', {
                'headertext': '3.7.2 Business Rules of the LCM',
                'headerlevel': 3,
                'tocheader': False
            })
        ]:
            tofile = filenamebase+'.html'
            print('File {} selection {}'.format(tofile, sheets))
            html = br_xslsx_to_html(infile, filenamebase, sheets, params)
            with open(tofile,'wb') as fd:
                fd.write(html)
    else:
        print('Usage:  python br_xlsx_to_html.py <filename>')

