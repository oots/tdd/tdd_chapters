<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    
    <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <!--Start of SR metadata-->
    <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EDM-ERR-S</sr:identifier>
            <sr:title xml:lang="en">EDM-ERR-S</sr:title>
            <sr:description xml:lang="en">Error Response Syntax Mapping (Structure)</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>STRUCTURE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-ERR</sr:identifier>
                    <sr:title xml:lang="en">EDM-ERR</sr:title>
                    <sr:description xml:lang="en">Evidence Error Response</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>EDM</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Added Rules to Schematron (Structure) to prove if a rim:Slot has a rim:SlotValue child element and rim:SlotValue has any child element. Updating Business Rules Spreadsheet, including business rule to prove XSD elemnet restrictions on "IsAbout" Element of EvidenceResponse</sr:versionNotes>
            </sr:Asset>?>
    <!--End of SR metadata-->
    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ln" value="local-name(/node())"/>
            <sch:assert test="$ln ='QueryResponse'" id="R-EDM-ERR-S001" role='FATAL'
                >The root element of a query response document MUST be 'query:QueryResponse'</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ns" value="namespace-uri(/node())"/>
            <sch:assert test="$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0'" id="R-EDM-ERR-S002" role='FATAL'
                >The namespace of root element of a 'query:QueryResponse' must be 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0'</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="@requestId or count(rs:Exception[@xsi:type='rs:InvalidRequestExceptionType'])=1" id="R-EDM-ERR-S003" role='CAUTION'
                >If the rs:Exception of @xsi:type='rs:InvalidRequestExceptionType' is used the 'requestID' attribute of a 'QueryResponse' MAY be absent.
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/@requestId">
            <sch:assert test="matches(normalize-space((.)),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" id="R-EDM-ERR-S004" role='FATAL'
                >The 'requestID' attribute of a 'QueryResponse' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:" and match the corresponding request.
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure'" id="R-EDM-ERR-S006" role='FATAL'
                >The 'status' attribute of an unsuccessfull 'query:QueryResponse' MUST be encoded as as 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure'.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="not(rim:RegistryObjectList)" id="R-EDM-ERR-S007" role='FATAL'
                >An unsuccessful 'query:QueryResponse' does not include a 'rim:RegistryObjectList'.</sch:assert>         
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="count(rs:Exception)>0" id="R-EDM-ERR-S008" role='FATAL'
                >An unsuccessful 'query:QueryResponse' includes an Exception</sch:assert>
        </sch:rule>
    </sch:pattern>  
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="count(rim:Slot[@name='SpecificationIdentifier'])=1" id="R-EDM-ERR-S009" role='FATAL'
                >The rim:Slot name="SpecificationIdentifier" MUST be present in the QueryResponse.</sch:assert>
        </sch:rule>
    </sch:pattern>  
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="count(rim:Slot[@name='EvidenceResponseIdentifier'])=1" id="R-EDM-ERR-S010" role='FATAL'
                >The rim:Slot name="EvidenceResponseIdentifier" MUST be present in the QueryResponse.</sch:assert>
        </sch:rule>
    </sch:pattern>  
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="count(rim:Slot[@name='EvidenceRequester'])=1" id="R-EDM-ERR-S012" role='CAUTION'
                >The rim:Slot name="EvidenceRequester" MAY be present in the QueryResponse.</sch:assert>
        </sch:rule>
    </sch:pattern>  
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="count(rim:Slot[@name='ErrorProvider'])=1" id="R-EDM-ERR-S011" role='FATAL'
                >The rim:Slot name="ErrorProvider" MUST be present in the QueryResponse.</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='StringValueType'" id="R-EDM-ERR-S018" role='FATAL'
                >The rim:SlotValue of rim:Slot name="SpecificationIdentifier" MUST be of "rim:StringValueType"</sch:assert>
        </sch:rule>
    </sch:pattern>  
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceResponseIdentifier']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='StringValueType'" id="R-EDM-ERR-S019" role='FATAL'
                >The rim:SlotValue of rim:Slot name="EvidenceResponseIdentifier" MUST be of "rim:StringValueType"</sch:assert>
        </sch:rule>
    </sch:pattern>  
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='Timestamp']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='DateTimeValueType'" id="R-EDM-ERR-S022" role='FATAL'
                >The rim:SlotValue of rim:Slot name="Timestamp" MUST be of "rim:DateTimeValueType"</sch:assert>
        </sch:rule>
    </sch:pattern>  
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='AnyValueType'" id="R-EDM-ERR-S021" role='FATAL'
                >The rim:SlotValue of rim:Slot name="EvidenceRequester" MUST be of "rim:AnyValueType"</sch:assert>
        </sch:rule>
    </sch:pattern>  
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='AnyValueType'" id="R-EDM-ERR-S020" role='FATAL'
                >The rim:SlotValue of rim:Slot name="ErrorProvider" MUST be of "rim:AnyValueType"</sch:assert>
            <sch:assert test="count(sdg:Agent)=1" id="R-EDM-ERR-S026" role='FATAL'
                >An OOTS response ErrorProvider slot MUST have one sdg:Agent element.</sch:assert>
        </sch:rule>
    </sch:pattern>
   
    <sch:pattern>
        <sch:rule context="rs:Exception">
            <sch:assert test="count(rim:Slot[@name='Timestamp'])=1" id="R-EDM-ERR-S013" role='FATAL'
                >The rim:Slot name="Timestamp" MUST be present in the rs:Exception.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <!--PreviewDescription and PreviewDescription are only present when PreviewLocation is present -->
    
    <sch:pattern>
        <sch:rule context="rs:Exception">
            <sch:assert test="count(rim:Slot[@name='PreviewLocation'])=1" id="R-EDM-ERR-S014" role='CAUTION'
                >The rim:Slot name="PreviewLocation" MAY be present in the rs:Exception.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rs:Exception">
            <sch:assert test="count(rim:Slot[@name='PreviewMethod'])=1" id="R-EDM-ERR-S016" role='CAUTION'
                >The rim:Slot name="PreviewMethod" MAY be present in the rs:Exception.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rs:Exception">
            <sch:assert test="count(rim:Slot[@name='PreviewDescription'])=1" id="R-EDM-ERR-S015" role='CAUTION'
                >The rim:Slot name="PreviewDescription" MAY be present in the rs:Exception.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <!-- Data types for Exception slot values-->
    
    <sch:pattern>
        <sch:rule context="rs:Exception/rim:Slot[@name='PreviewLocation']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='StringValueType'" id="R-EDM-ERR-S023" role='FATAL'
                >The rim:SlotValue of rim:Slot name="PreviewLocation" MUST be of "rim:StringValueType"</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rs:Exception/rim:Slot[@name='PreviewMethod']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='StringValueType'" id="R-EDM-ERR-S025" role='FATAL'
                >The rim:SlotValue of rim:Slot name="PreviewMethod" MUST be of "rim:StringValueType"</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rs:Exception/rim:Slot[@name='PreviewDescription']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='InternationalStringValueType'" id="R-EDM-ERR-S024" role='FATAL'
                >The rim:SlotValue of rim:Slot name="PreviewDescription" MUST be of "rim:InternationalStringValueType"</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse">   
            <sch:assert test="count(rim:Slot[@name!='SpecificationIdentifier' and @name!='EvidenceResponseIdentifier' and @name!='ErrorProvider' and
                @name!='EvidenceRequester']) = 0" id="R-EDM-ERR-S017" role='FATAL'
                >A 'query:QueryResponse' MUST not contain other rim:Slots than 'SpecificationIdentifier', 'EvidenceResponseIdentifier', 'ErrorProvider' and 'EvidenceRequester'.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>         
        <sch:rule context="query:QueryResponse/rs:Exception">   
            <sch:assert test="count(rim:Slot[@name!='Timestamp' and @name!= 'PreviewLocation' and @name!='PreviewDescription' and @name!='PreviewMethod']) = 0 " id="R-EDM-ERR-S027" role='FATAL'
                >A 'query:QueryResponse/rs:Exception' MUST not contain other rim:Slots than 'Timestamp', 'PreviewLocation', 'PreviewDescription' and 'PreviewMethod'.</sch:assert>
        </sch:rule>
    </sch:pattern>

    <!--
    Some generic RIM patterns
-->

    <sch:pattern>
        <sch:rule context="rim:Slot">
            <sch:assert test="count(child::rim:SlotValue) = 1" id="R-EDM-ERR-S028"
                        role="FATAL">A rim:Slot MUST have a rim:SlotValue child element</sch:assert>
        </sch:rule>
    </sch:pattern>


    <sch:pattern>
        <sch:rule context="rim:SlotValue">
            <sch:assert test="count(child::*) &gt; 0" id="R-EDM-ERR-S029"
                        role="FATAL">A rim:SlotValue MUST have child element content</sch:assert>
        </sch:rule>
    </sch:pattern>

    
</sch:schema>