<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    
    <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0" prefix="lcm"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <!--Start of SR metadata-->
    <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/LCM-SUC</sr:identifier>
            <sr:title xml:lang="en">LCM-SUC</sr:title>
            <sr:description xml:lang="en">Response for successful LCM submission</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>CONTENT</sr:theme>
            <sr:theme>STRUCTURE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/LCM-SUC</sr:identifier>
                    <sr:title xml:lang="en">LCM-SUC</sr:title>
                    <sr:description xml:lang="en">LCM Successfull Response</sr:description>
                    <sr:type>LCM</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>EB</sr:component>
                    <sr:component>DSD</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
            </sr:Asset>?>
    <!--End of SR metadata-->
    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ln" value="local-name(/node())"/>
            <sch:assert test="$ln ='RegistryResponse'" role='FATAL' id="R-LCM-SUC-001"
                >The root element of a query response document MUST be 'rs:RegistryResponse'</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ns" value="namespace-uri(/node())"/>
            <sch:assert test="$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0'" role='FATAL' id="R-LCM-SUC-002"
                >The namespace of root element of a 'rs:RegistryResponse' MUST be 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0'</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rs:RegistryResponse/@requestId">
            <sch:assert test="matches(normalize-space((.)),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" role='FATAL' id="R-LCM-SUC-004"
                >The 'requestId' attribute of a 'rs:RegistryResponse' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:" and match the corresponding request.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rs:RegistryResponse">
            <sch:let name="status" value="@status" />
            
            <sch:assert test="@requestId" role='FATAL' id="R-LCM-SUC-003"
                >The 'requestId' attribute of a 'rs:RegistryResponse' MUST be present. </sch:assert>
            
            <sch:assert test="@status" role='FATAL' id="R-LCM-SUC-005"
                >The 'status' attribute of a 'rs:RegistryResponse' MUST be present. </sch:assert>

            <sch:assert test="$status = 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success'" role='FATAL' id="R-LCM-SUC-006"
                >The 'status' attribute of a successful 'rs:RegistryResponse' MUST be encoded as as 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success'.</sch:assert>
            
            <sch:assert test="count(rim:RegistryObjectList) = 0" role='FATAL' id="R-LCM-SUC-007"
                >A successful 'rs:RegistryResponse' MUST not include a 'rim:RegistryObjectList'</sch:assert>
            
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rs:RegistryResponse">
            <sch:assert test="not(rim:Slot[@name != 'SpecificationIdentifier'])" role='FATAL' id="R-LCM-SUC-008"
            >An successful 'rs:RegistryResponse' MUST not contain any other rim:Slots than rim:Slot[@name='SpecificationIdentifier'].</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rs:RegistryResponse">
            <sch:assert test="rim:Slot[@name='SpecificationIdentifier']" role='FATAL' id="R-LCM-SUC-009"
            >The rim:Slot name="SpecificationIdentifier" MUST be present in the rs:RegistryResponse.</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rs:RegistryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')"/>
            <sch:assert test="$st ='StringValueType'" role='FATAL' id="R-LCM-SUC-010"
            >The rim:SlotValue of rim:Slot name="SpecificationIdentifier" MUST be of "rim:StringValueType".</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rs:RegistryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
            <sch:assert test="rim:Value='oots-cs:v1.1'" role='FATAL' id="R-LCM-SUC-011"
            >The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-cs:v1.1".</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    
</sch:schema>