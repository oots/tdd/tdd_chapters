<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <sch:ns prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EDM-ERR-C</sr:identifier>
           <sr:title xml:lang="en">EDM-ERR-C</sr:title>
           <sr:description xml:lang="en">Error Response Syntax Mapping (Content)</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CONTENT</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-ERR</sr:identifier>
                 <sr:title xml:lang="en">EDM-ERR</sr:title>
                 <sr:description xml:lang="en">Evidence Error Response</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>PAYLOAD</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.1.0</sr:version>
           <sr:versionNotes xml:lang="en">Updated the version information in Schematron and added Version information to cross validaton Schematron and</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:include href="codelist-include/EEA_COUNTRY-CODELIST_code.sch"/>
   <sch:include href="codelist-include/AGENTCLASSIFICATION-CODELIST_code.sch"/>
   <sch:include href="codelist-include/OOTSMEDIATYPES-CODELIST_code.sch"/>
   <sch:include href="codelist-include/LANGUAGECODE-CODELIST_code.sch"/>
   <sch:include href="codelist-include/EAS_Code.sch"/>
   <sch:include href="codelist-include/COUNTRYIDENTIFICATIONCODE-CODELIST_code.sch"/>
   <sch:include href="codelist-include/EDMERRORCODES-CODELIST_code.sch"/>
   <sch:include href="codelist-include/EDMERRORCODES-CODELIST_name-Type.sch"/>
   <sch:include href="codelist-include/ERRORSEVERITY-CODELIST_code.sch"/>
   
   <sch:pattern>       
      <sch:rule context="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
         <sch:assert test="rim:Value='oots-edm:v1.1'" id="R-EDM-ERR-C001" role='FATAL'
            >The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-edm:v1.1".</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>  
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceResponseIdentifier']/rim:SlotValue/rim:Value">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" id="R-EDM-ERR-C002" role='FATAL'
            >The 'rim:Value' of the 'EvidenceResponseIdentifier' MUST be unique UUID (RFC 4122) for each response.</sch:assert>
      </sch:rule>
   </sch:pattern> 
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier">
         <sch:assert test="@schemeID" id="R-EDM-ERR-C003" role='FATAL'
            >The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>         
      </sch:rule>
   </sch:pattern>
                       
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent">
         <sch:assert test="not(normalize-space(sdg:Classification)='')" id="R-EDM-ERR-C007" role='FATAL'
            >The value for 'Agent/Classification' MUST be provided.</sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier">
         <sch:assert test="@schemeID" id="R-EDM-ERR-C009" role='FATAL'
            >The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure']">
         <sch:assert test="count(rs:Exception)=1" id="R-EDM-ERR-C011" role='FATAL'
            >The 'rs:Exception' class of a 'QueryResponse' MUST be present if  'status' attribute of a 'QueryResponse' is 
            "urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure".</sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception">
         <sch:assert test="@xsi:type" id="R-EDM-ERR-C012" role='FATAL'
            >The 'xsi:type' attribute of a 'rs:Exception' MUST be present. </sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception[@code != 'EDM:ERR:0002']">
         <sch:assert test="matches(@severity,'urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error')" id="R-EDM-ERR-C015" role='FATAL'
            >The value of 'severity' attribute of a 'rs:Exception' MUST  be 'urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error' if the 
            rs:Exception  (@code 'EDM:ERR:0002') is NOT used.</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception">
         <sch:assert test="@message" id="R-EDM-ERR-C016" role='FATAL'
            >The 'message' attribute of a 'rs:Exception' MUST be present. </sch:assert>         
      </sch:rule>
   </sch:pattern>

   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception">
         <sch:assert test="@code" id="R-EDM-ERR-C026" role='FATAL'
         >The 'code' attribute of a 'rs:Exception' MUST be present. </sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='Timestamp']/rim:SlotValue/rim:Value">
         <sch:assert test="matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}','i')" id="R-EDM-ERR-C018" role='FATAL'
            >The 'rim:Value' of 'Timestamp' MUST be according to xsd:dateTime.</sch:assert>
      </sch:rule>
   </sch:pattern>
    
   <sch:pattern>        
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewLocation']/rim:SlotValue">
         <sch:assert test="starts-with(lower-case(rim:Value/text()), 'https://')" id="R-EDM-ERR-C019" role='FATAL'
            >The 'rim:Value' of a 'PreviewLocation' MUST be a URI starting with 'https://' according to RFC 3986.</sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>       
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewMethod']/rim:SlotValue">
         <sch:assert test="matches(rim:Value, '^GET$') or matches(rim:Value, '^POST$') or matches(rim:Value, '^PUT$')" id="R-EDM-ERR-C021" role='FATAL'
            >The 'rim:Value' of a 'PreviewMethod' MUST be either the HTTP verb 'GET', 'POST' or 'PUT'.</sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewLocation']">
         <sch:assert test="//@severity='urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired'" id="R-EDM-ERR-C022" role='FATAL'
            >If a 'rim:Slot[@name='PreviewLocation']' is provided, the 'rs:Exception' MUST  be 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired' and use the rs:Exception xsi:type='rs:AuthorizationExceptionType' (@code 'EDM:ERR:0002').</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue">
         <sch:assert test="count(sdg:Agent)=1" id="R-EDM-ERR-C023" role='FATAL'
            >In the ErrorProvider slot there MUST be a single agent.</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Address">
         <sch:assert test="count(sdg:AdminUnitLevel1)=1" id="R-EDM-ERR-C024" role='FATAL'
            >A value for 'AdminUnitLevel1' MUST be provided.</sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse">
         <sch:assert test="@requestId or rs:Exception/@xsi:type='rs:InvalidRequestExceptionType'" id="R-EDM-ERR-C025" role='FATAL'
            >If  the query:QueryResponse does not contain a requestId, the 'rs:Exception' MUST use the rs:Exception xsi:type='rs:InvalidRequestExceptionType' (@code 'EDM:ERR:0003') and no other value.</sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier">
         <!--{}[](EAS-CodeList, EEA_CountryCodeList)-->
         <sch:let name="suffix" value="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
         <sch:let name="suffix1" value="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
         <sch:assert test="((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256" id="R-EDM-ERR-C010" role='FATAL'
            >The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_CountryCodeList’. For testing purposes the code "oots" can be used.</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier">
         <!--{}[](EAS-CodeList, EEA_CountryCodeList)-->
         <sch:let name="suffix" value="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
         <sch:let name="suffix1" value="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
         <sch:assert test="((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256" id="R-EDM-ERR-C004" role='FATAL'
            >The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_CountryCodeList’. For testing purposes the code "oots" can be used. </sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code" id="R-EDM-ERR-C005" role='FATAL'
            >Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1'</sch:assert>
      </sch:rule>        
      <sch:rule context="query:QueryResponse/rim:Slot[@name='ErrorProvider']/rim:SlotValue/sdg:Agent">
         <!--{}[](AgentClassification-CodeList)-->
         <sch:assert test="(some $code in $AGENTCLASSIFICATION-CODELIST_code satisfies sdg:Classification=$code) and (matches(sdg:Classification,'EP') or matches(sdg:Classification,'IP') or matches(sdg:Classification,'ERRP'))" id="R-EDM-ERR-C008" role='FATAL'
            >Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="sdg:Classification"/>' is unacceptable for constraints identified by 'AgentClassification-CodeList' in the context 'query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Classification'.Default value: ERRP (Error Provider). The code 'ER' shall not be used by this transaction.</sch:assert>
      </sch:rule>  
      <sch:rule context="query:QueryResponse/rs:Exception/@xsi:type">
         <!--{}[](EDMErrorCodes-CodeList)-->
         <sch:assert test="some $code in $EDMERRORCODES-CODELIST_name-Type satisfies .=$code" id="R-EDM-ERR-C013" role='ERROR'>
            Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="@xsi:type"/>' is unacceptable for constraints identified by 'EDMErrorCodes-CodeList' in the context 'query:QueryResponse/rs:Exception/@xsi:type' check <sch:value-of select="."/></sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rs:Exception/@code">
         <!--{}[](EDMErrorCodes-CodeList)-->
         <sch:assert test="some $code in $EDMERRORCODES-CODELIST_code satisfies .=$code" id="R-EDM-ERR-C017" role='FATAL'
            >Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'EDMErrorCodes-CodeList' in the context 'query:QueryResponse/rs:Exception/@code'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rs:Exception/@severity">
         <!--{}[](ErrorSeverity-CodeList)-->
         <sch:assert test="(some $code in $ERRORSEVERITY-CODELIST_code satisfies .=$code) and not(.='urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:DSDErrorResponse:AdditionalInput') " id="R-EDM-ERR-C014" role='FATAL'
            >The 'severity' attribute of a 'rs:Exception' MUST be present. It must be part of the code list 'ErrorSeverity' (Error Severity). The code 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:DSDErrorResponse:AdditionalInput' shall not be used by this transaction.</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewDescription']/rim:SlotValue/rim:Value/rim:LocalizedString/@xml:lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code" id="R-EDM-ERR-C020" role='FATAL'
            >Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rs:Exception/rim:Slot[@name='PreviewDescription']/rim:SlotValue/rim:Value/rim:LocalizedString/@*[namespace-uri()='http://www.w3.org/XML/1998/namespace' and local-name()='lang']'</sch:assert>
      </sch:rule>
   </sch:pattern>
</sch:schema>   
