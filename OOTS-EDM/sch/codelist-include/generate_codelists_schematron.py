import lxml.etree
import os

def gen_codelist_fragment_from_file(name, file_path, code_variant):
    """
    Generates a Schematron fragment for the specified codelist file and code variant.

    :param name: The name of the Schematron variable.
    :param file_path: The path to the codelist file.
    :param code_variant: The variant of the code to search for (e.g., 'code', 'Code', 'name_Type').
    :return: A Schematron fragment string if valid values are found; otherwise, None.
    """
    try:
        with open(file_path, 'r', encoding='utf8') as file:
            gc_file = file.read()

        try:
            r = lxml.etree.fromstring(gc_file.encode('utf8'))
        except lxml.etree.XMLSyntaxError as e:
            return f"XML parsing error: {e}"

        try:
            # Extract the values for the given code variant
            codes = " ".join(r.xpath(
                f'//Value[@ColumnRef="{code_variant}"]/SimpleValue/text()',
                namespaces={'gc': 'http://docs.oasis-open.org/codelist/ns/genericode/1.0/'}
            )).strip()

            # Only return a fragment if codes are found
            if codes:
                return f"""<sch:let name="{name}_{code_variant}" xmlns:sch="http://purl.oclc.org/dsdl/schematron"
                value="tokenize('{codes}', '\\s')"/>"""
            else:
                return None
        except lxml.etree.XPathEvalError as e:
            return f"XPath evaluation error: {e}"
    
    except FileNotFoundError:
        return f"File not found: {file_path}"
    except IOError as e:
        return f"IO error: {e} for file: {file_path}"

def process_directory(input_directory, output_directory):
    """
    Processes all .gc files in the input directory, generating Schematron files
    for the specified code variants ('code', 'Code', 'name_Type') if values exist.

    :param input_directory: Directory containing the .gc files.
    :param output_directory: Directory to save the generated Schematron files.
    """
    if not os.path.exists(output_directory):
        try:
            os.makedirs(output_directory)
        except OSError as e:
            print(f"Failed to create output directory: {output_directory}, error: {e}")
            return

    excluded_files = {'LAU2022-CodeList.gc'}

    for root, dirs, files in os.walk(input_directory):
        for file in files:
            if file.endswith('.gc') and file not in excluded_files:
                file_path = os.path.join(root, file)
                name = os.path.splitext(file)[0].upper()

                for code_variant in ["code", "Code", "name-Type"]:
                    try:
                        fragment = gen_codelist_fragment_from_file(name, file_path, code_variant)
                        
                        if fragment and "tokenize" in fragment:
                            schematron_file_path = os.path.join(output_directory, f"{name}_{code_variant}.sch")
                            try:
                                with open(schematron_file_path, 'w', encoding='utf8') as sch_file:
                                    sch_file.write(fragment)
                                print(f"Generated Schematron file: {schematron_file_path}")
                            except IOError as e:
                                print(f"Failed to write Schematron file: {schematron_file_path}, error: {e}")
                        else:
                            print(f"No valid values found for {code_variant} in {file_path}, skipping.")
                    except Exception as e:
                        print(f"Error processing file: {file_path}, code_variant: {code_variant}, error: {e}")

if __name__ == "__main__":
    input_directory = '/Users/rotunacarmenionela/tdd_chapters/OOTS-EDM/codelists'
    output_directory = '/Users/rotunacarmenionela/tdd_chapters/OOTS-EDM/sch/codelist-include'
    
    process_directory(input_directory, output_directory)
    print("All files processed successfully.")
