# Codelist Tooling

## 1. Introduction 

The initial version of the codelist tooling proved to be difficult cu maintain because every time a change on a codelist genericode file was done the corresponding schematron rules needed an update. This is the reason it was decided to develop a new Codelist Tooling that automates the update process for codelist files updates.
In the first step of development the intention was to generate for each Genericode file in the codelist directory,  an importable Schematron fragment that defines a variable named after the filename and includes the codelist values.

## 2. Development
The Schematron <include> element is a feature used to modularize and organize Schematron schemas by allowing the inclusion of  external Schematron files or fragments within another Schematron schema. This improves maintainability and reusability, especially in complex validation scenarios where multiple rulesets need to be managed.
In the first step of development the intention was to generate for each Genericode file in the codelist directory,  an importable Schematron fragment that defines a variable named after the Genericode filename that includes the codelist values. Example:  "LanguageCode-Codelist.sch" is a fragment.  All generated fragments are available in tdd_chapters/OOTS-EDM/sch/codelist-include(https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/release_1.1.0/OOTS-EDM/sch/codelist-include) folder  
Developed a script that parses the codelist folder and for every codelist file generates a fragment with all “code” values present in codelist and creates the fragment files available in location: tdd_chapters/OOTS-EDM/sch/codelist-include.
 
Issue: from some codelist files for schematron checks we need 2 distinct values (which translates in generating 2 distinct fragments): one for “code” and another for “name-Type”
```<SimpleCodeList>
      <Row>
         <Value ColumnRef="code">
            <SimpleValue>EB:ERR:0001</SimpleValue>
         </Value>
         <Value ColumnRef="name-Value">
            <SimpleValue>The result set is empty</SimpleValue>
         </Value>
         <Value ColumnRef="name-Type">
            <SimpleValue>rs:ObjectNotFoundExceptionType</SimpleValue>
         </Value>
      </Row>
…..
<SimpleCodeList>
```

Thus for the genericode file EBErrorCodes-CodeList.gc, for example, and several other files 2 fragments were generated. Example: for EBErrorCodes-CodeList.gc as input the script generates 2 fragments: 
EBERRORCODES-CODELIST_code.sch 
EBERRORCODES-CODELIST_name-Type.sch
 
The script generates the second fragment only if name-Type exists. For EEA_Country-CodeList.gc as name-Type is absent the script generates only EEA_COUNTRY-CODELIST_code.sch fragment
 
Then for any Schematron that has a rule for a codelist,  we only need to import the relevant scheme fragment.  After the import, the value set is in a variable that can be used in any expression.

					
## 3. Example
   * Fragment example, saved in OOTSMEDIATYPES-CODELIST_code.sch file
```<sch:let name="OOTSMEDIATYPES-CODELIST_code" xmlns:sch="http://purl.oclc.org/dsdl/schematron"
 value="tokenize('application/pdf image/png image/jpeg text/csv application/vnd.openxmlformats-officedocument.spreadsheetml.sheet application/vnd.oasis.opendocument.spreadsheet', '\s')"/>
``` 
   * Schematron usage example:
 ```
 <?xml version="1.0" encoding="UTF-8"?>
 <sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2" xmlns:sqf="http://www.schematron-quickfix.com/validator/process">
    <sch:include href="OOTSMEDIATYPES-CODELIST_code.sch"/>...
    <sch:pattern>
        <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format">
          <!--{}[](OOTSMediaTypes-CodeList)-->
          <sch:assert test="some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code" id="R-EDM-REQ-C033" role='FATAL'
             >Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'OOTSMediaTypes-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format'</sch:assert>
       </sch:rule>
     </sch:pattern>...
 </sch:schema>
```
## 4. Code list generation tooling advantages
   * Easy maintenance:  we only need to maintain the codelists and make sure they are available as (exported to) Genericode files.
   * Easy use for Schematron writers:  each Schematron file only needs an import.
   * Easy to read for readers
   * Easy maintenance:  only maintain the codelists files and ensure they are available as (exported to) Genericode files.
   * Stable Schematron Rules: genericode file updates do not require changes to the Schematron rules. Running the Python script automatically regenerates all fragments with updated values.
   * User-friendly for Schematron developers:  each Schematron file only needs to import the necessary fragments.
   * Improved Consistency: the tooling ensures consistent validation across all Schematron rules by automatically incorporating updated codelists, reducing the risk of discrepancies.
   * Scalability: the system is easily scalable for adding new codelists or expanding existing ones without requiring significant changes to the Schematron rules.
   * Automation: the integration of a Python script automates the regeneration of fragments, saving time and reducing manual errors in updating the codelist values.
   * Reduced Complexity: the clear separation of concerns between codelists and Schematron logic reduces the complexity for both development and troubleshooting.
   * Enhanced Flexibility: tooling can be easily accommodated, offering more flexibility in adapting to future requirements without major refactoring.


