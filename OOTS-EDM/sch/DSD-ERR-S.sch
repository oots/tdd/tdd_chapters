<?xml version="1.0" encoding="UTF-8"?>
<sch:schema 
    xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    
    <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <!--Start of SR metadata-->
    <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/DSD-ERR-S</sr:identifier>
            <sr:title xml:lang="en">DSD-ERR-S</sr:title>
            <sr:description xml:lang="en">Query Error Response of the DSD (Structure)</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>STRUCTURE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-ERR</sr:identifier>
                    <sr:title xml:lang="en">DSD-ERR</sr:title>
                    <sr:description xml:lang="en">Query Error Response of the DSD</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:component>DSD</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
            </sr:Asset>?>
    <!--End of SR metadata-->
    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ln" value="local-name(/node())"/>
            <sch:assert test="$ln ='QueryResponse'" role='FATAL' id="R-DSD-ERR-S001" 
                >The root element of a query response document MUST be 'query:QueryResponse'</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ns" value="namespace-uri(/node())"/>
            <sch:assert test="$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0'" role='FATAL' id="R-DSD-ERR-S002"
                >The namespace of root element of a 'query:QueryResponse' MUST be 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0'</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="@status" role='FATAL' id="R-DSD-ERR-S005"
                >The 'status' attribute of a 'QueryResponse' MUST be present.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:let name="status" value="@status" />
            <sch:assert test="$status = 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure'" role='FATAL' id="R-DSD-ERR-S006"
                >The 'status' attribute of an unsuccessfull 'query:QueryResponse' MUST be encoded as 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure'.</sch:assert>
        </sch:rule>
    </sch:pattern>
    

    <sch:pattern>
        <!-- 
            If Response is Success there is only a RegistryObjectList 
            If Response is Failure then there is only an Exception 
        -->
        <sch:rule context="query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure']">
            <sch:assert test="count(rim:RegistryObjectList) = 0" role='FATAL' id="R-DSD-ERR-S007"
                >An unsuccessful 'query:QueryResponse' MUST not include a 'rim:RegistryObjectList'</sch:assert>
            
            <sch:assert test="count(rs:Exception)>0" role='FATAL' id="R-DSD-ERR-S008"
                >An unsuccessful 'query:QueryResponse' MUST include an rs:Exception</sch:assert>
            
            <sch:let name="rs_present" value="count(rs:Exception[@code='DSD:ERR:0005'])"/> 
            <sch:let name="slot_number" value="count(rs:Exception/rim:Slot)"/>   
            <sch:assert test="not($rs_present=0 and $slot_number>0)" role='FATAL' id="R-DSD-ERR-S009"
                >An unsuccessfull 'query:QueryResponse' which does not contain the rs:Exception xsi:type='rs:ObjectNotFoundExceptionType' (DSD:ERR:0005) MUST not contain any rim:Slots.</sch:assert>
        </sch:rule> 
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception[@code='DSD:ERR:0005']/rim:Slot">
            <sch:assert test="@name='DataServiceEvidenceType' or @name='UserRequestedClassificationConcepts' or @name='JurisdictionDetermination'" role='FATAL' id="R-DSD-ERR-S010"
                >An unsuccessful 'query:QueryResponse' which does contain the rs:Exception xsi:type='rs:ObjectNotFoundExceptionType' (DSD:ERR:0005) MUST not contain other rim:Slots than 'JurisdictionDetermination', 'UserRequestedClassificationConcepts' and 'DataServiceEvidenceType'.</sch:assert> 
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception[@code='DSD:ERR:0005']">
            <sch:assert test="count(rim:Slot[@name='JurisdictionDetermination'])=1" role='CAUTION' id="R-DSD-ERR-S011"
                >An unsuccessful 'query:QueryResponse' containing the rs:Exception xsi:type='rs:ObjectNotFoundExceptionType' (DSD:ERR:0005) MAY contain rim:Slot name="JurisdictionDetermination" requesting additional user provided attributes.</sch:assert>     
            
            <sch:assert test="count(rim:Slot[@name='UserRequestedClassificationConcepts'])=1" role='CAUTION' id="R-DSD-ERR-S012"
                >An unsuccessful 'query:QueryResponse' containing the rs:Exception xsi:type='rs:ObjectNotFoundExceptionType' (DSD:ERR:0005) MAY contain a  rim:Slot name="UserRequestedClassificationConcepts" requesting additional user provided attributes.</sch:assert>
            
            <sch:assert test="count(rim:Slot[@name='DataServiceEvidenceType'])=1" role='FATAL' id="R-DSD-ERR-S013" 
                >An unsuccessful 'query:QueryResponse' containing the rs:Exception xsi:type='rs:ObjectNotFoundExceptionType' (DSD:ERR:0005) MUST contain  rim:Slot name="DataServiceEvidenceType" requesting additional user provided attributes.</sch:assert>
        </sch:rule>  
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='JurisdictionDetermination']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='AnyValueType'" role='FATAL' id="R-DSD-ERR-S014"
                >The rim:SlotValue of rim:Slot name="JurisdictionDetermination" MUST be of "rim:AnyValueType"</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='UserRequestedClassificationConcepts']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='CollectionValueType'" role='FATAL' id="R-DSD-ERR-S015"
                >The rim:SlotValue of rim:Slot name="UserRequestedClassificationConcepts" MUST be of "rim:CollectionValueType"</sch:assert>
        </sch:rule>  
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='UserRequestedClassificationConcepts']/rim:SlotValue/rim:Element">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='AnyValueType'" role='FATAL' id="R-DSD-ERR-S016"
                >The rim:Element of rim:SlotValue of rim:Slot name="UserRequestedClassificationConcepts" MUST be of "rim:AnyValueType"</sch:assert>
        </sch:rule>  
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='AnyValueType'" role='FATAL' id="R-DSD-ERR-S017"
                >The rim:SlotValue of rim:Slot name="DataServiceEvidenceType" MUST be of "rim:AnyValueType"</sch:assert>
        </sch:rule> 
    </sch:pattern>
    
    <sch:pattern>            
        <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue">
            <sch:assert test="sdg:DataServiceEvidenceType" role='FATAL' id="R-DSD-ERR-S018"
                >The query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType'] MUST contain 'sdg:DataServiceEvidenceType' of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
        </sch:rule> 
    </sch:pattern>
    
    <sch:pattern>                       
        <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='UserRequestedClassificationConcepts']/rim:SlotValue/rim:Element">
            <sch:assert test="sdg:EvidenceProviderClassification" role='FATAL' id="R-DSD-ERR-S019"
                >If present, the query:QueryResponse/rs:Exception/rim:Slot[@name='UserRequestedClassificationConcepts'] MUST contain the 'sdg:EvidenceProviderClassification' of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>              
        </sch:rule>  
    </sch:pattern>
    
    <sch:pattern>    
        <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='JurisdictionDetermination']/rim:SlotValue">
            <sch:assert test="sdg:EvidenceProviderJurisdictionDetermination" role='FATAL' id="R-DSD-ERR-S020"
                >If present, the query:QueryResponse/rs:Exception/rim:Slot[@name='JurisdictionDetermination'] MUST contain one 'sdg:EvidenceProviderJurisdictionDetermination' of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
        </sch:rule> 
    </sch:pattern>
    
    <sch:pattern>                       
        <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType">
            <sch:assert test="count(sdg:Identifier) + count(sdg:EvidenceTypeClassification) + count(sdg:Title) + count(sdg:DistributedAs) + count(sdg:Description) + count(sdg:AuthenticationLevelOfAssurance) + count(sdg:Note) = count(child::*)" role='FATAL' id="R-DSD-ERR-S021"
                >A DataServiceEvidenceType 'rim:Slot[@name='DataServiceEvidenceType'/rim:SlotValue/sdg:DataServiceEvidenceType'] MUST not contain any other elements than 'sdg:Identifier', 'sdg:EvidenceTypeClassification', 'sdg:Title' and 'sdg:DistributedAs' and the optional elements 'sdg:Description', 'sdg:AuthenticationLevelOfAssurance' and 'sdg:Note'</sch:assert>
        </sch:rule> 
    </sch:pattern>
   
    <sch:pattern>
        <sch:rule context="rs:Exception[@code='DSD:ERR:0005']">
            <sch:assert test="count(rim:Slot[@name='UserRequestedClassificationConcepts'])+count(rim:Slot[@name='JurisdictionDetermination']) > 0" role='FATAL' id="R-DSD-ERR-S022"
                >An unsuccessful 'query:QueryResponse' containing the rs:Exception xsi:type='rs:ObjectNotFoundExceptionType' (DSD:ERR:0005) MUST contain either a rim:Slot name="UserRequestedClassificationConcepts" or a rim:Slot name="JurisdictionDetermination" or both.</sch:assert>
        </sch:rule> 
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='UserRequestedClassificationConcepts']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification">
            <sch:assert test="count(sdg:SupportedValue) = 0" role='FATAL' id="R-DSD-ERR-S023"
                >An EvidenceProviderClassification 'rim:Slot[@name='rim:Slot[@name='UserRequestedClassificationConcepts'/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification' MUST not 
                contain 'SupportedValue'.</sch:assert>
        </sch:rule> 
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="rim:Slot[@name='SpecificationIdentifier']" role='FATAL' id="R-DSD-ERR-S028"
            >The rim:Slot name="SpecificationIdentifier" MUST be present in the QueryResponse.</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')"/>
            <sch:assert test="$st ='StringValueType'" role='FATAL' id="R-DSD-ERR-S029"
            >The rim:SlotValue of rim:Slot name="SpecificationIdentifier" MUST be of "rim:StringValueType".</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="not(rim:Slot[@name != 'SpecificationIdentifier'])" role='FATAL' id="R-DSD-ERR-S030"
            >A 'query:QueryResponse' MUST not contain any other rim:Slots than rim:Slot[@name='SpecificationIdentifier'].</sch:assert>
        </sch:rule>
    </sch:pattern>

    <!--
   Some generic RIM patterns
-->

    <sch:pattern>
        <sch:rule context="rim:Slot">
            <sch:assert test="count(child::rim:SlotValue) = 1" id="R-DSD-ERR-S025"
                        role="FATAL">A rim:Slot MUST have a rim:SlotValue child element</sch:assert>
        </sch:rule>
    </sch:pattern>


    <sch:pattern>
        <sch:rule context="rim:SlotValue">
            <sch:assert test="count(child::*) &gt; 0" id="R-DSD-ERR-S026"
                        role="FATAL">A rim:SlotValue MUST have child element content</sch:assert>
        </sch:rule>
    </sch:pattern>


    <sch:pattern>
        <sch:rule context="rim:SlotValue[@xsi:type='rim:CollectionValueType']">
            <sch:assert test="count(child::rim:Element) &gt; 0" id="R-DSD-ERR-S024"
                role="FATAL">A rim:SlotValue of type CollectionValueType MUST have child rim:Element content</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rim:SlotValue[@xsi:type='rim:CollectionValueType']">
            <sch:assert test="@collectionType='urn:oasis:names:tc:ebxml-regrep:CollectionType:Set'" id="R-DSD-ERR-S027"
                role="FATAL">A rim:SlotValue of type CollectionValueType MUST have collectionType="urn:oasis:names:tc:ebxml-regrep:CollectionType:Set"</sch:assert>
        </sch:rule>
    </sch:pattern>


 
</sch:schema>