<?xml version="1.0" encoding="UTF-8"?>
<sch:schema 
    xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    
    <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <!--Start of SR metadata-->
    <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EB-ERR</sr:identifier>
            <sr:title xml:lang="en">EB-ERR</sr:title>
            <sr:description xml:lang="en">Query Error Response of the EB</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>CONTENT</sr:theme>
            <sr:theme>STRUCTURE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EB-ERR</sr:identifier>
                    <sr:title xml:lang="en">EB-ERR</sr:title>
                    <sr:description xml:lang="en">Query Error Response of the Evidence Broker</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:component>EB</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
            </sr:Asset>?>
    <!--End of SR metadata-->
    <sch:include href="codelist-include/ERRORSEVERITY-CODELIST_code.sch"/>
    <sch:include href="codelist-include/EBERRORCODES-CODELIST_code.sch"/>
    <sch:include href="codelist-include/EBERRORCODES-CODELIST_name-Type.sch"/>
    
    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ln" value="local-name(/node())"/>
            <sch:assert test="$ln ='QueryResponse'" role='FATAL' id="R-EB-ERR-001"
                >The root element of a query response document MUST be 'query:QueryResponse'</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ns" value="namespace-uri(/node())"/>
            <sch:assert test="$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0'" role='FATAL' id="R-EB-ERR-002"
                >The namespace of root element of a 'query:QueryResponse' MUST be 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0'</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="@status" role='FATAL' id="R-EB-ERR-005">
                The 'status' attribute of a 'QueryResponse' MUST be present. 
            </sch:assert>
        </sch:rule>
    </sch:pattern>
     
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure'" role='FATAL' id="R-EB-ERR-006"
                >The 'status' attribute of an unsuccessfull 'query:QueryResponse' MUST be encoded as as 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure'.</sch:assert>         
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <!-- 
            If Response is Success there is only a RegistryObjectList 
            If Response is Failure then there is only an Exception 
        -->
        <sch:rule context="query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure']">
            <sch:assert test="count(rim:RegistryObjectList) = 0" role='FATAL' id="R-EB-ERR-007"
                >An unsuccessful 'query:QueryResponse' MUST not include a 'rim:RegistryObjectList'.</sch:assert>
        </sch:rule> 
    </sch:pattern> 
    <sch:pattern>      
        <sch:rule context="query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure']">
            <sch:assert test="count(rs:Exception) = 1" role='FATAL' id="R-EB-ERR-008"
                >An unsuccessful 'query:QueryResponse' MUST include an Exception.</sch:assert>
        </sch:rule>       
    </sch:pattern>
   
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception">
            <sch:assert test="@xsi:type" role='FATAL' id="R-EB-ERR-009"
                >The 'xsi:type' attribute of a 'rs:Exception' MUST be present. </sch:assert>
        </sch:rule> 
    </sch:pattern> 
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception">          
            <sch:assert test="@severity" role='FATAL' id="R-EB-ERR-011"
                >The 'severity' attribute of a 'rs:Exception' MUST be present.</sch:assert>
            </sch:rule>
    </sch:pattern> 
    <sch:pattern>  
        <!--{}[](ErrorSeverity-CodeList)-->
        <sch:rule context="query:QueryResponse/rs:Exception">
            <sch:assert test="@severity='urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error'" role='FATAL' id="R-EB-ERR-012"
                >The value of 'severity' attribute of a 'rs:Exception' MUST  be 'urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error'. </sch:assert>          
        </sch:rule>   
    </sch:pattern> 
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception">
            <sch:assert test="@message" role='FATAL' id="R-EB-ERR-013"
                >The 'message' attribute of a 'rs:Exception' MUST be present. </sch:assert>
            </sch:rule> 
    </sch:pattern> 
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception">
            <sch:assert test="@code" role='FATAL' id="R-EB-ERR-015"
                >The 'code' attribute of a 'rs:Exception' MUST be present. </sch:assert>
            </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="not(rim:Slot[@name != 'SpecificationIdentifier'])" role='FATAL' id="R-EB-ERR-017"
            >A 'query:QueryResponse' MUST not contain any other rim:Slots than rim:Slot[@name='SpecificationIdentifier'].</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="rim:Slot[@name='SpecificationIdentifier']" role='FATAL' id="R-EB-ERR-018"
            >The rim:Slot name="SpecificationIdentifier" MUST be present in the QueryResponse.</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')"/>
            <sch:assert test="$st ='StringValueType'" role='FATAL' id="R-EB-ERR-019"
            >The rim:SlotValue of rim:Slot name="SpecificationIdentifier" MUST be of "rim:StringValueType".</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
            <sch:assert test="rim:Value='oots-cs:v1.1'" role='FATAL' id="R-EB-ERR-020"
            >The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-cs:v1.1".</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse/rs:Exception/@code">
            <!--{}[](EBErrorCodes-CodeList)-->
            <sch:assert test="some $code in $EBERRORCODES-CODELIST_code satisfies .=$code" role='FATAL' id="R-EB-ERR-016">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'EBErrorCodes-CodeList' in the context 'query:QueryResponse/rs:Exception/@code'</sch:assert>
        </sch:rule>
        <sch:rule context="query:QueryResponse/rs:Exception/@xsi:type">
            <!--{}[](EBDErrorCodes-CodeList)-->
            <sch:assert test="some $code in $EBERRORCODES-CODELIST_name-Type satisfies .=$code" role='FATAL' id="R-EB-ERR-010">The value of 'xsi:type' attribute of a 'rs:Exception' MUST be a 'type' provided by the code list 'EBErrorCodes' (Evidence Broker Error Response Codes).</sch:assert>
        </sch:rule>
    </sch:pattern>  
</sch:schema>