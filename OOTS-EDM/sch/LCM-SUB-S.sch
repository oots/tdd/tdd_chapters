<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    
    <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0" prefix="lcm"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <!--Start of SR metadata-->
    <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/LCM-SUB-S</sr:identifier>
            <sr:title xml:lang="en">LCM-SUB-S</sr:title>
            <sr:description xml:lang="en">Submit Registry Objects LCM (EB and DSD)</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>STRUCTURE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/LCM-SUB</sr:identifier>
                    <sr:title xml:lang="en">LCM-SUB</sr:title>
                    <sr:description xml:lang="en">Combined Regrep LCM Submission of DSD and EB artifacts</sr:description>
                    <sr:type>LCM</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>EB</sr:component>
                    <sr:component>DSD</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
            </sr:Asset>?>
    <!--End of SR metadata-->
    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ln" value="local-name(/node())"/>
            <sch:assert test="$ln ='SubmitObjectsRequest'" role='FATAL' id="R-LCM-SUB-S001" 
                >The root element of a query response document MUST be 'lcm:SubmitObjectsRequest'</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ns" value="namespace-uri(/node())"/>
            <sch:assert test="$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0'" role='FATAL' id="R-LCM-SUB-S002" 
                >The namespace of root element of a 'lcm:SubmitObjectsRequest' MUST be 'urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0'</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest">
            <sch:assert test="@id" role='FATAL' id="R-LCM-SUB-S003"
                >The 'id' attribute of a 'SubmitObjectsRequest' MUST be present.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest">
            <sch:assert test="matches(normalize-space(@id),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" 
                role='FATAL' id="R-LCM-SUB-S004"
                >The 'id' attribute of a 'SubmitObjectsRequest' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:".</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest">
            <sch:assert test="count(rim:RegistryObjectList)>0" role='FATAL' id="R-LCM-SUB-S005" 
                >A  'SubmitObjectsRequest' MUST include a 'rim:RegistryObjectList'.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rim:RegistryObjectList/rim:RegistryObject">
            <sch:let name="idattr" value="@id"/>
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="string-length($idattr)>0" role='FATAL' id="R-LCM-SUB-S006" 
                >Each 'rim:RegistryObject' MUST include an 'id' attribute</sch:assert>
            <sch:assert test="($st != 'AssociationType' and count(rim:Classification)>0) or ($st = 'AssociationType' and count(rim:Classification)=0)" role='FATAL' id="R-LCM-SUB-S007"
                >Each 'rim:RegistryObject' MUST include a 'rim:Classification' if the 'rim:RegistryObject' is not an 'xsi:type="rim:AssociationType"'</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rim:RegistryObjectList/rim:RegistryObject/rim:Classification">
            <sch:let name="idattr" value="@id"/>
            <sch:let name="schemeattr" value="@classificationScheme"/>
            <sch:let name="nodeattr" value="@classificationNode"/>
            <sch:assert test="string-length($idattr)>0"  role='FATAL' id="R-LCM-SUB-S008"   
                >Each 'rim:Classification' MUST include an 'id' attribute</sch:assert>
            <sch:assert test="matches(normalize-space(@id),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" 
                role='FATAL' id="R-LCM-SUB-S009"
                >Each id of 'rim:Classification' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:".</sch:assert>
            <sch:assert test="string-length($schemeattr)>0"  role='FATAL' id="R-LCM-SUB-S010"   
                >Each 'rim:Classification' MUST include an 'classificationScheme' attribute</sch:assert>
            <sch:assert test="string-length($nodeattr)>0"  role='FATAL' id="R-LCM-SUB-S011"   
                >Each 'rim:Classification' MUST include an 'classificationNode' attribute</sch:assert>
            <sch:assert test="$schemeattr = 'urn:fdc:oots:classification:dsd' or $schemeattr = 'urn:fdc:oots:classification:eb'" role='FATAL' id="R-LCM-SUB-S012"
                >  The 'classificationScheme' attribute MUST be 'urn:fdc:oots:classification:dsd' or 'urn:fdc:oots:classification:eb'</sch:assert>
            <sch:assert test="$nodeattr = 'EvidenceProvider' or $nodeattr = 'DataServiceEvidenceType' or $nodeattr = 'AccessService' or $nodeattr = 'EvidenceType' or $nodeattr = 'EvidenceTypeList' or $nodeattr = 'Requirement' or $nodeattr = 'ReferenceFramework'"  role='FATAL' id="R-LCM-SUB-S013"
                >  The 'classificationNode' attribute MUST be 'EvidenceProvider' or 'DataServiceEvidenceType' or 'AccessService' or 'EvidenceType' or 'EvidenceTypeList' or 'Requirement' or 'ReferenceFramework'</sch:assert>
        </sch:rule>
    </sch:pattern>
   
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Classification[@classificationNode='EvidenceProvider']">   
            <sch:assert test="count(../rim:Slot[@name='EvidenceProvider']) = 1 and count(../rim:Slot[@name!='EvidenceProvider']) = 0" role='FATAL' id="R-LCM-SUB-S014" 
                >A 'rim:RegistryObject' with the classificationNode 'EvidenceProvider' MUST include a  rim:Slot name="EvidenceProvider" and no other</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>  
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Classification[@classificationNode='DataServiceEvidenceType']">   
            <sch:assert test="count(../rim:Slot[@name='DataServiceEvidenceType']) = 1 and count(../rim:Slot[@name!='DataServiceEvidenceType']) = 0" role='FATAL' id="R-LCM-SUB-S015" 
             >A 'rim:RegistryObject' with the classificationNode 'DataServiceEvidenceType' MUST include a  rim:Slot name="DataServiceEvidenceType" and no other</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>          
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Classification[@classificationNode='AccessService']">   
            <sch:assert test="count(../rim:Slot[@name='AccessService']) = 1 and count(../rim:Slot[@name!='AccessService']) = 0" role='FATAL' id="R-LCM-SUB-S016" 
                >A 'rim:RegistryObject' with the classificationNode 'AccessService' MUST include a  rim:Slot name="AccessService" and no other</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>   
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')"/>
            <sch:assert test="$st ='AnyValueType'" role='FATAL' id="R-LCM-SUB-S021"
                >The rim:SlotValue of rim:Slot name="EvidenceProvider" MUST be of "rim:AnyValueType"</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>   
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')"/>
            <sch:assert test="$st ='AnyValueType'" role='FATAL' id="R-LCM-SUB-S022"
                >The rim:SlotValue of rim:Slot name="DataServiceEvidenceType" MUST be of "rim:AnyValueType"</sch:assert>
        </sch:rule>    
    </sch:pattern>
    
    <sch:pattern>   
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='AccessService']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')"/>
            <sch:assert test="$st ='AnyValueType'" role='FATAL' id="R-LCM-SUB-S023"
                >The rim:SlotValue of rim:Slot name="AccessService" MUST be of "rim:AnyValueType"</sch:assert>
        </sch:rule>    
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue">
            <sch:assert test="count(sdg:Publisher)=1" role='FATAL' id="R-LCM-SUB-S028"
                >A  'rim:Slot[@name='EvidenceProvider'/rim:SlotValue' MUST contain one sdg:Publisher of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
        </sch:rule>   
    </sch:pattern>
    
    <sch:pattern>          
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']">
            <sch:assert test="count(rim:SlotValue/sdg:DataServiceEvidenceType)=1" role='FATAL' id="R-LCM-SUB-S029"
                >A  'rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue' MUST contain one sdg:DataServiceEvidenceType of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
        </sch:rule>   
    </sch:pattern>
    
    <sch:pattern>          
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='AccessService']">
            <sch:assert test="count(rim:SlotValue/sdg:AccessService)=1" role='FATAL' id="R-LCM-SUB-S030"
                >A  'rim:Slot[@name='AccessService']/rim:SlotValue' MUST contain one sdg:AccessService of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
        </sch:rule>   
    </sch:pattern>
    
    <sch:pattern>          
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType">
            <sch:assert test="count(sdg:AccessService)=0" role='FATAL' id="R-LCM-SUB-S035"
                >A DataServiceEvidenceType 'rim:Slot[@name='DataServiceEvidenceType]/rim:SlotValue' MUST not contain an 'sdg:AccessService' </sch:assert>
        </sch:rule>   
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='AccessService']/rim:SlotValue">
            <sch:assert test="count(sdg:Publisher)=0" role='FATAL' id="R-LCM-SUB-S036"
                >An AccessService  'rim:Slot[@name='AccessService]'/rim:SlotValue' MUST not contain an 'sdg:Publisher'.</sch:assert>
        </sch:rule>   
    </sch:pattern>
    
    <sch:pattern>      
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject">
            <sch:let name="classification" value="count(rim:Classification)"/>
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="($st != 'AssociationType' and count(rim:Classification)>0) or ($st = 'AssociationType' and count(rim:Classification)=0)" role='FATAL' id="R-LCM-SUB-S041"
                >If a 'rim:RegistryObject' does not have a 'rim:Classification' it MUST have the attribute 'xsi:type=rim:AssociationType'</sch:assert>
        </sch:rule>
    </sch:pattern>

    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject[@sourceObject and @targetObject]">       
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:let name="targetObject" value="@targetObject"/>
            <sch:let name="etp_count" value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceProvider']])"/>
            <sch:let name="etd_count" value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='DataServiceEvidenceType']])"/>
            <sch:let name="etas_count" value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='AccessService']])"/>
            <sch:let name="etel_count" value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceTypeList']])"/>
            <sch:let name="etet_count" value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='EvidenceType']])"/>
            <sch:let name="etrs_count" value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='Requirement']])"/>
            <sch:let name="etrt_count" value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='Requirement']])"/>
            <sch:let name="etrf_count" value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='ReferenceFramework']])"/>           
            <sch:assert test="number($etp_count) + number($etd_count) + number($etas_count) + number($etel_count) + number($etet_count)+ number($etrs_count)+ number($etrt_count)+ number($etrf_count)= 2" role='FATAL' id="R-LCM-SUB-S042"
                >Each 'rim:RegistryObject' with a classificationNode 'EvidenceProvider' or 'DataServiceEvidenceType' or 'AccessService' or 'EvidenceType' or 'EvidenceTypeList' or 'Requirement' or 'ReferenceFramework' MUST have an association 
                described in the attribute  'xsi:type="rim:AssociationType"'
            </sch:assert>
        </sch:rule>
    </sch:pattern>
   

    <sch:pattern> 
        <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@xsi:type='rim:AssociationType']">
            <sch:assert test="count(@sourceObject)=1" role='FATAL' id="R-LCM-SUB-S043"
                >A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' MUST have an attribute 'sourceObject'.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType']">       
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:let name="resourceSource"  value="//rim:RegistryObject[@id=$sourceObject]"/>
            <sch:assert test="$resourceSource/rim:Classification[@classificationNode='EvidenceProvider']" role='FATAL' id="R-LCM-SUB-S044"
                >A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType" MUST have an attribute 'sourceObject' that lists the id 
                of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") from classificationNode 'EvidenceProvider'. 
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService']">       
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:let name="resourceSource"  value="//rim:RegistryObject[@id=$sourceObject]"/>
            <sch:assert test="$resourceSource/rim:Classification[@classificationNode='EvidenceProvider']" role='FATAL' id="R-LCM-SUB-S045"
                >A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService" MUST have an attribute 'sourceObject' 
                that lists the id of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") from classificationNode 'EvidenceProvider'. 
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>      
        <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@xsi:type='rim:AssociationType']">
            <sch:assert test="@targetObject" role='FATAL' id="R-LCM-SUB-S046"
                >A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' MUST have an attribute 'targetObject'.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>                                                           
        <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType']">
            <sch:let name="targetObject" value="@targetObject"/>
            <sch:let name="resourceTarget" value="//rim:RegistryObjectList/rim:RegistryObject[@id=$targetObject]"/>
            <sch:assert test="$resourceTarget/rim:Classification[@classificationNode='DataServiceEvidenceType']" role='FATAL' id="R-LCM-SUB-S047"
                >A 'rim:RegistryObject' with  type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType" MUST have an attribute 'targetObject'
                that lists the id of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") with classificationNode 'DataServiceEvidenceType'. 
                 </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@sourceObject and @targetObject]">       
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:let name="targetObject" value="@targetObject"/>
            <sch:let name="type" value="@type"/>
            <sch:let name="etl_count" value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceProvider']])" />
            <sch:let name="et_count" value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='AccessService']])"></sch:let>
            <sch:assert test="$type != 'urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService' or (number($etl_count) + number($et_count) = 2 and $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService')" role='FATAL' id="R-LCM-SUB-S049"
                >A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' linking 'rim:RegistryObject' with the classificationNode 
                'EvidenceProvider' (@sourceObject) to 'rim:RegistryObject' with the classificationNode 'AccessService' (@targetObject) MUST use the 
                type="urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService".
            </sch:assert>
        </sch:rule>
    </sch:pattern>
        
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest">       
            <sch:assert test="not(rim:Slot[@name != 'SpecificationIdentifier'])" role='FATAL' id="R-LCM-SUB-S057"
                >A 'lcm:SubmitObjectsRequest' MUST not contain any other rim:Slots than 'SpecificationIdentifier'.</sch:assert>
        </sch:rule> 
    </sch:pattern>    
  
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification">
            <sch:let name="hasSupportedValue" value="//rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:ClassificationConcept/sdg:SupportedValue[normalize-space(.)]"/>
            <sch:assert test="not(empty($hasSupportedValue))"  role='FATAL' id="R-LCM-SUB-S058"
                >A value for 'sdg:Publisher/sdg:ClassificationConcept/sdg:SupportedValue' MUST be provided if the 'sdg:DataServiceEvidenceType' which is associated to the 'sdg:Publisher' has the element 'sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification'.</sch:assert>
        </sch:rule>
    </sch:pattern>
 
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/@id">
            <sch:assert test="matches(normalize-space((.)),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" 
                role='FATAL' id="R-LCM-SUB-S059">
                Each id of 'rim:RegistryObject' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:". 
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
            <sch:assert test="rim:RegistryObject/rim:Slot[@name='EvidenceProvider']" 
                role='FATAL' id="R-LCM-SUB-S060"
                >A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="EvidenceProvider".</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
            <sch:assert test="rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']" 
                role='FATAL' id="R-LCM-SUB-S061"
                >A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="DataServiceEvidenceType".</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
            <sch:assert test="rim:RegistryObject/rim:Slot[@name='AccessService']" role='FATAL' id="R-LCM-SUB-S062">
                A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="AccessService". 
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
            <sch:assert test="rim:RegistryObject[@xsi:type='rim:AssociationType']" 
                role='FATAL' id="R-LCM-SUB-S063"
                >A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType".</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
            <sch:assert test="count(rim:RegistryObject)>6" role='FATAL' id="R-LCM-SUB-S064" 
                >A 'SubmitObjectsRequest' MUST include minimum 7 'rim:RegistryObject'.</sch:assert>
        </sch:rule>        
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService']"> 
            <sch:let name="targetObject" value="@targetObject"/>
            <sch:let name="resourceObject"  value="//rim:RegistryObject[@id=$targetObject]"/>
            <sch:assert test="$resourceObject/rim:Classification[@classificationNode='AccessService']" role='FATAL' id="R-LCM-SUB-S048"
                >A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService" MUST have an attribute 'targetObject'
                that lists the id of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") with classificationNode 'AccessService'. 
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@sourceObject and @targetObject]">       
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:let name="targetObject" value="@targetObject"/>
            <sch:let name="type" value="@type"/>
            <sch:let name="etl_count" value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceProvider']])" />
            <sch:let name="et_count" value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='DataServiceEvidenceType']])"></sch:let>
            <sch:assert test="(number($etl_count) + number($et_count) = 2 and $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType') or $type != 'urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType'" role='FATAL' id="R-LCM-SUB-S050"
                >A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' linking 'rim:RegistryObject' with the classificationNode 'EvidenceProvider' (@sourceObject)
                to 'rim:RegistryObject' with the classificationNode 'DataServiceEvidenceType' (@targetObject) MUST use the type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType"</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest">
            <sch:assert test="rim:Slot[@name='SpecificationIdentifier']" role='FATAL' id="R-LCM-SUB-S080"
                >The rim:Slot name="SpecificationIdentifier" MUST be present in the SubmitObjectsRequest.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')"/>
            <sch:assert test="$st ='StringValueType'" role='FATAL' id="R-LCM-SUB-S081"
                >The rim:SlotValue of rim:Slot name="SpecificationIdentifier" MUST be of "rim:StringValueType".</sch:assert>
        </sch:rule>
    </sch:pattern> 
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest">
            <sch:assert test="@checkReferences" role='FATAL' id="R-LCM-SUB-S082"
                >The 'checkReferences' attribute of a 'SubmitObjectsRequest' MUST be present. </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest">
            <sch:assert test="@checkReferences='true'" role='FATAL' id="R-LCM-SUB-S083"
                >The 'checkReferences' attribute of a 'SubmitObjectsRequest' MUST be set on "true".</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject">
            <sch:assert test="not(rim:Slot[not(@name=('EvidenceProvider', 'DataServiceEvidenceType', 'AccessService', 'EvidenceType', 'EvidenceTypeList', 'Requirement', 'ReferenceFramework'))])" role='FATAL' id="R-LCM-SUB-S084"
            >A 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/' MUST not contain any other rim:Slots than 'EvidenceProvider', 'DataServiceEvidenceType', 'AccessService', 'EvidenceType', 'EvidenceTypeList', 'Requirement' or 'ReferenceFramework'. </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject[@xsi:type='rim:AssociationType']">
            <sch:assert test="not(rim:Slot)" role='FATAL' id="R-LCM-SUB-S085"
                >A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType" MUST not include any slots.</sch:assert>
        </sch:rule>
    </sch:pattern>
        
    <sch:pattern>
        <sch:rule context="rim:RegistryObject/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification">
            <sch:let name="epc_id" value="sdg:Identifier" />
            <sch:let name="epc_desc" value="sdg:Description[@lang='EN']"/>
            <sch:let name="dset_name" value="../sdg:Title[1]" />
            <sch:let name="dset_id" value="../sdg:Identifier" />
            <sch:let name="regrep_id" value="../../../../@id"/>
            
            <sch:let 
                name="associated_provider_ids" 
                value="//rim:RegistryObject[
                @type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType'
                and @targetObject=$regrep_id]/@sourceObject" />
            
            <sch:let 
                name="associated_providers_without_matching_classificationconcept"
                value="//rim:RegistryObject[@id=$associated_provider_ids 
                and not(descendant::sdg:ClassificationConcept/sdg:Identifier=$epc_id)]/@id" />
            
            <sch:assert 
                test="count($associated_providers_without_matching_classificationconcept)=0" role='FATAL' id="R-LCM-SUB-S086"
                >Some providers associated to data service "<sch:value-of 
                    select="$dset_name"/>" in registry object <sch:value-of 
                        select="$regrep_id"/> do not have a value for classification concept "<sch:value-of 
                            select="$epc_desc"/>" <sch:value-of 
                                select="$epc_id"/>": RIM object(s) <sch:value-of 
                                    select="$associated_providers_without_matching_classificationconcept"/></sch:assert>
            
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Jurisdiction">         
            <sch:let name="adminLevel2" value="boolean(sdg:AdminUnitLevel2)"/>         
            <sch:let name="sourceObjectID" value="ancestor-or-self::rim:RegistryObject/@id"/>
            <sch:let name="targetObjectID" value="//rim:RegistryObject[@sourceObject=$sourceObjectID]/@targetObject"/>
            <sch:let name="checkJurisdiction" value="//rim:RegistryObject[@id = $targetObjectID]/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel"/>
            <sch:let name="checkNuts" value="contains($checkJurisdiction, 'NUTS')"/>
            <sch:assert test="($adminLevel2 and $checkNuts) or not($checkNuts)" role='WARNING' id="R-LCM-SUB-S087"
                >The element 'sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel2''  SHOULD be provided if the  'sdg:DataServiceEvidenceType' which is associated to the 'sdg:Publisher' has one
                of the values 'NUTS1', 'NUTS2' or 'NUTS3' in the element 'sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel' . 
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Jurisdiction">         
            <sch:let name="adminLevel3" value="boolean(sdg:AdminUnitLevel3)"/>         
            <sch:let name="sourceObjectID" value="ancestor-or-self::rim:RegistryObject/@id"/>
            <sch:let name="targetObjectID" value="//rim:RegistryObject[@sourceObject=$sourceObjectID]/@targetObject"/>
            <sch:let name="checkJurisdiction" value="//rim:RegistryObject[@id = $targetObjectID]/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel"/>
            <sch:let name="checkLau" value="contains($checkJurisdiction, 'LAU')"/>
            <sch:assert test="($adminLevel3 and $checkLau) or not($checkLau)" role='WARNING' id="R-LCM-SUB-S088"
                >The element '/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel3''  SHOULD be provided if the  'sdg:DataServiceEvidenceType' which is associated to the 'sdg:Publisher' 
                has the value 'LAU' in the element 'sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel' .
            </sch:assert>
        </sch:rule>
    </sch:pattern>

    <!--
Some generic RIM patterns
-->

    <sch:pattern>
        <sch:rule context="rim:Slot">
            <sch:assert test="count(child::rim:SlotValue) = 1" id="R-LCM-SUB-S089"
                        role="FATAL">A rim:Slot MUST have a rim:SlotValue child element</sch:assert>
        </sch:rule>
    </sch:pattern>


    <sch:pattern>
        <sch:rule context="rim:SlotValue">
            <sch:assert test="count(child::*) &gt; 0" id="R-LCM-SUB-S090"
                        role="FATAL">A rim:SlotValue MUST have child element content</sch:assert>
        </sch:rule>
    </sch:pattern>

    <!--
EB patterns
-->

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']">
            <sch:let name="nodeattr" value="../rim:Classification/@classificationNode"/>
            <sch:assert test="$nodeattr = 'EvidenceType'" role='FATAL' id="R-LCM-SUB-S017"
            >A 'rim:RegistryObject' with the classificationNode 'EvidenceType' MUST include a  rim:Slot name="EvidenceType" and no other</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceTypeList']">
            <sch:let name="nodeattr" value="../rim:Classification/@classificationNode"/>
            <sch:assert test="$nodeattr = 'EvidenceTypeList'" role='FATAL' id="R-LCM-SUB-S018"
            >A 'rim:RegistryObject' with the classificationNode 'EvidenceTypeList' MUST include a  rim:Slot name=EvidenceTypeList and no other</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']">
            <sch:let name="nodeattr" value="../rim:Classification/@classificationNode"/>
            <sch:assert test="$nodeattr = 'Requirement'" role='FATAL' id="R-LCM-SUB-S019"
            >A 'rim:RegistryObject' with the classificationNode 'Requirement' MUST include a  rim:Slot name=Requirement and no other</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']">
            <sch:let name="nodeattr" value="../rim:Classification/@classificationNode"/>
            <sch:assert test="$nodeattr = 'ReferenceFramework'" role='FATAL' id="R-LCM-SUB-S020"
            >A 'rim:RegistryObject' with the classificationNode 'ReferenceFramework' MUST include a  rim:Slot name=ReferenceFramework and no other</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='AnyValueType'" role='FATAL' id="R-LCM-SUB-S024"
            >The rim:SlotValue of rim:Slot name="EvidenceType" MUST be of "rim:AnyValueType"</sch:assert>

            <sch:assert test="sdg:EvidenceType" role='FATAL' id="R-LCM-SUB-S031"
            >A 'rim:Slot[@name='EvidenceType'/rim:SlotValue' MUST contain one sdg:EvidenceType of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceTypeList']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='AnyValueType'" role='FATAL' id="R-LCM-SUB-S025"
            >The rim:SlotValue of rim:Slot name="EvidenceTypeList" MUST be of "rim:AnyValueType"</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='AnyValueType'" role='FATAL' id="R-LCM-SUB-S026"
            >The rim:SlotValue of rim:Slot name="Requirement" MUST be of "rim:AnyValueType"</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='AnyValueType'" role='FATAL' id="R-LCM-SUB-S027"
            >The rim:SlotValue of rim:Slot name="ReferenceFramework" MUST be of "rim:AnyValueType"</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceTypeList']/rim:SlotValue">
            <sch:assert test="sdg:EvidenceTypeList" role='FATAL' id="R-LCM-SUB-S032"
            >A 'rim:Slot[@name='EvidenceTypeList'/rim:SlotValue' MUST contain one sdg:EvidenceTypeList of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue">
            <sch:assert test="sdg:Requirement" role='FATAL' id="R-LCM-SUB-S033"
            >A 'rim:Slot[@name='Requirement'/rim:SlotValue' MUST contain one sdg:Requirement of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue">
            <sch:assert test="sdg:ReferenceFramework" role='FATAL' id="R-LCM-SUB-S034"
            >A 'rim:Slot[@name='ReferenceFramework'/rim:SlotValue' MUST contain one sdg:ReferenceFramwork of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType">
            <sch:assert test="count(sdg:Distribution)=0" role='FATAL' id="R-LCM-SUB-S037"
            >A 'rim:slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType' MUST not contain a 'sdg:Distribution'</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceTypeList']/rim:SlotValue/sdg:EvidenceTypeList">
            <sch:assert test="count(sdg:EvidenceType)=0" role='FATAL' id="R-LCM-SUB-S038"
            >A 'rim:slot[@name='EvidenceTypeList']/rim:SlotValue/sdg:EvidenceTypeList' MUST not contain a 'sdg:EvidenceType'</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement">
            <sch:assert test="count(sdg:EvidenceTypeList)=0 and count(sdg:ReferenceFramework)=0" role='FATAL' id="R-LCM-SUB-S039"
            >A 'rim:slot[@name='Requirement']/rim:SlotValue/sdg:Requirement' MUST not contain a 'sdg:EvidenceTypeList' and a 'sdg:ReferenceFramework'</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:RelatedTo">
            <sch:assert test="sdg:Identifier" role='FATAL' id="R-LCM-SUB-S040"
            >The xs:element name="RelatedTo" type="sdg:ReferenceFrameworkType"/ MUST only contain the 'sdg:Identifier'</sch:assert>
        </sch:rule>
    </sch:pattern>

    <!--
EB association patterns
-->

    <sch:pattern>
        <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence']">
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:let name="targetObject" value="@targetObject"/>
            <sch:assert test="//rim:RegistryObject[@id=$sourceObject]/rim:Classification[@classificationNode='EvidenceTypeList']" role='FATAL' id="R-LCM-SUB-S051"
            >Source object of ContainsEvidence association must be classified as EvidenceTypeList <sch:value-of select="$sourceObject"/></sch:assert>
            <sch:assert test="//rim:RegistryObject[@id=$targetObject]/rim:Classification[@classificationNode='EvidenceType']" role='FATAL' id="R-LCM-SUB-S077"
            >Target object of ContainsEvidence association must be classified as EvidenceType <sch:value-of select="$targetObject"/></sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rim:RegistryObject[@sourceObject and @targetObject]">
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:let name="targetObject" value="@targetObject"/>
            <sch:let name="type" value="@type"/>
            <sch:let name="etl_count" value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceTypeList']])" />
            <sch:let name="et_count" value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='EvidenceType']])"></sch:let>
            <sch:assert test="number($etl_count) + number($et_count) != 2 or $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence'" role='FATAL' id="R-LCM-SUB-S052"
            >A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' linking 'rim:RegistryObject' with the classificationNode 'EvidenceTypeList'
                (@sourceObject) to 'rim:RegistryObject' with the classificationNode 'EvidenceType' (@targetObject) MUST use the
                type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence"</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement']">
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:let name="targetObject" value="@targetObject"/>
            <sch:assert test="//rim:RegistryObject[@id=$sourceObject]/rim:Classification[@classificationNode='EvidenceTypeList']" role='FATAL' id="R-LCM-SUB-S053"
            >Source object of FulfillsRequirement association must be classified as EvidenceTypeList <sch:value-of select="$sourceObject"/></sch:assert>
            <sch:assert test="//rim:RegistryObject[@id=$targetObject]/rim:Classification[@classificationNode='Requirement']" role='FATAL' id="R-LCM-SUB-S078"
            >Target object of FulfillsRequirement association must be classified as Requirement <sch:value-of select="$targetObject"/></sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rim:RegistryObject[@sourceObject and @targetObject]">
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:let name="targetObject" value="@targetObject"/>
            <sch:let name="type" value="@type"/>
            <sch:let name="etl_count" value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceTypeList']])" />
            <sch:let name="et_count" value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='Requirement']])"></sch:let>
            <sch:assert test="number($etl_count) + number($et_count) != 2 or $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement'" role='FATAL' id="R-LCM-SUB-S054"
            >A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' linking 'rim:RegistryObject' with the classificationNode 'EvidenceTypeList'
                (@sourceObject) to 'rim:RegistryObject' with the classificationNode 'Requirement' (@targetObject) or to a unique UUID (RFC 4122) starting with prefix
                "urn:uuid:" (@targetObject) MUST use the type="urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement"</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework']">
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:let name="targetObject" value="@targetObject"/>
            <sch:assert test="//rim:RegistryObject[@id=$sourceObject]/rim:Classification[@classificationNode='Requirement']" role='FATAL' id="R-LCM-SUB-S055"
            >Source object of DerivesFromReferenceFramework association must be classified as Requirement <sch:value-of select="$sourceObject"/></sch:assert>
            <sch:assert test="//rim:RegistryObject[@id=$targetObject]/rim:Classification[@classificationNode='ReferenceFramework']"  role='FATAL' id="R-LCM-SUB-S079"
            >Target object of DerivesFromReferenceFramework association must be classified as ReferenceFramework <sch:value-of select="$targetObject"/></sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rim:RegistryObject[@sourceObject and @targetObject]">
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:let name="targetObject" value="@targetObject"/>
            <sch:let name="type" value="@type"/>
            <sch:let name="etl_count" value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='Requirement']])"/>
            <sch:let name="et_count" value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='ReferenceFramework']])"></sch:let>
            <sch:assert test="number($etl_count) + number($et_count) != 2 or $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework'"  role='FATAL' id="R-LCM-SUB-S056"
            >A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' linking a unique UUID (RFC 4122) starting with prefix "urn:uuid:"
                (@sourceObject) or a 'rim:RegistryObject' with the classificationNode 'Requirement' (@sourceObject) to 'rim:RegistryObject' with the classificationNode
                'ReferenceFramework' (@targetObject) MUST use the type="urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework"</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
            <sch:assert test="count(rim:RegistryObject/rim:Slot[@name='EvidenceType'])>0" role='FATAL' id="R-LCM-SUB-S065"
            >A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="EvidenceType".
            </sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
            <sch:assert test="count(rim:RegistryObject/rim:Slot[@name='EvidenceTypeList'])>0" role='FATAL' id="R-LCM-SUB-S066"
            >A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="EvidenceTypeList".
            </sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
            <sch:assert test="count(rim:RegistryObject[@xsi:type='rim:AssociationType'])>0" role='FATAL' id="R-LCM-SUB-S067">
                A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"
            </sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
            <sch:assert test="count(rim:RegistryObject/rim:Slot[@name='ReferenceFramework'])>0" role='FATAL' id="R-LCM-SUB-S068"
            >A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="ReferenceFramework".
            </sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot/rim:SlotValue/sdg:ReferenceFramework">
            <sch:assert test="sdg:RelatedTo" role='FATAL' id="R-LCM-SUB-S069">
                The xs:element name="RelatedTo" type="sdg:ReferenceFrameworkType" MUST be present.
            </sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot/rim:SlotValue/sdg:ReferenceFramework">
            <sch:assert test="sdg:Jurisdiction" role='FATAL' id="R-LCM-SUB-S070">
                The xs:element name="sdg:Jurisdiction" type="sdg:JurisdictionType" MUST be present.
            </sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework']">
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:assert test="//rim:RegistryObject[@id=$sourceObject]/rim:Classification[@classificationNode='Requirement']" role='FATAL' id="R-LCM-SUB-S071"
            >A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework" MUST have a @sourceObject
                that uses same id of the 'rim:RegistryObject'
                (starting with prefix "urn:uuid:") with the classificationNode 'Requirement' </sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework']">
            <sch:let name="targetObject"  value="@targetObject"/>
            <sch:assert test="//rim:RegistryObject[@id=$targetObject]/rim:Classification[@classificationNode='ReferenceFramework']" role='FATAL' id="R-LCM-SUB-S072"
            >A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework" MUST have a
                @targetObject that uses same UUID (RFC 4122) of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") with the classificationNode
                'ReferenceFramework' .
            </sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement']">
            <sch:let name="targetObject"  value="@targetObject"/>
            <sch:assert test="//rim:RegistryObject[@id=$targetObject]/rim:Classification[@classificationNode='Requirement']"  role='FATAL' id="R-LCM-SUB-S073"
            >A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement" MUST have a @targetObject
                that uses a unique UUID (RFC 4122) (starting with prefix "urn:uuid:") or that uses same id of the 'rim:RegistryObject'
                (starting with prefix "urn:uuid:") with the classificationNode 'Requirement' .
            </sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence']">
            <sch:let name="sourceObject"  value="@sourceObject"/>
            <sch:assert test="//rim:RegistryObject[@id=$sourceObject]/rim:Classification[@classificationNode='EvidenceTypeList']" role='FATAL' id="R-LCM-SUB-S074"
            >A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence" MUST have a @sourceObject that
                uses the same id of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") with the classificationNode 'EvidenceTypeList'.
            </sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence']">
            <sch:let name="targetObject"  value="@targetObject"/>
            <sch:assert test="//rim:RegistryObject[@id=$targetObject]/rim:Classification[@classificationNode='EvidenceType']" role='FATAL' id="R-LCM-SUB-S075"
            >A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence" MUST have a @targetObject
                that uses the same id of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") with the classificationNode 'EvidenceType'.
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/@type">
            <sch:let name="associationTypePrefix" value="'urn:oasis:names:tc:ebxml-regrep:AssociationType:'"/>
            <sch:assert test=". = concat($associationTypePrefix, 'ContainsEvidence') or . = concat($associationTypePrefix, 'FulfillsRequirement') or . = concat($associationTypePrefix, 'DerivesFromReferenceFramework') or . = concat($associationTypePrefix, 'ProvidesEvidenceType') or . = concat($associationTypePrefix, 'UsesAccessService')" role="FATAL" id="R-LCM-SUB-S091">        
                The value of @type of AssociationType must be 'urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence' or  'urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement' or 'urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework' or urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType or 'urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService'</sch:assert>
        </sch:rule>
    </sch:pattern>

</sch:schema>