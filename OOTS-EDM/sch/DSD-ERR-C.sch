<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/DSD-ERR-C</sr:identifier>
           <sr:title xml:lang="en">DSD-ERR-C</sr:title>
           <sr:description xml:lang="en">Query Error Response of the DSD (Content)</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CONTENT</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-ERR</sr:identifier>
                 <sr:title xml:lang="en">DSD-ERR</sr:title>
                 <sr:description xml:lang="en">Query Error Response of the DSD</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:component>DSD</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.1.0</sr:version>
           <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:include href="codelist-include/EEA_COUNTRY-CODELIST_code.sch"/>
   <sch:include href="codelist-include/LEVELSOFASSURANCE-CODELIST_code.sch"/>
   <sch:include href="codelist-include/JURISDICTIONLEVEL-CODELIST_code.sch"/>
   <sch:include href="codelist-include/DSDERRORCODES-CODELIST_code.sch"/>
   <sch:include href="codelist-include/DSDERRORCODES-CODELIST_name-Type.sch"/>
   <sch:include href="codelist-include/LANGUAGECODE-CODELIST_code.sch"/>
   <sch:include href="codelist-include/ERRORSEVERITY-CODELIST_code.sch"/>
   
   <sch:pattern>  
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='JurisdictionDetermination']/rim:SlotValue/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" role='FATAL' id="R-DSD-ERR-C001"
            >The value of 'JurisdictionContextId' of MUST be unique UUID (RFC 4122).</sch:assert>
      </sch:rule>
   </sch:pattern> 
   
   <sch:pattern>        
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='JurisdictionDetermination']/rim:SlotValue/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext">
         <sch:assert test="not(normalize-space(@lang)='')" role='FATAL' id="R-DSD-ERR-C003"
            >The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern> 
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='UserRequestedClassificationConcepts']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Identifier">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" role='FATAL' id="R-DSD-ERR-C005"
            >The value of 'Identifier' of MUST be unique UUID (RFC 4122).</sch:assert>
      </sch:rule>
   </sch:pattern> 
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='UserRequestedClassificationConcepts']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification">
         <sch:assert test="count(sdg:Type) > 0" role='FATAL' id="R-DSD-ERR-C006"
            >A 'Type' MUST be provided.</sch:assert>   
         
         <sch:assert test="count(sdg:Description) > 0" role='FATAL' id="R-DSD-ERR-C007"
            >A 'Description' MUST be provided. </sch:assert>      
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>        
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='UserRequestedClassificationConcepts']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description">
         <sch:assert test="not(normalize-space(@lang)='')" role='FATAL' id="R-DSD-ERR-C009"
            >The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern> 
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" role='FATAL' id="R-DSD-ERR-C010"
            >The value of 'Identifier' of an 'DataServiceEvidenceType' MUST be unique UUID (RFC 4122).</sch:assert>
      </sch:rule>
   </sch:pattern> 
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification">
         <!--{}[](EEA_COUNTRY-CODELIST)-->
         <sch:assert test="matches(., '^https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(oots|(' || string-join($EEA_COUNTRY-CODELIST_code, '|') || '))/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$')" role='FATAL' id="R-DSD-ERR-C011"
            >The value of 'EvidenceTypeClassification' of a 'DataServiceEvidenceType' MUST be a UUID of the Evidence Broker and include a code of the code list 'EEA_Country-CodeList' (ISO 3166-1' alpha-2 codes subset of EEA countries) 
            using the prefix and scheme ''https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/[EEA_CountryCode]/[UUID]'' pointing to the Semantic Repository. For testing purposes and agreed OOTS data models the code "oots" can be used. </sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>        
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title">
         <sch:assert test="not(normalize-space(@lang)='')" role='FATAL' id="R-DSD-ERR-C013"
            >The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>        
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description">
         <sch:assert test="not(normalize-space(@lang)='')" role='FATAL' id="R-DSD-ERR-C015"
            >The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
     
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception">
         <sch:assert test="@xsi:type" role='FATAL' id="R-DSD-ERR-C016"
            >The 'xsi:type' attribute of a 'rs:Exception' MUST be present. </sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception">
         <sch:assert test="@severity" role='FATAL' id="R-DSD-ERR-C018"
            >The 'severity' attribute of a 'rs:Exception' MUST be present. </sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception[@code!='DSD:ERR:0005']">
         <sch:assert test="matches(@severity,'urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error')" role='FATAL' id="R-DSD-ERR-C019"
            >The value of 'severity' attribute of a 'rs:Exception' MUST  be 'urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error' if the rs:Exception 
            xsi:type='rs:ObjectNotFoundExceptionType' (@code 'DSD:ERR:0005') is NOT used.</sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception">
         <sch:assert test="@message" role='FATAL' id="R-DSD-ERR-C020"
            >The 'message' attribute of a 'rs:Exception' MUST be present. </sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception">
         <sch:assert test="@code" role='FATAL' id="R-DSD-ERR-C021"
            >The 'code' attribute of a 'rs:Exception' MUST be present. </sch:assert>         
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rs:Exception[@code='DSD:ERR:0005']">
         <sch:assert test="matches(@severity,'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:DSDErrorResponse:AdditionalInput')" role='FATAL' id="R-DSD-ERR-C025"
            >The value of 'severity' attribute of a 'rs:Exception' MUST  be 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:DSDErrorResponse:AdditionalInput' 
            if the rs:Exception xsi:type='rs:ObjectNotFoundExceptionType' (@code 'DSD:ERR:0005') is used. </sch:assert>         
      </sch:rule>
   </sch:pattern>

   <sch:pattern>
         <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note">
            <sch:assert test="not(normalize-space(@lang)='')" role='FATAL' id="R-DSD-ERR-C027"
               >The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
         </sch:rule>
   </sch:pattern>

   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
         <sch:assert test="rim:Value='oots-cs:v1.1'" role='FATAL' id="R-DSD-ERR-C029"
         >The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-cs:v1.1".</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>   
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='JurisdictionDetermination']/rim:SlotValue/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code" role='FATAL' id="R-DSD-ERR-C002">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rs:Exception/rim:Slot[@name='JurisdictionDetermination']/rim:SlotValue/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang'</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>       
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='UserRequestedClassificationConcepts']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code" role='FATAL' id="R-DSD-ERR-C008">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rs:Exception/rim:Slot[@name='UserRequestedClassificationConcepts']/rim:SlotValue/sdg:EvidenceProviderClassification/sdg:Description/@lang'</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>   
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code" role='FATAL' id="R-DSD-ERR-C012">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang'</sch:assert>
      </sch:rule>  
   </sch:pattern>
   
   <sch:pattern>   
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code" role='FATAL' id="R-DSD-ERR-C014">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang'</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>   
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='JurisdictionDetermination']/rim:SlotValue/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel">
         <!--{}[](JurisdictionLevel-CodeList)-->
         <sch:assert test="some $code in $JURISDICTIONLEVEL-CODELIST_code satisfies .=$code" role='FATAL' id="R-DSD-ERR-C004" >Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'JurisdictionLevel-CodeList' in the context 'query:QueryResponse/rs:Exception/rim:Slot[@name='JurisdictionDetermination']/rim:SlotValue/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel'</sch:assert>
      </sch:rule>  
   </sch:pattern>
   
   <sch:pattern>   
      <sch:rule context="query:QueryResponse/rs:Exception/@xsi:type">
         <!--{}[](DSDErrorCodes-CodeList)-->
         <sch:assert test="some $type in $DSDERRORCODES-CODELIST_name-Type satisfies .=$type" role='FATAL' id="R-DSD-ERR-C017">The value of 'xsi:type' attribute of a 'rs:Exception' MUST be a 'type' provided by the code list 'DSDErrorCodes' (Data Service Directory Error Response Codes).</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>   
      <sch:rule context="query:QueryResponse/rs:Exception/@code">
         <!--{}[](DSDErrorCodes-CodeList)-->
         <sch:assert test="some $code in $DSDERRORCODES-CODELIST_code satisfies .=$code" role='FATAL' id="R-DSD-ERR-C023">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'DSDErrorCodes-CodeList' in the context 'query:QueryResponse/rs:Exception/@xsi:type'</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>   
      <sch:rule context="query:QueryResponse/rs:Exception/@severity">
         <!--{}[](ErrorSeverity-CodeList)-->
         <sch:assert test="(some $code in $ERRORSEVERITY-CODELIST_code satisfies .=$code) and not(.='urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired')" role='FATAL' id="R-DSD-ERR-C028">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="@severity"/>' is unacceptable for constraints identified by 'ErrorSeverity-CodeList' in the context 'query:QueryResponse/rs:Exception/@severity'. The code 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired' SHALL not be used by this transaction. </sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>   
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance">
         <!--{}[](LevelsOfAssurance-CodeList)-->
         <sch:assert test="some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code" role='FATAL' id="R-DSD-ERR-C024">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LevelsOfAssurance-CodeList' in the context 'query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance'</sch:assert>
      </sch:rule>
   </sch:pattern>
   
   <sch:pattern>   
      <sch:rule context="query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code" role='FATAL' id="R-DSD-ERR-C026">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rs:Exception/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang'</sch:assert>
      </sch:rule>
   </sch:pattern>  
</sch:schema>   
