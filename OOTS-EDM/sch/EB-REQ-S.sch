<?xml version="1.0" encoding="UTF-8"?>
<sch:schema 
    xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    
    <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <!--Start of SR metadata-->
    <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EB-REQ-S</sr:identifier>
            <sr:title xml:lang="en">EB-REQ-S</sr:title>
            <sr:description xml:lang="en">Get List of Requirements Query (Structure)</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>STRUCTURE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EB-REQ</sr:identifier>
                    <sr:title xml:lang="en">EB-REQ</sr:title>
                    <sr:description xml:lang="en">Query Response of the EB for the 'Requirement Query'</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:component>EB</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
            </sr:Asset>?>
    <!--End of SR metadata-->
    <!-- 
           EB-REQ-S excel sheet 
           Validates test sample tdd_chapters/OOTS-EDM/xml/EB/Response_for_Get_List_of_Requirements_Query.xml
     -->
    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ln" value="local-name(/node())"/>
            <sch:assert test="$ln ='QueryResponse'" role='FATAL' id="R-EB-REQ-S001"
                >The root element of a query response document MUST be 'query:QueryResponse'</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ns" value="namespace-uri(/node())"/>
            <sch:assert test="$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0'" role='FATAL' id="R-EB-REQ-S002"
                >The namespace of root element of a query response document MUST be 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0'</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="@status" role='FATAL' id="R-EB-REQ-S005"
                >The 'status' attribute of a 'QueryResponse' MUST be present.</sch:assert>         
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success'" role='FATAL' id="R-EB-REQ-S006"
                >The 'status' attribute of a successfull 'QueryResponse' MUST be encoded as as 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success'.</sch:assert>         
        </sch:rule>
    </sch:pattern>


    <sch:pattern>
        <!-- 
            If Response is Success there is only a RegistryObjectList 
            If Response is Failure then there is only an Exception 
        -->
        <sch:rule context="query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success']">
            <sch:assert test="count(rim:RegistryObjectList) = 1" role='FATAL' id="R-EB-REQ-S007"
                >A successful QueryResponse MUST include a RegistryObjectList</sch:assert>
            <sch:assert test="count(rim:RegistryObjectList/rim:RegistryObject) > 0" role='FATAL' id="R-EB-REQ-S024"
            >A successful QueryResponse MUST include a 'rim:RegistryObject'.</sch:assert>
            <sch:assert test="count(rs:Exception) = 0" role='FATAL' id="R-EB-REQ-S008"
                >A successful QueryResponse MUST not include an Exception</sch:assert>
        </sch:rule>       
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject">
            <sch:assert test="count(rim:Slot[@name='Requirement'])=1" role='FATAL' id="R-EB-REQ-S011"
                >The rim:Slot name="Requirement" MUST be present in the QueryResponse.</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="not(rim:Slot[@name != 'SpecificationIdentifier'])" role='FATAL' id="R-EB-REQ-S012"
            >A 'query:QueryResponse' MUST not contain any other rim:Slots than rim:Slot[@name='SpecificationIdentifier'].</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern >
        <sch:rule context="query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType">   
            <sch:assert test="(count(sdg:Identifier) + count(sdg:EvidenceTypeClassification) + count(sdg:Title) + count(sdg:Description) + count(sdg:DistributedAs) +count(sdg:Note)= count(child::*))" role='FATAL' id="R-EDM-REQ-S045"
                >An EvidenceRequest 'rim:SlotValue/DataServiceEvidenceType' MUST not contain any other elements than 'sdg:Identifier', 'sdg:EvidenceTypeClassification', 'sdg:Title', 'sdg:Description', 'sdg:DistributedAs' and 'sdg:Note'.</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="rim:Slot[@name='Requirement']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='AnyValueType'" role='FATAL' id="R-EB-REQ-S013"
                >The rim:SlotValue of rim:Slot name="Requirement" MUST be of "rim:AnyValueType"<sch:value-of select="$st"/></sch:assert>
            <sch:assert test="count(sdg:Requirement)=1" role='FATAL' id="R-EB-REQ-S014"
                >A  rim:Slot[@name=Requirement]/rim:SlotValue MUST contain one sdg:Requirement of the targetNamespace="http://data.europa.eu/p4s</sch:assert>
        </sch:rule>
    </sch:pattern>         

    <sch:pattern>
        <sch:rule context="rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:RelatedTo">
            <sch:assert test="count(sdg:Identifier)=1" role='FATAL' id="R-EB-REQ-S015"
                >The xs:element name="RelatedTo" type="sdg:ReferenceFrameworkType"/ MUST only contain the 'sdg:Identifier'</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement">          
            <sch:assert test="count(sdg:EvidenceTypeList)=0" role='FATAL' id="R-EB-REQ-S016"
                >The xs:element name="Requirement" type="sdg:RequirementType" / MUST not contain the EvidenceTypeList.</sch:assert>      
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement">          
            <sch:assert test="sdg:ReferenceFramework" role='WARNING' id="R-EB-REQ-S017"
                >The xs:element name="ReferenceFramework" type="sdg:ReferenceFrameworkType" SHOULD be present. Note: If the query was started with optional parameters such as 'procedure-id', 'country-code', 'jurisdiction-admin-l2' or 'jurisdiction-admin-l2', the EB always returns only those requirements that have a reference framework included. Requirements without a reference framework are not listed by the EB if optional parameters are used.  For a query without optional parameters, however, the EB returns all requirements, i.e. also those that do not have a reference framework. These are not valid instances in the sense of the follow-up processes of the OOTS. In the follow-up query of the EB "GetEvidenceType", it must be ensured that only those requirements are queried that contain a reference framework. A query without optional parameters returns requirements without corresponding reference framework to support development teams to find applicable (incomplete) requirements that need to be populated via the Life Cycle Management (LCM).</sch:assert>      
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/@id">          
            <sch:assert test="matches(normalize-space((.)),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" role='FATAL' id="R-EB-REQ-S019">
                The 'id' of a 'RegistryObject' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:".
            </sch:assert>   
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework">          
            <sch:assert test="sdg:RelatedTo" role='FATAL' id="R-EB-REQ-S020"
                >The xs:element name="RelatedTo" type="sdg:ReferenceFrameworkType" MUST be present.</sch:assert>      
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework">          
            <sch:assert test="sdg:Jurisdiction" role='FATAL' id="R-EB-REQ-S021"
                >The xs:element name="sdg:Jurisdiction" type="sdg:JurisdictionType" MUST be present.</sch:assert>      
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="rim:Slot[@name='SpecificationIdentifier']" role='FATAL' id="R-EB-REQ-S025"
            >The rim:Slot name="SpecificationIdentifier" MUST be present in the QueryResponse.</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')"/>
            <sch:assert test="$st ='StringValueType'" role='FATAL' id="R-EB-REQ-S026"
            >The rim:SlotValue of rim:Slot name="SpecificationIdentifier" MUST be of "rim:StringValueType".</sch:assert>
        </sch:rule>
    </sch:pattern>


    <!--
Some generic RIM patterns
-->

    <sch:pattern>
        <sch:rule context="rim:Slot">
            <sch:assert test="count(child::rim:SlotValue) = 1" id="R-EB-REQ-S022"
                        role="FATAL">A rim:Slot MUST have a rim:SlotValue child element</sch:assert>
        </sch:rule>
    </sch:pattern>


    <sch:pattern>
        <sch:rule context="rim:SlotValue">
            <sch:assert test="count(child::*) &gt; 0" id="R-EB-REQ-S023"
                        role="FATAL">A rim:SlotValue MUST have child element content</sch:assert>
        </sch:rule>
    </sch:pattern>
   
</sch:schema>