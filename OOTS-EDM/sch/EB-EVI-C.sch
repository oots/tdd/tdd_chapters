<?xml version="1.0" encoding="utf-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <sch:ns prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
    <!--Start of SR metadata-->
    <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EB-EVI-C</sr:identifier>
            <sr:title xml:lang="en">EB-EVI-C</sr:title>
            <sr:description xml:lang="en">Get Evidence Types for Requirement Query (Content)</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>CONTENT</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EB-EVI</sr:identifier>
                    <sr:title xml:lang="en">EB-EVI</sr:title>
                    <sr:description xml:lang="en">Query Response of the EB for the 'Get Evidence Types for Requirement Query'</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:component>EB</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
            </sr:Asset>?>
    <!--End of SR metadata-->
    <sch:include href="codelist-include/EEA_COUNTRY-CODELIST_code.sch"/>
    <sch:include href="codelist-include/LANGUAGECODE-CODELIST_code.sch"/>
    <sch:include href="codelist-include/PROCEDURES-CODELIST_code.sch"/>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Identifier">
            <sch:assert test="matches(normalize-space((.)),'https://sr.oots.tech.ec.europa.eu/requirements/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" role='FATAL' id="R-EB-EVI-C001"
                >The value of 'Identifier' of a 'Requirement' MUST be a unique UUID (RFC 4122) listed in the EvidenceBroker and use the prefix ''https://sr.oots.tech.ec.europa.eu/requirements/[UUID]'' pointing to the Semantic Repository. </sch:assert>         
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Name">
            <sch:assert test="not(normalize-space(@lang)='')" role='FATAL' id="R-EB-EVI-C003"
                >The value of 'lang' attribute MUST be provided. Default choice "EN". </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern> 
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Identifier">
            <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" role='FATAL' id="R-EB-EVI-C004"
                >The 'Identifier' of a 'ReferenceFramework' MUST be unique UUID (RFC 4122).</sch:assert>
        </sch:rule>
    </sch:pattern> 
    
    <sch:pattern>        
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Title">
            <sch:assert test="not(normalize-space(@lang)='')" role='FATAL' id="R-EB-EVI-C006"
                >The value of 'lang' attribute MUST be provided. Default choice "EN". </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>        
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Description">
            <sch:assert test="not(normalize-space(@lang)='')" role='FATAL' id="R-EB-EVI-C008"
                >The value of 'lang' attribute MUST be provided. Default choice "EN". </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern> 
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:Identifier">
            <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')" role='FATAL' id="R-EB-EVI-C013"
                >The 'Identifier' of 'EvidenceTypeList' MUST be unique UUID (RFC 4122).</sch:assert>
        </sch:rule>
    </sch:pattern> 
    
    <sch:pattern>        
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:Name">
            <sch:assert test="not(normalize-space(@lang)='')" role='FATAL' id="R-EB-EVI-C015"
                >The value of 'lang' attribute MUST be provided. Default choice "EN". </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>           
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:EvidenceTypeClassification">
            <sch:assert test="matches(., '^https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(oots|(' || string-join($EEA_COUNTRY-CODELIST_code, '|') || '))/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$')" role='FATAL' id="R-EB-EVI-C016"
                >The value of 'EvidenceTypeClassification' of a 'DataServiceEvidenceType' MUST be a UUID of the Evidence Broker and include a code of the code list 'EEA_Country-CodeList' (ISO 3166-1 alpha-2 codes EEA subset of country codes)
                using the prefix and scheme ''https://sr.oots.tech.eceuropa.eu/evidencetypeclassifications/[EEA_CountryCode]/[UUID]'' pointing to the Semantic Repository. For testing purposes and agreed OOTS data models the code "oots" can be used.  </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>        
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:Title">
            <sch:assert test="not(normalize-space(@lang)='')" role='FATAL' id="R-EB-EVI-C018"
                >The value of 'lang' attribute MUST be provided. Default choice "EN". </sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>        
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:Description">
            <sch:assert test="not(normalize-space(@lang)='')" role='FATAL' id="R-EB-EVI-C020"
                >The value of 'lang' attribute MUST be provided. Default choice "EN". </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>       
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType">
            <sch:assert test="(sdg:Jurisdiction)" role='FATAL' id="R-EB-EVI-C021"
                >A 'Jurisdiction' MUST be provided.</sch:assert>         
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
            <sch:assert test="rim:Value='oots-cs:v1.1'" role='FATAL' id="R-EB-EVI-C025"
            >The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-cs:v1.1".</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Jurisdiction">
            <!-- This assert checks if AdminUnitLevel3 is present -->
            <sch:assert test="not(sdg:AdminUnitLevel3) or (sdg:AdminUnitLevel1 and sdg:AdminUnitLevel2)"  role='FATAL' id="R-EB-EVI-C026">
                If AdminUnitLevel 3 of the ReferenceFramework Jurisdiction is present, both AdminUnitLevel 1 and AdminUnitLevel 2 of the ReferenceFramework Jurisdiction must be present.
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Jurisdiction">
            <!-- This assert checks if AdminUnitLevel2 is present -->
            <sch:assert test="not(sdg:AdminUnitLevel2) or sdg:AdminUnitLevel1"  role='FATAL' id="R-EB-EVI-C027">
                If AdminUnitLevel 2 of the ReferenceFramework Jurisdiction is present, AdminUnitLevel 1 of the ReferenceFramework Jurisdiction must be present.
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:Jurisdiction">
            <!-- This assert checks if AdminUnitLevel3 is present -->
            <sch:assert test="not(sdg:AdminUnitLevel3) or (sdg:AdminUnitLevel1 and sdg:AdminUnitLevel2)"  role='FATAL' id="R-EB-EVI-C028">
                If AdminUnitLevel 3 of the EvidenceType Jurisdiction is present, both AdminUnitLevel 1 and AdminUnitLevel 2 of the EvidenceType Jurisdiction must be present.
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:Jurisdiction">
            <!-- This assert checks if AdminUnitLevel2 is present -->
            <sch:assert test="not(sdg:AdminUnitLevel2) or sdg:AdminUnitLevel1"  role='FATAL' id="R-EB-EVI-C029">
                If AdminUnitLevel 2 of the EvidenceType Jurisdiction is present, AdminUnitLevel 1 of the EvidenceType Jurisdiction must be present.
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Name/@lang">
            <!--{}[](LanguageCode)-->
            <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code" role='FATAL' id="R-EB-EVI-C002">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Name/@lang'</sch:assert>
        </sch:rule>       
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Title/@lang">
            <!--{}[](LanguageCode)-->
            <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code" role='FATAL' id="R-EB-EVI-C005">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Title/@lang'</sch:assert>
        </sch:rule>     
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Description/@lang">
            <!--{}[](LanguageCode)-->
            <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code" role='FATAL' id="R-EB-EVI-C007">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Description/@lang'</sch:assert>
        </sch:rule>       
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:Name/@lang">
            <!--{}[](LanguageCode)-->
            <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code" role='FATAL' id="R-EB-EVI-C014">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:Name/@lang'</sch:assert>
        </sch:rule>      
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:Title/@lang">
            <!--{}[](LanguageCode)-->
            <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code" role='FATAL' id="R-EB-EVI-C017">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:Title/@lang'</sch:assert>
        </sch:rule>       
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:Description/@lang">
            <!--{}[](LanguageCode)-->
            <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code" role='FATAL' id="R-EB-EVI-C019">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:Description/@lang'</sch:assert>
        </sch:rule>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Jurisdiction/sdg:AdminUnitLevel1">
            <!--{}[](EEA_Country-CodeList)-->
            <sch:assert test="some $code in $EEA_COUNTRY-CODELIST_code satisfies .=$code" role='FATAL' id="R-EB-EVI-C009">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'EEA_Country-CodeList' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:Jurisdiction/sdg:AdminUnitLevel1'</sch:assert>
        </sch:rule>       
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:Jurisdiction/sdg:AdminUnitLevel1">
            <!--{}[](EEA_Country-CodeList)-->
            <sch:assert test="some $code in $EEA_COUNTRY-CODELIST_code satisfies .=$code" role='FATAL' id="R-EB-EVI-C022">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'EEA_Country-CodeList' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:EvidenceTypeList/sdg:EvidenceType/sdg:Jurisdiction/sdg:AdminUnitLevel1'</sch:assert>
        </sch:rule>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:RelatedTo/sdg:Identifier">
            <!--{}[](Procedures-CodeList)-->
            <sch:assert test="some $code in $PROCEDURES-CODELIST_code satisfies .=$code" role='FATAL' id="R-EB-EVI-C012">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'Procedures-CodeList' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:ReferenceFramework/sdg:RelatedTo/sdg:Identifier'</sch:assert>
        </sch:rule>
    </sch:pattern>
</sch:schema>   
