<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
    
    <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
    <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
    <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <!--Start of SR metadata-->
    <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/DSD-RESP-S</sr:identifier>
            <sr:title xml:lang="en">DSD-RESP-S</sr:title>
            <sr:description xml:lang="en">Query Response of the DSD (Structure)</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>STRUCTURE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-RESP</sr:identifier>
                    <sr:title xml:lang="en">DSD-RESP</sr:title>
                    <sr:description xml:lang="en">Query Response of the DSD</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:component>DSD</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
            </sr:Asset>?>
    <!--End of SR metadata-->
    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ln" value="local-name(/node())"/>
            <sch:assert test="$ln ='QueryResponse'" role='FATAL' id="R-DSD-RESP-S001"
                >The root element of a query response document MUST be 'query:QueryResponse'.</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="/node()">
            <sch:let name="ns" value="namespace-uri(/node())"/>
            <sch:assert test="$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0'" role='FATAL' id="R-DSD-RESP-S002"
                >The namespace of root element of a query response document MUST be 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0'.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:let name="status" value="@status" />
            <sch:assert test="@status" role='FATAL' id="R-DSD-RESP-S005"
                >The 'status' attribute of a 'QueryResponse' MUST be present.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:let name="status" value="@status" />
            <sch:assert test="$status = 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success'" role='FATAL' id="R-DSD-RESP-S006"
                >The 'status' attribute of a successfull 'QueryResponse' MUST be encoded as 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success'.</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success']">
            <sch:assert test="count(rim:RegistryObjectList) = 1" role='FATAL' id="R-DSD-RESP-S007"
                >A successful QueryResponse MUST include a 'rim:RegistryObjectList'.</sch:assert>
            <sch:assert test="count(rim:RegistryObjectList/rim:RegistryObject) > 0" role='FATAL' id="R-DSD-RESP-S016"
                >A successful QueryResponse MUST include a 'rim:RegistryObject'.</sch:assert>
            <sch:assert test="count(rs:Exception) = 0" role='FATAL' id="R-DSD-RESP-S008"
                >A successful QueryResponse MUST not include an Exception.</sch:assert>
        </sch:rule>       
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rim:RegistryObject">
            <sch:assert test="rim:Slot[@name='DataServiceEvidenceType']" role='FATAL' id="R-DSD-RESP-S009"
                >The rim:Slot name="DataServiceEvidenceType" MUST be present in the QueryResponse.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject">
            <sch:assert test="count(rim:Slot) = 1" role='FATAL' id="R-DSD-RESP-S010"
                >A 'rim:RegistryObject' MUST not contain any other rim:Slots than rim:Slot[@name='DataServiceEvidenceType'].</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')" />
            <sch:assert test="$st ='AnyValueType'" role='FATAL' id="R-DSD-RESP-S011"
                >The rim:SlotValue of rim:Slot name="DataServiceEvidenceType" MUST be of "rim:AnyValueType".</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue">
            <sch:assert test="count(sdg:DataServiceEvidenceType)=1" role='FATAL' id="R-DSD-RESP-S012"
                >A  'rim:Slot[@name='DataServiceEvidenceType'/rim:SlotValue/sdg:DataServiceEvidenceType' MUST contain one sdg:DataServiceEvidenceType of the targetNamespace="http://data.europa.eu/p4s".</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType">
            <sch:assert test="count(sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:SupportedValue) >= count(sdg:EvidenceProviderClassification)" role='FATAL' id="R-DSD-RESP-S013"
                >A 'sdg:SupportedValue' MUST be provided if the 'sdg:EvidenceProviderClassification' is present in the 'rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']'.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType">
            <sch:assert test="sdg:AccessService" role='FATAL' id="R-DSD-RESP-S014"
                >The xs:element name="sdg:AccessService" type="sdg:DataServiceType" MUST be present.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/@id">
            <sch:assert test="matches(normalize-space((.)),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"  role='FATAL' id="R-DSD-RESP-S015"
                >Each 'id' of 'rim:RegistryObject' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:".
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AccessService">
            <sch:assert test="sdg:Publisher" role='FATAL' id="R-DSD-RESP-S017"
                >The xs:element name="sdg:Publisher" type="sdg:EvidenceProviderType" MUST be present.</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="rim:Slot[@name='SpecificationIdentifier']" role='FATAL' id="R-DSD-RESP-S020"
            >The rim:Slot name="SpecificationIdentifier" MUST be present in the QueryResponse.</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
            <sch:let name="st"  value="substring-after(@xsi:type, ':')"/>
            <sch:assert test="$st ='StringValueType'" role='FATAL' id="R-DSD-RESP-S021"
            >The rim:SlotValue of rim:Slot name="SpecificationIdentifier" MUST be of "rim:StringValueType".</sch:assert>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="query:QueryResponse">
            <sch:assert test="not(rim:Slot[@name != 'SpecificationIdentifier'])" role='FATAL' id="R-DSD-RESP-S022"
            >A 'query:QueryResponse' MUST not contain any other rim:Slots than rim:Slot[@name='SpecificationIdentifier'].</sch:assert>
        </sch:rule>
    </sch:pattern>

    <!--
  Some generic RIM patterns
-->

    <sch:pattern>
        <sch:rule context="rim:Slot">
            <sch:assert test="count(child::rim:SlotValue) = 1" id="R-DSD-RESP-S018"
                        role="FATAL">A rim:Slot MUST have a rim:SlotValue child element</sch:assert>
        </sch:rule>
    </sch:pattern>


    <sch:pattern>
        <sch:rule context="rim:SlotValue">
            <sch:assert test="count(child::*) &gt; 0" id="R-DSD-RESP-S019"
                        role="FATAL">A rim:SlotValue MUST have child element content</sch:assert>
        </sch:rule>
    </sch:pattern>



</sch:schema>