# Data Service Directory (DSD) XML examples

## 1. Introduction to the DSD
The Data Service Directory (DSD) is a Common Service defined in the OOTS HLA. It maintains a catalog of Evidence Providers with the Evidence Types they are able to provide upon request using their Data Services. It is used in the Evidence Exchange process by the Evidence Requesters to discover the Evidence Providers that can provide the evidences they require, together with the required metadata and attributes imposed by the Data Services, like the classifications and context determinations of the Evidence Providers.

The information data model is based on the SDGR Application Profile for the DSD. The main functionality of the DSD is to publish Data Services of Evidence Providers that provide distributions of Evidence Types and make them discoverable through queries. The functionality requires four main classes of Information:
* The DataServiceEvidenceType, providing the semantic information and requirements for retrieving an evidence type from a Data Service.
* The Distribution of the DataServiceEvidenceType, describing the format, the semantic and syntactic conformance, under which the Evidence Type can be distributed.
* The DataService, describing the technical endpoint from which an Evidence Requester can request the Evidence distributions.
* The EvidenceProvider, describing the details of the Provider of the Evidence.

## 2. Query Interface Specification of the DSD
The Service API of the DSD is implemented using the OASIS RegRep v4 Query Protocol with the REST API Binding. The query interface specification for the DSD has only simple, single-value parameters, the REST binding is used to implement the DSD query interface. This implies that the query transaction is executed as an HTTP GET request with the URL representing the query to execute and the HTTP response carrying the query response as an XML document. 

The full specification of the DSD can be found in section 3.1.1 of the [Technical Design Documents](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home)

## 3. XML DSD response examples
Additional XML DSD response samples can be found in this GitLab section [OOTS-EDM/XML folder of the tdd_chapters git repository](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xml/DSD).

The section lists: 

[Complete XML examples](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xml/DSD/Complete%20XML%20examples%20(DSD)) for different use cases (e.g. Birth Certificate, Secondary Education and Social Security completion). The git directory distinguishes between the different response transactions of the DSD
* Query Response of the DSD (pointing to the specification in subsection 4.3 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.1.1)
* Query Error Response of the DSD (pointing to the specification in subsection 4.4.1 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.1.1)
* Query Error Response of the DSD requesting additional User Provided Attributes (pointing to the specification in subsection 4.4.3 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.1.1)

[RegressionTests](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xml/DSD/RegressionTests) that include valid and invalid snippets to illustrate the use and to test the validity of schematrons associated to the different transactions. 

[Snippets - 3.1.1 Data Service Directory (DSD)](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xml/DSD/Snippets%20-%203.1.1%20Data%20Service%20Directory%20(DSD)) that reflect the examples that are also illustrated in section 3.1.3 and 3.1.4 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.1.1)





