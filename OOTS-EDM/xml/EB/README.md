# Evidence Broker (EB) and XML Examples

## 1. Introduction to the EB
The Evidence Broker is one of the Common Services of the OOTS HLA. It is a service that publishes which types of evidence Member States can provide to prove a particular requirement of a procedure. It provides metadata on the requirements applicable in a procedure and which type of evidence can be used by the User to prove fulfilment. Using the mapping from criteria or information requirements to possible evidence types, the Evidence Requester can find the evidence types that can prove that the User fulfills the requirements of the procedure.

The EB Information Model is based on the ISA2 SEMIC Core Criterion and Core Evidence Vocabulary (CCCEV) v2.0. The CCCEV is designed to support the exchange of information between organisations defining requirements and organisations responding to these requirements by means of evidence types.

The CCCEV contains two basic and complementary core concepts:
* The Requirement, which is used as the basis for making a judgement or decision, e.g. a requirement set in a public tender or a condition that has to be fulfilled for a public service to be executed;
* The Evidence Type, which proves that something else exists or is true. In particular, an evidence is used to prove that a specific requirement is met by someone or by something.

Each requirement is linked with one or more Evidence Type Lists of Evidence Types. An Evidence Type List is satisfied, if and only if, for all included Evidence Types in this List, corresponding to conformant Evidence(s), are provided. The Evidence Type List describes thus an AND condition on the different Evidence Types within the list and an OR condition between two or more Evidence Type Lists. Combinations of alternative Evidence Type Lists can be provided for a respondent of a Requirement to choose amongst them. The following behavior is supported by the EB Mechanism:

* To fulfill a 'Requirement' at least one Evidence for each Evidence Type specified in one Evidence Type List MUST be provided.
* The user can select an 'Evidence Type List' amongst the ones proposed by the Evidence Broker. Thus,
  * Evidence Type Lists must have at least one Evidence Type.
  * Evidence Types of an Evidence Type List are combined via the logic operator ‘AND’. 
  * Evidence Type Lists that fulfill the same Requirement are combined via the logic operator ‘OR’.
* The 'Evidence Types' are described in detail with respect to their jurisdiction level.
* The ‘Evidence’ is an instance of an Evidence Types that provides the means to support responses to Requirements.

The EB holds a mapping between the Requirements and their associated Evidence Type Lists and Evidence Types that are able to prove these requirements. The mapping can be done either nationally or it can be harmonized among  MS (e.g. if an agreed pan-European standard is used as Evidence Type).  An Evidence instance that is requested and provided throughout the OOTS Evidence Exchange process should refer to its corresponding Evidence Type.

## 2. Query Interface Specification of the EB
The Service API of the EB is implemented using the OASIS RegRep v4 Query Protocol with the REST API Binding. The query interface specification for the EB has only simple, single-value parameters, the REST binding is used to implement the DSD query interface. This implies that the query transaction is executed as an HTTP GET request with the URL representing the query to execute and the HTTP response carrying the query response as an XML document.

The full specification of the EB can be found in section 3.2.1 of the [Technical Design Documents](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home)

## 3. XML EB response examples

Additional XML EB response samples can be found in this GitLab section [OOTS-EDM/XML folder of the tdd_chapters git repository](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xml/EB).

The section lists:

[Complete XML examples](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xml/EB/Complete%20XML%20examples%20(EB)) for different use cases (e.g. Birth Certificate, Secondary Education and Social Security completion). The git directory distinguishes between the different response transactions of the EB
* Get List of Requirements Query (pointing to the specification in subsection 4.2 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.2.1)
* Get Evidence Types for Requirement Query (pointing to the specification in subsection 4.3 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.2.1)
* Query Error Response of the Evidence Broker (pointing to the specification in subsection 4.4 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.2.1)

[RegressionTests](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xml/EB/RegressionTests) that include valid and invalid snippets to illustrate the use and to test the validity of schematrons associated to the different transactions.

[Snippets - 3.2.1 Evidence Broker](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xml/EB/Snippets%20-%203.2.1%20Evidence%20Broker/3.2.4%20Query%20Interface%20Specification%20of%20the%20EB) that reflect the examples that are also illustrated in subsection 3.2.4 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.2.1)

