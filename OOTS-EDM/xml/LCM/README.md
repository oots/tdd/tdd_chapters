# The Lifecycle Management Specification(LCM) Samples

## 1. Introduction 
A Common Service MAY provide a Lifecycle Management (LCM) Interface for a bulk update of the information stored in the Common Services of a Member State. This LCM interface is a highly constrained profile based on the RegRep 4.0 LCM Manager Interface Specification that has the following limitations:

* Only the SubmitObjects protocol is supported.
* Each Member State shall authorize at most one authorized competent authority to make submissions.    
* A submission is linked to a single Member State and made on behalf of that Member State.
* Submission linked to a single Member State do not affect data related to other Member States.
* A submission contains a complete, internally consistent set of data.  It is not possible to incrementally submit objects using a series of submissions.
* In case of a successful submission made for a Member State to a Common Service, any existing data previously submitted to the Common Service for that Member State is replaced. This obviates the need for the RemoveObjects protocol.
* In case of an unsuccessful submission made for a Member State to a Common Service, the existing data held by the Common Service is retained.
  
Since object submission updates data, this profile use eDelivery for secure and reliable messaging.

A service implementing the LCM specification MUST define a Classification Scheme using a unique urn. The Classification Scheme MUST contain all the Classification Nodes that properly characterize the registry objects and associations under submission.

The SubmitObjects Protocol defines a SubmitObjectsRequest message for sending the objects to be added together with successful and error responses. 

## 2. The Regrep LCM Submission

For the LCM submission, the profile uses the SubmitObjectsRequest message, as defined by the RegRep 4.0 LCM Manager Interface Specification SubmitObjects Protocol. A detailed guideline how the SubmitObjects Protocol is implemented for the Common Services is illustrated in the following sections:

* LCM Interface Specification of the Evidence Broker (section 3.2.5) of the [Technical Design Documents](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home)
* LCM Interface Specification of the Data Service Directory (section 3.1.5 and 3.1.6) of the [Technical Design Documents](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home)

## 3. XML LCM examples

Additional XML EB response samples can be found in this GitLab section [OOTS-EDM/XML folder of the tdd_chapters git repository](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xml/LCM).

The section lists:

[Complete XML examples](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xml/LCM/Complete%20XML%20Examples) for different use cases (e.g. Birth Certificate, Bulk submission). The git directory distinguishes between the different LCM Submit Object transactions of the Common Services, the Evidence Broker (EB) and the Data Service Directory (DSD):
* LCM Submit Objects to the DSD - Baseline (pointing to the specification in subsection 5.5 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.1.5)
* LCM Submit Objects to the DSD - Refactored (pointing to the specification in subsection 6.5 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.1.6)
* LCM Submit Objects to the EB (pointing to the specification in subsection 5.4 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.2.5)

[RegressionTests](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xml/LCM/RegressionTests) that include valid and invalid snippets to illustrate the use and to test the validity of schematrons associated to the different transactions.

[Snippets](https://code.europa.eu/oots/tdd/tdd_chapters/-/tree/master/OOTS-EDM/xml/LCM/Snippets) that reflect the examples that are also illustrated in 
* LCM Interface Specification of the DSD - Baseline (described in subsection 5.5 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.1.5)
* LCM Interface Specification of the DSD - Refactored (described in subsection 6.5 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.1.6)
* LCM Interface Specification of the EB (described in subsection 5.4 of the [TDD](https://ec.europa.eu/digital-building-blocks/sites/display/TDD/OOTS+Technical+Design+Documents+Home) section 3.2.5)

