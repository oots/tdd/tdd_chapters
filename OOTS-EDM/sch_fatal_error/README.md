# Filtered Schematron Definitions

## Overview

The OOTS Schematron definitions follow the severity levels defined in 
[Standard Severity Levels with Schematron @role](https://schematron.com/document/141.html).  

In some contexts, only the rules with severity "FATAL" or "WARNING" are relevant.  Also, 
some tooling, such as tooling used for the OOTS testing service, 
(incorrectly) considers the "CAUTION" role to be at a higher level than "WARN".
For convenience, the [sch_fatal_error.py](sch_fatal_error.py) script produces a set of copies
of the Schematron files limited (filtered) to those rules that have severity "FATAL" or "WARNING". 

The filter also remove inline Dublic Core annotations as some tooling does not accepts this either.

## WARNING

These files are generated automatically.  The functionality is defined and maintained in Schematrons in the 
[OOTS EDM Schematron](../sch) folder.



