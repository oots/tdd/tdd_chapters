<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:sch="http://purl.oclc.org/dsdl/schematron"
    exclude-result-prefixes=""
    version="2.0">
    
    <xsl:output indent="yes"  method="xml"/>
    
    <xsl:template match="*[namespace-uri()='http://purl.org/dc/terms/' ]">
        <xsl:comment>Suppressing embedded Dublic Core annotation</xsl:comment>
    </xsl:template>
    
    <xsl:template match="*[namespace-uri()='http://www.w3.org/ns/dcat#']">
        <xsl:comment>Suppressing embedded DCAT annotation</xsl:comment>
    </xsl:template>
    
    <xsl:template match="sch:assert[@role!='FATAL' and @role !='ERROR']"  priority="100">
        <xsl:comment>Role is <xsl:value-of select="@role"/> so stripped the following assertion:
            <xsl:copy-of select="."/> 
        </xsl:comment>
    </xsl:template>
    
    <xsl:template match="sch:assert">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="sch:include" priority="100">
        <xsl:comment>Changing the import to point to the "sch" directory </xsl:comment>
        <sch:include href="../sch/{@href}"/>
    </xsl:template>
    
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    
</xsl:stylesheet>