<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://docs.oasis-open.org/ebxml-msg/ebms/v3.0/ns/core/200704/"
           prefix="eb"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <sch:ns uri="http://www.w3.org/XML/1998/namespace" prefix="xml"/>
   <sch:ns uri="http://www.w3.org/2003/05/soap-envelope" prefix="S12"/>
   <sch:ns uri="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"
           prefix="wsu"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EDM-ebMS</sr:identifier>
           <sr:title xml:lang="en">EDM-ebMS</sr:title>
           <sr:description xml:lang="en">Evidence Broker message header</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CONTENT</sr:theme>
           <sr:theme>STRUCTURE</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                 <sr:title xml:lang="en">EDM-REQ</sr:title>
                 <sr:description xml:lang="en">Evidence Request</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>HEADER</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
                 <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-RESP</sr:identifier>
                 <sr:title xml:lang="en">EDM-RESP</sr:title>
                 <sr:description xml:lang="en">Evidence Response</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>HEADER</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-ERR</sr:identifier>
                 <sr:title xml:lang="en">EDM-ERR</sr:title>
                 <sr:description xml:lang="en">Evidence Error Response</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>HEADER</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.1.0</sr:version>
           <sr:versionNotes xml:lang="en">Updated version number of the two modified Schematrons</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/EAS_Code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/EEA_COUNTRY-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/OOTSMEDIATYPES-CODELIST_code.sch"/>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/@*">
         <sch:let name="namespace" value="namespace-uri()"/>
         <sch:let name="attribute" value="local-name()"/>
         <!--Role is CAUTION so stripped the following assertion:
             The eb:Messaging envelope should only contain the attributes S12:mustUnderstand, wsu:Id or xml:id. The attribute '' from the '' namespace is not allowed.-->
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging">
         <sch:assert test="count(eb:SignalMessage) + count(eb:UserMessage/eb:MessageInfo/eb:RefToMessageId) +              count(eb:UserMessage/eb:CollaborationInfo/eb:AgreementRef) + count(eb:UserMessage/eb:PayloadInfo/eb:PartInfo/eb:Schema) +             count(eb:UserMessage/eb:PayloadInfo/eb:PartInfo/eb:Description) = 0"
                     id="R-EDM-ebMS-001"
                     role="FATAL">The eb:Messaging envelope should not contain the elements 'SignalMessage', 'MessageInfo/RefToMessageId', 'CollaborationInfo/AgreementRef', 'PartInfo/Schema' and 'PartInfo/Description'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage">
         <sch:assert test="eb:MessageProperties" id="R-EDM-ebMS-002" role="FATAL">There MUST be exactly one element eb:MessageProperties.</sch:assert>
         <sch:assert test="eb:PayloadInfo" id="R-EDM-ebMS-003" role="FATAL">There MUST be one element eb:PayloadInfo.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:MessageInfo/eb:MessageId">
         <sch:assert test="matches(normalize-space((.)),'^[a-z0-9!#$%*+/=?^_`|~-]+(?:\.[a-z0-9!#$%*+/=?^_`|~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$','i')"
                     id="R-EDM-ebMS-004"
                     role="FATAL">eb:MessageId MUST be expressed according to RFC2822.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From">
         <sch:assert test="count(eb:PartyId) = 1" id="R-EDM-ebMS-005" role="FATAL">The eb:PartyId MUST be used only once.</sch:assert>
      </sch:rule>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To">
         <sch:assert test="count(eb:PartyId) = 1" id="R-EDM-ebMS-009" role="FATAL">The eb:PartyId MUST be used only once.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <!--{}[](EAS-CodeList, EEA_Country-Codelist)-->
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:PartyId">
         <!--{}[](EAS-CodeList, EEA_Country-Codelist)-->
         <sch:let name="suffix"
                  value="substring-after(@type, 'urn:cef.eu:names:identifier:EAS:')"/>
         <sch:let name="suffix1"
                  value="substring-after(@type, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
         <sch:assert test="((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256"
                     role="FATAL"
                     id="R-EDM-ebMS-010">
            The value of the 'eb:PartyId/@type' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country-CodeList’. For testing purposes the code "oots" can be used.
         </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:From/eb:Role">
         <sch:assert test="matches(normalize-space(text()),'^http://sdg.europa.eu/edelivery/gateway$')"
                     id="R-EDM-ebMS-008"
                     role="FATAL">The eb:Role MUST be set to fixed value http://sdg.europa.eu/edelivery/gateway.</sch:assert>
      </sch:rule>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:PartyInfo/eb:To/eb:Role">
         <sch:assert test="matches(normalize-space(text()),'^http://sdg.europa.eu/edelivery/gateway$')"
                     id="R-EDM-ebMS-012"
                     role="FATAL">The eb:Role MUST be set to fixed value http://sdg.europa.eu/edelivery/gateway.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Service">
         <sch:assert test="matches(normalize-space(text()),'^QueryManager$')"
                     id="R-EDM-ebMS-013"
                     role="FATAL">The eb:Service MUST be set to fixed value 'QueryManager'.</sch:assert>
         <sch:assert test="@type" id="R-EDM-ebMS-014" role="FATAL">The eb:Service/@type MUST be present.</sch:assert>
         <sch:assert test="matches(normalize-space(@type),'^urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0$')"
                     id="R-EDM-ebMS-015"
                     role="FATAL">The eb:Service/@type MUST be set to fixed value 'urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:Action">
         <sch:assert test="matches(normalize-space(text()),'^ExecuteQueryRequest$') or matches(normalize-space(text()),'^ExecuteQueryResponse$') or matches(normalize-space(text()),'^ExceptionResponse$')"
                     id="R-EDM-ebMS-016"
                     role="FATAL">eb:Action MUST use the values 'ExecuteQueryRequest' 'ExecuteQueryResponse' or 'ExceptionResponse'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:CollaborationInfo/eb:ConversationId">
         <sch:assert test="matches(normalize-space(text()),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     id="R-EDM-ebMS-017"
                     role="FATAL">The eb:ConversationId MUST be expressed as UUID.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:MessageProperties">
         <sch:assert test="count(eb:Property) = 2" id="R-EDM-ebMS-018" role="FATAL">There MUST be exately two elements eb:Property of eb:MessageProperties.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:MessageProperties/eb:Property">
         <sch:assert test="matches(normalize-space(@name),'^originalSender$') or matches(normalize-space(@name),'^finalRecipient$')"
                     id="R-EDM-ebMS-019"
                     role="FATAL">One of the values from eb:Property/@name uses 'originalSender' and the other one 'finalRecipient'.</sch:assert>
         <sch:assert test="@type" id="R-EDM-ebMS-020" role="FATAL">There MUST be one element eb:Property/@type for eb:Property.</sch:assert>
         <!--{}[](EAS-CodeList, EEA_Country-Codelist)-->
         <sch:let name="suffix"
                  value="substring-after(@type, 'urn:cef.eu:names:identifier:EAS:')"/>
         <sch:let name="suffix1"
                  value="substring-after(@type, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
         <sch:assert test="((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256"
                     role="FATAL"
                     id="R-EDM-ebMS-021">
            The value of the 'eb:PartyId/@type' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country-CodeList’. For testing purposes the code "oots" can be used.
         </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage">
         <sch:assert test="(count(eb:PayloadInfo/eb:PartInfo) = 1 and (matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExecuteQueryRequest$') or matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExceptionResponse$'))) or not(matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExecuteQueryRequest$') or matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExceptionResponse$'))"
                     id="R-EDM-ebMS-023"
                     role="FATAL">There MUST be exactly one element eb:PartInfo in the eb:PayloadInfo if the eb:Action value is 'ExecuteQueryRequest' or 'ExceptionResponse'.</sch:assert>
         <sch:assert test="(count(eb:PayloadInfo/eb:PartInfo) &gt; 0 and matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExecuteQueryResponse$')) or not(matches(normalize-space(eb:CollaborationInfo/eb:Action),'^ExecuteQueryResponse$'))"
                     id="R-EDM-ebMS-024"
                     role="FATAL">There MUST be at least one element eb:PartInfo in the eb:PayloadInfo if the eb:Action value is 'ExecuteQueryResponse'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:PayloadInfo">
         <sch:let name="mt"
                  value="eb:PartInfo/eb:PartProperties/eb:Property[@name='MimeType']"/>
         <sch:let name="total" value="$mt[text() = 'application/x-ebrs+xml']"/>
         <sch:assert test="count($mt) &gt;= 1 and count($total) = 1"
                     id="R-EDM-ebMS-030"
                     role="FATAL">In the message there MUST be exactly one eb:PartInfo/eb:PartProperties/eb:Property with name="MimeType" that has the fixed value 'application/x-ebrs+xml'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:PayloadInfo/eb:PartInfo">
         <sch:assert test="@href" id="R-EDM-ebMS-025" role="FATAL">The attribute href of eb:PartInfo MUST be provided.</sch:assert>
         <sch:assert test="matches(normalize-space(@href),'^cid:[a-z0-9!#$%*+/=?^_`|~-]+(?:\.[a-z0-9!#$%*+/=?^_`|~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$','i')"
                     id="R-EDM-ebMS-026"
                     role="FATAL">eb:PartInfo/@href MUST use the prefix 'cid:' and be expressed according to RFC2822.</sch:assert>
         <sch:assert test="matches(normalize-space(@href),'^cid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}@[a-z]*','i')"
                     id="R-EDM-ebMS-027"
                     role="FATAL">In eb:PartInfo/@href, the value on the right side of the prefix 'cid:' and left from the symbol '@' MUST be a valid UUID.</sch:assert>
         <sch:assert test="eb:PartProperties" id="R-EDM-ebMS-028" role="FATAL">There MUST be exactly one element eb:PartProperties.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage/eb:PayloadInfo/eb:PartInfo/eb:PartProperties">
         <sch:assert test="count(eb:Property) = 1 or count(eb:Property) = 2"
                     id="R-EDM-ebMS-029"
                     role="FATAL">There MUST be one or two eb:Property of eb:PartProperties.</sch:assert>
         <sch:assert test="count(eb:Property[@name='MimeType']) = 1"
                     id="R-EDM-ebMS-033"
                     role="FATAL">There MUST be one eb:Property/@name with the fixed value "MimeType".</sch:assert>
         <sch:assert test="count(eb:Property) = 1 or count(eb:Property) = count(eb:Property[@name='MimeType']) + count(eb:Property[@name='CompressionType'])"
                     id="R-EDM-ebMS-035"
                     role="FATAL">If there is a second eb:Property the eb:Property MUST have a name attribute with the fixed value "CompressionType".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="eb:PartProperties/eb:Property[@name='CompressionType']">
         <sch:assert test="text()='application/gzip'" id="R-EDM-ebMS-034" role="FATAL">A eb:Property with name="CompressionType" MUST have the fixed value 'application/gzip'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage[eb:CollaborationInfo/eb:Action/text() = 'ExecuteQueryResponse']/eb:PayloadInfo/eb:PartInfo/eb:PartProperties/eb:Property[@name='MimeType']">
         <sch:assert test="matches(normalize-space(text()),'^application/x-ebrs\+xml$','i') or (some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code) "
                     id="R-EDM-ebMS-031"
                     role="FATAL">If the eb:Action value is 'ExecuteQueryResponse' the eb:Property with  name="MimeType" MUST be either 'application/x-ebrs+xml' or a a valid mime type of the code list 'OOTSMediaTypes'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="//eb:Messaging/eb:UserMessage[eb:CollaborationInfo/eb:Action/text() = 'ExecuteQueryResponse']/eb:PayloadInfo/eb:PartInfo[1]/eb:PartProperties">
         <sch:let name="mt" value="eb:Property[@name='MimeType']"/>
         <sch:let name="total" value="$mt[text() = 'application/x-ebrs+xml']"/>
         <!--Role is WARNING so stripped the following assertion:
             If the eb:Action value is 'ExecuteQueryResponse' the first eb:PartInfo[1]/eb:PartProperties/eb:Property with name="MimeType" SHALL be 'application/x-ebrs+xml'.-->
      </sch:rule>
   </sch:pattern>
</sch:schema>
