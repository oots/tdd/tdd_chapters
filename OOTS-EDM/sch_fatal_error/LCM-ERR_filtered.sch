<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0" prefix="lcm"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/LCM-ERR</sr:identifier>
            <sr:title xml:lang="en">LCM-ERR</sr:title>
            <sr:description xml:lang="en">Response for unsuccessful LCM submission</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>CONTENT</sr:theme>
            <sr:theme>STRUCTURE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/LCM-ERR</sr:identifier>
                    <sr:title xml:lang="en">LCM-ERR</sr:title>
                    <sr:description xml:lang="en">LCM Error Response</sr:description>
                    <sr:type>LCM</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>EB</sr:component>
                    <sr:component>DSD</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
            </sr:Asset>?>
   <!--End of SR metadata-->
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/ERRORSEVERITY-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/LCMERRORCODES-CODELIST_code.sch"/>
   <sch:pattern>
      <sch:rule context="/node()">
         <sch:let name="ln" value="local-name(/node())"/>
         <sch:assert test="$ln ='RegistryResponse'" role="FATAL" id="R-LCM-ERR-001">The root element of a query response document MUST be 'rs:RegistryResponse'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="/node()">
         <sch:let name="ns" value="namespace-uri(/node())"/>
         <sch:assert test="$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0'"
                     role="FATAL"
                     id="R-LCM-ERR-002">The namespace of root element of a 'rs:RegistryResponse' MUST be 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rs:RegistryResponse">
         <sch:assert test="count(@requestId) + count(rs:Exception[@code='LCM:ERR:0003'])&gt;0"
                     role="FATAL"
                     id="R-LCM-ERR-003">The 'requestId' attribute of a 'rs:RegistryResponse' MUST be present unless the rs:exception/@code 'LCM:ERR:0003' is used from the code list 'LCMErrorCodes' (Life Cycle Management Error Response Codes). </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rs:RegistryResponse[@requestId]">
         <sch:assert test="matches(normalize-space(@requestId),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-LCM-ERR-004">The 'requestId' attribute of a 'rs:RegistryResponse' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:" and match the corresponding request.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rs:RegistryResponse">
         <sch:assert test="@status" role="FATAL" id="R-LCM-ERR-005">The 'status' attribute of a 'rs:RegistryResponse' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rs:RegistryResponse">
         <sch:let name="status" value="@status"/>
         <sch:assert test="$status = 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure'"
                     role="FATAL"
                     id="R-LCM-ERR-006">The 'status' attribute of a unsuccessful 'rs:RegistryResponse' MUST be encoded as as 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure'.</sch:assert>
         <sch:assert test="count(rim:RegistryObjectList) = 0"
                     role="FATAL"
                     id="R-LCM-ERR-007">An unsuccessful 'rs:RegistryResponse' MUST not include a 'rim:RegistryObjectList'</sch:assert>
         <sch:assert test="count(rs:Exception)&gt;0" role="FATAL" id="R-LCM-ERR-008">An unsuccessful 'rs:RegistryResponse' MUST include an Exception</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rs:RegistryResponse/rs:Exception">
         <sch:assert test="@xsi:type" role="FATAL" id="R-LCM-ERR-009">The 'xsi:type' attribute of a 'rs:Exception' MUST be present. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rs:RegistryResponse/rs:Exception">
         <sch:assert test="@xsi:type='rs:InvalidRequestExceptionType'"
                     role="FATAL"
                     id="R-LCM-ERR-010">The value of 'xsi:type' attribute of a 'rs:Exception' MUST be rs:InvalidRequestExceptionType</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rs:RegistryResponse/rs:Exception">
         <sch:assert test="@severity" role="FATAL" id="R-LCM-ERR-011">The 'severity' attribute of a 'rs:Exception' MUST be present. It must be part of the code list 'ErrorSeverity' (Error Severity). The codes 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired' and 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:DSDErrorResponse:AdditionalInput' shall not be used by this transaction.</sch:assert>
         <sch:assert test="@severity='urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error'"
                     role="FATAL"
                     id="R-LCM-ERR-012">The value of 'severity' attribute of a 'rs:Exception' MUST  be "urn:oasis:names:tc:ebxml-regrep:ErrorSeverityType:Error" from the code list 'ErrorSeverity'.</sch:assert>
         <sch:assert test="@message" role="FATAL" id="R-LCM-ERR-013">The 'message' attribute of a 'rs:Exception' MUST be present. </sch:assert>
         <sch:assert test="@code" role="FATAL" id="R-LCM-ERR-015">The 'code' attribute of a 'rs:Exception' MUST be present. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rs:RegistryResponse/rs:Exception/@code">
         <sch:assert test="some $code in $LCMERRORCODES-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-LCM-ERR-016">The value of 'code' attribute of a 'rs:Exception' MUST be a 'code' matching the 'type' of the code list 'LCMErrorCodes' (Life Cycle Management Error Response Codes). </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rs:RegistryResponse">
         <sch:assert test="not(rim:Slot[@name != 'SpecificationIdentifier'])"
                     role="FATAL"
                     id="R-LCM-ERR-017">An unsuccessful 'rs:RegistryResponse' MUST not contain any other rim:Slots than rim:Slot[@name='SpecificationIdentifier'].</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rs:RegistryResponse">
         <sch:assert test="rim:Slot[@name='SpecificationIdentifier']"
                     role="FATAL"
                     id="R-LCM-ERR-018">The rim:Slot name="SpecificationIdentifier" MUST be present in the rs:RegistryResponse.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rs:RegistryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='StringValueType'" role="FATAL" id="R-LCM-ERR-019">The rim:SlotValue of rim:Slot name="SpecificationIdentifier" MUST be of "rim:StringValueType".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rs:RegistryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
         <sch:assert test="rim:Value='oots-cs:v1.1'" role="FATAL" id="R-LCM-ERR-020">The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-cs:v1.1".</sch:assert>
      </sch:rule>
   </sch:pattern>
</sch:schema>
