
# test
from inspect import getsourcefile
import glob, re

#from  saxonpy  import PySaxonProcessor
from  saxonche  import PySaxonProcessor, PyXslt30Processor

with PySaxonProcessor(license=False) as proc:

    #transformer = proc.new_xslt_processor()
    #transformer.compile_stylesheet(stylesheet_file='sch_fatal_error.xsl')

    processor = PySaxonProcessor(license=False)

    xslt_processor = processor.new_xslt30_processor()
    transformer = xslt_processor.compile_stylesheet(
        stylesheet_file='sch_fatal_error.xsl'
    )

    for source in glob.glob('../sch/*.sch'):
        print(source)
        m = re.search(r'([\w_-]*)\.sch$', source)
        output = "{}_filtered.sch".format(m[1])
        print(output)

        xslt_processor.transform_to_file(
            source_file=source, stylesheet_file="sch_fatal_error.xsl", output_file=output
        )


