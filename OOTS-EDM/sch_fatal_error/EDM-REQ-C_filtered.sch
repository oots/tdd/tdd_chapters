<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <sch:ns prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EDM-REQ-C</sr:identifier>
           <sr:title xml:lang="en">EDM-REQ-C</sr:title>
           <sr:description xml:lang="en">Evidence Request Syntax Mapping (Content)</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CONTENT</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-REQ</sr:identifier>
                 <sr:title xml:lang="en">EDM-REQ</sr:title>
                 <sr:description xml:lang="en">Evidence Request</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>PAYLOAD</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.1.0</sr:version>
           <sr:versionNotes xml:lang="en">Updated Version information in schematron files</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/EEA_COUNTRY-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/LEVELSOFASSURANCE-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/AGENTCLASSIFICATION-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/OOTSMEDIATYPES-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/LANGUAGECODE-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/EAS_Code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/PROCEDURES-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/COUNTRYIDENTIFICATIONCODE-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/IDENTIFIERSCHEMES-CODELIST_code.sch"/>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
         <sch:assert test="rim:Value='oots-edm:v1.1'" id="R-EDM-REQ-C001" role="FATAL">The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-edm:v1.1".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='IssueDateTime']/rim:SlotValue/rim:Value">
         <sch:assert test="matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}','i')"
                     id="R-EDM-REQ-C002"
                     role="FATAL">The 'rim:Value' of 'IssueDateTime' MUST be according to xsd:dateTime.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='PreviewLocation']/rim:SlotValue">
         <sch:assert test="starts-with(lower-case(rim:Value/text()), 'https://')"
                     id="R-EDM-REQ-C005"
                     role="FATAL">The 'rim:Value' of a 'PreviewLocation' MUST be a URI starting with 'https://' according to RFC 3986.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Identifier">
         <sch:assert test="matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/requirements/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$')"
                     id="R-EDM-REQ-C008"
                     role="FATAL">The value of 'Identifier' of a 'Requirement' MUST be a unique UUID (RFC 4122) listed in the EvidenceBroker and use the prefix ''https://sr.oots.tech.ec.europa.eu/requirements/[UUID]'' pointing to the Semantic Repository.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Name">
         <sch:assert test="not(normalize-space(@lang)='')"
                     id="R-EDM-REQ-C010"
                     role="FATAL">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier">
         <sch:assert test="@schemeID" id="R-EDM-REQ-C011" role="FATAL">The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent">
         <sch:assert test="not(normalize-space(sdg:Classification)='')"
                     id="R-EDM-REQ-C013"
                     role="FATAL">The value for 'Agent/Classification' MUST be provided. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier">
         <sch:assert test="@schemeID" id="R-EDM-REQ-C017" role="FATAL">The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Identifier">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     id="R-EDM-REQ-C019"
                     role="FATAL">The value of 'Identifier' of MUST be unique UUID (RFC 4122).</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification">
         <sch:assert test="not(normalize-space(sdg:Type)='')"
                     id="R-EDM-REQ-C020"
                     role="FATAL">The value for 'Type' MUST be provided.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description">
         <sch:assert test="not(normalize-space(@lang)='')"
                     id="R-EDM-REQ-C022"
                     role="FATAL">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification">
         <sch:assert test="not(normalize-space(sdg:SupportedValue)='')"
                     id="R-EDM-REQ-C023"
                     role="FATAL">A value for 'SupportedValue' MUST be provided. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:ResponseOption">
         <sch:assert test="@returnType='LeafClassWithRepositoryItem'"
                     id="R-EDM-REQ-C024"
                     role="FATAL">The 'returnType' attribute of 'ResponseOption' MUST be the fixed value "LeafClassWithRepositoryItem".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query">
         <sch:assert test="@queryDefinition='DocumentQuery'"
                     id="R-EDM-REQ-C025"
                     role="FATAL">The 'queryDefinition' attribute of 'Query' MUST be the fixed value "DocumentQuery".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     id="R-EDM-REQ-C026"
                     role="FATAL"> The value of 'Identifier' of an 'DataServiceEvidenceType' MUST be unique UUID (RFC 4122) retrieved from the Data Service Directory.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification">
         <!--{}[](EEA_CountryCodeList)-->
         <sch:assert test="matches(., '^https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(oots|(' || string-join($EEA_COUNTRY-CODELIST_code, '|') || '))/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$')"
                     id="R-EDM-REQ-C027"
                     role="FATAL">The value of 'EvidenceTypeClassification' of a 'DataServiceEvidenceType' MUST be a UUID retrieved from the Evidence Broker and include a code of the code list 'EEA_Country-CodeList' (ISO 3166-1' alpha-2 codes subset of EEA countries) using the prefix and scheme ''https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/[EEA_Country-Codelist]/[UUID]'' pointing to the Semantic Repository. For testing purposes and agreed OOTS data models the code "oots" can be used. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title">
         <sch:assert test="not(normalize-space(@lang)='')"
                     id="R-EDM-REQ-C029"
                     role="FATAL">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description">
         <sch:assert test="not(normalize-space(@lang)='')"
                     id="R-EDM-REQ-C031"
                     role="FATAL">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType">
         <sch:assert test="count(sdg:DistributedAs)=1 or count(sdg:DistributedAs)=0"
                     id="R-EDM-REQ-C032"
                     role="FATAL">The Element 'DistributedAs' must occur not more than once (maxOccurs="1)' in the EvidenceRequest. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo">
         <sch:assert test="matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')"
                     id="R-EDM-REQ-C034"
                     role="FATAL">The value of 'ConformsTo' of the requested distribution MUST be a persistent URL with a link to a "DataModelScheme" of the Evidence Type retrieved by the DSD and described in the Semantic Repository which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]". </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation">
         <sch:assert test="matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')"
                     id="R-EDM-REQ-C035"
                     role="FATAL">The value of 'Transformation' of the requested distribution MUST be a persistent URL with link to a "DataModelScheme" and "Subset" of the EvidenceType retrieved from the Data Service Directory, described in the Semantic Repository which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]/[Subset]".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person">
         <sch:assert test="not(normalize-space(sdg:LevelOfAssurance)='')"
                     id="R-EDM-REQ-C036"
                     role="FATAL">The Element 'LevelOfAssurance' must be provided  (minOccurs="1)' in the EvidenceRequest when rim:Slot[@name='NaturalPerson'] is used. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person">
         <!--Role is WARNING so stripped the following assertion:
             The value of a Person 'Identifier' SHOULD be provided.-->         
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier">
         <!--{}[](EEA_CountryCodeList)-->
         <sch:assert test="matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i')"
                     id="R-EDM-REQ-C040"
                     role="FATAL">The value of Person 'Identifier' MUST have the format XX/YY/Z...Z where the values of XX and YY MUST be part of the code list 'EEA_CountryCodeList' 
            (ISO 3166-1' alpha-2 codes subset of EEA countries ) and Z...Z is an undefined combination of up to 256 characters which uniquely identifies the 
            identity asserted in the country of origin. Example: ES/AT/02635542Y  </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier">
         <sch:assert test="@schemeID" id="R-EDM-REQ-C041" role="FATAL">The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Identifier">
         <sch:assert test="@schemeID='eidas'" id="R-EDM-REQ-C042" role="FATAL">The 'schemeID' attribute of the 'Identifier' MUST have the fixed value 'eidas'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:DateOfBirth">
         <sch:assert test="matches(normalize-space(text()),'^[0-9]{4}-[0-9]{2}-[0-9]{2}$','i')"
                     id="R-EDM-REQ-C043"
                     role="FATAL">The value of 'DateOfBirth' MUST use the following format YYYY + “-“ + MM + “-“ + DD (as defined for xsd:date) </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson">
         <sch:assert test="not(normalize-space(sdg:LevelOfAssurance)='')"
                     id="R-EDM-REQ-C047"
                     role="FATAL">The Element 'LevelOfAssurance' must be provided  (minOccurs="1)' in the EvidenceRequest when rim:Slot[@name='LegalPerson'] is used. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson">
         <sch:assert test="not(normalize-space(sdg:LegalPersonIdentifier)='')"
                     id="R-EDM-REQ-C049"
                     role="FATAL">The value of a Legal Person 'LegalPersonIdentifier' MUST be provided. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson">
         <sch:assert test="not(normalize-space(sdg:LegalPersonIdentifier)='')"
                     id="R-EDM-REQ-C084"
                     role="FATAL">The value of a Legal Person 'LegalPersonIdentifier' MUST be provided. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier">
         <!--{}[](EEA_CountryCodeList)-->
         <sch:assert test="matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i')"
                     id="R-EDM-REQ-C051"
                     role="FATAL">The value of a 'LegalPersonIdentifier' MUST have the format XX/YY/Z…Z where the values of XX and YY MUST be part of the code list 'EEA_Country-CodeList' 
            (ISO 3166-1' alpha-2 codes subset of EEA countries) and Z...Z is an undefined combination of up to 256 characters which uniquely identifies the identity asserted in the 
            country of origin. Example: ES/AT/02635542Y   </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier">
         <!--{}[](EEA_CountryCodeList)-->
         <sch:assert test="matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i')"
                     id="R-EDM-REQ-C085"
                     role="FATAL">The value of a 'LegalPersonIdentifier' MUST have the format XX/YY/Z…Z where the values of XX and YY MUST be part of the code list 'EEA_Country-CodeList'
            (ISO 3166-1' alpha-2 codes subset of EEA countries) and Z...Z is an undefined combination of up to 256 characters which uniquely identifies the identity asserted in the
            country of origin. Example: ES/AT/02635542Y   </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier">
         <sch:assert test="@schemeID" id="R-EDM-REQ-C052" role="FATAL">The 'schemeID' attribute of 'LegalPersonIdentifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier">
         <sch:assert test="@schemeID" id="R-EDM-REQ-C086" role="FATAL">The 'schemeID' attribute of 'LegalPersonIdentifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier">
         <sch:assert test="@schemeID='eidas'" id="R-EDM-REQ-C053" role="FATAL">The 'schemeID' attribute of the 'LegalPersonIdentifier' MUST have the fixed value 'eidas'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LegalPersonIdentifier">
         <sch:assert test="@schemeID='eidas'" id="R-EDM-REQ-C087" role="FATAL">The 'schemeID' attribute of the 'LegalPersonIdentifier' MUST have the fixed value 'eidas'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier">
         <sch:assert test="@schemeID" id="R-EDM-REQ-C054" role="FATAL">The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier">
         <sch:assert test="@schemeID" id="R-EDM-REQ-C088" role="FATAL">The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person">
         <sch:assert test="not(normalize-space(sdg:LevelOfAssurance)='')"
                     id="R-EDM-REQ-C058"
                     role="FATAL">The Element 'LevelOfAssurance' must be provided (minOccurs="1)' in the EvidenceRequest when rim:Slot[@name='AuthorizedRepresentative'] is used.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person">
         <!--Role is WARNING so stripped the following assertion:
             The value of a Person 'Identifier' SHOULD be provided. -->         
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier">
         <!--{}[](EEA_CountryCodeList)-->
         <sch:assert test="matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i')"
                     id="R-EDM-REQ-C061"
                     role="FATAL">The value of a Person 'Identifier' MUST have the format XX/YY/Z...Z where XX and YY MUST be part of the code list 'EEA_Country-CodeList' (ISO 3166-1' alpha-2 codes subset 
            of EEA countries) and Z...Z is an undefined combination of up to 256 characters which uniquely identifies the identity asserted in the country of origin. Example: ES/AT/02635542Y </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier">
         <sch:assert test="@schemeID" id="R-EDM-REQ-C062" role="FATAL">The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Identifier">
         <sch:assert test="@schemeID='eidas'" id="R-EDM-REQ-C063" role="FATAL">The 'schemeID' attribute of the 'Identifier' MUST have the fixed value 'eidas'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:DateOfBirth">
         <sch:assert test="matches(normalize-space(text()),'^[0-9]{4}-[0-9]{2}-[0-9]{2}$','i')"
                     id="R-EDM-REQ-C064"
                     role="FATAL">The value of 'DateOfBirth' MUST use the following format YYYY + “-“ + MM + “-“ + DD (as defined for xsd:date)  </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs">
         <sch:let name="ct1" value="count(sdg:ConformsTo)"/>
         <sch:let name="isxml" value="matches(sdg:Format,'xml')"/>
         <sch:let name="isjson" value="matches(sdg:Format, 'json')"/>
         <!--Role is WARNING so stripped the following assertion:
             The value of 'sdg:ConformsTo' of the distribution SHOULD be present if the 'sdg:DistributedAs/sdg:Format' uses the codes 'application/xml' of the codelist 'OOTSMediaTypes'.-->
         <!--Role is WARNING so stripped the following assertion:
             The value of 'sdg:ConformsTo' of the distribution SHOULD be present if the 'sdg:DistributedAs/sdg:Format' uses the codes 'application/json' of the codelist 'OOTSMediaTypes'.-->
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation">
         <sch:assert test="count(../sdg:ConformsTo)=1" id="R-EDM-REQ-C072" role="FATAL">The value of  'sdg:ConformsTo'  of the distribution MUST be present if the Element 'sdg:Transformation' of the distribution is present. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification/text() = 'ER']/sdg:Address">
         <sch:assert test="count(sdg:AdminUnitLevel1)=1"
                     id="R-EDM-REQ-C073"
                     role="FATAL">A value for 'AdminUnitLevel1' MUST be provided if the sdg:Agent/sdg:Classification value is "ER".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue">
         <sch:assert test="count(rim:Element/sdg:Agent[sdg:Classification='ER'])=1"
                     id="R-EDM-REQ-C074"
                     role="FATAL">The EvidenceRequester slot MUST include one Agent with the classification value ER.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson">
         <sch:assert test="not(normalize-space(sdg:LevelOfAssurance)='')"
                     id="R-EDM-REQ-C082"
                     role="FATAL">The Element 'LevelOfAssurance' must be provided  (minOccurs="1)' in the EvidenceRequest when rim:Slot[@name='AuthorizedRepresentativeLegalPerson'] is used.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Classification">
         <!--{}[](AgentClassification-CodeList)-->
         <sch:assert test="some $code in $AGENTCLASSIFICATION-CODELIST_code[. != 'EP' and . != 'ERRP'] satisfies .=$code"
                     id="R-EDM-REQ-C014"
                     role="FATAL">The value MUST be part of the code list 'AgentClassification' and shall be one of the codes ER (Evidence Requester) or IP(Intermediary Platform). The codes 'EP' and 'ERRP' shall not be used by this transaction. </sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance">
         <!--{}[](LevelsOfAssurance-CodeList)-->
         <sch:assert test="some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C037"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LevelsOfAssurance-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance' <sch:value-of select="."/>
         </sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance">
         <!--{}[](LevelsOfAssurance-CodeList)-->
         <sch:assert test="some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C048"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LevelsOfAssurance-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance">
         <!--{}[](LevelsOfAssurance-CodeList)-->
         <sch:assert test="some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C083"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LevelsOfAssurance-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:LevelOfAssurance'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance">
         <!--{}[](LevelsOfAssurance-CodeList)-->
         <sch:assert test="some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C059"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LevelsOfAssurance-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:LevelOfAssurance'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@value">
         <!--{}[](Procedures-CodeList)-->
         <sch:assert test="some $code in $PROCEDURES-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C003"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'Procedures-CodeList' in the context 'query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@value'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format">
         <!--{}[](OOTSMediaTypes-CodeList)-->
         <sch:assert test="some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C033"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'OOTSMediaTypes-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Agent/sdg:Identifier">
         <!--{}[](EAS-CodeList, EEACountry-CodeList)-->
         <sch:let name="suffix"
                  value="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
         <sch:let name="suffix1"
                  value="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
         <sch:assert test="((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256"
                     id="R-EDM-REQ-C018"
                     role="FATAL">The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country_CodeList’. For testing purposes the code "oots" can be used.</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier">
         <!--{}[](EAS-CodeList, EEACountry-CodeList)-->
         <sch:let name="suffix"
                  value="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
         <sch:let name="suffix1"
                  value="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
         <sch:assert test="((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256"
                     id="R-EDM-REQ-C012"
                     role="FATAL">The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country-CodeList’. For testing purposes the code "oots" can be used.</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='Requirements']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Name/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C009"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryRequest/rim:Slot[@name='Requirement']/rim:SlotValue/rim:Element/sdg:Requirement/sdg:Name/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C021"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryRequest/rim:Slot[@name='EvidenceProviderClassification']/rim:SlotValue/rim:Element/sdg:EvidenceProviderClassification/sdg:Description/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C028"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@xml:lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C004"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryRequest/rim:Slot[@name='Procedure']/rim:SlotValue/rim:Value/rim:LocalizedString/@xml:lang'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C030"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='EvidenceRequest']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/@xml:lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C069"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryRequest/@xml:lang'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C015"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C045"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier/@schemeID">
         <!--{}[](IdentifierSchemes)-->
         <sch:assert test="some $code in $IDENTIFIERSCHEMES-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C055"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:RegisteredAddress/sdg:AdminUnitLevel1">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C056"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='LegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:RegisteredAddress/sdg:AdminUnitLevel1'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C067"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CurrentAddress/sdg:AdminUnitLevel1'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:CurrentAddress/sdg:AdminUnitLevel1">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C090"
                     role="FATAL">The value of the 'AdminUnitLevel1' MUST be part of the code list the code list 'CountryIdentificationCode' (ISO 3166-1' alpha-2 codes).</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Nationality">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C075"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:Nationality'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C076"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C077"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='NaturalPerson']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Nationality">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C078"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:Nationality'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C079"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfBirth'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C080"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:CountryOfResidence'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:SectorSpecificAttribute/sdg:AttributeValue">
         <!--{}[](Procedures-CodeList)-->
         <sch:assert test="some $code in $PROCEDURES-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C081"
                     role="FATAL">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'Procedures-CodeList' in the context 'query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentative']/rim:SlotValue/sdg:Person/sdg:SectorSpecificAttribute/sdg:AttributeValue'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:Identifier/@schemeID">
         <!--{}[](IdentifierSchemes)-->
         <sch:assert test="some $code in $IDENTIFIERSCHEMES-CODELIST_code satisfies .=$code"
                     id="R-EDM-REQ-C089"
                     role="FATAL">The 'schemeID' attribute of the 'Identifier' MUST have be part of the code list 'IdentifierSchemes' (eIDAS Legal Person Identifier Schemes).</sch:assert>
      </sch:rule>
      <sch:let name="attrval"
               value="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:SectorSpecificAttribute/sdg:AttributeValue"/>
      <sch:rule context="query:QueryRequest/query:Query/rim:Slot[@name='AuthorizedRepresentativeLegalPerson']/rim:SlotValue/sdg:LegalPerson/sdg:SectorSpecificAttribute/sdg:AttributeValue">
         <!--{}[](Procedures-CodeList)-->
         <sch:assert test="every $val in tokenize($attrval, ',\s*') satisfies (some $code in $PROCEDURES-CODELIST_code satisfies ( $val=$code))"
                     id="R-EDM-REQ-C091"
                     role="FATAL">The 'AttributeValue' of a 'SectorSpecificAttribute' with the URI 'http://data.europa.eu/p4s/attributes/PowerOfRepresentationScope' MUST be one or more comma-separated values a value of the code list 'Procedures' <sch:value-of select="tokenize(replace($attrval, ',', ' '), ' ')"/>
         </sch:assert>
      </sch:rule>
   </sch:pattern>
</sch:schema>
