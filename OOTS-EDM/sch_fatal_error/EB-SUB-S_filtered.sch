<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0" prefix="lcm"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EB-SUB-S</sr:identifier>
            <sr:title xml:lang="en">EB-SUB-S</sr:title>
            <sr:description xml:lang="en">Submit Registry Objects EvidenceType, EvidenceTypeList, Requirement and ReferenceFramework (Structure)</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>STRUCTURE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EB-SUB</sr:identifier>
                    <sr:title xml:lang="en">EB-SUB</sr:title>
                    <sr:description xml:lang="en">LCM Submit Objects to EB </sr:description>
                    <sr:type>LCM</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>EB</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
            </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:pattern>
      <sch:rule context="/node()">
         <sch:let name="ln" value="local-name(/node())"/>
         <sch:assert test="$ln ='SubmitObjectsRequest'" role="FATAL" id="R-EB-SUB-S001">The root element of a query response document MUST be 'lcm:SubmitObjectsRequest'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="/node()">
         <sch:let name="ns" value="namespace-uri(/node())"/>
         <sch:assert test="$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0'"
                     role="FATAL"
                     id="R-EB-SUB-S002">The namespace of root element of a 'lcm:SubmitObjectsRequest' must be 'urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="matches(normalize-space(@id),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-EB-SUB-S004">The 'id' attribute of a 'SubmitObjectsRequest' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="count(rim:RegistryObjectList)&gt;0"
                     role="FATAL"
                     id="R-EB-SUB-S005">A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObjectList'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject">
         <sch:let name="idattr" value="@id"/>
         <sch:assert test="string-length($idattr)&gt;0" role="FATAL" id="R-EB-SUB-S006">Each 'rim:RegistryObject' MUST include an 'id' attribute</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject">
         <sch:let name="classification" value="boolean(rim:Classification)"/>
         <sch:let name="association" value="boolean(@xsi:type='rim:AssociationType')"/>
         <sch:assert test="$classification or $association "
                     role="FATAL"
                     id="R-EB-SUB-S007">Each 'rim:RegistryObject' MUST include a 'rim:Classification' if the 'rim:RegistryObject' is not an 'xsi:type="rim:AssociationType"'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject/rim:Classification">
         <sch:let name="idattr" value="@id"/>
         <sch:let name="schemeattr" value="@classificationScheme"/>
         <sch:let name="nodeattr" value="@classificationNode"/>
         <sch:assert test="string-length($idattr)&gt;0" role="FATAL" id="R-EB-SUB-S008">Each 'rim:Classification' MUST include an 'id' attribute</sch:assert>
         <sch:assert test="matches(normalize-space(@id),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-EB-SUB-S009">Each id of 'rim:Classification' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:".</sch:assert>
         <sch:assert test="string-length($schemeattr)&gt;0" role="FATAL" id="R-EB-SUB-S010">Each 'rim:Classification' MUST include an 'classificationScheme' attribute</sch:assert>
         <sch:assert test="string-length($nodeattr)&gt;0" role="FATAL" id="R-EB-SUB-S011">Each 'rim:Classification' MUST include an 'classificationNode' attribute</sch:assert>
         <sch:assert test="$schemeattr = 'urn:fdc:oots:classification:eb'"
                     role="FATAL"
                     id="R-EB-SUB-S012"> The 'classificationScheme' attribute MUST be 'urn:fdc:oots:classification:eb'</sch:assert>
         <sch:assert test="$nodeattr = 'EvidenceType' or $nodeattr = 'EvidenceTypeList' or $nodeattr = 'Requirement' or $nodeattr = 'ReferenceFramework'"
                     role="FATAL"
                     id="R-EB-SUB-S013"> The 'classificationNode' attribute MUST be 'EvidenceType', 'EvidenceTypeList', 'Requirement' or 'ReferenceFramework'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']">
         <sch:let name="nodeattr" value="../rim:Classification/@classificationNode"/>
         <sch:assert test="$nodeattr = 'EvidenceType'" role="FATAL" id="R-EB-SUB-S014">A 'rim:RegistryObject' with the classificationNode 'EvidenceType' MUST include a  rim:Slot name="EvidenceType" and no other</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceTypeList']">
         <sch:let name="nodeattr" value="../rim:Classification/@classificationNode"/>
         <sch:assert test="$nodeattr = 'EvidenceTypeList'"
                     role="FATAL"
                     id="R-EB-SUB-S015">A 'rim:RegistryObject' with the classificationNode 'EvidenceTypeList' MUST include a  rim:Slot name=EvidenceTypeList and no other</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']">
         <sch:let name="nodeattr" value="../rim:Classification/@classificationNode"/>
         <sch:assert test="$nodeattr = 'Requirement'" role="FATAL" id="R-EB-SUB-S016">A 'rim:RegistryObject' with the classificationNode 'Requirement' MUST include a  rim:Slot name=Requirement and no other</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']">
         <sch:let name="nodeattr" value="../rim:Classification/@classificationNode"/>
         <sch:assert test="$nodeattr = 'ReferenceFramework'"
                     role="FATAL"
                     id="R-EB-SUB-S017">A 'rim:RegistryObject' with the classificationNode 'ReferenceFramework' MUST include a  rim:Slot name=ReferenceFramework and no other</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='AnyValueType'" role="FATAL" id="R-EB-SUB-S018">The rim:SlotValue of rim:Slot name="EvidenceType" MUST be of "rim:AnyValueType"</sch:assert>
         <sch:assert test="sdg:EvidenceType" role="FATAL" id="R-EB-SUB-S022">A 'rim:Slot[@name='EvidenceType'/rim:SlotValue' MUST contain one sdg:EvidenceType of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceTypeList']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='AnyValueType'" role="FATAL" id="R-EB-SUB-S019">The rim:SlotValue of rim:Slot name="EvidenceTypeList" MUST be of "rim:AnyValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='AnyValueType'" role="FATAL" id="R-EB-SUB-S020">The rim:SlotValue of rim:Slot name="Requirement" MUST be of "rim:AnyValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='AnyValueType'" role="FATAL" id="R-EB-SUB-S021">The rim:SlotValue of rim:Slot name="ReferenceFramework" MUST be of "rim:AnyValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceTypeList']/rim:SlotValue">
         <sch:assert test="sdg:EvidenceTypeList" role="FATAL" id="R-EB-SUB-S023">A 'rim:Slot[@name='EvidenceTypeList'/rim:SlotValue' MUST contain one sdg:EvidenceTypeList of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue">
         <sch:assert test="sdg:Requirement" role="FATAL" id="R-EB-SUB-S024">A 'rim:Slot[@name='Requirement'/rim:SlotValue' MUST contain one sdg:Requirement of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue">
         <sch:assert test="sdg:ReferenceFramework" role="FATAL" id="R-EB-SUB-S025">A 'rim:Slot[@name='ReferenceFramework'/rim:SlotValue' MUST contain one sdg:ReferenceFramwork of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType">
         <sch:assert test="count(sdg:Distribution)=0" role="FATAL" id="R-EB-SUB-S026">A 'rim:slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType' MUST not contain a 'sdg:Distribution'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceTypeList']/rim:SlotValue/sdg:EvidenceTypeList">
         <sch:assert test="count(sdg:EvidenceType)=0" role="FATAL" id="R-EB-SUB-S027">A 'rim:slot[@name='EvidenceTypeList']/rim:SlotValue/sdg:EvidenceTypeList' MUST not contain a 'sdg:EvidenceType'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement">
         <sch:assert test="count(sdg:EvidenceTypeList)=0 and count(sdg:ReferenceFramework)=0"
                     role="FATAL"
                     id="R-EB-SUB-S028">A 'rim:slot[@name='Requirement']/rim:SlotValue/sdg:Requirement' MUST not contain a 'sdg:EvidenceTypeList' and a 'sdg:ReferenceFramework'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:RelatedTo">
         <sch:assert test="sdg:Identifier" role="FATAL" id="R-EB-SUB-S029">The xs:element name="RelatedTo" type="sdg:ReferenceFrameworkType"/ MUST only contain the 'sdg:Identifier'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence']">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:assert test="//rim:RegistryObject[@id=$sourceObject]/rim:Classification[@classificationNode='EvidenceTypeList']"
                     role="FATAL"
                     id="R-EB-SUB-S036">Source object of ContainsEvidence association must be classified as EvidenceTypeList <sch:value-of select="$sourceObject"/>
         </sch:assert>
         <sch:assert test="//rim:RegistryObject[@id=$targetObject]/rim:Classification[@classificationNode='EvidenceType']"
                     role="FATAL"
                     id="R-EB-SUB-S057">Target object of ContainsEvidence association must be classified as EvidenceType <sch:value-of select="$targetObject"/>
         </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@sourceObject and @targetObject]">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:let name="type" value="@type"/>
         <sch:let name="etl_count"
                  value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceTypeList']])"/>
         <sch:let name="et_count"
                  value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='EvidenceType']])"/>
         <sch:assert test="number($etl_count) + number($et_count) != 2 or $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence'"
                     role="FATAL"
                     id="R-EB-SUB-S037">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' linking 'rim:RegistryObject' with the classificationNode 'EvidenceTypeList' 
                (@sourceObject) to 'rim:RegistryObject' with the classificationNode 'EvidenceType' (@targetObject) MUST use the 
                type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement']">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:assert test="//rim:RegistryObject[@id=$sourceObject]/rim:Classification[@classificationNode='EvidenceTypeList']"
                     role="FATAL"
                     id="R-EB-SUB-S038">Source object of FulfillsRequirement association must be classified as EvidenceTypeList <sch:value-of select="$sourceObject"/>
         </sch:assert>
         <sch:assert test="//rim:RegistryObject[@id=$targetObject]/rim:Classification[@classificationNode='Requirement']"
                     role="FATAL"
                     id="R-EB-SUB-S058">Target object of FulfillsRequirement association must be classified as Requirement <sch:value-of select="$targetObject"/>
         </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@sourceObject and @targetObject]">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:let name="type" value="@type"/>
         <sch:let name="etl_count"
                  value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceTypeList']])"/>
         <sch:let name="et_count"
                  value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='Requirement']])"/>
         <sch:assert test="number($etl_count) + number($et_count) != 2 or $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement'"
                     role="FATAL"
                     id="R-EB-SUB-S039">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' linking 'rim:RegistryObject' with the classificationNode 'EvidenceTypeList' 
                (@sourceObject) to 'rim:RegistryObject' with the classificationNode 'Requirement' (@targetObject) or to a unique UUID (RFC 4122) starting with prefix
                "urn:uuid:" (@targetObject) MUST use the type="urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework']">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:assert test="//rim:RegistryObject[@id=$sourceObject]/rim:Classification[@classificationNode='Requirement']"
                     role="FATAL"
                     id="R-EB-SUB-S040">Source object of DerivesFromReferenceFramework association must be classified as Requirement <sch:value-of select="$sourceObject"/>
         </sch:assert>
         <sch:assert test="//rim:RegistryObject[@id=$targetObject]/rim:Classification[@classificationNode='ReferenceFramework']"
                     role="FATAL"
                     id="R-EB-SUB-S059">Target object of DerivesFromReferenceFramework association must be classified as ReferenceFramework <sch:value-of select="$targetObject"/>
         </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@sourceObject and @targetObject]">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:let name="type" value="@type"/>
         <sch:let name="etl_count"
                  value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='Requirement']])"/>
         <sch:let name="et_count"
                  value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='ReferenceFramework']])"/>
         <sch:assert test="number($etl_count) + number($et_count) != 2 or $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework'"
                     role="FATAL"
                     id="R-EB-SUB-S041">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' linking a unique UUID (RFC 4122) starting with prefix "urn:uuid:"  
                (@sourceObject) or a 'rim:RegistryObject' with the classificationNode 'Requirement' (@sourceObject) to 'rim:RegistryObject' with the classificationNode 
                'ReferenceFramework' (@targetObject) MUST use the type="urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <!--

    -->
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="not(rim:Slot[@name != 'SpecificationIdentifier'])"
                     role="FATAL"
                     id="R-EB-SUB-S042">A 'lcm:SubmitObjectsRequest' MUST not contain any other rim:Slots than 'SpecificationIdentifier'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/@id">
         <sch:assert test="matches(normalize-space((.)),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-EB-SUB-S043">
                    Each id of 'rim:RegistryObject' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:".
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="count(rim:RegistryObject/rim:Slot[@name='EvidenceType'])&gt;0"
                     role="FATAL"
                     id="R-EB-SUB-S044">A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="EvidenceType". 
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="count(rim:RegistryObject/rim:Slot[@name='EvidenceTypeList'])&gt;0"
                     role="FATAL"
                     id="R-EB-SUB-S045">A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="EvidenceTypeList".
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="count(rim:RegistryObject[@xsi:type='rim:AssociationType'])&gt;0"
                     role="FATAL"
                     id="R-EB-SUB-S047">
                    A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType" 
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="count(rim:RegistryObject/rim:Slot[@name='ReferenceFramework'])&gt;0"
                     role="FATAL"
                     id="R-EB-SUB-S048">A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="ReferenceFramework".
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot/rim:SlotValue/sdg:ReferenceFramework">
         <sch:assert test="sdg:RelatedTo" role="FATAL" id="R-EB-SUB-S049">
                    The xs:element name="RelatedTo" type="sdg:ReferenceFrameworkType" MUST be present.
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot/rim:SlotValue/sdg:ReferenceFramework">
         <sch:assert test="sdg:Jurisdiction" role="FATAL" id="R-EB-SUB-S050">
                    The xs:element name="sdg:Jurisdiction" type="sdg:JurisdictionType" MUST be present.
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework']">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:assert test="//rim:RegistryObject[@id=$sourceObject]/rim:Classification[@classificationNode='Requirement']"
                     role="FATAL"
                     id="R-EB-SUB-S051">A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework" MUST have a @sourceObject 
                     that uses same id of the 'rim:RegistryObject' 
                    (starting with prefix "urn:uuid:") with the classificationNode 'Requirement' </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework']">
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:assert test="//rim:RegistryObject[@id=$targetObject]/rim:Classification[@classificationNode='ReferenceFramework']"
                     role="FATAL"
                     id="R-EB-SUB-S052">A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework" MUST have a 
                    @targetObject that uses same UUID (RFC 4122) of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") with the classificationNode 
                    'ReferenceFramework' .
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement']">
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:assert test="//rim:RegistryObject[@id=$targetObject]/rim:Classification[@classificationNode='Requirement']"
                     role="FATAL"
                     id="R-EB-SUB-S053">A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement" MUST have a @targetObject
                    that uses a unique UUID (RFC 4122) (starting with prefix "urn:uuid:") or that uses same id of the 'rim:RegistryObject' 
                    (starting with prefix "urn:uuid:") with the classificationNode 'Requirement' .
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence']">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:assert test="//rim:RegistryObject[@id=$sourceObject]/rim:Classification[@classificationNode='EvidenceTypeList']"
                     role="FATAL"
                     id="R-EB-SUB-S054">A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence" MUST have a @sourceObject that 
                    uses the same id of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") with the classificationNode 'EvidenceTypeList'. 
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence']">
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:assert test="//rim:RegistryObject[@id=$targetObject]/rim:Classification[@classificationNode='EvidenceType']"
                     role="FATAL"
                     id="R-EB-SUB-S055">A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence" MUST have a @targetObject 
                    that uses the same id of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") with the classificationNode 'EvidenceType'. 
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement']">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:assert test="//rim:RegistryObject[@id=$sourceObject]/rim:Classification[@classificationNode='EvidenceTypeList']"
                     role="FATAL"
                     id="R-EB-SUB-S056">A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement" MUST have a @sourceObject that 
                    uses the same id of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") with the classificationNode 'EvidenceTypeList'. 
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="count(rim:RegistryObject)&gt;0" role="FATAL" id="R-EB-SUB-S060">
                    A 'SubmitObjectsRequest' MUST include minimum a 'rim:RegistryObject' 
                </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="rim:Slot[@name='SpecificationIdentifier']"
                     role="FATAL"
                     id="R-EB-SUB-S070">The rim:Slot name="SpecificationIdentifier" MUST be present in the SubmitObjectsRequest.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='StringValueType'" role="FATAL" id="R-EB-SUB-S062">The rim:SlotValue of rim:Slot name="SpecificationIdentifier" MUST be of "rim:StringValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="@checkReferences" role="FATAL" id="R-EB-SUB-S063">The 'checkReferences' attribute of a 'SubmitObjectsRequest' MUST be present. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="@checkReferences='true'" role="FATAL" id="R-EB-SUB-S064">The 'checkReferences' attribute of a 'SubmitObjectsRequest' MUST be set on "true".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject">
         <sch:assert test="not(rim:Slot[not(@name=('EvidenceType', 'EvidenceTypeList', 'Requirement', 'ReferenceFramework'))])"
                     role="FATAL"
                     id="R-EB-SUB-S065">A 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/' MUST not contain any other rim:Slots than 'EvidenceType', 'EvidenceTypeList', 'Requirement' or 'ReferenceFramework'. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject[@xsi:type='rim:AssociationType']">
         <sch:assert test="not(rim:Slot)" role="FATAL" id="R-EB-SUB-S066">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType" MUST not include any slots</sch:assert>
      </sch:rule>
   </sch:pattern>
   <!--
Some generic RIM patterns
-->
   <sch:pattern>
      <sch:rule context="rim:Slot">
         <sch:assert test="count(child::rim:SlotValue) = 1"
                     id="R-EB-SUB-S067"
                     role="FATAL">A rim:Slot MUST have a rim:SlotValue child element</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:SlotValue">
         <sch:assert test="count(child::*) &gt; 0" id="R-EB-SUB-S068" role="FATAL">A rim:SlotValue MUST have child element content</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/@type">
         <sch:let name="associationTypePrefix"
                  value="'urn:oasis:names:tc:ebxml-regrep:AssociationType:'"/>
         <sch:assert test=". = concat($associationTypePrefix, 'ContainsEvidence') or . = concat($associationTypePrefix, 'FulfillsRequirement') or . = concat($associationTypePrefix, 'DerivesFromReferenceFramework')"
                     role="FATAL"
                     id="R-EB-SUB-S069">        
                The value of @xsi:type of AssociationType must be 'urn:oasis:names:tc:ebxml-regrep:AssociationType:ContainsEvidence' or  'urn:oasis:names:tc:ebxml-regrep:AssociationType:FulfillsRequirement' or 'urn:oasis:names:tc:ebxml-regrep:AssociationType:DerivesFromReferenceFramework'</sch:assert>
      </sch:rule>
   </sch:pattern>
</sch:schema>
