<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0" prefix="lcm"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <sch:ns prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EB-SUB-C</sr:identifier>
            <sr:title xml:lang="en">EB-SUB-C</sr:title>
            <sr:description xml:lang="en">Submit Registry Objects EvidenceType, EvidenceTypeList, Requirement and ReferenceFramework (Content)</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>CONTENT</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EB-SUB</sr:identifier>
                    <sr:title xml:lang="en">EB-SUB</sr:title>
                    <sr:description xml:lang="en">LCM Submit Objects to EB </sr:description>
                    <sr:type>LCM</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>EB</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
            </sr:Asset>?>
   <!--End of SR metadata-->
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/EEA_COUNTRY-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/LANGUAGECODE-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/PROCEDURES-CODELIST_code.sch"/>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType/sdg:EvidenceTypeClassification">
         <sch:assert test="matches(., '^https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(oots|(' || string-join($EEA_COUNTRY-CODELIST_code, '|') || '))/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$')"
                     role="FATAL"
                     id="R-EB-SUB-C001">The value of 'EvidenceTypeClassification' of a 'DataServiceEvidenceType' MUST be a UUID and include a code of the code list 'EEA_Country-CodeList' 
                (ISO 3166-1' alpha-2 codes, EEA subset of country codes) using the prefix and scheme ''https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/[EEA_CountryCode]/[UUID]'' 
                pointing to the Semantic Repository. For testing purposes and agreed OOTS data models the code "oots" can be used. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType/sdg:Title">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-EB-SUB-C003">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType/sdg:Description">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-EB-SUB-C005">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceTypeList']/rim:SlotValue/sdg:EvidenceTypeList/sdg:Identifier">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-EB-SUB-C009">The identifier of 'EvidenceTypeList' MUST be unique UUID (RFC 4122).</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceTypeList']/rim:SlotValue/sdg:EvidenceTypeList/sdg:Name">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-EB-SUB-C011">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Name">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-EB-SUB-C014">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Identifier">
         <sch:assert test="matches(normalize-space((.)),'https://sr.oots.tech.ec.europa.eu/requirements/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-EB-SUB-C012">The value of 'Identifier' of a 'Requirement' MUST be a unique UUID (RFC 4122) listed in the EvidenceBroker and use the prefix 
                'https://sr.oots.tech.ec.europa.eu/requirements/[UUID]' pointing to the Semantic Repository.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:Identifier">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-EB-SUB-C015">The 'Identifier' of a 'ReferenceFramework' MUST be unique UUID (RFC 4122).</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:Title">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-EB-SUB-C017">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:Description">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-EB-SUB-C019">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
         <sch:assert test="rim:Value='oots-cs:v1.1'" id="R-EB-SUB-C024" role="FATAL">The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-cs:v1.1".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:Jurisdiction">
            <!-- This assert checks if AdminUnitLevel3 is present -->
         <sch:assert test="not(sdg:AdminUnitLevel3) or (sdg:AdminUnitLevel1 and sdg:AdminUnitLevel2)"
                     role="FATAL"
                     id="R-EB-SUB-C025">
                If AdminUnitLevel 3 of the ReferenceFramework Jurisdiction is present, both AdminUnitLevel 1 and AdminUnitLevel 2 of the ReferenceFramework Jurisdiction must be present.
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:Jurisdiction">
            <!-- This assert checks if AdminUnitLevel2 is present -->
         <sch:assert test="not(sdg:AdminUnitLevel2) or sdg:AdminUnitLevel1"
                     role="FATAL"
                     id="R-EB-SUB-C026">
                If AdminUnitLevel 2 of the ReferenceFramework Jurisdiction is present, AdminUnitLevel 1 of the ReferenceFramework Jurisdiction must be present.
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType/sdg:Jurisdiction">
            <!-- This assert checks if AdminUnitLevel3 is present -->
         <sch:assert test="not(sdg:AdminUnitLevel3) or (sdg:AdminUnitLevel1 and sdg:AdminUnitLevel2)"
                     role="FATAL"
                     id="R-EB-SUB-C027">
                If AdminUnitLevel 3 of the EvidenceType Jurisdiction is present, both AdminUnitLevel 1 and AdminUnitLevel 2 of the EvidenceType Jurisdiction must be present.
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType/sdg:Jurisdiction">
            <!-- This assert checks if AdminUnitLevel2 is present -->
         <sch:assert test="not(sdg:AdminUnitLevel2) or sdg:AdminUnitLevel1"
                     role="FATAL"
                     id="R-EB-SUB-C028">
                If AdminUnitLevel 2 of the EvidenceType Jurisdiction is present, AdminUnitLevel 1 of the EvidenceType Jurisdiction must be present.
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType/sdg:Jurisdiction/sdg:AdminUnitLevel1">
            <!--{}[](EEA_Country-CodeList)-->
         <sch:assert test="some $code in $EEA_COUNTRY-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EB-SUB-C006">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'EEA_Country-CodeList' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType/sdg:Jurisdiction/sdg:AdminUnitLevel1'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:Jurisdiction/sdg:AdminUnitLevel1">
            <!--{}[](EEA_Country-CodeList)-->
         <sch:assert test="some $code in $EEA_COUNTRY-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EB-SUB-C020">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'EEA_Country-CodeList' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:Jurisdiction/sdg:AdminUnitLevel1'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType/sdg:Title/@lang">
            <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EB-SUB-C002">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType/sdg:Title/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType/sdg:Description/@lang">
            <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EB-SUB-C004">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceType']/rim:SlotValue/sdg:EvidenceType/sdg:Description/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceTypeList']/rim:SlotValue/sdg:EvidenceTypeList/sdg:Name/@lang">
            <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EB-SUB-C010">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceTypeList']/rim:SlotValue/sdg:EvidenceTypeList/sdg:Name/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Name/@lang">
            <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EB-SUB-C013">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='Requirement']/rim:SlotValue/sdg:Requirement/sdg:Name/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:Title/@lang">
            <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EB-SUB-C016">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:Title/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:Description/@lang">
            <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EB-SUB-C018">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:Description/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:RelatedTo/sdg:Identifier">
            <!--{}[](Procedures-CodeList)-->
         <sch:assert test="some $code in $PROCEDURES-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EB-SUB-C023">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'Procedures-CodeList' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='ReferenceFramework']/rim:SlotValue/sdg:ReferenceFramework/sdg:RelatedTo/sdg:Identifier'</sch:assert>
      </sch:rule>
   </sch:pattern>
</sch:schema>
