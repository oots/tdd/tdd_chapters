<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0" prefix="lcm"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/DSD-SUB-S</sr:identifier>
            <sr:title xml:lang="en">DSD-SUB-S</sr:title>
            <sr:description xml:lang="en">Submit Registry Objects DataServiceEvidenceType and EvidenceProvider (Structure)</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>STRUCTURE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-SUB</sr:identifier>
                    <sr:title xml:lang="en">DSD-SUB</sr:title>
                    <sr:description xml:lang="en">LCM Submit Objects to the DSD (Baseline)</sr:description>
                    <sr:type>LCM</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>DSD</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.0.2</sr:version>
            <sr:versionNotes xml:lang="en">Added Rules to Schematron (Structure) to prove if a rim:Slot has a rim:SlotValue child element and rim:SlotValue has any child element. Updating Business Rules Spreadsheet, including business rule to prove XSD elemnet restrictions on "IsAbout" Element of EvidenceResponse</sr:versionNotes>
            </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:pattern>
      <sch:rule context="/node()">
         <sch:let name="ln" value="local-name(/node())"/>
         <sch:assert test="$ln ='SubmitObjectsRequest'" role="FATAL" id="R-DSD-SUB-S001">The root element of a query response document MUST be 'lcm:SubmitObjectsRequest'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="/node()">
         <sch:let name="ns" value="namespace-uri(/node())"/>
         <sch:assert test="$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0'"
                     role="FATAL"
                     id="R-DSD-SUB-S002">The namespace of root element of a 'lcm:SubmitObjectsRequest' must be 'urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="@id" role="FATAL" id="R-DSD-SUB-S003">The 'id' attribute of a 'SubmitObjectsRequest' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="matches(normalize-space(@id),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-DSD-SUB-S004">The 'id' attribute of a 'SubmitObjectsRequest' MUST be unique UUID (RFC 4122).</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="count(rim:RegistryObjectList)&gt;0"
                     role="FATAL"
                     id="R-DSD-SUB-S005">A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObjectList'.</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="count(rim:RegistryObject)&gt;0" role="FATAL" id="R-DSD-SUB-S036">A 'SubmitObjectsRequest' MUST include minimum a 'rim:RegistryObject'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject">
         <sch:let name="idattr" value="@id"/>
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="string-length($idattr)&gt;0" role="FATAL" id="R-DSD-SUB-S006">Each 'rim:RegistryObject' MUST include an 'id' attribute</sch:assert>
         <sch:assert test="($st != 'AssociationType' and count(rim:Classification)&gt;0) or ($st = 'AssociationType' and count(rim:Classification)=0)"
                     role="FATAL"
                     id="R-DSD-SUB-S007">Each 'rim:RegistryObject' MUST include a 'rim:Classification' if the 'rim:RegistryObject' is not an 'xsi:type="rim:AssociationType"'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject/rim:Classification">
         <sch:let name="idattr" value="@id"/>
         <sch:let name="schemeattr" value="@classificationScheme"/>
         <sch:let name="nodeattr" value="@classificationNode"/>
         <sch:assert test="string-length($idattr)&gt;0" role="FATAL" id="R-DSD-SUB-S008">Each 'rim:Classification' MUST include an 'id' attribute</sch:assert>
         <sch:assert test="matches(normalize-space(@id),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-DSD-SUB-S009">Each id of 'rim:Classification' MUST be unique UUID (RFC 4122).</sch:assert>
         <sch:assert test="string-length($schemeattr)&gt;0"
                     role="FATAL"
                     id="R-DSD-SUB-S010">Each 'rim:Classification' MUST include an 'classificationScheme' attribute</sch:assert>
         <sch:assert test="string-length($nodeattr)&gt;0" role="FATAL" id="R-DSD-SUB-S011">Each 'rim:Classification' MUST include an 'classificationNode' attribute</sch:assert>
         <sch:assert test="$schemeattr = 'urn:fdc:oots:classification:dsd'"
                     role="FATAL"
                     id="R-DSD-SUB-S012"> The 'classificationScheme' attribute MUST be 'urn:fdc:oots:classification:dsd'</sch:assert>
         <sch:assert test="$nodeattr = 'EvidenceProvider' or $nodeattr = 'DataServiceEvidenceType'"
                     role="FATAL"
                     id="R-DSD-SUB-S013"> The 'classificationNode' attribute MUST be 'EvidenceProvider' or 'DataServiceEvidenceType'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']">
         <sch:let name="nodeattr" value="../rim:Classification/@classificationNode"/>
         <sch:assert test="$nodeattr = 'EvidenceProvider'"
                     role="FATAL"
                     id="R-DSD-SUB-S014">A 'rim:RegistryObject' with the classificationNode 'EvidenceProvider' MUST include a rim:Slot name="EvidenceProvider" and no other</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']">
         <sch:let name="nodeattr" value="../rim:Classification/@classificationNode"/>
         <sch:assert test="$nodeattr = 'DataServiceEvidenceType'"
                     role="FATAL"
                     id="R-DSD-SUB-S015">A 'rim:RegistryObject' with the classificationNode 'DataServiceEvidenceType' MUST include a  rim:Slot name="DataServiceEvidenceType" and no other</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='AnyValueType'" role="FATAL" id="R-DSD-SUB-S016">The rim:SlotValue of rim:Slot name="EvidenceProvider" MUST be of "rim:AnyValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='AnyValueType'" role="FATAL" id="R-DSD-SUB-S017">The rim:SlotValue of rim:Slot name="DataServiceEvidenceType" MUST be of "rim:AnyValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']">
         <sch:assert test="count(rim:SlotValue/sdg:AccessService)=1"
                     role="FATAL"
                     id="R-DSD-SUB-S018">A 'rim:Slot[@name='EvidenceProvider']/rim:SlotValue' MUST contain one sdg:AccessService of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']">
         <sch:assert test="count(rim:SlotValue/sdg:DataServiceEvidenceType)=1"
                     role="FATAL"
                     id="R-DSD-SUB-S019">A 'rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue' MUST contain one sdg:DataServiceEvidenceType of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue">
         <sch:assert test="count(sdg:AccessService)=0" role="FATAL" id="R-DSD-SUB-S020">A DataServiceEvidenceType 'rim:Slot[@name='DataServiceEvidenceType]/rim:SlotValue' MUST not contain an 'sdg:AccessService'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject">
         <sch:let name="idtype" value="@xsi:type"/>
         <sch:assert test="(count(rim:Classification) = 0 and matches(normalize-space($idtype),'AssociationType$','i')) or                  (count(rim:Classification) = 1 and not(matches(normalize-space($idtype),'AssociationType$','i')))"
                     role="FATAL"
                     id="R-DSD-SUB-S022">If a 'rim:RegistryObject' does not have a 'rim:Classification" it MUST have the attribute 'xsi:type=rim:AssociationType'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@sourceObject and @targetObject]">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:let name="etl_count"
                  value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceProvider']])"/>
         <sch:let name="et_count"
                  value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='DataServiceEvidenceType']])"/>
         <sch:assert test="$etl_count + $et_count = 2" role="FATAL" id="R-DSD-SUB-S023">Each 'rim:RegistryObject' with a classificationNode 'EvidenceProvider' or 'DataServiceEvidenceType' MUST have an association described in a
                the attribute 'xsi:type="rim:AssociationType"'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@xsi:type='rim:AssociationType']">
         <sch:assert test="@sourceObject" role="FATAL" id="R-DSD-SUB-S024">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' MUST have an attribute 'sourceObject'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@xsi:type='rim:AssociationType']">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:let name="registrySourceObject"
                  value="//rim:RegistryObject[@id = $sourceObject]"/>
         <sch:assert test="$registrySourceObject/rim:Classification[@classificationNode='EvidenceProvider']"
                     role="FATAL"
                     id="R-DSD-SUB-S025">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' MUST have an attribute 'sourceObject' that lists the id of the 
                'rim:RegistryObject' (starting with prefix "urn:uuid:") from classificationNode 'EvidenceProvider' </sch:assert>
         <sch:let name="registryTargetObject"
                  value="//rim:RegistryObject[@id = $targetObject]"/>
         <sch:assert test="$registryTargetObject/rim:Classification[@classificationNode='DataServiceEvidenceType']"
                     role="FATAL"
                     id="R-DSD-SUB-S027">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' MUST have an attribute 'targetObject' that lists the id of the 
                'rim:RegistryObject' (starting with prefix "urn:uuid:") with classificationNode 'DataServiceEvidenceType'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@xsi:type='rim:AssociationType']">
         <sch:assert test="@targetObject" role="FATAL" id="R-DSD-SUB-S026">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' MUST have an attribute 'targetObject'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ServesEvidenceType']">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="registrySourceObject"
                  value="//rim:RegistryObject[@id = $sourceObject]"/>
         <sch:assert test="$registrySourceObject/rim:Classification[@classificationNode='EvidenceProvider']"
                     role="FATAL"
                     id="R-DSD-SUB-S028">Source object of ServesEvidenceType association must be classified as EvidenceProvider <sch:value-of select="$sourceObject"/>
         </sch:assert>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:let name="registryTargetObject"
                  value="//rim:RegistryObject[@id = $targetObject]"/>
         <sch:assert test="$registryTargetObject/rim:Classification[@classificationNode='DataServiceEvidenceType']"
                     role="FATAL"
                     id="R-DSD-SUB-S037">Target object of ServesEvidenceType association must be classified as DataServiceEvidenceType <sch:value-of select="$targetObject"/>
         </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject[@sourceObject and @targetObject]">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:let name="type" value="@type"/>
         <sch:let name="etl_count"
                  value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceProvider']])"/>
         <sch:let name="et_count"
                  value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='DataServiceEvidenceType']])"/>
         <sch:assert test="number($etl_count) + number($et_count) != 2 or $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:ServesEvidenceType'"
                     role="FATAL"
                     id="R-DSD-SUB-S029">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' linking 'rim:RegistryObject' with the classificationNode 
                'EvidenceProvider' (@sourceObject) to 'rim:RegistryObject' with the classificationNode 'DataServiceEvidenceType' (@targetObject) MUST 
                use the type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ServesEvidenceType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="count(rim:Slot)=0" role="FATAL" id="R-DSD-SUB-S030">A 'lcm:SubmitObjectsRequest' MUST not contain any other rim:Slots.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification">
         <sch:let name="hasSupportedValue"
                  value="//rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:SupportedValue"/>
         <sch:assert test="not(empty($hasSupportedValue))"
                     role="FATAL"
                     id="R-DSD-SUB-S031">A value for 'sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:SupportedValue' MUST be provided if the  
                'sdg:DataServiceEvidenceType' which is associated to the 'sdg:AccessService' has the element 'sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification'.
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/@id">
         <sch:assert test="matches(normalize-space((.)),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-DSD-SUB-S032">Each id of 'rim:RegistryObject' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:". 
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="rim:RegistryObject/rim:Slot[@name='EvidenceProvider']"
                     role="FATAL"
                     id="R-DSD-SUB-S033">A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="EvidenceProvider"  
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']"
                     role="FATAL"
                     id="R-DSD-SUB-S034">
                A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="DataServiceEvidenceType"  
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="rim:RegistryObject[@xsi:type='rim:AssociationType']"
                     role="FATAL"
                     id="R-DSD-SUB-S035">
                A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService">
         <sch:assert test="count(sdg:Publisher)=1" role="FATAL" id="R-DSD-SUB-S038">The xs:element name="sdg:Publisher" type="sdg:EvidenceProviderType" MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="@checkReferences" role="FATAL" id="R-DSD-SUB-S039">The 'checkReferences' attribute of a 'SubmitObjectsRequest' MUST be present. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="@checkReferences='true'" role="FATAL" id="R-DSD-SUB-S040">The 'checkReferences' attribute of a 'SubmitObjectsRequest' MUST be set on "true".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject">
         <sch:assert test="not(rim:Slot[not(@name=('EvidenceProvider', 'DataServiceEvidenceType', 'AccessService'))])"
                     role="FATAL"
                     id="R-DSD-SUB-S041">A 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/' MUST not contain any other rim:Slots than 'EvidenceProvider', 'DataServiceEvidenceType' or 'AccessService'. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject[@xsi:type='rim:AssociationType']">
         <sch:assert test="not(rim:Slot)" role="FATAL" id="R-DSD-SUB-S042">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType" MUST not include any slots</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept">
         <sch:let name="accessSeviceID" value="sdg:Identifier"/>
         <sch:let name="dataServiceID"
                  value="//rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier"/>
         <sch:let name="sourceObjectID" value="ancestor-or-self::rim:RegistryObject/@id"/>
         <sch:let name="targetObjectID"
                  value="//rim:RegistryObject[@sourceObject=$sourceObjectID]/@targetObject"/>
         <sch:assert test="$accessSeviceID=$dataServiceID and not($targetObjectID='')"
                     role="FATAL"
                     id="R-DSD-SUB-S043">The value of 'sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Identifier' MUST match with one existing  'sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier'  
                provided by the  'sdg:DataServiceEvidenceType' which is associated to the 'EvidenceProvider' (sdg:AccessService).      
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction">
         <sch:let name="adminLevel2" value="boolean(sdg:AdminUnitLevel2)"/>
         <sch:let name="sourceObjectID" value="ancestor-or-self::rim:RegistryObject/@id"/>
         <sch:let name="targetObjectID"
                  value="//rim:RegistryObject[@sourceObject=$sourceObjectID]/@targetObject"/>
         <sch:let name="checkJurisdiction"
                  value="//rim:RegistryObject[@id = $targetObjectID]/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel"/>
         <sch:let name="checkNuts" value="contains($checkJurisdiction, 'NUTS')"/>
         <!--Role is WARNING so stripped the following assertion:
             The element 'sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel2'  SHOULD be provided if the  'sdg:DataServiceEvidenceType' which is associated to 
                the 'sdg:AccessService' has on of the values 'NUTS1', 'NUTS2' or 'NUTS3' in the element 'sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel'.
            -->
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Jurisdiction">
         <sch:let name="adminLevel3" value="boolean(sdg:AdminUnitLevel3)"/>
         <sch:let name="sourceObjectID" value="ancestor-or-self::rim:RegistryObject/@id"/>
         <sch:let name="targetObjectID"
                  value="//rim:RegistryObject[@sourceObject=$sourceObjectID]/@targetObject"/>
         <sch:let name="checkJurisdiction"
                  value="//rim:RegistryObject[@id = $targetObjectID]/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel"/>
         <sch:let name="checkLau" value="contains($checkJurisdiction, 'LAU')"/>
         <!--Role is WARNING so stripped the following assertion:
             The element 'sdg:AccessService/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel3'  SHOULD be provided if the 'sdg:DataServiceEvidenceType' which is associated to 
                the 'sdg:AccessService' has the value 'LAU' in the element 'sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel'.
            -->
      </sch:rule>
   </sch:pattern>
   <!--
 Some generic RIM patterns
-->
   <sch:pattern>
      <sch:rule context="rim:Slot">
         <sch:assert test="count(child::rim:SlotValue) = 1"
                     id="R-DSD-SUB-S046"
                     role="FATAL">A rim:Slot MUST have a rim:SlotValue child element</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:SlotValue">
         <sch:assert test="count(child::*) &gt; 0" id="R-DSD-SUB-S047" role="FATAL">A rim:SlotValue MUST have child element content</sch:assert>
      </sch:rule>
   </sch:pattern>
</sch:schema>
