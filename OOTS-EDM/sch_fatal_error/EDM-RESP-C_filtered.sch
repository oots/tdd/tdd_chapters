<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <sch:ns prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EDM-RESP-C</sr:identifier>
           <sr:title xml:lang="en">EDM-RESP-C</sr:title>
           <sr:description xml:lang="en">Evidence Response Syntax Mapping (Content)</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CONTENT</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-RESP</sr:identifier>
                 <sr:title xml:lang="en">EDM-RESP</sr:title>
                 <sr:description xml:lang="en">Evidence Response</sr:description>
                 <sr:type>QUERY</sr:type>
                 <sr:subject>PAYLOAD</sr:subject>
                 <sr:component>EDM</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.1.0</sr:version>
           <sr:versionNotes xml:lang="en">Updated Version information in schematron files</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/EEA_COUNTRY-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/AGENTCLASSIFICATION-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/OOTSMEDIATYPES-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/LANGUAGECODE-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/EAS_Code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/COUNTRYIDENTIFICATIONCODE-CODELIST_code.sch"/>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
         <sch:assert test="rim:Value='oots-edm:v1.1'" role="FATAL" id="R-EDM-RESP-C002">The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-edm:v1.1".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceResponseIdentifier']/rim:SlotValue/rim:Value">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-EDM-RESP-C003">The 'rim:Value' of the 'EvidenceResponseIdentifier' MUST be unique UUID (RFC 4122) for each response.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='IssueDateTime']/rim:SlotValue/rim:Value">
         <sch:assert test="matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}','i')"
                     role="FATAL"
                     id="R-EDM-RESP-C004">
            The 'rim:Value' of 'IssueDateTime' MUST be according to xsd:dateTime.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='ResponseAvailableDateTime']/rim:SlotValue/rim:Value">
         <sch:assert test="matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}','i')"
                     role="FATAL"
                     id="R-EDM-RESP-C005">
            The 'rim:Value' of 'ResponseAvailableDateTime' MUST be according to xsd:dateTime. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier">
         <sch:assert test="@schemeID" role="FATAL" id="R-EDM-RESP-C006">The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent">
         <sch:assert test="not(normalize-space(sdg:Classification)='')"
                     role="FATAL"
                     id="R-EDM-RESP-C010">The value for 'Agent/Classification' MUST be provided.  </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier">
         <sch:assert test="@schemeID" role="FATAL" id="R-EDM-RESP-C012">The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Identifier">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-EDM-RESP-C015">The value of 'Identifier' of an 'Evidence' MUST be unique UUID (RFC 4122).</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingDate">
         <sch:assert test="matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}','i')"
                     role="FATAL"
                     id="R-EDM-RESP-C016">
            The value of 'IssuingDate' of an 'Evidence' MUST be according to xsd:date.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:EvidenceTypeClassification">
         <!--{}[](EEA_Country-Codelist)-->
         <sch:assert test="matches(., '^https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(oots|(' || string-join($EEA_COUNTRY-CODELIST_code, '|') || '))/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$')"
                     role="FATAL"
                     id="R-EDM-RESP-C017">The value of 'EvidenceTypeClassification' of 'IsConformantTo' MUST be a UUID desribed in the Evidence Broker and include a code of the code list 
            'EEA_Country-CodeList' (ISO 3166-1' alpha-2 codes EEA subset of country codes) using the prefix and scheme 
            'https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/[EEA_CountryCode]/[UUID]' pointing to the Semantic Repository. For testing purposes and agreed OOTS data models the code "oots" can be used.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Title">
         <sch:assert test="@lang" role="FATAL" id="R-EDM-RESP-C019">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Description">
         <sch:assert test="@lang" role="FATAL" id="R-EDM-RESP-C021">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:ConformsTo">
         <sch:assert test="matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')"
                     role="FATAL"
                     id="R-EDM-RESP-C022">The value of 'ConformsTo' of the requested distribution MUST be a persistent URL with a link to a "DataModelScheme" of the Evidence Type described in the Semantic Repository which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]". </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson">
         <!--Role is WARNING so stripped the following assertion:
             The value of a Person 'Identifier' SHOULD be provided. -->         
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier">
         <!--{}[](EEA_CountryCodeList)-->
         <sch:assert test="matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i')"
                     role="FATAL"
                     id="R-EDM-RESP-C028">The value of a Person 'Identifier' MUST have the format XX/YY/ZZZZZZZZZ where XX is the Nationality Code of the identifier and YY is the Nationality Code of 
            the destination country (EEA_Country-CodeList subset of country codes) and ZZZZZZZZZ is an undefined combination of characters which uniquely identifies the identity asserted in the country of origin.
            Example: ES/AT/02635542Y</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier">
         <sch:assert test="@schemeID" role="FATAL" id="R-EDM-RESP-C030">The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:Identifier">
         <sch:assert test="@schemeID='eidas'" role="FATAL" id="R-EDM-RESP-C031">The 'schemeID' attribute of the 'Identifier' MUST have the fixed value 'eidas'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson/sdg:DateOfBirth">
         <sch:assert test="matches(normalize-space(text()),'^[0-9]{4}-[0-9]{2}-[0-9]{2}$','i')"
                     role="FATAL"
                     id="R-EDM-RESP-C032">
            The value of 'DateOfBirth' MUST use the following format YYYY + “-“ + MM + “-“ + DD (as defined for xsd:date) </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson">
         <sch:assert test="not(normalize-space(sdg:LegalPersonIdentifier)='')"
                     role="FATAL"
                     id="R-EDM-RESP-C033">The value of a Legal Person 'LegalPersonIdentifier' MUST be provided.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <!--{}[](EEA_CountryCodeList)-->
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier">
         <sch:assert test="matches(.,'((' || string-join($EEA_COUNTRY-CODELIST_code, '|') || ')/){2}[\S]{6,256}$','i')"
                     role="FATAL"
                     id="R-EDM-RESP-C035">The value of a 'LegalPersonIdentifier' MUST have the format XX/YY/ZZZZZZZ where the values of XX and YY MUST be part of the code list 'EEA_CountryCodeList' (ISO 3166-1 alpha-2 codes).  Example: ES/AT/02635542Y </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier">
         <sch:assert test="@schemeID" role="FATAL" id="R-EDM-RESP-C036">The 'schemeID' attribute of 'LegalPersonIdentifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson/sdg:LegalPersonIdentifier">
         <sch:assert test="@schemeID='eidas'" role="FATAL" id="R-EDM-RESP-C037">The 'schemeID' attribute of the 'LegalPersonIdentifier' MUST have the fixed value 'eidas'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingAuthority/sdg:Identifier">
         <sch:assert test="@schemeID" role="FATAL" id="R-EDM-RESP-C038">The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:ValidityPeriod/sdg:StartDate">
         <sch:assert test="matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}','i')"
                     role="FATAL"
                     id="R-EDM-RESP-C040">
            The value of 'StartDate' of an 'ValidityPeriod' MUST be according to xsd:date.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:ValidityPeriod/sdg:EndDate">
         <sch:assert test="matches(normalize-space(text()),'[0-9]{4}-[0-9]{2}-[0-9]{2}','i')"
                     role="FATAL"
                     id="R-EDM-RESP-C041">
            The value of 'EndDate' of an 'ValidityPeriod' MUST be according to xsd:date.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:RepositoryItemRef">
         <sch:assert test="matches(@xlink:href, '^cid:[A-Za-z0-9-.]*@[A-Za-z0-9.]*$')"
                     role="FATAL"
                     id="R-EDM-RESP-C042"> The value of the attribute "xlink:href" of "rim:RepositoryItemRef" MUST follow the URI scheme cid and start with the prefix 'cid:…@…'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Transformation">
         <sch:assert test="matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')"
                     role="FATAL"
                     id="R-EDM-RESP-C043">The value of 'Transformation' of the requested distribution MUST be a persistent URL with link to a "DataModelScheme" and "Subset" of the EvidenceType described in the Semantic Repository which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]/[Subset]".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution">
         <sch:let name="ct1" value="count(sdg:ConformsTo)"/>
         <sch:let name="isxml" value="matches(sdg:Format,'application/xml')"/>
         <sch:let name="isjson" value="matches(sdg:Format, 'application/json')"/>
         <!--Role is WARNING so stripped the following assertion:
             The value of 'sdg:ConformsTo' of the distribution SHOULD be present if the 'sdg:DistributedAs/sdg:Format' uses the codes 'application/xml' of the codelist 'OOTSMediaTypes'.-->
         <!--Role is WARNING so stripped the following assertion:
             The value of 'sdg:ConformsTo' of the distribution SHOULD be present if the 'sdg:DistributedAs/sdg:Format' uses the codes 'application/json' of the codelist 'OOTSMediaTypes'.-->
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue">
         <sch:assert test="count(rim:Element/sdg:Agent[sdg:Classification='EP'])=1"
                     role="FATAL"
                     id="R-EDM-RESP-C046">The EvidenceProvider slot MUST include one Agent with the classification value EP.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent[sdg:Classification/text() = 'EP']/sdg:Address">
         <sch:assert test="count(sdg:AdminUnitLevel1)=1"
                     role="FATAL"
                     id="R-EDM-RESP-C047">A value for 'AdminUnitLevel1' MUST be provided if the sdg:Agent/sdg:Classification value is "EP".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Transformation">
         <sch:assert test="count(../sdg:ConformsTo)=1" role="FATAL" id="R-EDM-RESP-C048">The value of  'sdg:ConformsTo'  of the distribution MUST be present if the Element 'sdg:Transformation' of the distribution is present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Identifier">
         <!--{}[](EAS-CodeList, EEA_CountryCodeList)-->
         <sch:let name="suffix"
                  value="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
         <sch:let name="suffix1"
                  value="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
         <sch:assert test="((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256"
                     role="FATAL"
                     id="R-EDM-RESP-C007">The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_CountryCodeList’. For testing purposes the code "oots" can be used. </sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent/sdg:Identifier">
         <!--{}[](EAS-CodeList, EEA_CountryCodeList)-->
         <sch:let name="suffix"
                  value="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
         <sch:let name="suffix1"
                  value="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
         <sch:assert test="((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256"
                     role="FATAL"
                     id="R-EDM-RESP-C013">The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_CountryCodeList’. For testing purposes the code "oots" can be used.</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IssuingAuthority/sdg:Identifier">
         <!--{}[](EAS-CodeList, EEA_CountryCodeList)-->
         <sch:let name="suffix"
                  value="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
         <sch:let name="suffix1"
                  value="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
         <sch:assert test="((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256"
                     role="FATAL"
                     id="R-EDM-RESP-C039">The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_CountryCodeList’. For testing purposes the code "oots" can be used.</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EDM-RESP-C008">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent/sdg:Address/sdg:AdminUnitLevel1'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent">
         <!--{}[](AgentClassification-CodeList)-->
         <sch:assert test="(some $code in $AGENTCLASSIFICATION-CODELIST_code satisfies sdg:Classification=$code) and (matches(sdg:Classification,'EP') or matches(sdg:Classification,'IP'))"
                     role="FATAL"
                     id="R-EDM-RESP-C011">The value (<sch:value-of select="sdg:Classification"/>) MUST be part of the code list 'AgentClassification' and must be one of the codes EP (Evidence Provider) or IP (Intermediary Platform). The codes 'ER' and 'ERRP' must not be used by this transaction. </sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Title/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EDM-RESP-C018">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Title/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Description/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EDM-RESP-C020">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context ' query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo/sdg:Description/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Language">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EDM-RESP-C026">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Language'</sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Format">
         <!--{}[](OOTSMediaTypes-CodeList)-->
         <sch:assert test="some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-EDM-RESP-C023">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'OOTSMediaTypes-CodeList' in the context 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:Distribution/sdg:Format'</sch:assert>
      </sch:rule>
   </sch:pattern>
</sch:schema>
