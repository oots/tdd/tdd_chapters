<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0" prefix="query"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/EDM-RESP-S</sr:identifier>
            <sr:title xml:lang="en">EDM-RESP-S</sr:title>
            <sr:description xml:lang="en">Evidence Response Syntax Mapping (Structure)</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>STRUCTURE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/EDM-RESP</sr:identifier>
                    <sr:title xml:lang="en">EDM-RESP</sr:title>
                    <sr:description xml:lang="en">Evidence Response</sr:description>
                    <sr:type>QUERY</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>EDM</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Updated Version information in schematron files</sr:versionNotes>
            </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:pattern>
      <sch:rule context="/node()">
         <sch:let name="ln" value="local-name(/node())"/>
         <sch:assert test="$ln ='QueryResponse'" role="FATAL" id="R-EDM-RESP-S001">The root element of a query response document MUST be 'query:QueryResponse'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="/node()">
         <sch:let name="ns" value="namespace-uri(/node())"/>
         <sch:assert test="$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0'"
                     role="FATAL"
                     id="R-EDM-RESP-S002">The namespace of root element of a 'query:QueryResponse' must be 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/@requestId">
         <sch:assert test="matches(normalize-space((.)),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-EDM-RESP-S004">The 'requestID' attribute of a 'QueryResponse' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:" and match the corresponding request.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse">
         <sch:assert test="@requestId" role="FATAL" id="R-EDM-RESP-S003">The 'requestID' attribute of a 'QueryResponse' MUST be present.</sch:assert>
         <sch:assert test="@status" role="FATAL" id="R-EDM-RESP-S005">The 'status' attribute of a 'QueryResponse' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse">
         <sch:assert test="@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success'                  or @status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Unavailable'"
                     role="FATAL"
                     id="R-EDM-RESP-S006">The 'status' attribute of a 'QueryResponse' MUST be encoded as "urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success" for successful responses or as "urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Unavailable" for responses that will be available at a later time .</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success']">
         <sch:assert test="count(rim:RegistryObjectList) &gt; 0"
                     role="FATAL"
                     id="R-EDM-RESP-S007">A successful 'query:QueryResponse' includes a 'rim:RegistryObjectList'                
            </sch:assert>
      </sch:rule>
      <sch:rule context="query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success']/rim:RegistryObjectList/rim:RegistryObject">
         <sch:assert test="rim:RepositoryItemRef" role="FATAL" id="R-EDM-RESP-S033">The 'rim:RepositoryItemRef' of a 'rim:RegistryObject' MUST be present.</sch:assert>
         <sch:assert test="@id" role="FATAL" id="R-EDM-RESP-S036">The 'id' attribute of a 'RegistryObject' MUST be present.             
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
        <!-- 
           rim:RegistryObject/rim:RepositoryItemRef should include @xlink:href and @xlink:title attributes
        -->
      <sch:rule context="query:QueryResponse[@status='urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success']/rim:RegistryObjectList/rim:RegistryObject/rim:RepositoryItemRef">
         <sch:assert test="@xlink:href" role="FATAL" id="R-EDM-RESP-S034">The 'xlink:href' attribute of 'RepositoryItemRef' MUST be present.             
            </sch:assert>
         <sch:assert test="@xlink:title" role="FATAL" id="R-EDM-RESP-S035">The 'xlink:title' attribute of 'RepositoryItemRef' MUST be present.          
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
        <!-- 
            If Response is Success RegistryObjectList must be non-empty
        -->
      <sch:rule context="query:QueryResponse/rs:Exception">
         <sch:assert test="count(rs:Exception) &gt; 0" role="FATAL" id="R-EDM-RESP-S008">A successful 'query:QueryResponse' does not include an Exception                
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse">
         <sch:assert test="count(child::rim:Slot[@name='SpecificationIdentifier'])=1"
                     role="FATAL"
                     id="R-EDM-RESP-S009">The rim:Slot name="SpecificationIdentifier" MUST be present in the QueryResponse.</sch:assert>
         <sch:assert test="count(child::rim:Slot[@name='EvidenceResponseIdentifier'])=1"
                     role="FATAL"
                     id="R-EDM-RESP-S010">The rim:Slot name="EvidenceResponseIdentifier" MUST be present in the QueryResponse.</sch:assert>
         <sch:assert test="count(child::rim:Slot[@name='IssueDateTime'])=1"
                     role="FATAL"
                     id="R-EDM-RESP-S011">The rim:Slot name="IssueDateTime" MUST be present  in the QueryResponse.</sch:assert>
         <sch:assert test="count(child::rim:Slot[@name='EvidenceProvider'])=1"
                     role="FATAL"
                     id="R-EDM-RESP-S012">The rim:Slot name="EvidenceProvider" MUST be present in the QueryResponse.</sch:assert>
         <sch:assert test="count(child::rim:Slot[@name='EvidenceRequester'])=1"
                     role="FATAL"
                     id="R-EDM-RESP-S013">The rim:Slot name="EvidenceRequester" MUST be present in the QueryResponse.</sch:assert>
         <!--Role is CAUTION so stripped the following assertion:
             The rim:Slot name="ResponseAvailableDateTime" MAY be present in the QueryResponse.-->
         <sch:assert test="count(rim:Slot[@name='SpecificationIdentifier']) + count(rim:Slot[@name='EvidenceResponseIdentifier']) + count(rim:Slot[@name='IssueDateTime']) + count(rim:Slot[@name='EvidenceProvider']) + count(rim:Slot[@name='EvidenceRequester']) + + count(rim:RegistryObjectList) + count(rim:Slot[@name='ResponseAvailableDateTime']) = count(child::*)"
                     role="FATAL"
                     id="R-EDM-RESP-S016">A query:QueryResponse MUST not contain any other rim:Slots than SpecificationIdentifier, EvidenceResponseIdentifier, IssueDateTime, EvidenceProvider, EvidenceRequester, rim:RegistryObjectList and optional ResponseAvailableDateTime</sch:assert>
         <!-- 
            Maybe add assertion that report any slots with unexpected values?  
            For now they are just ignored. 
            -->
      </sch:rule>
   </sch:pattern>
   <sch:pattern> 
        <!-- Rules for data types of specific slots in responses -->
      <sch:rule context="query:QueryResponse/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='StringValueType'" role="FATAL" id="R-EDM-RESP-S017">The rim:SlotValue of rim:Slot name="SpecificationIdentifier" MUST be of "rim:StringValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceResponseIdentifier']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='StringValueType'" role="FATAL" id="R-EDM-RESP-S018">The rim:SlotValue of rim:Slot name="EvidenceResponseIdentifier" MUST be of "rim:StringValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='IssueDateTime']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='DateTimeValueType'" role="FATAL" id="R-EDM-RESP-S019">The rim:SlotValue of rim:Slot name="IssueDateTime" MUST be of "rim:DateTimeValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='CollectionValueType'" role="FATAL" id="R-EDM-RESP-S020">The rim:SlotValue of rim:Slot name="EvidenceProvider" MUST be of "rim:CollectionValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='AnyValueType'" role="FATAL" id="R-EDM-RESP-S021">The rim:Element of rim:SlotValue of rim:Slot name="EvidenceProvider" MUST be of "rim:AnyValueType"</sch:assert>
         <sch:assert test="sdg:Agent" role="FATAL" id="R-EDM-RESP-S025">The 'query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue' MUST use the 'sdg:Agent' of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/rim:Element/sdg:Agent">
         <sch:assert test="(count(sdg:Identifier) + count(sdg:Name) + count(sdg:Address) + count(sdg:Classification)= count(child::*))"
                     role="FATAL"
                     id="R-EDM-RESP-S026">An EvidenceProvider 'rim:SlotValue/sdg:Agent' MUST not contain any other elements than 'sdg:Identifier' and 'sdg:Name', 'sdg:Address', 'sdg:Classification'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='AnyValueType'" role="FATAL" id="R-EDM-RESP-S022">The rim:SlotValue of rim:Slot name="EvidenceRequester" MUST be of "rim:AnyValueType"</sch:assert>
         <sch:assert test="sdg:Agent" role="FATAL" id="R-EDM-RESP-S027">The 'query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue' MUST 'sdg:Agent' of the targetNamespace="http://data.europa.eu/p4s".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='EvidenceRequester']/rim:SlotValue/sdg:Agent">
         <sch:assert test="(count(sdg:Identifier) + count(sdg:Name)= count(child::*))"
                     role="FATAL"
                     id="R-EDM-RESP-S028">An EvidenceRequester ''rim:SlotValue/sdg:Agent' MUST not contain any other elements than 'sdg:Identifier' and 'sdg:Name'. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:Slot[@name='ResponseAvailableDateTime']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='DateTimeValueType'" role="FATAL" id="R-EDM-RESP-S023">The rim:SlotValue of rim:Slot name="ResponseAvailableDateTime" MUST be of "rim:DateTimeValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsConformantTo">
         <sch:assert test="(count(sdg:EvidenceTypeClassification) + count(sdg:Title) + count(sdg:Description)= count(child::*))"
                     role="FATAL"
                     id="R-EDM-RESP-S030">The class 'IsConformantTo' of 'Evidence' MUST not contain any other elements than 'sdg:EvidenceTypeClassification', 'sdg:Title' and 'sdg:Description'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
        <!-- Registry Object -->
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject">
         <sch:assert test="count(child::rim:Slot[@name='EvidenceMetadata'])=1"
                     role="FATAL"
                     id="R-EDM-RESP-S015">The rim:Slot name="EvidenceMetadata" MUST be present in the RegistryObject.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='AnyValueType'" role="FATAL" id="R-EDM-RESP-S024">The rim:SlotValue of rim:Slot name="EvidenceMetadata MUST be of "rim:AnyValueType"</sch:assert>
         <sch:assert test="sdg:Evidence" role="FATAL" id="R-EDM-RESP-S029">The 'query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue' MUST use the 'sdg:Evidence' of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/@id">
         <sch:assert test="matches(normalize-space((.)),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-EDM-RESP-S037">
                The 'id' attribute of a 'RegistryObject' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:".
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <!-- 
        Some generic RIM patterns 
    -->
   <sch:pattern>
      <sch:rule context="rim:Slot">
         <sch:assert test="count(child::rim:SlotValue) = 1"
                     id="R-EDM-RESP-S038"
                     role="FATAL">A rim:Slot MUST have a rim:SlotValue child element</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:SlotValue">
         <sch:assert test="count(child::*) &gt; 0" id="R-EDM-RESP-S039" role="FATAL">A rim:SlotValue MUST have child element content</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:SlotValue[@xsi:type='rim:CollectionValueType']">
         <sch:assert test="count(child::rim:Element) &gt; 0"
                     id="R-EDM-RESP-S040"
                     role="FATAL">A rim:SlotValue of type CollectionValueType MUST have child rim:Element content</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:NaturalPerson">
         <sch:assert test="(count(sdg:Identifier) + count(sdg:FamilyName) + count(sdg:GivenName) + count(sdg:DateOfBirth) + count(sdg:PlaceOfBirth)= count(child::*))"
                     id="R-EDM-RESP-S041"
                     role="FATAL">A NaturalPerson 'sdg:IsAbout/sdg:NaturalPerson' MUST not contain any other elements than 'sdg:Identifier', 'sdg:FamilyName', 'sdg:GivenName', 'sdg:DateOfBirth' and 'sdg:PlaceOfBirth'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="query:QueryResponse/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceMetadata']/rim:SlotValue/sdg:Evidence/sdg:IsAbout/sdg:LegalPerson">
         <sch:assert test="(count(sdg:LegalPersonIdentifier) + count(sdg:LegalName)= count(child::*))"
                     id="R-EDM-RESP-S042"
                     role="FATAL">A LegalPerson 'sdg:IsAbout/sdg:LegalPerson' MUST not contain any other elements than 'sdg:LegalPersonIdentifier' and 'sdg:LegalName'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:SlotValue[@xsi:type='rim:CollectionValueType']">
         <sch:assert test="@collectionType='urn:oasis:names:tc:ebxml-regrep:CollectionType:Set'"
                     id="R-EDM-RESP-S043"
                     role="FATAL">A rim:SlotValue of type CollectionValueType MUST have collectionType="urn:oasis:names:tc:ebxml-regrep:CollectionType:Set"</sch:assert>
      </sch:rule>
   </sch:pattern>
</sch:schema>
