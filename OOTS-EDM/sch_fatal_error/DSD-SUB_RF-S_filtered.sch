<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0" prefix="lcm"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
            <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/DSD-SUB_RF-S</sr:identifier>
            <sr:title xml:lang="en">DSD-SUB_RF-S</sr:title>
            <sr:description xml:lang="en">Refactored Submit Registry Objects DataServiceEvidenceType, Access Service and EvidenceProvider (Structure)</sr:description>
            <sr:type>SCHEMATRON</sr:type>
            <sr:theme>STRUCTURE</sr:theme>
            <sr:associatedTransaction>
                <sr:Transaction>
                    <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-SUB_RF</sr:identifier>
                    <sr:title xml:lang="en">DSD-SUB_RF</sr:title>
                    <sr:description xml:lang="en">LCM Submit Objects to the DSD (Refactored)</sr:description>
                    <sr:type>LCM</sr:type>
                    <sr:subject>PAYLOAD</sr:subject>
                    <sr:component>DSD</sr:component>
                </sr:Transaction>
            </sr:associatedTransaction>
            <sr:version>1.1.0</sr:version>
            <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
            </sr:Asset>?>
   <!--End of SR metadata-->
   <sch:pattern>
      <sch:rule context="/node()">
         <sch:let name="ln" value="local-name(/node())"/>
         <sch:assert test="$ln ='SubmitObjectsRequest'"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S001">The root element of a query response document MUST be 'lcm:SubmitObjectsRequest'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="/node()">
         <sch:let name="ns" value="namespace-uri(/node())"/>
         <sch:assert test="$ns ='urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0'"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S002">The namespace of root element of a 'lcm:SubmitObjectsRequest' MUST be 'urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="@id" role="FATAL" id="R-DSD-SUB_RF-S003">The 'id' attribute of a 'SubmitObjectsRequest' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="matches(normalize-space(@id),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S004">The 'id' attribute of a 'SubmitObjectsRequest' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="count(rim:RegistryObjectList)&gt;0"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S005">A  'SubmitObjectsRequest' MUST include a 'rim:RegistryObjectList'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObjectList/rim:RegistryObject">
         <sch:let name="idattr" value="@id"/>
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="string-length($idattr)&gt;0" role="FATAL" id="R-DSD-SUB_RF-S006">Each 'rim:RegistryObject' MUST include an 'id' attribute</sch:assert>
         <sch:assert test="($st != 'AssociationType' and count(rim:Classification)&gt;0) or ($st = 'AssociationType' and count(rim:Classification)=0)"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S007">Each 'rim:RegistryObject' MUST include a 'rim:Classification' if the 'rim:RegistryObject' is not an 'xsi:type="rim:AssociationType"'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObjectList/rim:RegistryObject/rim:Classification">
         <sch:let name="idattr" value="@id"/>
         <sch:let name="schemeattr" value="@classificationScheme"/>
         <sch:let name="nodeattr" value="@classificationNode"/>
         <sch:assert test="string-length($idattr)&gt;0" role="FATAL" id="R-DSD-SUB_RF-S008">Each 'rim:Classification' MUST include an 'id' attribute</sch:assert>
         <sch:assert test="matches(normalize-space(@id),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S009">Each id of 'rim:Classification' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:".</sch:assert>
         <sch:assert test="string-length($schemeattr)&gt;0"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S010">Each 'rim:Classification' MUST include an 'classificationScheme' attribute</sch:assert>
         <sch:assert test="string-length($nodeattr)&gt;0"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S011">Each 'rim:Classification' MUST include an 'classificationNode' attribute</sch:assert>
         <sch:assert test="$schemeattr = 'urn:fdc:oots:classification:dsd'"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S012"> The 'classificationScheme' attribute MUST be 'urn:fdc:oots:classification:dsd'</sch:assert>
         <sch:assert test="$nodeattr = 'EvidenceProvider' or $nodeattr = 'DataServiceEvidenceType'or $nodeattr = 'AccessService'"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S013"> The 'classificationNode' attribute MUST be 'EvidenceProvider' or 'DataServiceEvidenceType' or 'AccessService'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Classification[@classificationNode='EvidenceProvider']">
         <sch:assert test="count(../rim:Slot[@name='EvidenceProvider']) = 1 and count(../rim:Slot[@name!='EvidenceProvider']) = 0"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S014">A 'rim:RegistryObject' with the classificationNode 'EvidenceProvider' MUST include a  rim:Slot name="EvidenceProvider" and no other</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Classification[@classificationNode='DataServiceEvidenceType']">
         <sch:assert test="count(../rim:Slot[@name='DataServiceEvidenceType']) = 1 and count(../rim:Slot[@name!='DataServiceEvidenceType']) = 0"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S015">A 'rim:RegistryObject' with the classificationNode 'DataServiceEvidenceType' MUST include a  rim:Slot name="DataServiceEvidenceType" and no other</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Classification[@classificationNode='AccessService']">
         <sch:assert test="count(../rim:Slot[@name='AccessService']) = 1 and count(../rim:Slot[@name!='AccessService']) = 0"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S038">A 'rim:RegistryObject' with the classificationNode 'AccessService' MUST include a  rim:Slot name="AccessService" and no other</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='AnyValueType'" role="FATAL" id="R-DSD-SUB_RF-S016">The rim:SlotValue of rim:Slot name="EvidenceProvider" MUST be of "rim:AnyValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='AnyValueType'" role="FATAL" id="R-DSD-SUB_RF-S017">The rim:SlotValue of rim:Slot name="DataServiceEvidenceType" MUST be of "rim:AnyValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='AccessService']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='AnyValueType'" role="FATAL" id="R-DSD-SUB_RF-S039">The rim:SlotValue of rim:Slot name="AccessService" MUST be of "rim:AnyValueType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue">
         <sch:assert test="count(sdg:Publisher)=1" role="FATAL" id="R-DSD-SUB_RF-S018">A  'rim:Slot[@name='EvidenceProvider'/rim:SlotValue' MUST contain one sdg:Publisher of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']">
         <sch:assert test="count(rim:SlotValue/sdg:DataServiceEvidenceType)=1"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S019">A  'rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue' MUST contain one sdg:DataServiceEvidenceType of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='AccessService']">
         <sch:assert test="count(rim:SlotValue/sdg:AccessService)=1"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S040">A  'rim:Slot[@name='AccessService']/rim:SlotValue' MUST contain one sdg:AccessService of the targetNamespace="http://data.europa.eu/p4s"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType">
         <sch:assert test="count(sdg:AccessService)=0"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S020">A DataServiceEvidenceType 'rim:Slot[@name='DataServiceEvidenceType]/rim:SlotValue' MUST not contain an 'sdg:AccessService' </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='AccessService']/rim:SlotValue">
         <sch:assert test="count(sdg:Publisher)=0" role="FATAL" id="R-DSD-SUB_RF-S041">An AccessService  'rim:Slot[@name='AccessService]'/rim:SlotValue' MUST not contain an 'sdg:Publisher'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject">
         <sch:let name="classification" value="count(rim:Classification)"/>
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="($st != 'AssociationType' and count(rim:Classification)&gt;0) or ($st = 'AssociationType' and count(rim:Classification)=0)"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S022">If a 'rim:RegistryObject' does not have a 'rim:Classification' it MUST have the attribute 'xsi:type=rim:AssociationType'</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject[@sourceObject and @targetObject]">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:let name="etp_count"
                  value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceProvider']])"/>
         <sch:let name="etd_count"
                  value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='DataServiceEvidenceType']])"/>
         <sch:let name="etat_count"
                  value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='AccessService']])"/>
         <sch:let name="etas_count"
                  value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='AccessService']])"/>
         <sch:assert test="number($etp_count) + number($etd_count) + number($etat_count) + number($etas_count) = 2"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S023">Each 'rim:RegistryObject' with a classificationNode 'EvidenceProvider' or 'DataServiceEvidenceType' or 'AccessService' MUST have an association 
                described in the attribute  'xsi:type="rim:AssociationType"'
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@xsi:type='rim:AssociationType']">
         <sch:assert test="count(@sourceObject)=1" role="FATAL" id="R-DSD-SUB_RF-S024">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' MUST have an attribute 'sourceObject'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType']">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="resourceSource" value="//rim:RegistryObject[@id=$sourceObject]"/>
         <sch:assert test="$resourceSource/rim:Classification[@classificationNode='EvidenceProvider']"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S025">A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType" MUST have an attribute 'sourceObject' that lists the id 
                of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") from classificationNode 'EvidenceProvider'. 
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService']">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="resourceSource" value="//rim:RegistryObject[@id=$sourceObject]"/>
         <sch:assert test="$resourceSource/rim:Classification[@classificationNode='EvidenceProvider']"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S042">A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService" MUST have an attribute 'sourceObject' 
                that lists the id of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") from classificationNode 'EvidenceProvider'. 
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@xsi:type='rim:AssociationType']">
         <sch:assert test="@targetObject" role="FATAL" id="R-DSD-SUB_RF-S026">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' MUST have an attribute 'targetObject'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType']">
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:let name="resourceTarget"
                  value="//rim:RegistryObjectList/rim:RegistryObject[@id=$targetObject]"/>
         <sch:assert test="$resourceTarget/rim:Classification[@classificationNode='DataServiceEvidenceType']"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S027">A 'rim:RegistryObject' with  type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType" MUST have an attribute 'targetObject'
                that lists the id of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") with classificationNode 'DataServiceEvidenceType'. 
                 </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@sourceObject and @targetObject]">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:let name="type" value="@type"/>
         <sch:let name="etl_count"
                  value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceProvider']])"/>
         <sch:let name="et_count"
                  value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='AccessService']])"/>
         <sch:assert test="$type != 'urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService' or (number($etl_count) + number($et_count) = 2 and $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S029">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' linking 'rim:RegistryObject' with the classificationNode 
                'EvidenceProvider' (@sourceObject) to 'rim:RegistryObject' with the classificationNode 'AccessService' (@targetObject) MUST use the 
                type="urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService".
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="not(rim:Slot[@name != 'SpecificationIdentifier'])"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S030">A 'lcm:SubmitObjectsRequest' MUST not contain any other rim:Slots than rim:Slot[@name='SpecificationIdentifier'].</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification">
         <sch:let name="hasSupportedValue"
                  value="//rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:ClassificationConcept/sdg:SupportedValue[normalize-space(.)]"/>
         <sch:assert test="not(empty($hasSupportedValue))"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S031">A value for 'sdg:Publisher/sdg:ClassificationConcept/sdg:SupportedValue' MUST be provided if the 'sdg:DataServiceEvidenceType' which is associated to the 'sdg:Publisher' has the element 'sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/@id">
         <sch:assert test="matches(normalize-space((.)),'^urn:uuid:[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S032">
                Each id of 'rim:RegistryObject' MUST be unique UUID (RFC 4122) starting with prefix "urn:uuid:". 
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="rim:RegistryObject/rim:Slot[@name='EvidenceProvider']"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S033">A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="EvidenceProvider".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S034">A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="DataServiceEvidenceType".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="rim:RegistryObject/rim:Slot[@name='AccessService']"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S046">
                A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with a rim:Slot name="AccessService". 
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="rim:RegistryObject[@xsi:type='rim:AssociationType']"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S035">A 'SubmitObjectsRequest' MUST include a 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList">
         <sch:assert test="count(rim:RegistryObject)&gt;2"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S036">A 'SubmitObjectsRequest' MUST include minimum 3 'rim:RegistryObject'.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@type='urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService']">
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:let name="resourceObject" value="//rim:RegistryObject[@id=$targetObject]"/>
         <sch:assert test="$resourceObject/rim:Classification[@classificationNode='AccessService']"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S043">A 'rim:RegistryObject' with type="urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService" MUST have an attribute 'targetObject'
                that lists the id of the 'rim:RegistryObject' (starting with prefix "urn:uuid:") with classificationNode 'AccessService'. 
            </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObjectList/rim:RegistryObject[@sourceObject and @targetObject]">
         <sch:let name="sourceObject" value="@sourceObject"/>
         <sch:let name="targetObject" value="@targetObject"/>
         <sch:let name="type" value="@type"/>
         <sch:let name="etl_count"
                  value="count(//rim:RegistryObject[@id=$sourceObject and child::rim:Classification[@classificationNode='EvidenceProvider']])"/>
         <sch:let name="et_count"
                  value="count(//rim:RegistryObject[@id=$targetObject and child::rim:Classification[@classificationNode='DataServiceEvidenceType']])"/>
         <sch:assert test="(number($etl_count) + number($et_count) = 2 and $type = 'urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType') or $type != 'urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType'"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S045">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType"' linking 'rim:RegistryObject' with the classificationNode 'EvidenceProvider' (@sourceObject)
                to 'rim:RegistryObject' with the classificationNode 'DataServiceEvidenceType' (@targetObject) MUST use the type="urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType"</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="rim:Slot[@name='SpecificationIdentifier']"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S048">The rim:Slot name="SpecificationIdentifier" MUST be present in the SubmitObjectsRequest.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
         <sch:let name="st" value="substring-after(@xsi:type, ':')"/>
         <sch:assert test="$st ='StringValueType'" role="FATAL" id="R-DSD-SUB_RF-S049">The rim:SlotValue of rim:Slot name="SpecificationIdentifier" MUST be of "rim:StringValueType".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="@checkReferences" role="FATAL" id="R-DSD-SUB_RF-S050">The 'checkReferences' attribute of a 'SubmitObjectsRequest' MUST be present. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest">
         <sch:assert test="@checkReferences='true'" role="FATAL" id="R-DSD-SUB_RF-S051">The 'checkReferences' attribute of a 'SubmitObjectsRequest' MUST be set on "true".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject">
         <sch:assert test="not(rim:Slot[not(@name=('EvidenceProvider', 'DataServiceEvidenceType', 'AccessService'))])"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S052">A 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/' MUST not contain any other rim:Slots than 'EvidenceProvider', 'DataServiceEvidenceType' or 'AccessService'. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject[@xsi:type='rim:AssociationType']">
         <sch:assert test="not(rim:Slot)" role="FATAL" id="R-DSD-SUB_RF-S053">A 'rim:RegistryObject' with the attribute 'xsi:type="rim:AssociationType" MUST not include any slots.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:RegistryObject/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification">
         <sch:let name="epc_id" value="sdg:Identifier"/>
         <sch:let name="epc_desc" value="sdg:Description[@lang='EN']"/>
         <sch:let name="dset_name" value="../sdg:Title[1]"/>
         <sch:let name="dset_id" value="../sdg:Identifier"/>
         <sch:let name="regrep_id" value="../../../../@id"/>
         <sch:let name="associated_provider_ids"
                  value="//rim:RegistryObject[                 @type='urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType'                 and @targetObject=$regrep_id]/@sourceObject"/>
         <sch:let name="associated_providers_without_matching_classificationconcept"
                  value="//rim:RegistryObject[@id=$associated_provider_ids                  and not(descendant::sdg:ClassificationConcept/sdg:Identifier=$epc_id)]/@id"/>
         <sch:assert test="count($associated_providers_without_matching_classificationconcept)=0"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S054">Some providers associated to data service "<sch:value-of select="$dset_name"/>" in registry object <sch:value-of select="$regrep_id"/> do not have a value for classification concept "<sch:value-of select="$epc_desc"/>" <sch:value-of select="$epc_id"/>": RIM object(s) <sch:value-of select="$associated_providers_without_matching_classificationconcept"/>
         </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Jurisdiction">
         <sch:let name="adminLevel2" value="boolean(sdg:AdminUnitLevel2)"/>
         <sch:let name="sourceObjectID" value="ancestor-or-self::rim:RegistryObject/@id"/>
         <sch:let name="targetObjectID"
                  value="//rim:RegistryObject[@sourceObject=$sourceObjectID]/@targetObject"/>
         <sch:let name="checkJurisdiction"
                  value="//rim:RegistryObject[@id = $targetObjectID]/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel"/>
         <sch:let name="checkNuts" value="contains($checkJurisdiction, 'NUTS')"/>
         <!--Role is WARNING so stripped the following assertion:
             The element 'sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel2''  SHOULD be provided if the  'sdg:DataServiceEvidenceType' which is associated to the 'sdg:Publisher' has one
                of the values 'NUTS1', 'NUTS2' or 'NUTS3' in the element 'sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel' . 
            -->
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Jurisdiction">
         <sch:let name="adminLevel3" value="boolean(sdg:AdminUnitLevel3)"/>
         <sch:let name="sourceObjectID" value="ancestor-or-self::rim:RegistryObject/@id"/>
         <sch:let name="targetObjectID"
                  value="//rim:RegistryObject[@sourceObject=$sourceObjectID]/@targetObject"/>
         <sch:let name="checkJurisdiction"
                  value="//rim:RegistryObject[@id = $targetObjectID]/rim:Slot/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel"/>
         <sch:let name="checkLau" value="contains($checkJurisdiction, 'LAU')"/>
         <!--Role is WARNING so stripped the following assertion:
             The element '/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel3''  SHOULD be provided if the  'sdg:DataServiceEvidenceType' which is associated to the 'sdg:Publisher' 
                has the value 'LAU' in the element 'sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel' .
            -->
      </sch:rule>
   </sch:pattern>
   <!--
Some generic RIM patterns
-->
   <sch:pattern>
      <sch:rule context="rim:Slot">
         <sch:assert test="count(child::rim:SlotValue) = 1"
                     id="R-DSD-SUB_RF-S057"
                     role="FATAL">A rim:Slot MUST have a rim:SlotValue child element</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="rim:SlotValue">
         <sch:assert test="count(child::*) &gt; 0" id="R-DSD-SUB_RF-S058" role="FATAL">A rim:SlotValue MUST have child element content</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/@type">
         <sch:let name="associationTypePrefix"
                  value="'urn:oasis:names:tc:ebxml-regrep:AssociationType:'"/>
         <sch:assert test=". = concat($associationTypePrefix, 'ProvidesEvidenceType') or . = concat($associationTypePrefix, 'UsesAccessService')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-S059">        
                The value of @type of AssociationType must be urn:oasis:names:tc:ebxml-regrep:AssociationType:ProvidesEvidenceType or 'urn:oasis:names:tc:ebxml-regrep:AssociationType:UsesAccessService'</sch:assert>
      </sch:rule>
   </sch:pattern>
</sch:schema>
