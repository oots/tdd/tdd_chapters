<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <sch:ns uri="http://data.europa.eu/p4s" prefix="sdg"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0" prefix="rs"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0" prefix="rim"/>
   <sch:ns uri="urn:oasis:names:tc:ebxml-regrep:xsd:lcm:4.0" prefix="lcm"/>
   <sch:ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
   <sch:ns prefix="x" uri="https://www.w3.org/TR/REC-html40"/>
   <!--Start of SR metadata-->
   <?SR_metadata <sr:Asset xmlns:sr="https://sr.oots.tech.ec.europa.eu/model/srdm#">
           <sr:identifier>https://sr.oots.tech.ec.europa.eu/schematrons/DSD-SUB_RF-C</sr:identifier>
           <sr:title xml:lang="en">DSD-SUB_RF-C</sr:title>
           <sr:description xml:lang="en">Refactored Submit Registry Objects DataServiceEvidenceType, Access Service and EvidenceProvider (Content)</sr:description>
           <sr:type>SCHEMATRON</sr:type>
           <sr:theme>CONTENT</sr:theme>
           <sr:associatedTransaction>
              <sr:Transaction>
                 <sr:identifier>https://sr.oots.tech.ec.europa.eu/codelist/transaction/DSD-SUB_RF</sr:identifier>
                 <sr:title xml:lang="en">DSD-SUB_RF</sr:title>
                 <sr:description xml:lang="en">LCM Submit Objects to the DSD (Refactored)</sr:description>
                 <sr:type>LCM</sr:type>
                 <sr:subject>PAYLOAD</sr:subject>
                 <sr:component>DSD</sr:component>
              </sr:Transaction>
           </sr:associatedTransaction>
           <sr:version>1.1.0</sr:version>
           <sr:versionNotes xml:lang="en">Added rules for rim:Slot[@name='SpecificationIdentifier']</sr:versionNotes>
           </sr:Asset>?>
   <!--End of SR metadata-->
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/EEA_COUNTRY-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/LEVELSOFASSURANCE-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/AGENTCLASSIFICATION-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/OOTSMEDIATYPES-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/LANGUAGECODE-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/EAS_Code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/COUNTRYIDENTIFICATIONCODE-CODELIST_code.sch"/>
   <!--Changing the import to point to the "sch" directory -->
   <sch:include href="../sch/codelist-include/JURISDICTIONLEVEL-CODELIST_code.sch"/>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Identifier">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C001">The value of 'Identifier' of an 'DataServiceEvidenceType' MUST be unique UUID (RFC 4122).</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceTypeClassification">
         <sch:assert test="matches(., '^https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/(oots|(' || string-join($EEA_COUNTRY-CODELIST_code, '|') || '))/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C002">The value of 'EvidenceTypeClassification' of a 'DataServiceEvidenceType' MUST be a UUID of the Evidence Broker and include a code of the code list 'EEA_Country-CodeList' (ISO 3166-1' alpha-2 codes subset of EEA countries) 
            using the prefix and scheme ''https://sr.oots.tech.eceuropa.eu/evidencetypeclassifications/[EEA_CountryCode]/[UUID]'' pointing to the Semantic Repository. For testing purposes and agreed OOTS data models the code "oots" can be used. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C004">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C006">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:ConformsTo">
         <sch:assert test="matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C008">The value of 'ConformsTo' of the distribution MUST be a persistent URL with a link to a "DataModelScheme" of the Evidence Type described in the Semantic Repository
            which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Transformation">
         <sch:assert test="matches(normalize-space(text()),'^https://sr.oots.tech.ec.europa.eu/datamodels/')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C009">The value of 'Transformation' of the distribution MUST be a persistent URL with link to a "DataModelScheme" and "Subset" of the EvidenceType desribed in the 
            Semantic Repository which uses the prefix "https://sr.oots.tech.ec.europa.eu/datamodels/[DataModelScheme]/[Subset]".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Identifier">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C010">The value of 'Identifier' of MUST be unique UUID (RFC 4122).</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification">
         <sch:assert test="count(sdg:Type) &gt; 0" role="FATAL" id="R-DSD-SUB_RF-C011">A 'Type' MUST be provided. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification">
         <sch:assert test="count(sdg:Description) &gt; 0"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C012">A 'Description' MUST be provided. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C014">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContextId">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C015">The value of 'JurisdictionContextId' of MUST be unique UUID (RFC 4122).</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C017">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='AccessService']/rim:SlotValue/sdg:AccessService/sdg:Identifier">
         <sch:assert test="not(normalize-space(@schemeID)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C020">The 'schemeID' MUST be provided. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='AccessService']/rim:SlotValue/sdg:AccessService">
         <sch:assert test="sdg:ConformsTo='oots-edm:v1.0' or sdg:ConformsTo='oots-edm:v1.1'"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C023">The value of 'ConformsTo' of the Access Service MUST point to the underlying eDelivery and Evidence Exchange Data Model Profile e.g. 'oots-edm:v1.0', 'oots-edm:v1.1', ... .</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Identifier">
         <sch:assert test="not(normalize-space(@schemeID)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C024">The 'schemeID' attribute of 'Identifier' MUST be present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:ClassificationConcept/sdg:Identifier">
         <sch:assert test="matches(normalize-space((.)),'^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}$','i')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C031">The value of 'Identifier' of MUST be unique UUID (RFC 4122).</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:ClassificationConcept">
         <sch:assert test="not(normalize-space(sdg:Type)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C032">A 'Type' MUST be provided. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:ClassificationConcept">
         <sch:assert test="not(normalize-space(sdg:Description[1])='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C033">A 'Description' MUST be provided. </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:ClassificationConcept/sdg:Description">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C035">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:ClassificationConcept">
         <sch:assert test="not(normalize-space(sdg:SupportedValue)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C036">A value for 'SupportedValue' MUST be provided if the 'sdg:ClassificationConcept' is present</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs">
         <sch:let name="ct1" value="count(sdg:ConformsTo)"/>
         <sch:let name="isxml" value="matches(sdg:Format,'xml')"/>
         <sch:let name="isjson" value="matches(sdg:Format, 'json')"/>
         <sch:let name="otherformat" value="../sdg:DistributedAs/sdg:Format"/>
         <sch:let name="other"
                  value="some $format in $otherformat satisfies matches(string($format), string-join($OOTSMEDIATYPES-CODELIST_code[. != 'application/json' and . != 'application/xml'], '|'))"/>
         <sch:assert test="not($isxml and $ct1=0) or $other"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C038">The value of 'sdg:ConformsTo' of the distribution MUST be present if the 'sdg:DistributedAs/sdg:Format' uses the code 'application/xml' of the codelist 'OOTSMediaTypes' unless another 'sdg:DistributedAs/sdg:Format' 
            exists that uses the code 'image/jpeg' or 'image/png' or 'application/pdf' or 'image/svg+xml' for the same DataServiceEvidenceType./&gt;</sch:assert>
         <sch:assert test="not($isjson and $ct1=0) or $other"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C040">The value of 'sdg:ConformsTo' of the distribution MUST be present if the 'sdg:DistributedAs/sdg:Format' uses the code 'application/json' of the codelist 'OOTSMediaTypes' unless another 'sdg:DistributedAs/sdg:Format' 
            exists that uses the code 'image/jpeg' or 'image/png' or 'application/pdf' or 'image/svg+xml' for the same DataServiceEvidenceType.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs">
         <sch:let name="tr" value="count(sdg:Transformation)"/>
         <sch:let name="ct" value="count(sdg:ConformsTo)"/>
         <sch:assert test="not($tr=1 and $ct=0)" role="FATAL" id="R-DSD-SUB_RF-C039">The value of  'sdg:ConformsTo'  of the distribution MUST be present if the Element 'sdg:Transformation' of the distribution is present.</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C042">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='AccessService']/rim:SlotValue/sdg:AccessService/sdg:Name">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C044">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Name">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C046">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Description">
         <sch:assert test="not(normalize-space(@lang)='')"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C048">The value of 'lang' attribute MUST be provided. Default choice "EN".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:Slot[@name='SpecificationIdentifier']/rim:SlotValue">
         <sch:assert test="rim:Value='oots-cs:v1.1'" role="FATAL" id="R-DSD-SUB_RF-C049">The 'rim:Value' of the 'SpecificationIdentifier' MUST be the fixed value "oots-cs:v1.1".</sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Jurisdiction">
         <!-- This assert checks if AdminUnitLevel3 is present -->
         <sch:assert test="not(sdg:AdminUnitLevel3) or (sdg:AdminUnitLevel1 and sdg:AdminUnitLevel2)"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C051">
            If AdminUnitLevel 3 of the Jurisdiction is present, both AdminUnitLevel 1 and AdminUnitLevel 2 of the Jurisdiction must be present.
         </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Jurisdiction">
         <!-- This assert checks if AdminUnitLevel2 is present -->
         <sch:assert test="not(sdg:AdminUnitLevel2) or sdg:AdminUnitLevel1"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C052">
            If AdminUnitLevel 2 of the Jurisdiction is present, AdminUnitLevel 1 of the Jurisdiction must be present.
         </sch:assert>
      </sch:rule>
   </sch:pattern>
   <sch:pattern>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C003">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Title/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C005">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Description/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Address/sdg:AdminUnitLevel1">
         <!--{}[](CountryIdentificationCode)-->
         <sch:assert test="some $code in $COUNTRYIDENTIFICATIONCODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C026">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'CountryIdentificationCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Address/sdg:AdminUnitLevel1'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Jurisdiction/sdg:AdminUnitLevel1">
         <!--{}[](EEA_Country-CodeList)-->
         <sch:assert test="some $code in $EEA_COUNTRY-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C028">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'EEA_Country-CodeList' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Juristiction/sdg:AdminUnitLevel1'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format">
         <!--{}[](OOTSMediaTypes-CodeList)-->
         <sch:assert test="some $code in $OOTSMEDIATYPES-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C007">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'OOTSMediaTypes-CodeList' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:DistributedAs/sdg:Format'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C013">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderClassification/sdg:Description/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C016">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionContext/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:ClassificationConcept/sdg:Description/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C034">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:ClassificationConcept/sdg:Description/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel">
         <!--{}[](JurisdictionLevel-CodeList)-->
         <sch:assert test="some $code in $JURISDICTIONLEVEL-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C018">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'JurisdictionLevel-CodeList' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:EvidenceProviderJurisdictionDetermination/sdg:JurisdictionLevel'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='AccessService']/rim:SlotValue/sdg:AccessService/sdg:Identifier">
         <!--{}[](EAS-CodeList, EEA_Country-CodeList)-->
         <sch:let name="suffix"
                  value="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
         <sch:let name="suffix1"
                  value="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
         <sch:assert test="((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C022">The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country-CodeList’. For testing purposes the code "oots" can be used. </sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Identifier">
         <!--{}[](EAS-CodeList, EEA_Country-CodeList)-->
         <sch:let name="suffix"
                  value="substring-after(@schemeID, 'urn:cef.eu:names:identifier:EAS:')"/>
         <sch:let name="suffix1"
                  value="substring-after(@schemeID, 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:')"/>
         <sch:assert test="((some $code in $EAS_Code satisfies $suffix=$code) or (some $code in $EEA_COUNTRY-CODELIST_code satisfies $suffix1=$code) or $suffix1='oots') and string-length(.) &lt; 256"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C025">The value of the 'schemeID' attribute of the 'Identifier' MUST not extend 256 characters and it either, MUST use the prefix 'urn:cef.eu:names:identifier:EAS:[Code]' and a code being part of the code list 'EAS' (Electronic Address Scheme ) OR it MUST use the prefix 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:[Code]' and a code being part of the code list ‘EEA_Country-CodeList’. For testing purposes the code "oots" can be used. </sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance">
         <!--{}[](LevelsOfAssurance-CodeList)-->
         <sch:assert test="some $code in $LEVELSOFASSURANCE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C037">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LevelsOfAssurance-CodeList' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:AuthenticationLevelOfAssurance'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C041">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='DataServiceEvidenceType']/rim:SlotValue/sdg:DataServiceEvidenceType/sdg:Note/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='AccessService']/rim:SlotValue/sdg:AccessService/sdg:Name/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C043">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Name/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Name/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C045">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:AccessService/sdg:Publisher/sdg:Name/@lang'</sch:assert>
      </sch:rule>
      <sch:rule context="lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Description/@lang">
         <!--{}[](LanguageCode)-->
         <sch:assert test="some $code in $LANGUAGECODE-CODELIST_code satisfies .=$code"
                     role="FATAL"
                     id="R-DSD-SUB_RF-C050">Value supplied '<value-of xmlns="http://purl.oclc.org/dsdl/schematron" select="."/>' is unacceptable for constraints identified by 'LanguageCode' in the context 'lcm:SubmitObjectsRequest/rim:RegistryObjectList/rim:RegistryObject/rim:Slot[@name='EvidenceProvider']/rim:SlotValue/sdg:Publisher/sdg:Description/@lang'</sch:assert>
      </sch:rule>
   </sch:pattern>
</sch:schema>
