import glob, json, os, sys, logging, shutil

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# create console handler to print to screen
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

fh = logging.FileHandler('prepare_test_suite.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)

logger.addHandler(fh)

# add the handlers to the logger
logger.addHandler(ch)

def copy_to_target(source_path_prefix, target_path_prefix, mapping):
    for to, fromlist in mapping:
        targetdir = os.path.join(target_path_prefix, to)
        if os.path.exists(targetdir):
            if os.path.isdir(targetdir):
                for xmlfile in glob.glob("*.xml", root_dir=targetdir):
                    fn = os.path.join(targetdir, xmlfile)
                    logging.info("Clean up:  {}".format(fn))
                    os.remove(fn)
                for fr in fromlist:
                    fr_source = os.path.join(source_path_prefix, fr)
                    if os.path.isdir(fr_source):
                        logging.info("{} is a directory".format(fr_source))
                        dir_file_count = 0
                        for xmlfile in glob.glob("*.xml", root_dir=fr_source):
                            source_file = os.path.join(fr_source, xmlfile)
                            logging.info("Copy from dir: {} to: {}".format(source_file, targetdir))
                            shutil.copy(source_file, targetdir)
                            dir_file_count += 1
                        logging.info('Copied {} files from {} to {}'.format(
                            dir_file_count, fr_source, targetdir))
                    elif os.path.isfile(fr_source):
                        logging.info("Copy file {} to: {}".format(fr_source, targetdir))
                        shutil.copy(fr_source, targetdir)
                    else:
                        logging.error("{} is neither a directory nor a file".format(fr_source))
            else:
                logging.error("Not a directory: {}".format(targetdir))
        else:
            logging.error("No such file or directory: {}".format(targetdir))

if __name__ == "__main__":
    print(sys.argv)
    if len(sys.argv) == 2:
        infile = sys.argv[1]
        with open(infile) as cf:
            config = json.load(cf)
            source_path_prefix = config["source_path_prefix"]
            target_path_prefix = config["target_path_prefix"]
            mapping = config["mapping"].items()
            logging.info('XXXX get this in the log ??')
            copy_to_target(source_path_prefix, target_path_prefix, mapping)
    else:
        logging.error("Usage: prepare_test_suite.py <config.json>")
        exit(1)
